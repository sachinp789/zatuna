<?php
$this->load->model('admin/Ingredients_model');
/*$this->load->model('admin/Recipes_model'); */
$this->load->model('admin/Levels_model');
$ids = array();
if (count($resting) > 0) {

	foreach ($resting as $value) {
		$ids[] = /*$this->Ingredients_model->getRestIngredients($value->ingredient_id);*/
		$value->ingredient_id;
	}
}
if (count($ids) > 0) {

	$ingRecipes = $this->Ingredients_model->getRestIngredients($ids);
} else {
	$ingRecipes = $this->Ingredients_model->getIngredients();
}

$levels = $this->Levels_model->getLevels();

$methodmedia = $this->db->select('*')
	->from('recipe_steps')
	->where('recipe_id', $recipes->id)
	->order_by("id", "asc")
	->get()->result();

/*
$recipes=$this->Recipes_model->get($param2);
$recipescats = $this->Recipes_model->getRecipescat();

$recp_ing = $this->Recipes_model->getRecipeingredients($param2);

//echo '<pre>';print_r($recipes);exit; */

//print_r($recp_ing);exit;

?>
<section class="content">
<!-- Start .row -->
<div class=row>

<div class=col-lg-12>
<!-- col-lg-12 start here -->
<div class="box box-primary">
<div class=box-body>

    <?php echo form_open(base_url() . 'admin/recipes/update/' . $recipes->id, array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'editrecipeform', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>
    <?php if ($this->session->flashdata('error')) {?>
        <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
    <?php }?>
    <div class="padded">
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line('name'); ?><span style="color:red">*</span></label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="title" value="<?php echo $recipes->recipe_name; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line("name_ar"); ?><span style="color:red"></span></label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="title_ar" value="<?php echo $recipes->recipe_name_ar; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line("name_fr"); ?><span style="color:red"></span></label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="title_fr" value="<?php echo $recipes->recipe_name_fr; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line('media'); ?><span style="color:red"></span></label>
            <div class="col-sm-6">
                <input type="file" class="form-control" name="recipe_media[]" id="projectfile" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line('description'); ?><span style="color:red">*</span></label>
            <div class="col-sm-6">
                <textarea name="recipe_description" class="form-control" rows="3"><?php echo $recipes->recipe_description; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line("description_ar"); ?><span style="color:red"></span></label>
            <div class="col-sm-6">
                <textarea name="recipe_description_ar" class="form-control" rows="3" placeholder="Description"><?php echo $recipes->recipe_description_ar; ?></textarea>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line("description_fr"); ?><span style="color:red"></span></label>
            <div class="col-sm-6">
                <textarea name="recipe_description_fr" class="form-control" rows="3" placeholder="Description"><?php echo $recipes->recipe_description_fr; ?></textarea>
            </div>
        </div>
        <div class="form-group method">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line("method"); ?><span style="color:red"></span></label>
                <div class="col-sm-6 prepmethod">

                <div class="table-responsive">

                    <table class="table table-striped" id="tblmethod">
                        <thead>
                            <tr>
                                <th><?php echo $this->lang->line("step_no"); ?></th>
                                <th><?php echo $this->lang->line("step_desc"); ?></th>
                                <th><?php echo $this->lang->line("step_imgvid"); ?></th>
                                <th>#</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
if (count($methodmedia) > 0) {
	foreach ($methodmedia as $key => $value) {
		//echo $key;
		?>
                                <tr id="item_<?php echo $value->id; ?>">
                                    <td id="steprow_<?php echo $value->id; ?>"><?php echo $value->step_no; ?></span></td>
                                    <td id="descrow_<?php echo $value->id; ?>">
                                    <?php
if ($this->session->userdata('siteLang') == 'ar') {
			echo $value->step_description_ar;
		} else if ($this->session->userdata('siteLang') == 'fr') {
			echo $value->step_description_fr;
		} else {
			echo $value->step_description;
		}
		?></td>
                                   <td id="imgmediarow_<?php echo $value->id; ?>">
                                    <?php
if (in_array(pathinfo($value->image, PATHINFO_EXTENSION), array('gif', 'png', 'jpg', 'jpeg'))):
		?>
                                    <img src="<?php echo base_url() . $value->image; ?>" width="100"/>
                                    <input type="hidden" name="methodmedia_<?php echo $value->id; ?>" id="methodmedia_<?php echo $value->id; ?>" value="<?php echo $value->image ?>">
                                    <?php elseif (in_array(pathinfo($value->image, PATHINFO_EXTENSION), array('mp4', 'avi', '3gp'))): ?>
                                     <video id="video<?php echo $value->id; ?>" width="100">
                                        <source src="<?php echo base_url() . $value->image ?>" type="video/mp4">
                                        <span>Your browser does not support HTML5 video.</span>
                                    </video>
                                    <input type="hidden" name="methodmedia_<?php echo $value->id; ?>" id="methodmedia_<?php echo $value->id; ?>" value="<?php echo $value->image ?>">
                                     <a onclick="playPause(<?php echo $value->id; ?>)" style="margin:0;float: right;cursor: pointer;">Play/Pause</a>
                                    <?php else: ?>
                                    <span>No image / video</span>
                                    <input type="hidden" name="methodmedia_<?php echo $value->id; ?>" id="methodmedia_<?php echo $value->id; ?>" value="<?php echo $value->image ?>">
                                    <?php endif;?>
                                    </td>
                                    <td>
                                    <a style="cursor:pointer;" onclick="edit_row(<?php echo $value->id; ?>)" id="edit1_button_<?php echo $value->id; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                                    <a style="cursor:pointer;" onclick="close_row(<?php echo $value->id; ?>)" id="close_button_<?php echo $value->id; ?>" class="save"><span class="glyphicon glyphicon-remove"></span></a>
                                   <!--  <input type="button" class="btn btn-xs btn-primary edit" value="Edit" onclick="edit_row(<?php echo $value->id; ?>)" id="edit_button<?php echo $value->id; ?>">   --><!-- <a id="save_button<?php echo $value->id; ?>" class="save save_image" onclick="save_row(<?php echo $value->id; ?>)"><span class="glyphicon glyphicon-floppy-disk"></span></a> -->

                                   <a style="cursor:pointer;" onclick="save_row(<?php echo $value->id; ?>)" id="savemedia_<?php echo $value->id; ?>" class="save save_image"><span class="glyphicon glyphicon-floppy-disk"></span></a>

                                   <!--  <input type="button" id="save_button<?php echo $value->id; ?>" class="btn btn-xs btn-default save save-image" value="Save">  -->

                                    <a href="#" class="delete" id="del-<?php echo $value->id; ?>-<?php echo $value->step_no; ?>" title="Remove <?php echo ucwords($value->step_no); ?>"><span class="glyphicon glyphicon-trash"><?php ?></span></a></td>
                                </tr>
                            <?php
}
}
?>
                        </tbody>
                    </table>

                </div>

                <div class="col-sm-12" style="padding:0">
                <input type="number" name="txtstep[]" id="txtstep" class="form-control" placeholder="Step No">
                <label class="control-label"><?php echo $this->lang->line("img_vid"); ?></label>
                <input type="file" name="stepmedia[]" class="form-control" placeholder="Image/Video" id="stepmedia1" onchange="checkfile(1)">
                <label></label>
                <textarea name="preparation_method[]" id="preparation_method" class="form-control" rows="3" placeholder="Preparation Description"></textarea>
                <textarea name="preparation_method_ar[]" id="preparation_method" class="form-control" rows="3" placeholder="AR_Preparation Description"></textarea>
                <textarea name="preparation_method_fr[]" id="preparation_method" class="form-control" rows="3" placeholder="FR_Preparation Description"></textarea>
                <button class="btn btn-info btn-xs insert"><i class="fa fa-plus"></i></</button>
                </div>
                </div>
        </div>
       <!--  <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo ucwords("method"); ?><span style="color:red">*</span></label>
            <div class="col-sm-6">
                <textarea name="method" class="form-control" rows="6"><?php echo $recipes->recipe_method; ?></textarea>
            </div>
        </div> -->

         <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line("serving"); ?><span style="color:red">*</span></label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="serving" value="<?php echo $recipes->serving; ?>" id="serving"/>
            </div>
        </div>

        <div class="form-group ing">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line("ingredients"); ?><span style="color:red"></span></label>
        <div class="col-sm-6 ing3">
        <div class="col-sm-12" style="padding:0">
        <div class="table-responsive">
        <table class="table table-striped" id="tblremove">
            <thead>
            <tr>
                <th><?php echo $this->lang->line("ingredients"); ?></th>
                <th><?php echo $this->lang->line("qty"); ?></th>
                <th><?php echo $this->lang->line("unit"); ?></th>
                <th>#</th>
            </tr>
            </thead>
            <tbody>
                <?php
if (count($recp_ing) > 0) {
	foreach ($recp_ing as $key => $value) {
		//print_r($value);
		?>
                    <tr id="item_<?php echo $value->id; ?>">
                        <input type="hidden" id="kcal_<?php echo $value->id; ?>" value="<?php echo $value->kcal_grams; ?>">
                        <input type="hidden" id="protine_<?php echo $value->id; ?>" value="<?php echo $value->protein; ?>">
                        <input type="hidden" id="fat_<?php echo $value->id; ?>" value="<?php echo $value->fat; ?>">
                        <input type="hidden" id="carbs_<?php echo $value->id; ?>" value="<?php echo $value->carbs; ?>">
                         <input type="hidden" id="final_calories_ajax_<?php echo $value->id; ?>" value="<?php echo $value->measure_amount; ?>">
                         <input type="hidden" id="protine_calories_ajax_<?php echo $value->id; ?>" value="<?php echo $value->protein_amount; ?>">
                         <input type="hidden" id="fat_calories_ajax_<?php echo $value->id; ?>" value="<?php echo $value->fat_amount; ?>">
                         <input type="hidden" id="carbs_calories_ajax_<?php echo $value->id; ?>" value="<?php echo $value->carbs_amount; ?>">
                        <td id="ingname_<?php echo $value->id; ?>">
                        <?php
if ($this->session->userdata('siteLang') == 'ar') {
			echo $value->name_ar;} else if ($this->session->userdata('siteLang') == 'fr') {
			echo $value->name_fr;
		} else {
			echo $value->name;
		}
		?>
                        </td>
                        <td id="ingqty_<?php echo $value->id; ?>"><?php echo $value->qty; ?></td>
                        <!-- <td id="ingunit_<?php echo $value->id; ?>"><?php echo $value->unit; ?></td> -->
                        <td id="ingunit_<?php echo $value->id; ?>">
                        <?php
if ($this->session->userdata('siteLang') == 'ar') {
			echo $value->unit_name_ar;} else if ($this->session->userdata('siteLang') == 'fr') {
			echo $value->unit_name_fr;
		} else {
			echo $value->unit_name;
		}
		?>
                        </td>
                        <td>
                        <a style="cursor:pointer;" onclick="editing_row(<?php echo $value->id; ?>,<?php echo $value->ingredient_id; ?>,<?php echo $value->finalunit_id; ?>)" id="editing_button<?php echo $value->id; ?>"><span class="glyphicon glyphicon-pencil"></span></a>
                        <a style="cursor:pointer;" onclick="close_row2(<?php echo $value->id; ?>)" id="close_button_<?php echo $value->id; ?>" class="save"><span class="glyphicon glyphicon-remove"></span></a>
                        <!-- <input type="button" class="btn btn-xs btn-primary edit" value="Edit" onclick="editing_row(<?php echo $value->id; ?>,<?php echo $value->ingredient_id; ?>)" id="editing_button<?php echo $value->id; ?>">  -->
                        <!-- <input type="button" id="save_button<?php echo $value->id; ?>" value="Save" class="save" onclick="saveing_row(<?php echo $value->id; ?>)">  -->
                        <a style="cursor:pointer;" id="save_button<?php echo $value->id; ?>" class="save" onclick="saveing_row(<?php echo $value->id; ?>)"><span class="glyphicon glyphicon-floppy-disk"></span></a>
                        <a href="#" class="delete" id="del-<?php echo $value->id; ?>-<?php echo $value->name; ?>" title="Remove <?php echo ucwords($value->name); ?>"><span class="glyphicon glyphicon-trash"><?php ?></span></a></td>
                    </tr>
                <?php
}
}

?>
            </tbody>
        </table>
        </div>
        </div>
        <div class="col-sm-12" style="padding:0">
        <div class="col-sm-4" style="padding:0">
        <span><?php echo $this->lang->line("ingredients"); ?></span>
        <select name="ingname[]" id="ingname" class="form-control" onchange="get_units(this.value,1)">
        <option value="">Select Ingredients</option>
        <?php
$ing = '';
foreach ($ingRecipes as $key => $value):
	$ing .= "<option value=$value->id>";
	if ($this->session->userdata('siteLang') == 'ar') {
		$ing .= $value->name_ar;
	} else if ($this->session->userdata('siteLang') == 'fr') {
	$ing .= $value->name_fr;
} else {
	$ing .= $value->name;
}
$ing .= "</option>";
endforeach;
echo $ing;
?>
        </select>
        <?php
$ingvalue = '';
foreach ($ingRecipes as $key => $value):
	$ingvalue .= "<input type=hidden id=ing_value_{$value->id} value={$value->kcal_grams}>";
	$ingvalue .= "<input type=hidden id=protine_value_{$value->id} value={$value->protein}>";
	$ingvalue .= "<input type=hidden id=fat_value_{$value->id} value={$value->fat}>";
	$ingvalue .= "<input type=hidden id=carbs_value_{$value->id} value={$value->carbs}>";
	//$ingvalue.="<input type=hidden id=ingfinal_price_{$value->id} name=ingfinal_price[$value->id]>";
endforeach;
echo $ingvalue;
?>
        <input type="hidden" id="ingfinal_price_1" name="ingfinal_price[]">
        <input type="hidden" id="protinefinal_price_1" name="protinefinal_price[]">
        <input type="hidden" id="fatfinal_price_1" name="fatfinal_price[]">
        <input type="hidden" id="carbsfinal_price_1" name="carbsfinal_price[]">
        </div>
        <div class="col-sm-3" style="padding:0">
        <span><?php echo $this->lang->line("qty"); ?></span>
        <input type="text" name="ingmaterial[]" class="form-control" placeholder="Quantity" id="ingmaterial_1" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46 || event.charCode == 0" onkeyup="Calfunction(0,1)">
        </div>
        <div class="col-sm-3" style="padding:0">
        <span><?php echo $this->lang->line("unit"); ?></span><!-- <input type="text" name="ingunit[]" class="form-control" id="ingunit"> -->
        <select name="unitname[]" id="unitname_1" class="form-control" onchange="calculate_calories(this.value,1)"">
            <option value="">Select Unit</option>
            </select>
        </div>
        <input type="hidden" name="final_calories[]" id="final_calories_1">
        <input type="hidden" name="protine_calories[]" id="protine_calories_1">
        <input type="hidden" name="fat_calories[]" id="fat_calories_1">
        <input type="hidden" name="carbs_calories[]" id="carbs_calories_1">
        <div class="col-sm-2" style="padding:0">
        <button class="btn btn-info btn-xs add" style="margin: 30% 10%;
"><i class="fa fa-plus"></i></</button>
        </div>
        </div>
        </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line("prep_time"); ?><span style="color:red">*</span></label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="prepration_time" value="<?php echo $recipes->preparation_time; ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line("cook_time"); ?><span style="color:red">*</span></label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="cooking_time" value="<?php echo $recipes->cooking_time; ?>"/>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line("notes"); ?><span style="color:red"></span></label>
            <div class="col-sm-6">
                <textarea name="notes" class="form-control" rows="3"><?php echo $recipes->notes; ?></textarea>
            </div>
        </div>
       <!--  <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo ucwords("calories"); ?><span style="color:red">*</span></label>
            <div class="col-sm-6">
            <input type="text" class="form-control" name="calories" value="<?php echo $recipes->calories; ?>"/>
            </div>
        </div> -->

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line("featured"); ?><span style="color:red"></span></label>
            <div class="col-sm-6">
                <input type="checkbox" name="is_featured" <?php echo ($recipes->is_featured == 1) ? "checked='checked'" : "" ?>/>
            </div>
        </div>

         <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line("status"); ?><span style="color:red"></span></label>
            <div class="col-sm-6">
                <label for="active" class="control-label">
                <input type="radio" name="is_status" <?php echo ($recipes->status == 0) ? "checked='checked'" : "" ?> value="0" id="active"/> <?php echo $this->lang->line("status_name_act"); ?>
                </label>
                <label for="inactive" class="control-label">
                <input type="radio" name="is_status" <?php echo ($recipes->status == 1) ? "checked='checked'" : "" ?> value="1" id="inactive"/> <?php echo $this->lang->line("status_name_dact"); ?>
                </label>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line("recipe_category"); ?><span style="color:red">*</span></label>
            <div class="col-sm-6">
               <select name="recipe_category" id="recipe_category" class="form-control">
                <option value="">Select Category</option>
                <?php
$recp = '';
foreach ($recipescats as $key => $value):
	if ($value->id == $recipes->recipe_category_id) {
		$select = "selected='selected'";
	} else { $select = "";}
	$recp .= "<option value=$value->id $select>";
	if ($this->session->userdata('siteLang') == 'ar') {
		$recp .= $value->cat_name_ar;
	} else if ($this->session->userdata('siteLang') == 'fr') {
	$recp .= $value->cat_name_fr;
} else {
	$recp .= $value->cat_name;
}
$recp .= "</option>";
endforeach;
echo $recp;
?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line("sub_category"); ?><span style="color:red">*</span></label>
            <div class="col-sm-6">
                <select name='recipe_subcat' id='recipe_subcat' class="form-control">
                    <option value="">Select Subcategory</option>
                </select>
            </div>
        </div>

         <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('source_title'); ?><span style="color:red"></span></label>
        <div class="col-sm-6">
           <input type="text" name="source_name" class="form-control" value="<?php echo $recipes->source_title ?>">
        </div>
        </div>

        <div class="form-group">
        <label class="col-sm-3 control-label"><?php echo $this->lang->line('source_link'); ?><span style="color:red"></span></label>
        <div class="col-sm-6">
            <input type="text" name="source_link" class="form-control" value="<?php echo $recipes->source_link ?>">
        </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line('level'); ?><span style="color:red"></span></label>
            <div class="col-sm-6">
                <select name="level" id="level" class="form-control">
                <option value="">Select Level</option>
                <?php
$level = '';
foreach ($levels as $key => $value):
	if ($value->id == $recipes->level_id) {
		$select = "selected='selected'";
	} else { $select = "";}
	$level .= "<option value=$value->id $select>";
	$level .= $value->level_type;
	$level .= "</option>";
endforeach;
echo $level;
?>
                </select>
            </div>
        </div>
        <?php

foreach ($recp_ing as $key => $value) {
	?>
        <input name="mastid[]" value="<?php echo $value->id ?>" type="hidden"/>
        <?php
}
?>
        <input type="hidden" name="old_image[]" value="<?php echo $recipes->recipe_media ?>">
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btnupdate'); ?></button>
                <a href="<?php echo base_url('admin/recipes'); ?>" class="btn btn-default"><?php echo $this->lang->line('btncancel'); ?></a>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
</div>
</div>
<!-- End .panel -->
</div>
<!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End #content -->
</section>

<script type="text/javascript">

function checkfile(x){
    var a=0;
    var ext = $('#stepmedia'+x).val().split('.').pop().toLowerCase();
    //alert(ext);
    if ($.inArray(ext, ['gif','png','jpg','jpeg','mp4']) == -1){
        alert('Format Must Be JPG, JPEG, PNG or GIF or MP4')
        $('#stepmedia'+x).val('');
        a=0;
        }else{
        var media = $("#stepmedia"+x).prop("files")[0];
       // console.log(media.name)
        if (media.size > 8000000){
        $('#stepmedia'+x).val('');
        alert('Maximum File Size Limit is 4MB.');
        //$('#error2').slideDown("slow");
        a=0;
        }else{
        a=1;
        $('#error2').slideUp("slow");
        }
        $('#error1').slideUp("slow");
        if (a==1){
        $('input:submit').attr('disabled',false);
        }
    }
}

function checkajaxfile(x){
    var a=0;
    var ext = $('#imgmedia_'+x).val().split('.').pop().toLowerCase();
    //alert(ext);
    if ($.inArray(ext, ['gif','png','jpg','jpeg','mp4']) == -1){
        alert('Format Must Be JPG, JPEG, PNG or GIF or MP4')
        $('#imgmedia_'+x).val('');
        a=0;
        }else{
        var media = $("#imgmedia_"+x).prop("files")[0];
       // console.log(media.name)
        if (media.size > 8000000){
        $('#imgmedia_'+x).val('');
        alert('Maximum File Size Limit is 4MB.');
        //$('#error2').slideDown("slow");
        a=0;
        }else{
        a=1;
        $('#error2').slideUp("slow");
        }
        $('#error1').slideUp("slow");
        if (a==1){
        $('input:submit').attr('disabled',false);
        }
    }
}

function playPause(id) {
    var myVideo = document.getElementById("video"+id);

    if (myVideo.paused)
        myVideo.play();
    else
        myVideo.pause();
}
/********************************* INGREIDENTS *************/
function editing_row(no,ingid,unitid)
    { //alert("Fdff");return false;
     getIngData(no);
     //getUnitsData(no);
     getUnitsData(no,ingid,unitid);
      //return false;
     document.getElementById("editing_button"+no).style.display="none";
     document.getElementById("save_button"+no).style.display="block";
     document.getElementById("close_button_"+no).style.display="block";

     var name=document.getElementById("ingname_"+no);
     var calory = document.getElementById("kcal_"+no).value;
     var protinecalory = document.getElementById("protine_"+no).value;
     var fatcalory = document.getElementById("fat_"+no).value;
     var carbscalory = document.getElementById("carbs_"+no).value;

     var oldcalory = document.getElementById("final_calories_ajax_"+no).value;
     var oldprotinecalory = document.getElementById("protine_calories_ajax_"+no).value;
     var oldfatcalory = document.getElementById("fat_calories_ajax_"+no).value;
     var oldcarbscalory = document.getElementById("carbs_calories_ajax_"+no).value;
     //console.log(oldcalory);
     var qty=document.getElementById("ingqty_"+no);
     //var img=document.getElementById("imgmediarow_"+no);
     var unit = document.getElementById("ingunit_"+no);
     //console.log(unit);return false;

     var name_data=name.innerHTML;
     var qty_data=qty.innerHTML;
     var unit_data=unit.innerHTML;
     var kcal_data=calory.innerHTML;
    // var oldcal_data=oldcalory.innerHTML;

     // <input type='text' id='newingname_"+no+"' value='"+name_data+"'>
     //calory.innerHTML = "<input type='text' name='AA' value='"+kcal_data+"'>";
     name.innerHTML="<select name='newingname' id='newingname_"+no+"' onchange='list_units(this.value,"+no+")'><option value='"+ingid+"'>"+name_data+"</option></select><input type='hidden' name='kcal' id='kcalgrams_"+no+"' value='"+calory+"'><input type='hidden' name='protine' id='protinegrams_"+no+"' value='"+protinecalory+"'><input type='hidden' name='fat' id='fatgrams_"+no+"' value='"+fatcalory+"'><input type='hidden' name='carbs' id='carbsgrams_"+no+"' value='"+carbscalory+"'>";
     qty.innerHTML="<input type='number' id='newingqty_"+no+"' value='"+qty_data+"' onkeyup=ajaxCall(0,"+no+")>";
     unit.innerHTML="<select name='newingunit' id='newingunit_"+no+"' onchange='calory_calculate(this.value,"+no+")'><option value='"+unitid+"'>"+unit_data+"</option></select><input type='hidden' name='total_calory' id='final_calories_ajax_"+no+"' value='"+oldcalory+"'><input type='hidden' name='protine_calory' id='protine_calories_ajax_"+no+"' value='"+oldprotinecalory+"'><input type='hidden' name='fat_calory' id='fat_calories_ajax_"+no+"' value='"+oldfatcalory+"'><input type='hidden' name='carbs_calory' id='carbs_calories_ajax_"+no+"' value='"+oldcarbscalory+"'>";
     /*unit.innerHTML="<input type='text' id='newingunit_"+no+"' value='"+unit_data+"'>";*/
    }

    function getIngData(no){
       var lang = "<?php echo $this->session->userdata('siteLang'); ?>";
       $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'admin/ingredients/getIngredientsList' ?>",
            dataType:"json",
            success: function(response) {
                //console.log(response);return false;
                $.each(response,function(index,object){
                    if(lang == 'ar'){
                     $('select#newingname_'+no).append('<option value="'+object['id']+'">' +object['name_ar']+ '</option>');
                    }
                    else if(lang == 'fr'){
                     $('select#newingname_'+no).append('<option value="'+object['id']+'">' +object['name_fr']+ '</option>');
                    }
                    $('select#newingname_'+no).append('<option value="'+object['id']+'">' +object['name']+ '</option>');
                    //console.log(object['kcal_grams'])
                    //console.log(object['']);
                })
            }
          });
    }

    function getUnitsData(no,ingID,unitID){
    var uname = $("#ingunit_"+no).text();
    var unitname = uname.replace(/\s/g, '');
       $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'admin/recipes/getUnitslist/' ?>"+ingID,
            dataType:"json",
            success: function(response) {
                //console.log(response);return false;
                $.each(response,function(index,object){
                    if(object['unit_name'] !== unitname){
                    $('select#newingunit_'+no).append('<option value="'+object['id']+'">' +object['unit_name']+ '</option>');
                    }
                })
            }
          });
    }

    function saveing_row(no)
    {
     var name_ing=document.getElementById("newingname_"+no).value;
     var qty_ing=document.getElementById("newingqty_"+no).value;
     /*var unit_ing=document.getElementById("newingunit_"+no).value;*/
     var unit_weight=document.getElementById("newingunit_"+no).value;

     calory_calculate(unit_weight,no); // Calories calculation

     var calory = document.getElementById("final_calories_ajax_"+no).value;
     var protinecalory = document.getElementById("protine_calories_ajax_"+no).value;
     var fatcalory = document.getElementById("fat_calories_ajax_"+no).value;
     var carbscalory = document.getElementById("carbs_calories_ajax_"+no).value;

     //alert(calory);return false;
     document.getElementById("ingqty_"+no).innerHTML=qty_ing;
     /*document.getElementById("ingunit_"+no).innerHTML=unit_ing;*/
     document.getElementById("ingunit_"+no).innerHTML=unit_weight;
     //&unit='+unit_ing
      $.ajax({
        type: "POST",
        url: "<?php echo base_url() . 'admin/ingredients/updateIngredients/' ?>"+no,
        //dataType:"json",
        data:'ignname='+name_ing+'&qty='+qty_ing+'&unit_measure='+unit_weight+'&totalcalory='+calory+'&totalprotine='+protinecalory+'&totalfat='+fatcalory+'&totalcarbs='+carbscalory,
        async: true,
        beforeSend: function()
            {
                 $('#item_'+no).html('<img style="z-index: 999999999;position: relative;left: 200px;" src="<?php echo base_url(); ?>uploads/ajax-loader.gif"/>');
            },
        success: function(response) {
           // console.log(response);return false;
            if(response == 'success'){
                $('#tblremove').load(document.URL + ' #tblremove');
            }
            else{
                alert('error');
            }
        }
     });
     document.getElementById("editing_button"+no).style.display="block";
     document.getElementById("save_button"+no).style.display="none";
     document.getElementById("close_button_"+no).style.display="none";
    }

/********************************* END ******************************/

    function edit_row(no)
    { //alert(no);
     document.getElementById("edit1_button_"+no).style.display="none";
     document.getElementById("savemedia_"+no).style.display="block";
     document.getElementById("close_button_"+no).style.display="block";

    // var step=document.getElementById("steprow_"+no);
     var desc=document.getElementById("descrow_"+no);
     var img=document.getElementById("imgmediarow_"+no);
     //console.log(img);return false;
     var odlimg = document.getElementById("methodmedia_"+no).value;

     $.ajax({
            url: "<?php echo base_url() . 'admin/recipes/getStepContent/' ?>"+no,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            type: 'post',
            success: function(response) {
                desc.innerHTML="<textarea id='desc_"+no+"'>"+response[0].step_description+"</textarea><textarea id='desc1_"+no+"'>"+response[0].step_description_ar+"</textarea><textarea id='desc2_"+no+"'>"+response[0].step_description_fr+"</textarea>";
            }
          });
     //return false;
    // var step_data=step.innerHTML;
     //var desc_data=desc.innerHTML;

     //desc.innerHTML="<textarea id='desc_"+no+"'>"+desc_data+"</textarea><textarea id='desc1_"+no+"'>"+desc_data+"</textarea><textarea id='desc2_"+no+"'>"+desc_data+"</textarea>";
     img.innerHTML="<input type='file' id='imgmedia_"+no+"' name='recipestepmedia' onchange=checkajaxfile("+no+")><input type='hidden' value='"+odlimg+"' id='oldmedia_"+no+"'>";
    }
    function save_row(id){

        var imgmedia = document.getElementById("imgmedia_"+id).value;
        var existimg = document.getElementById("oldmedia_"+id).value;
        if(imgmedia.length == 0){
            var oldimg = document.getElementById("oldmedia_"+id).value;
         }
         else{
           var file_data = $("#imgmedia_"+id).prop("files")[0];
        }
        //alert(oldimg);return false;
        var desc = document.getElementById("desc_"+id).value;
        var desc_ar = document.getElementById("desc1_"+id).value;
        var desc_fr = document.getElementById("desc2_"+id).value;
       // alert(desc);
        var form_data = new FormData();                  // Creating object of FormData class
        form_data.append("file", file_data)
        form_data.append("fileold", oldimg)              // Appending parameter named file with properties of file_field to form_data
        form_data.append("fileexists", existimg)
        form_data.append("description",desc)                 // Adding extra parameters to form_data
        form_data.append("description_ar",desc_ar)
        form_data.append("description_fr",desc_fr)



        $.ajax({
            url: "<?php echo base_url() . 'admin/recipes/updateRecipeList/' ?>"+id,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            data: form_data,                         // Setting the data attribute of ajax with file_data
            async: true,
            type: 'post',
            beforeSend: function()
            {
                 $('#item_'+id).html('<img style="z-index: 999999999;position: relative;left: 200px;" src="<?php echo base_url(); ?>uploads/ajax-loader.gif"/>');
            },
            success: function(response) {
              if (response.msg == "success")
              {
                $('#tblmethod').load(document.URL + ' #tblmethod');
                //$('#item_'+id).hide();
              }
              else
              {
                alert("error");
              }

            }
          });
        document.getElementById("edit1_button_"+id).style.display="block";
        document.getElementById("savemedia_"+id).style.display="none";
        document.getElementById("close_button_"+id).style.display="none";
        return false;
    }
    /************************** Recipes Steps Close Row *********/
    function close_row(id){
        document.getElementById("edit1_button_"+id).style.display="block";
        document.getElementById("savemedia_"+id).style.display="none";
        document.getElementById("close_button_"+id).style.display="none";
        $('#tblmethod').load(document.URL + ' #tblmethod');
    }
    /********************************* END ************************/
    /************************** Recipes Steps Close Row *********/
    function close_row2(id){
        document.getElementById("editing_button"+id).style.display="block";
        document.getElementById("save_button"+id).style.display="none";
        document.getElementById("close_button_"+id).style.display="none";
        $('#tblremove').load(document.URL + ' #tblremove');
    }
    /********************************* END ************************/

    function ajaxCall(unit,x){
        var unit = $("#newingunit_"+x).val();
        if(unit == ''){
            calory_calculate(0,x)
        }
        else{
        calory_calculate(unit,x);
        }
    }

    function Calfunction(unit,x){
        var unit = $("#unitname_"+x).val();
        if(unit == ''){
            calculate_calories(0,x)
        }
        else{
        calculate_calories(unit,x);
        }
    }


    $(document).ready(function () {

        var subcatId = "<?php echo $recipes->recipe_subcategory_id; ?>";
        listSubcategory();
        $("select#recipe_category").change(listSubcategory);
        //alert(subcat);
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, 'Please enter a valid number.');
        jQuery.validator.addMethod(
            "maxfilesize",
            function (value, element) {
                return this.optional(element) || (element.files && element.files[0]
                                       && element.files[0].size < 1024 * 1024 * 5);
            },
            'The file size can not exceed 2MB.'
        );
        jQuery.validator.addMethod("requiredUrl", function(value, element, param) {
            var expression = /^(http:\/\/|https:\/\/)?((([\w-]+\.)+[\w-]+)|localhost)(\/[\w- .\/?%&=]*)?/i;
            return expression.test(value);
        }, jQuery.validator.messages.url);

        $("#editrecipeform").validate({
            rules: {
                title:{    required: true,
                           //character: true,
                        },
                'recipe_media[]': {
                      extension: 'gif|png|jpg|jpeg',
                      maxfilesize:true
                    },
                prepration_time: {   required: true
                        },
                cooking_time: {   required: true
                        },
                recipe_description:{required:true},
                /*'txtstep[]':{
                    required: true,
                    digits:true
                },  */
                /*method: {   required: true
                        }, */
                serving: {   required: true
                        },
              /*  notes: {   required: true
                        },*/
                calories: {   required: true
                        },
                recipe_category: {   required: true
                        },
                recipe_subcat:{ required: true
                        },
               // source_name:{required:true},
                source_link:{url: true},
               /* level: {   required: true,
                           number:true,
                        },*/
            },
            messages: {
                title: {   required: "Enter recipe name",
                            //character: "Enter valid title",
                        },
                prepration_time: {   required: "Enter preparation time",
                        },
                cooking_time: {   required: "Enter cooking time",
                        },
                recipe_description:{required:"Enter recipe description"},
               /* method: {   required: "Enter first title",
                        },*/
                serving: {   required: "Enter number of serving",
                        },
               /* 'txtstep[]':{
                    required: 'please enter preparation method step',
                },*/
                /*notes: {   required: "Enter notes",
                        },*/
               /* calories: {   required: "Enter calories",
                        },*/
               /* level: {   required: "Enter first level",
                            number: "Enter valid level",
                        },*/
                recipe_category: {   required: "Select recipe category",
                        },
                recipe_subcat: {
                        required: "Select recipe sub category",
                        },
               // source_name:{required:"Enter source title"},
                //source_link:{required:"Enter source link"},
                'recipe_media[]':{
                   extension:"Only gif|png|jpg|jpeg file is allowed!",
                   maxfilesize:"The file size can not exceed 5MB."
                }
            }
        });

    var max_fields      = 100; //maximum input boxes allowed
    var wrapper         = $(".ing3"); //Fields wrapper
    var add_button      = $(".add"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            /*$(wrapper).append('<div class="col-sm-12" style="padding:0"><div class="col-sm-4" style="padding:0"><span>Ingredients</span> <select name="ingname[]" id="ingname" class="form-control"><option value="">Select Ingredients</option><?php echo $ing; ?></select></div><div class="col-sm-3" style="padding:0"><span>Amt</span> <input type="text" name="ingmaterial[]" class="form-control"/></div><div class="col-sm-3" style="padding:0"><span>Unit</span> <input type="text" name="ingunit[]" class="form-control" id="ingunit"/></div><div class="col-sm-2" style="padding:0"><a href="#" class="btn btn-xs btn-danger remove" style="margin:30% 10%"><i class="fa fa-close"></i></a></div></div>'); //add input box*/
             $(wrapper).append("<div class=col-sm-12 style=padding:0><div class=col-sm-4 style=padding:0><span><?php echo $this->lang->line('ingredients') ?></span> <select name=ingname[] id=ingname class=form-control onchange=get_units(this.value,"+x+")><option value=>Select Ingredients</option><?php echo $ing ?></select><?php echo $ingvalue ?><input type=hidden id=ingfinal_price_"+x+" name=ingfinal_price[]><input type=hidden id=protinefinal_price_"+x+" name=protinefinal_price[]><input type=hidden id=fatfinal_price_"+x+" name=fatfinal_price[]><input type=hidden id=carbsfinal_price_"+x+" name=carbsfinal_price[]></div><div class=col-sm-3 style=padding:0><span><?php echo $this->lang->line('qty') ?></span> <input type=text name=ingmaterial[] class=form-control placeholder=Quantity id=ingmaterial_"+x+" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46 || event.charCode == 0' onkeyup=Calfunction(0,"+x+")></div><div class=col-sm-3 style=padding:0><span><?php echo $this->lang->line('unit') ?></span> <select name=unitname[] id=unitname_"+x+" class=form-control onchange=calculate_calories(this.value,"+x+")><option value=>Select Unit</option><?php //echo $unit ?></select></div><input type=hidden name=final_calories[] id=final_calories_"+x+"><input type=hidden name=protine_calories[] id=protine_calories_"+x+"><input type=hidden name=fat_calories[] id=fat_calories_"+x+"><input type=hidden name=carbs_calories[] id=carbs_calories_"+x+"><div class=col-sm-2 style=padding:0><a href='#' class='btn btn-xs btn-danger remove' style='margin: 30% 10%;'><i class='fa fa-close'></i></a></div></div>"); //add input box
        }
    });

    $(wrapper).on("click",".remove", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').parent('div').remove(); x--;
    })

    /*********************** Preparation Method ***********/


    var max_fieldss      = 100; //maximum input boxes allowed
    var wrappermethod   = $(".prepmethod"); //Fields wrapper
    var insert_button   = $(".insert"); //Add button ID

    var x = 1; //initlal text box count
    $(insert_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fieldss){ //max input box allowed
            x++; //text box increment
            $(wrappermethod).append('<div class="col-sm-12" style="padding:0"><input type="number" name="txtstep[]" id="txtstep" class="form-control" placeholder="Step No"> <label class="control-label"><?php echo $this->lang->line('img_vid') ?></label> <input type="file" name="stepmedia[]" class="form-control" placeholder="Image/Video"><label></label> <textarea name="preparation_method[]" id="preparation_method" class="form-control" rows="3" placeholder="Preparation Description"></textarea> <textarea name="preparation_method_ar[]" id="preparation_method" class="form-control" rows="3" placeholder="AR_Preparation Description"></textarea><textarea name="preparation_method_fr[]" id="preparation_method" class="form-control" rows="3" placeholder="FR_Preparation Description"></textarea><a href="#" class="btn btn-xs btn-danger remove"><i class="fa fa-close"></i></a> <a class="btn btn-info btn-xs insertmore"><i class="fa fa-plus"></i></</a></div>'); //add input box
        }
    });

    $(wrappermethod).on("click",".remove", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    });

    $(wrappermethod).on("click",".insertmore", function(e){
        if(x < max_fieldss){ //max input box allowed
            x++; //text box increment
            $(wrappermethod).append('<div class="col-sm-12" style="padding:0"><input type="number" name="txtstep[]" id="txtstep" class="form-control required" placeholder="Step No"><br/><label class="control-label"><?php echo $this->lang->line('img_vid') ?></label> <input type="file" name="stepmedia[]" class="form-control" placeholder="Image/Video"><label></label> <textarea name="preparation_method[]" id="preparation_method" class="form-control required" rows="3" placeholder="Preparation Description"></textarea><textarea name="preparation_method_ar[]" id="preparation_method_ar" class="form-control" rows="3" placeholder="AR_Preparation Description"></textarea><textarea name="preparation_method_fr[]" id="preparation_method_fr" class="form-control" rows="3" placeholder="FR_Preparation Description"></textarea><a href="#" class="btn btn-xs btn-danger remove"><i class="fa fa-close"></i></a> <a class="btn btn-info btn-xs insertmore"><i class="fa fa-plus"></i></</a></div>');
        }
    });


    /**************************** END ******************************/

    function listSubcategory(){
       var lang = "<?php echo $this->session->userdata('siteLang'); ?>";
       var catId = $("select#recipe_category option:selected").attr('value');
       $.ajax({
            type: "POST",
            url: "<?php echo base_url('admin/recipes/get_subcategory'); ?>/"+catId,
            dataType: "json",
            success: function(data){
                 $('#recipe_subcat')
                .find('option')
                .remove()
                .end()
                .append('<option value="">Select Subcategory</option>');
                 $.each(data, function (key,value){
                     var opt = $('<option />'); // here we're creating a new select option with for each city
                     opt.val(value.id);
                     //opt.select(selected);
                     if(lang == 'ar'){
                     opt.text(value.sub_cate_name_ar);
                     }
                     else if(lang == 'fr'){
                     opt.text(value.sub_cate_name_fr);
                     }
                     else{
                     opt.text(value.sub_cate_name);
                     }
                     //opt.text(value.sub_cate_name);
                     $('#recipe_subcat').append(opt);
                     $('#recipe_subcat option[value='+subcatId+']').val(subcatId).attr('selected', true);
                 });
            },
        });
    }

    // Remove Ingredients
    $(".table-responsive").on('click','#tblremove .delete',function(event){

         //alert(event);return false;

        var clickedID = this.id.split('-');
        var DbNumberID = clickedID[1];
        var string = clickedID[2];

        var ingname = string.charAt(0).toUpperCase() + string.slice(1);
        //$('#item_'+DbNumberID).fadeOut();
        //return false;
        var href = $(this).attr("href")
        //console.log(href);return false;
        var btn = this;

        if(confirm("Are you sure want to remove "+ingname+"?")) {

          $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'admin/recipes/removeRecipeIngredient/' ?>"+DbNumberID,
            dataType:"json",
            success: function(response) {
              if (response.msg == "success")
              {
                $('#item_'+DbNumberID).fadeOut();
              }
              else
              {
                alert("Error");
              }

            }
          });
         }

         event.preventDefault();
        });

    // Remove Preparation method
    $(".table-responsive").on('click','#tblmethod .delete',function(event){

        var clickedID = this.id.split('-');

       // console.log(clickedID);return false;

        var DbNumberID = clickedID[1];
        var string = clickedID[2];

        var stepno = string.charAt(0).toUpperCase() + string.slice(1);

        var href = $(this).attr("href")
        //console.log(href);return false;
        var btn = this;

        if(confirm("Are you sure want to remove step "+stepno+"?")) {

          $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'admin/recipes/removeRecipeStep/' ?>"+DbNumberID,
            dataType:"json",
            success: function(response) {
                //console.log(response);return false;
              if (response.msg == "success")
              {
                $('#item_'+DbNumberID).fadeOut();
              }
              else
              {
                alert("Error");
              }

            }
          });
         }

         event.preventDefault();
        });

    $("#ingunit").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
        }
    });

    });

    function list_units(ingid,no){
         var lang = "<?php echo $this->session->userdata('siteLang'); ?>";
         $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'admin/recipes/getUnitslist/' ?>"+ingid,
            dataType:"json",
            success: function(response) {
                //console.log(response);return false;
                $('#newingunit_'+no)
                .find('option')
                .remove()
                .end()
                //.append('<option value="">Select Unit</option>');
                 $.each(response, function (key,value){
                     var opt1 = $('<option />'); // here we're creating a new select option with for each city
                     opt1.val(value.id);
                     if(lang == 'ar'){
                     opt1.text(value.unit_name_ar);
                     }
                     else if(lang == 'fr'){
                     opt1.text(value.unit_name_fr);
                     }
                     else{
                     opt1.text(value.unit_name);
                     }
                     //opt.select(selected);
                     $('#newingunit_'+no).append(opt1);
                     $('#kcalgrams_'+no).val(response[0].kcal_grams);
                     $('#protinegrams_'+no).val(response[0].protein);
                     $('#fatrams_'+no).val(response[0].fat);
                     $('#carbsgrams_'+no).val(response[0].carbs);
                    // console.log($('#newingunit_'+no).val());
                     //$('#unitname option[value='+subcatId+']').val(subcatId).attr('selected', true);
                 });
              }
          });
    }

    function get_units(id,x){
        //alert(id);return false;
      // var catId = $("select#recipe_category option:selected").attr('value');
       var qty = $("#ingmaterial_"+x).val();
       var finalprice = $("#ing_value_"+id).val();
       var protine = $("#protine_value_"+id).val();
       var fat = $("#fat_value_"+id).val();
       var carbs = $("#carbs_value_"+id).val();
       //console.log(finalprice);return false;
       var lang = "<?php echo $this->session->userdata('siteLang'); ?>";
       $("#ingfinal_price_"+x).val(finalprice);
       $("#protinefinal_price_"+x).val(protine);
       $("#fatfinal_price_"+x).val(fat);
       $("#carbsfinal_price_"+x).val(carbs);
       //alert($("#ing_value_"+id).val());return false;
       $.ajax({
            type: "POST",
            url: "<?php echo base_url('admin/units/getIngUnits'); ?>/"+id,
            dataType: "json",
            success: function(data){
                // console.log(data);return false;
                 $('#unitname_'+x)
                .find('option')
                .remove()
                .end()
                .append('<option value="">Select Unit</option>');
                 $.each(data, function (key,value){
                     var opt = $('<option />'); // here we're creating a new select option with for each city
                     opt.val(value.id);
                     if(lang == 'ar'){
                     opt.text(value.unit_name_ar);
                     }
                     else if(lang == 'fr'){
                     opt.text(value.unit_name_fr);
                     }
                     else{
                     opt.text(value.unit_name);
                     }
                     //opt.select(selected);
                     $('#unitname_'+x).append(opt);
                     //$('#unitname option[value='+subcatId+']').val(subcatId).attr('selected', true);
                 });
            },
        });
}

function calory_calculate(id,x){
      // var catId = $("select#recipe_category option:selected").attr('value');
    var quantity = parseFloat($("#newingqty_"+x).val());
    var nutrientPer100g = parseFloat($("#kcalgrams_"+x).val());
    var nutrientPer100gprotine = parseFloat($("#protinegrams_"+x).val());
    var nutrientPer100gfat = parseFloat($("#fatgrams_"+x).val());
    var nutrientPer100gcarbs = parseFloat($("#carbsgrams_"+x).val());
    //alert(nutrientPer100gcarbs);
    //var gramsPerServing = $("#serving").val(); // Serving size (id)
   // console.log(nutrientPer100g+gramsPerServing);
    $.ajax({
            type: "POST",
            url: "<?php echo base_url('admin/units/getUnitamount'); ?>/"+id,
            dataType: "json",
            success: function(data){
               // console.log(quantity);
                var nutrientGrams = quantity * (data.unit_gram);
                //alert(quantity * data.unit_gram);
                var nutrientPerServing = (nutrientPer100g * nutrientGrams) / 100.0;
                var nutrientPerServingprotine = (nutrientPer100gprotine * nutrientGrams) / 100.0;
                var nutrientPerServingfat = (nutrientPer100gfat * nutrientGrams) / 100.0;
                var nutrientPerServingcarbs = (nutrientPer100gcarbs * nutrientGrams) / 100.0;
              //  var nutrientGrams = (quantity * data.unit_gram) * nutrientPerServing;
                $("#final_calories_ajax_"+x).val(Math.floor(nutrientPerServing));
                $("#protine_calories_ajax_"+x).val(Math.floor(nutrientPerServingprotine));
                $("#fat_calories_ajax_"+x).val(Math.floor(nutrientPerServingfat));
                $("#carbs_calories_ajax_"+x).val(Math.floor(nutrientPerServingcarbs));
                //return Math.floor(nutrientGrams);
                //console.log(data.unit_gram);return false;
            }
    });
    /*var nutrientPerServing = (nutrientPer100g * gramsPerServing) / 100.0;

    var nutrientGrams = quantity * nutrientPerServing;
    $("#final_calories_ajax_"+x).val(Math.floor(nutrientGrams));*/
    //return Math.floor(nutrientGrams);
}

function calculate_calories(id,x){
      // var catId = $("select#recipe_category option:selected").attr('value');
    var quantity = parseFloat($("#ingmaterial_"+x).val());
    var nutrientPer100g = parseFloat($("#ingfinal_price_"+x).val());
     var protinePer100g = parseFloat($("#protinefinal_price_"+x).val());
    var fatPer100g = parseFloat($("#fatfinal_price_"+x).val());
    var carbsPer100g = parseFloat($("#carbsfinal_price_"+x).val());
    //var gramsPerServing = $("#serving").val(); // (id) Serving size
   // console.log(nutrientPer100g+gramsPerServing);
     $.ajax({
            type: "POST",
            url: "<?php echo base_url('admin/units/getUnitamount'); ?>/"+id,
            dataType: "json",
            success: function(data){
               // console.log(quantity);
                var nutrientGrams = quantity * (data.unit_gram);
                //alert(quantity * data.unit_gram);
                var nutrientPerServing = (nutrientPer100g * nutrientGrams) / 100.0;
                 var nutrientPerServingprotine = (protinePer100g * nutrientGrams) / 100.0;
                var nutrientPerServingfat= (fatPer100g * nutrientGrams) / 100.0;
                var nutrientPerServingcarbs = (carbsPer100g * nutrientGrams) / 100.0;
              //  var nutrientGrams = (quantity * data.unit_gram) * nutrientPerServing;
                $("#final_calories_"+x).val(Math.floor(nutrientPerServing));
                $("#protine_calories_"+x).val(Math.floor(nutrientPerServingprotine));
                $("#fat_calories_"+x).val(Math.floor(nutrientPerServingfat));
                $("#carbs_calories_"+x).val(Math.floor(nutrientPerServingcarbs));
                //return Math.floor(nutrientGrams);
                //console.log(data.unit_gram);return false;
            }
    });
    /*var nutrientPerServing = (nutrientPer100g * gramsPerServing) / 100.0;

    var nutrientGrams = quantity * nutrientPerServing;
    $("#final_calories_"+x).val(Math.floor(nutrientGrams));*/
    //return Math.floor(nutrientGrams);
}
</script>