<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recipes extends MY_Controller {

	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		lang_switcher($this->session->userdata('siteLang'));
		$this->load->model('admin/Recipes_model');
		$this->load->model('admin/Recipes_filter_model');
		$this->load->model('admin/Ingredients_model');
		$this->load->model('admin/Units_model');
		$this->load->model('admin/Master_recipeingredients_model');
		$this->load->model('admin/Recipes_filter_model');
		$this->load->model('admin/Recipe_categories_model');
		$this->load->library('excel');
		if (!$this->session->userdata('admin_id')) {
			redirect(base_url() . 'admin/login');
		}
	}

	/**
	 * Index
	 */
	function index() {
		$this->data['title'] = $this->lang->line('recp_index');
		$this->data['page'] = 'recipes';
		//$this->data['recipes'] = $this->Recipes_model->getRecipies();

		// $this->data['recipes'] = $this->Recipes_model->order_by_column('id');
		$this->__template('admin/recipes/index', $this->data);
	}
	/**
	 *  All recipes list
	 */
	public function recipesajax_list() {
		$list = $this->Recipes_filter_model->get_datatables();
		//echo $this->db->last_query();exit;
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $recipes) {

			$ingredients_names = array();
			$recipes->ingredients_names = array();

			$ingredients = $this->Recipes_filter_model->get_ingredients($recipes->id);

			for ($k = 0; $k < count($ingredients); $k++) {
				if ($ingredients[$k]->recipe_id == $recipes->id) {
					$ingredients_names[] = $ingredients[$k]->name;
				}
			}

			$recipes->ingredients_names = implode(" | ", $ingredients_names);
			if (!empty($recipes->recipe_media)) {
				$img = '<img src=' . $recipes->recipe_media . ' width="50">';
			} else {
				$img = '';
			}
			$no++;
			$row = array();
			$row[] = $recipes->id;
			$row[] = $recipes->recipe_name;
			$row[] = $img;
			/*$row[] = $recipes->ingredients_names;*/
			$row[] = $recipes->preparation_time;
			$row[] = ($recipes->status == 0) ? "<i id=" . $recipes->id . " class='status_checks btn btn-success' data=" . $recipes->id . ">Active</i>" : "<i id=" . $recipes->id . " class='status_checks btn btn-danger' data=" . $recipes->id . ">Inactive</i>";
			$row[] = "<a href=" . base_url() . "admin/recipes/edit/" . $recipes->id . " class='btn btn-sm btn-primary'><span><i class='fa fa-pencil' aria-hidden='true'></i>" . $this->lang->line('btnedit') . "</span></a> <a href=" . base_url() . "admin/recipes/delete_recipe/" . $recipes->id . " class='btn btn-sm btn-danger' onclick=\"return confirm('Are you sure want to remove {$recipes->recipe_name} ?');\"><span><i class='fa fa-trash-o' aria-hidden='true'></i>" . $this->lang->line('btnremove') . "</span></a>";
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Recipes_filter_model->count_all(),
			"recordsFiltered" => $this->Recipes_filter_model->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	/**
	 * Index action
	 */
	function create() {
		if ($_POST) {

			/********************* Preparation Images ****************/
			$filesmethod = $_FILES['stepmedia'];
			$config = array(
				'upload_path' => FCPATH . 'uploads/recipesteps',
				'allowed_types' => 'jpg|gif|png|jpeg|mp4|avi|3gp',
				'overwrite' => 1,
			);
			$images = array();
			//echo '<pre>';
			for ($i = 1; $i <= count($filesmethod); $i++) {

				if ($filesmethod['name'][$i] != '') {

					$this->load->library('upload', $config);

					$_FILES['stepmedia[]']['name'] = $filesmethod['name'][$i];
					$_FILES['stepmedia[]']['type'] = $filesmethod['type'][$i];
					$_FILES['stepmedia[]']['tmp_name'] = $filesmethod['tmp_name'][$i];
					$_FILES['stepmedia[]']['error'] = $filesmethod['error'][$i];
					$_FILES['stepmedia[]']['size'] = $filesmethod['size'][$i];

					//$fileName = date('dmYhis') . '-' . $filesmethod['name'][$i];

					$fileName = preg_replace('/[^A-Za-z0-9.]/', "", $filesmethod['name'][$i]);
					$images[$i] = $fileName;

					$config['file_name'] = $fileName;

					$this->upload->initialize($config);

					if ($this->upload->do_upload('stepmedia[]')) {
						$this->upload->data();
					} else {
						echo $this->upload->display_errors();
					}
				} else {
					$images[$i] = "";
				}
			}
			/**************************** END *****************/

			/*********************** Recipes Images *************/
			$files = $_FILES['recipe_media'];
			$config1 = array(
				'upload_path' => FCPATH . 'uploads/recipes',
				'allowed_types' => 'jpg|gif|png|jpeg|mp4|avi',
				'overwrite' => 1,
			);

			if ($files['name'][0] != '') {
				/********************* Recipes ****************/

				$this->load->library('upload', $config1);

				$recipe_image = array();

				foreach ($files['name'] as $key => $image) {
// die("DF");
					$_FILES['recipe_media[]']['name'] = $files['name'][$key];
					$_FILES['recipe_media[]']['type'] = $files['type'][$key];
					$_FILES['recipe_media[]']['tmp_name'] = $files['tmp_name'][$key];
					$_FILES['recipe_media[]']['error'] = $files['error'][$key];
					$_FILES['recipe_media[]']['size'] = $files['size'][$key];

					//$fileName = date('dmYhis') . '-' . $image;

					$fileName = preg_replace('/[^A-Za-z0-9.]/', "", $_FILES["recipe_media[]"]["name"]);
					$recipe_image[] = $fileName;

					$config1['file_name'] = $fileName;

					$this->upload->initialize($config1);

					if ($this->upload->do_upload('recipe_media[]')) {
						$this->upload->data();
					} else {
						return false;
					}
				}
			}
			/************************** END *********************/

			$recipes_title = $_POST['recipe_name'];
			$recipes_title_ar = $_POST['recipe_name_ar'];
			$recipes_title_fr = $_POST['recipe_name_fr'];
			$recipe_description = $_POST['recipe_description'];
			$recipe_description_ar = $_POST['recipe_description_ar'];
			$recipe_description_fr = $_POST['recipe_description_fr'];
			// $recipe_method = $_POST['recipe_method'];
			//$recipe_image = '';
			$recipe_prep_time = $_POST['preparation_time'];
			$cooking_time = $_POST['cooking_time'];
			$serving = $_POST['serving'];
			$recipe_notes = $_POST['notes'];
			//$recipe_calories = $_POST['calories'];
			$rec_cat = $_POST['recipe_category'];
			$rec_subcat = $_POST['recipe_subcat'];
			$rec_stitle = trim($_POST['source_name']);
			$rec_slink = trim($_POST['source_link']);
			$level = $_POST['level'] ? $_POST['level'] : 1;
			$featured = $_POST['is_featured'] ? 1 : 0;

			$recipes = array(
				"recipe_name" => trim($recipes_title),
				"recipe_name_ar" => trim($recipes_title_ar),
				"recipe_name_fr" => trim($recipes_title_fr),
				"recipe_media" => base_url() . 'uploads/recipes/' . $recipe_image[0],
				//"recipe_method"         =>$recipe_method,
				"preparation_time" => trim($recipe_prep_time),
				"cooking_time" => trim($cooking_time),
				"recipe_description" => trim($recipe_description),
				"recipe_description_ar" => trim($recipe_description_ar),
				"recipe_description_fr" => trim($recipe_description_fr),
				"notes" => trim($recipe_notes),
				"serving" => trim($serving),
				//"calories"              =>$recipe_calories,
				"admin_by" => $this->session->userdata('admin_id'),
				"is_featured" => $featured,
				"recipe_category_id" => $rec_cat,
				"recipe_subcategory_id" => $rec_subcat,
				"source_title" => $rec_stitle,
				"source_link" => $rec_slink,
				"level_id" => $level,
			);

			$recipie_id = $this->Recipes_model->insert($recipes);

			/********************* Recipes Steps Store ************/
			$steps = array(
				"recipe_id" => $recipie_id,
			);

			for ($i = 1; $i <= count($_POST['txtstep']); $i++) {
				$steps['step_no'] = trim($_POST['txtstep'][$i]);
				$steps['step_description'] = trim($_POST['preparation_method'][$i]);
				$steps['step_description_ar'] = trim($_POST['preparation_method_ar'][$i]);
				$steps['step_description_fr'] = trim($_POST['preparation_method_fr'][$i]);
				$steps['image'] = 'uploads/recipesteps/' . $images[$i];
				$this->db->insert('recipe_steps', $steps);
			}

			/*********************** END ***************************/

			/**************** Ingredients Master table stored *****/
			$ingredients = array(
				"recipe_id" => $recipie_id,
			);

			for ($i = 1; $i <= count($_POST['ingname']); $i++) {

				$checkID = $this->Recipes_model->exitsRecipesIngredient($recipie_id, $_POST['ingname'][$i]);

				if ($checkID == 1) {
					continue;
				} else {
					$ingredients['ingredient_id'] = $_POST['ingname'][$i];
					$ingredients['qty'] = trim($_POST['ingmaterial'][$i]);
					//$ingredients['unit'] = $_POST['ingunit'][$i];
					//$ingredients['measureunit_id'] = $_POST['unitname'][$i];
					$ingredients['finalunit_id'] = $_POST['unitname'][$i];
					$ingredients['measure_amount'] = $_POST['final_calories'][$i];
					$ingredients['protein_amount'] = $_POST['protine_calories'][$i];
					$ingredients['fat_amount'] = $_POST['fat_calories'][$i];
					$ingredients['carbs_amount'] = $_POST['carbs_calories'][$i];
					$this->db->insert('master_recipeingredients', $ingredients);
				}
			}

			/*********************** Calculate total calory stored in reicpes table ****/

			$totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$recipie_id}")->row()->maxid;

			$caldata = array('total_calory' => $totalcalory);

			$this->db->where('id', $recipie_id);
			$this->db->update('recipes', $caldata);

			/******************** END **************************/

			$this->flash_notification('Recipe is successfully added.');
			redirect(base_url() . 'admin/recipes/');

		}

		$this->data['title'] = $this->lang->line('recp_add');
		$this->data['page'] = 'create';
		$this->data['units'] = $this->getUnits();
		$this->__template('admin/recipes/create', $this->data);

	}

	// Get recipes from id
	function edit($recipe_id = '') {

		if ($recipe_id) {
			$this->data['title'] = $this->lang->line('recp_edit');
			$this->data['page'] = 'recipes';
			$this->data['recipes'] = $this->Recipes_model->get($recipe_id);
			$this->data['recp_ing'] = $this->Recipes_model->getRecipeingredients($recipe_id);
			$this->data['resting'] = $this->Recipes_model->getSelectedingredients($recipe_id);
			$this->data['recipescats'] = $this->Recipes_model->getRecipescat();
			$this->data['units'] = $this->getUnits();
			//$this->data['param2'] = '';
			$this->__template('admin/recipes/edit', $this->data);
		}

		//redirect('quiz');
	}

	/**
	 * Update recipes table , master_recipesingrdients
	 * @param string $id
	 */
	function update($id = '') {

		$fullpath = base_url() . 'uploads/recipes/';
		//echo $fullpath;exit;
		if ($_POST) {
			$ingredients = array();
			$this->Recipes_model->update($id, array(
				'recipe_name' => trim($_POST['title']),
				'recipe_name_ar' => trim($_POST['title_ar']),
				'recipe_name_fr' => trim($_POST['title_fr']),
				'recipe_description' => trim($_POST['recipe_description']),
				'recipe_description_ar' => trim($_POST['recipe_description_ar']),
				'recipe_description_fr' => trim($_POST['recipe_description_fr']),
				//'recipe_method'         => $_POST['method'],
				'preparation_time' => trim($_POST['prepration_time']),
				'cooking_time' => trim($_POST['cooking_time']),
				'serving' => trim($_POST['serving']),
				'notes' => trim($_POST['notes']),
				'recipe_category_id' => $_POST['recipe_category'],
				"recipe_subcategory_id" => $_POST['recipe_subcat'],
				"source_title" => trim($_POST['source_name']),
				"source_link" => trim($_POST['source_link']),
				//'calories'              => $_POST['calories'],
				//"admin_by"              => $this->session->userdata('admin_id'),
				"is_featured" => $_POST['is_featured'] ? 1 : 0,
				"status" => $_POST['is_status'],
				'level_id' => $_POST['level'] ? $_POST['level'] : 1,
				'updated_at' => date('Y-m-d H:i:s'),
			));

			/********************* Preparation Images ****************/
			$filesmethod = $_FILES['stepmedia'];
			$config2 = array(
				'upload_path' => FCPATH . 'uploads/recipesteps',
				'allowed_types' => 'jpg|gif|png|jpeg|mp4|avi|3gp',
				'overwrite' => 1,
			);
			$images = array();
			//echo '<pre>';
			for ($i = 0; $i < count($filesmethod); $i++) {

				if ($filesmethod['name'][$i] != '') {

					$this->load->library('upload', $config2);

					$_FILES['stepmedia[]']['name'] = $filesmethod['name'][$i];
					$_FILES['stepmedia[]']['type'] = $filesmethod['type'][$i];
					$_FILES['stepmedia[]']['tmp_name'] = $filesmethod['tmp_name'][$i];
					$_FILES['stepmedia[]']['error'] = $filesmethod['error'][$i];
					$_FILES['stepmedia[]']['size'] = $filesmethod['size'][$i];

					//$fileName = date('dmYhis') . '-' . $filesmethod['name'][$i];

					$fileName = preg_replace('/[^A-Za-z0-9.]/', "", $filesmethod['name'][$i]);
					$images[$i] = $fileName;

					$config2['file_name'] = $fileName;

					$this->upload->initialize($config2);

					if ($this->upload->do_upload('stepmedia[]')) {
						$this->upload->data();
					} else {
						echo $this->upload->display_errors();
					}
				} else {
					$images[$i] = "";
				}
			}
			/**************************** END *****************/

			$number_of_files = sizeof($_FILES['recipe_media']['tmp_name']);

			if (!empty($_FILES['recipe_media']['name'][0])) {

				$uploads = '';
				for ($i = 0; $i < $number_of_files; $i++) {

					$file_ext = end(explode(".", $_FILES['recipe_media']['name'][$i]));
					$uploads .= $files . date('dmYhis') . '.' . $file_ext;
					$config[] = $i . date('dmYhis') . '.' . $file_ext;

					move_uploaded_file($_FILES['recipe_media']['tmp_name'][$i], FCPATH . 'uploads/recipes/' . $config[$i]);
				}
				$files = implode(',', $config);
				$this->Recipes_model->update($id, array('recipe_media' => $fullpath . $files));

				/********************* Recipes Steps Store ************/

				$steps = array(
					"recipe_id" => $id,
				);

				for ($i = 0; $i < count($_POST['txtstep']); $i++) {
					if (!empty($_POST['txtstep'][$i])) {
						$steps['step_no'] = trim($_POST['txtstep'][$i]);
						$steps['step_description'] = trim($_POST['preparation_method'][$i]);
						$steps['step_description_ar'] = trim($_POST['preparation_method_ar'][$i]);
						$steps['step_description_fr'] = trim($_POST['preparation_method_fr'][$i]);
						$steps['image'] = 'uploads/recipesteps/' . $images[$i];
						$this->db->insert('recipe_steps', $steps);
					} else {
						continue;
					}
				}

				/*********************** END ***************************/
				$ingredients = array(
					"recipe_id" => $id,
				);
				/**************** Ingredients Master table stored *****/
				for ($i = 0; $i < count($_POST['ingname']); $i++) {
					if (!empty($_POST['ingname'][$i])) {

						$checkID = $this->Recipes_model->exitsRecipesIngredient($id, $_POST['ingname'][$i]);

						if ($checkID == 1) {
							continue;
						} else {
							$ingredients['ingredient_id'] = $_POST['ingname'][$i];
							$ingredients['qty'] = trim($_POST['ingmaterial'][$i]);
							/*$ingredients['unit'] = $_POST['ingunit'][$i];*/
							//$ingredients['measureunit_id'] = $_POST['unitname'][$i];
							$ingredients['finalunit_id'] = $_POST['unitname'][$i];
							$ingredients['measure_amount'] = $_POST['final_calories'][$i];
							$ingredients['protein_amount'] = $_POST['protine_calories'][$i];
							$ingredients['fat_amount'] = $_POST['fat_calories'][$i];
							$ingredients['carbs_amount'] = $_POST['carbs_calories'][$i];
							$this->db->insert('master_recipeingredients', $ingredients);
						}
					} else {
						continue;
					}
				}
				$totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$id}")->row()->maxid;

				$caldata = array('total_calory' => $totalcalory);

				$this->db->where('id', $id);
				$this->db->update('recipes', $caldata);
				/******************** END **************************/
			} else {
				/********************* Recipes Steps Store ************/
				$steps = array(
					"recipe_id" => $id,
				);

				for ($i = 0; $i < count($_POST['txtstep']); $i++) {
					if (!empty($_POST['txtstep'][$i])) {
						$steps['step_no'] = trim($_POST['txtstep'][$i]);
						$steps['step_description'] = trim($_POST['preparation_method'][$i]);
						$steps['step_description_ar'] = trim($_POST['preparation_method_ar'][$i]);
						$steps['step_description_fr'] = trim($_POST['preparation_method_fr'][$i]);
						$steps['image'] = 'uploads/recipesteps/' . $images[$i];
						$this->db->insert('recipe_steps', $steps);
					} else {
						continue;
					}
				}

				/*********************** END ***************************/
				$ingredients = array(
					"recipe_id" => $id,
				);
				/**************** Ingredients Master table stored *****/
				for ($i = 0; $i < count($_POST['ingname']); $i++) {
					if (!empty($_POST['ingname'][$i])) {

						$checkID = $this->Recipes_model->exitsRecipesIngredient($id, $_POST['ingname'][$i]);

						if ($checkID == 1) {
							continue;
						} else {
							$ingredients['ingredient_id'] = $_POST['ingname'][$i];
							$ingredients['qty'] = trim($_POST['ingmaterial'][$i]);
							/*$ingredients['unit'] = $_POST['ingunit'][$i];*/
							/*$ingredients['measureunit_id'] = $_POST['unitname'][$i];*/
							$ingredients['finalunit_id'] = $_POST['unitname'][$i];
							$ingredients['measure_amount'] = $_POST['final_calories'][$i];
							$ingredients['protein_amount'] = $_POST['protine_calories'][$i];
							$ingredients['fat_amount'] = $_POST['fat_calories'][$i];
							$ingredients['carbs_amount'] = $_POST['carbs_calories'][$i];
							$this->db->insert('master_recipeingredients', $ingredients);
						}
					} else {
						continue;
					}
				}
				$totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$id}")->row()->maxid;

				$caldata = array('total_calory' => $totalcalory);

				$this->db->where('id', $id);
				$this->db->update('recipes', $caldata);
				/******************** END **************************/
			}
			$this->flash_notification('Recipe is successfully updated.');
		}
		redirect(base_url() . 'admin/recipes/');
	}

	/**
	 * Delete recipe details
	 * @param string $recipe_id
	 *
	 */
	function delete_recipe($recipe_id = '') {
		if ($recipe_id) {

			$image = $this->Recipes_model->find($recipe_id); // Recipes image
			$images = explode("/", $image->recipe_media);

			$steps = $this->db->select('image') // Steps images
				->from('recipe_steps')
				->where('recipe_id', $recipe_id)
				->get()->row();

			$this->db->trans_start();

			unlink(FCPATH . 'uploads/recipes/' . $images[6]); // Remove recipe image
			unlink(FCPATH . $steps->image); // Remove steps images

			$this->db->query("DELETE FROM recipes WHERE id='$recipe_id';");
			//$this->db->query("DELETE FROM master_recipeingredients WHERE recipe_id='$recipe_id';");

			$this->db->trans_complete();
			//echo $recipe_id;exit;
			//$this->Recipes_model->delete($recipe_id);
			$this->flash_notification('Recipe is successfully deleted.');
		}
		redirect(base_url() . 'admin/recipes/');
	}

	// Get recipes sub category
	function get_subcategory($recipecat = null) {

		$this->db->select('id, sub_cate_name,sub_cate_name_ar,sub_cate_name_fr');
		if ($recipecat != NULL) {

			$this->db->where('recipe_category_id', $recipecat);
			$this->db->where('status', 0);
			$query = $this->db->get('recipe_subcategories');
			echo json_encode($query->result());
		} else {
			return FALSE;
		}
	}

	// User recipe approve
	function approveUserRecipe() {

		$recID = $_POST['recid'];
		$userid = $_POST['userid'];
		$data = array(
			'isapprove' => $_POST['status'],
		); // Status update with 0 to 1

		$success = $this->Recipes_model->approveRejectRecipe($data, $recID, $userid);
		echo $success;
	}

	/**
	 * Reicpes status update
	 */

	function update_status() {
		$status = $_POST['status'];
		$id = $_POST['id'];

		$this->db->where("id", $id);
		$this->db->update("recipes", array('status' => $status));
	}

	/**
	 *  Recipes excel Import
	 */
	function recipeImportExcel() {

		if ($_POST) {

			$objPHPExcel = PHPExcel_IOFactory::load($_FILES["file"]["tmp_name"]);

			$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

			foreach ($cell_collection as $cell) {
				$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
				$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
				//header will/should be in row 1 only.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				} else {
					$arr_data[$row][$column] = $data_value;
				}
			}
			//echo '<pre>';
			if (count($arr_data) > 0) {

				foreach ($arr_data as $key => $value) {

					//print_r($value);
					$recipes = array();

					$recipes['recipe_name'] = trim($value[A]);
					$recipes['recipe_name_ar'] = trim($value[B]);
					$recipes['recipe_name_fr'] = trim($value[C]);
					$recipes['recipe_description'] = trim($value[E]);
					$recipes['recipe_description_ar'] = trim($value[F]);
					$recipes['recipe_description_fr'] = trim($value[G]);
					$recipes['serving'] = trim($value[I]);
					$recipes['preparation_time'] = trim($value[K]);
					$recipes['cooking_time'] = trim($value[L]);
					$recipes['is_featured'] = $value[M];
					//$recipes[$i]['recipe_category_id'] = $csv_line[13];

					$methods = explode(";", $value[H]);
					$ingredients = explode(";", $value[J]);

					$check = $this->Recipe_categories_model->checkCategory(trim($value[N]));
					if (count($check) == 0) {
						$this->db->insert('recipe_categories', array('cat_name' => $value[N]));
						$check->id = $this->db->insert_id(); // Get last id
					}
					//$ing_csv['count'] = count($check);
					$recipes['recipe_category_id'] = $check->id;

					$recipes['source_title'] = $value[O];
					$recipes['source_link'] = $value[P];

					$checkAll = $this->db->get_where('recipes',
						array(
							'recipe_name' => trim($recipes['recipe_name']),
						)
					)->row(); // Check reicpes name exists

					if (!$checkAll) {
						// Insert in reicpes table
						if (!empty($value[D])) {

							$filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($value[D])));

							$type = explode(".", $filename);
							if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
								$img_destitation_path = getcwd() . "/uploads/recipes/" . date('Y') . '-' . $filename;
								if (!copy(trim($value[D]), $img_destitation_path)) {
									$recipes['recipe_media'] = '';
								} else {
									$recipes['recipe_media'] = base_url('uploads/recipes') . '/' . date('Y') . '-' . $filename;
								}
							} else {

								$recipes['recipe_media'] = '';
							}

						}

						$this->db->insert('recipes', $recipes);
						$insert_id = $this->db->insert_id(); // Get last id

						if (count($methods) >= 1) {

							foreach ($methods as $key => $value) {

								$stepsdata = explode("|", $value);
								if (!empty($stepsdata[4])) {
									$filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($stepsdata[4])));
									$type = explode(".", $filename);
									if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
										$img_destitation_path = getcwd() . "/uploads/recipesteps/" . date('Y') . '-' . $filename;
										if (!copy(trim($stepsdata[4]), $img_destitation_path)) {
											$stepsdata['img_video'] = '';
										} else {
											$stepsdata['img_video'] = 'uploads/recipesteps/' . date('Y') . '-' . $filename;
										}
									} else {

										$stepsdata['img_video'] = '';
									}
								}

								if (count($stepsdata) > 1) {
									$steps = array(
										'recipe_id' => $insert_id,
										'step_no' => trim($stepsdata[0]),
										'step_description' => trim($stepsdata[1]),
										'step_description_ar' => trim($stepsdata[2]),
										'step_description_fr' => trim($stepsdata[3]),
										'image' => $stepsdata['img_video'],
										//'image'	=> $stepsdata[1]
									);
									//print_r($steps);
									$this->db->insert('recipe_steps', $steps);
								}
							}
						}

						if (count($ingredients) >= 1 && !empty($ingredients[0])) {
							$masterArray = array();
							foreach ($ingredients as $key => $value) {
								$ingdata = explode("|", $value);
								//print_r($key);
								if (count($ingdata) > 1) {
									$checking = $this->Ingredients_model->checkIngredient(trim($ingdata[0]));
									if (count($checking) == 0) {
										$this->db->insert('ingredients', array('name' => $ingdata[0]));
										$ing_id = $this->db->insert_id(); // Get last id
										$checking->id = $ing_id;
									}
									$checking->qty = $ingdata[1];

									$masterArray[$key]['recipe_id'] = $insert_id;
									$masterArray[$key]['ingredient_id'] = $checking->id;
									$masterArray[$key]['qty'] = $checking->qty;

									if (count($checking) > 0) {
										$checkunit = $this->Units_model->checkUnit(trim($ingdata[2]));

										if (count($checkunit) == 0) {
											$this->db->insert('units', array('unit_name' => $ingdata[2]));
											$unit_id = $this->db->insert_id(); // Get last id
											$checkunit->id = $unit_id;
											//$checkunit->unit_gram = 0;
										}
										$masterArray[$key]['finalunit_id'] = $checkunit->id;
										$kcal[$key] = $this->calculateCalory('kcal', $checking->qty, $checking->kcal_grams, $checkunit->unit_gram);
										$protein[$key] = $this->calculateCalory('protein', $checking->qty, $checking->protein, $checkunit->unit_gram);
										$fat[$key] = $this->calculateCalory('fat', $checking->qty, $checking->fat, $checkunit->unit_gram);
										$carbs[$key] = $this->calculateCalory('carbs', $checking->qty, $checking->carbs, $checkunit->unit_gram);
										$masterArray[$key]['measure_amount'] = $kcal[$key]['total_calory'];
										$masterArray[$key]['protein_amount'] = $protein[$key]['total_protein'];
										$masterArray[$key]['fat_amount'] = $fat[$key]['total_fat'];
										$masterArray[$key]['carbs_amount'] = $carbs[$key]['total_carbs'];
									}
								}
							}
							$this->db->insert_batch('master_recipeingredients', $masterArray);
							/************************* Total calory stored **********/
							$totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$insert_id}")->row()->maxid;

							$caldata = array('total_calory' => $totalcalory);

							$this->db->where('id', $insert_id);
							$this->db->update('recipes', $caldata);
							/******************************** END *********************/
						}

					} else {
						// update
						if (!empty($value[D])) {

							$filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($value[D])));

							$type = explode(".", $filename);
							if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
								$img_destitation_path = getcwd() . "/uploads/recipes/" . date('Y') . '-' . $filename;
								if (!copy(trim($value[D]), $img_destitation_path)) {
									$recipes['recipe_media'] = '';
								} else {
									$recipes['recipe_media'] = base_url('uploads/recipes') . '/' . date('Y') . '-' . $filename;
								}
							} else {

								$recipes['recipe_media'] = '';
							}

						}
						// Update recipes table
						$this->db->where('id', $checkAll->id);
						$recipes['updated_at'] = date('Y-m-d H:i:s');
						//print_r($ingcsvdata);
						$this->db->update('recipes', $recipes);

						//delete all step which belongs to current recipe
						//delete all ingredients which belongs to recipes
						$deleted = $this->row_deletesteps($checkAll->id);
						$ingdeleted = $this->Ingredients_model->row_deleteRecipeIngredients($checkAll->id);

						if (count($methods) >= 1) {

							foreach ($methods as $key => $value) {

								$stepsdata = explode("|", $value);
								if (!empty($stepsdata[4])) {
									$filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($stepsdata[4])));
									$type = explode(".", $filename);
									if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
										$img_destitation_path = getcwd() . "/uploads/recipesteps/" . date('Y') . '-' . $filename;
										if (!copy(trim($stepsdata[4]), $img_destitation_path)) {
											$stepsdata['img_video'] = '';
										} else {
											$stepsdata['img_video'] = 'uploads/recipesteps/' . date('Y') . '-' . $filename;
										}
									} else {

										$stepsdata['img_video'] = '';
									}
								}

								if (count($stepsdata) > 1) {
									$steps = array(
										'recipe_id' => $checkAll->id,
										'step_no' => trim($stepsdata[0]),
										'step_description' => trim($stepsdata[1]),
										'step_description_ar' => trim($stepsdata[2]),
										'step_description_fr' => trim($stepsdata[3]),
										'image' => $stepsdata['img_video'],
									);
									//print_r($steps);
									$this->db->insert('recipe_steps', $steps);
								}
							}

						}

						if (count($ingredients) >= 1 && !empty($ingredients[0])) {
							$masterArray = array();
							foreach ($ingredients as $key => $value) {
								$ingdata = explode("|", $value);
								//print_r($key);
								if (count($ingdata) > 1) {
									$checking = $this->Ingredients_model->checkIngredient(trim($ingdata[0]));
									if (count($checking) == 0) {
										$this->db->insert('ingredients', array('name' => $ingdata[0]));
										$ing_id = $this->db->insert_id(); // Get last id
										$checking->id = $ing_id;
									}
									$checking->qty = $ingdata[1];

									$masterArray[$key]['recipe_id'] = $checkAll->id;
									$masterArray[$key]['ingredient_id'] = $checking->id;
									$masterArray[$key]['qty'] = $checking->qty;

									if (count($checking) > 0) {
										$checkunit = $this->Units_model->checkUnit(trim($ingdata[2]));

										if (count($checkunit) == 0) {
											$this->db->insert('units', array('unit_name' => $ingdata[2]));
											$unit_id = $this->db->insert_id(); // Get last id
											//echo $unit_id;
											$checkunit->id = $unit_id;
											//$checkunit->unit_gram = 0;
										}
										$masterArray[$key]['finalunit_id'] = $checkunit->id;
										$kcal[$key] = $this->calculateCalory('kcal', $checking->qty, $checking->kcal_grams, $checkunit->unit_gram);
										$protein[$key] = $this->calculateCalory('protein', $checking->qty, $checking->protein, $checkunit->unit_gram);
										$fat[$key] = $this->calculateCalory('fat', $checking->qty, $checking->fat, $checkunit->unit_gram);
										$carbs[$key] = $this->calculateCalory('carbs', $checking->qty, $checking->carbs, $checkunit->unit_gram);
										$masterArray[$key]['measure_amount'] = $kcal[$key]['total_calory'];
										$masterArray[$key]['protein_amount'] = $protein[$key]['total_protein'];
										$masterArray[$key]['fat_amount'] = $fat[$key]['total_fat'];
										$masterArray[$key]['carbs_amount'] = $carbs[$key]['total_carbs'];

									}
								}
							}
							$this->db->insert_batch('master_recipeingredients', $masterArray);
							/************************* Total calory stored **********/
							$totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$checkAll->id}")->row()->maxid;

							$caldata = array('total_calory' => $totalcalory);

							$this->db->where('id', $checkAll->id);
							$this->db->update('recipes', $caldata);
							/******************************** END *********************/
						}
					}
				}
				$this->flash_notification('File import successfully.');
			} else {
				$this->session->set_flashdata("error", "* File is empty.");
			}
		}
		redirect(base_url() . 'admin/recipes');
	}

	// Remove Recipes Ingredients
	function removeRecipeIngredient($materID) {
		$deleted = $this->Recipes_model->deleteRecipesIngredient($materID);

		if ($deleted == 1) {
			echo json_encode(array('msg' => 'success'));
		} else {
			echo array('msg' => 'error');
		}
	}

	// Remove Recipes Step No
	function removeRecipeStep($stepID) {
		//echo $stepID;exit;
		$deleted = $this->db->delete('recipe_steps', array('id' => $stepID));
		if ($deleted) {
			echo json_encode(array('msg' => 'success'));
		} else {
			echo array('msg' => 'error');
		}
	}

	// Update Recipes List
	function updateRecipeList($stepID) {

		/********************* Preparation Images ****************/
		$config = array(
			'upload_path' => FCPATH . 'uploads/recipesteps',
			'allowed_types' => 'jpg|gif|png|jpeg|mp4|avi',
			'overwrite' => 1,
		);

		//print_r($_POST);
		//print_r($_FILES);
		//exit;

		if ($_POST['fileold'] == 'undefined') {
			$fileName = date('dmYhis') . '-' . $_FILES['file']['name'];
			//echo $fileName;exit;
			$config['file_name'] = $fileName;
			$imgname = 'uploads/recipesteps/' . $fileName;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ($this->upload->do_upload('file')) {
				echo $this->upload->display_errors();
				$this->upload->data();
				unlink(FCPATH . 'uploads/recipesteps/' . $_POST['fileexists']);
			} else {
				echo $this->upload->display_errors();
				return false;
			}} else {
			$imgname = $_POST['fileold'];
		}
		//echo '<pre>';
		/**************************** END *****************/
		$updateList = array(
			'step_description' => trim($_POST['description']),
			'step_description_ar' => trim($_POST['description_ar']),
			'step_description_fr' => trim($_POST['description_fr']),
			'image' => $imgname,
			'updated_at' => date('Y-m-d H:i:s'),
		);

		//print_r($updateList);exit;

		$this->db->where('id', $stepID);
		if ($this->db->update('recipe_steps', $updateList)) {
			echo json_encode(array('msg' => 'success'));
		} else {
			echo array('msg' => 'error');
		}

	}

	/************************* GET UNITS **********************/
	function getUnits() {
		$result = $this->db->select('id,metric_name')
			->from('weights')
			->order_by('id', 'asc')
			->group_by('metric_name')
			->get()->result();
		return $result;
	}

	function getUnitslist($inGID) {
		$result = $this->db->select('un.id,un.unit_name,un.unit_name_ar,un.unit_name_fr,ig.kcal_grams,ig.protein,ig.fat,ig.carbs')
			->from('units as un')
			->join('ingredient_units as iu', 'iu.unit_id = un.id')
			->join('ingredients as ig', 'ig.id = iu.ingredient_id', 'LEFT')
			->where('iu.ingredient_id', $inGID)
			->where('un.status', 1)
		//->order_by('un.id','asc')
		//->group_by('un.unit_name')
			->get()->result();

		echo json_encode($result);
	}

	/****************************** END **************************/

	/**
	 * Gets the step content.
	 *
	 * @param      <integer>  $stepID  The step id
	 */
	function getStepContent($stepID) {
		//echo $stepID;exit;
		$result = $this->db->select('step_description,step_description_ar,step_description_fr')
			->from('recipe_steps')
			->where('id', $stepID)
			->get()->result();
		//echo $this->db->last_query();exit;
		echo json_encode($result);
	}
	/***************************** END *************************/

	/**
	 * Steps remove of recipes
	 *
	 * @param      integer  $id     The identifier
	 */
	function row_deletesteps($id) {
		$this->db->where('recipe_id', $id);
		$this->db->delete('recipe_steps');
	}

	/**
	 * Calculates the calory.
	 *
	 * @param      string  $qty       The qty
	 * @param      string  $ingdata   The ingdata
	 * @param      string  $unitdata  The unitdata
	 */
	public function calculateCalory($name = '', $qty = '', $ingdata = '', $unitdata = '') {

		$nutrientGrams = $qty * $unitdata;

		if ($name == 'kcal') {
			$nutrientPerServing = ($ingdata * $nutrientGrams) / 100.0;
		} else if ($name == 'fat') {
			$nutrientPerServingfat = ($ingdata * $nutrientGrams) / 100.0;
		} else if ($name == 'carbs') {
			$nutrientPerServingcarbs = ($ingdata * $nutrientGrams) / 100.0;
		} else if ($name == 'protein') {
			$nutrientPerServingprotine = ($ingdata * $nutrientGrams) / 100.0;
		}

		$calories = array(
			'total_calory' => $nutrientPerServing,
			'total_protein' => $nutrientPerServingprotine,
			'total_fat' => $nutrientPerServingfat,
			'total_carbs' => $nutrientPerServingcarbs,
		);

		return $calories;
	}

	function change_dateformat() {
		error_reporting(0);
		$date = $_POST['getdate'];
		$days = $_POST['days'];
		$days = $days . " days";
		$date = date_create($_POST['getdate']);
		date_add($date, date_interval_create_from_date_string($days));
		$date_final = date_format($date, "Y-m-d");
		echo date_formats($date_final);

	}
}