<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Zatuna</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
<div>
   <div style="font-size: 26px;font-weight: 700;letter-spacing: -0.02em;line-height: 20px;color: #41637e;font-family: sans-serif;text-align: center" align="center" id="emb-email-header"></div>

<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 20px;Margin-bottom: 15px">Hello,</p> 

<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 20px;Margin-bottom: 15px">This email has been sent as a request to reset our password</p>

<p style="Margin-top: 0;color: #565656;font-family: Georgia,serif;font-size: 16px;line-height: 20px;Margin-bottom: 0px;">
<a href="<?php echo base_url()."resetpassword/resetCode/$password"?>">Click here </a>if you want to reset your password, if not, then ignore.</p>

</div>
</body>
</html>