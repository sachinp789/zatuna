<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_roles_model extends MY_Model {

    protected $primary_key = 'id';

     // List of roles with permission
    function getRolesPermission($id=NULL){
    	//echo $id;exit;
    	if(!empty($id)){
    	$this->db->select('ur.id,ur.permission_id,ur.role_name, perm.permission_type');
    	$this->db->from('user_roles as ur');
    	$this->db->join('permissions AS perm', 'ur.permission_id = perm.id', 'LEFT');
    	$this->db->where_in('ur.id',explode('|',$id));
    	//$this->db->group_by('ur.role_name');
    	$query = $this->db->get();
    	//echo $this->db->last_query();exit;		
    	}
    	else{

    	$this->db->select('ur.id,ur.permission_id,ur.role_name, perm.permission_type');
    	$this->db->from('user_roles as ur');
    	$this->db->join('permissions AS perm', 'ur.permission_id = perm.id', 'LEFT');
    	$this->db->group_by('ur.role_name');
    	$query = $this->db->get();
    	
    	}
    	return $query->result();
    }

    function getUniqueRole($rolename){
    	$this->db->select('ur.id,ur.permission_id,ur.role_name, perm.permission_type');
    	$this->db->from('user_roles as ur');
    	$this->db->join('permissions AS perm', 'ur.permission_id = perm.id', 'LEFT');
    	$this->db->where('ur.role_name', $rolename);
    	//$this->db->group_by('perm.id');
    	$query=$this->db->get(); 
    	return $query->result();  
    }

    // User role delete
    function deleteRole($data){
    	if (!empty($data)) {
        $this->db->where_in('id', $data);
        $this->db->delete('user_roles');
    	}
    }

    // Check permission exists or not
    function checkPermission($data,$roles){
    	//print_r($roles);exit;
		$text = '';
		$this->db->select('permission_id');
		$this->db->where_in('permission_id',$roles);
		$this->db->where_in('id',$data);
		//$this->db->where('role_name', $role_name);
 		$query = $this->db->get('user_roles');
 		//echo $this->db->last_query();exit;
		if($query->num_rows() > 0){
			$text = $query->result_array();
		}
		else{
			$text = '';
		}
		return $text;	
	}

	// Role permission update of users
	function updateRolePermission($data,$users){
		if (!empty($data)) {
        	$this->db->where_in('id', $users);
        	$this->db->delete('user_roles');
        	$this->db->insert('user_roles', $data);
		}
	}
   
}