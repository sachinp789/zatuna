<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recipe_subcategories_model extends MY_Model {

	protected $primary_key = 'id';

	/**
	 * Get All Subcaegories
	 */
	function getallSubCategories() {
		$this->db->select('recipe_subcategories.*,recipe_categories.cat_name,recipe_categories.cat_name_fr,recipe_categories.cat_name_fr');
		$this->db->from('recipe_subcategories');
		$this->db->join('recipe_categories', 'recipe_categories.id = recipe_subcategories.recipe_category_id');
		//$this->db->where('recipe_categories.status', 0);
		$query = $this->db->get();
		return $query->result();
	}

	// Check recipe subcategory exists
	function checkRecipeSubcategoryName($name) {
		$this->db->where('sub_cate_name', $name);
		$query = $this->db->get('recipe_subcategories');
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	// Check Recipes subcategory exists in dependecy table
	function checkDependency($id = NULL) {
		$this->db->select('rcsub.*');
		$this->db->from('recipe_subcategories AS rcsub');
		$this->db->join('recipes AS rec', 'rec.recipe_subcategory_id = rcsub.id');
		$this->db->where('rcsub.id', $id);
		$this->db->group_by('rcsub.id');
		$result = $this->db->get();
		if ($result->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	// Get subcategory name with ID
	function checkSubCategory($name) {
		$this->db->select('id');
		$this->db->from('recipe_subcategories');
		$this->db->where('sub_cate_name', $name);
		$query = $this->db->get();
		return $query->row();
	}
}