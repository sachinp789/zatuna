<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Site_configuration_model extends MY_Model {

    protected $primary_key = 'id';

    function getAllRecords()
    {
        $q = $this->db->get("site_configuration");
        if($q->num_rows() > 0)
        {
            return $q->result();
        }
        return array();
    }

    function updateRecord($id,$data){
    	$this->db->where('id',$id);
        if($this->db->update('site_configuration',$data)){
            return 1;
        }
        else{
            return 0;
        }
    }

}
