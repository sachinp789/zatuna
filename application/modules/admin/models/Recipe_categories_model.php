<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recipe_categories_model extends MY_Model {

	protected $primary_key = 'id';

	public function getCategories() {
		$query = $this->db->select('id,cat_name_ar,cat_name,cat_name_fr')
			->from('recipe_categories')
			->get()->result();
		return $query;
	}

	// Get All Categories
	function getallCategories() {
		$this->db->select('*');
		$this->db->from('recipe_categories');
		//$this->db->where('status', 0);
		$query = $this->db->get();
		return $query->result();
	}

	// Get All Active Categories
	function getallActiveCategories() {
		$this->db->select('*');
		$this->db->from('recipe_categories');
		$this->db->where('status', 0);
		$query = $this->db->get();
		return $query->result();
	}

	// Check category exists
	function checkRecipeCategoryName($name) {
		$this->db->where('cat_name', $name);
		$query = $this->db->get('recipe_categories');
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	// Get category name with ID
	function checkCategory($name) {
		$this->db->select('id');
		$this->db->from('recipe_categories');
		$this->db->where('cat_name', $name);
		$query = $this->db->get();
		return $query->row();
	}

	// Check recipes category exists in dependecy table
	function checkDependency($id = NULL) {
		$this->db->select('rc.*');
		$this->db->from('recipe_categories AS rc');
		$this->db->join('recipe_subcategories AS rcsub', 'rcsub.recipe_category_id = rc.id');
		$this->db->join('recipes AS rec', 'rec.recipe_category_id = rc.id');
		$this->db->where('rc.id', $id);
		$this->db->group_by('rc.id');
		$result = $this->db->get();
		if ($result->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}

}