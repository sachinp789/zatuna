<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Units_model extends MY_Model {

	protected $primary_key = 'id';

	// Unit list
	function getUnitList() {
		return $this->db->select('*')
			->from('units')
			->order_by('unit_name', 'asc')
			->get()->result();
	}

	// Unit list
	function getUnitListByStatus() {
		return $this->db->select('*')
			->where('status', 1)
			->from('units')
			->order_by('id', 'asc')
			->get()->result();
	}

	// Unit name check
	function checkUnitName($name) {
		$this->db->where('unit_name', $name);
		$this->db->or_where('unit_name_us', $name);
		$query = $this->db->get('units');
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	// Unit name check in US
	/*function checkUnitNameUS($name) {
		$this->db->where('unit_name_us', $name);
		$query = $this->db->get('units');
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}*/

	// Get unit id by name
	function checkUnit($name) {
		$this->db->select('id,unit_gram');
		$this->db->from('units');
		$this->db->where('unit_name', $name);
		$this->db->or_where('unit_name_ar', $name);
		$this->db->or_where('unit_name_fr', $name);
		$query = $this->db->get();
		return $query->row();
	}

	// Check Units exists in dependecy table
	function checkDependency($id = NULL) {
		$this->db->select('u.*');
		//$recipeslist = $this->db->select('*');
		$this->db->from('units AS u');
		$this->db->join('master_recipeingredients AS mr', 'mr.finalunit_id = u.id');
		$this->db->join('ingredient_units AS ingunit', 'ingunit.unit_id = u.id');
		$this->db->where('u.id', $id);
		$this->db->group_by('u.id');
		$result = $this->db->get();
		if ($result->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}
}