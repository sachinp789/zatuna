<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingredient_categories_model extends MY_Model {

	protected $primary_key = 'id';

	public function getCategories() {
		$query = $this->db->select('id,name')
			->get_where('ingredient_categories', array('status' => 0))
			->result();
		return $query;
	}

	// Get All Categories
	function getallCategories() {
		$this->db->select('*');
		$this->db->from('ingredient_categories');
		//->get_where('levels',array('status' => 0))
		$query = $this->db->get();
		return $query->result();
	}

	function checkCategory($name) {
		$query = $this->db->select('id')
			->get_where('ingredient_categories', array('name' => $name))
			->row();
		//echo $this->db->last_query();
		return $query;
	}

	// Check ingrdeints exists
	function checkIngCategory($name) {
		$this->db->where('name', $name);
		$query = $this->db->get('ingredient_categories');
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	// Check ingredients category exists in dependecy table
	function checkDependency($id = NULL) {
		$this->db->select('ic.*');
		$this->db->from('ingredient_categories AS ic');
		$this->db->join('ingredients AS ing', 'ing.ingredient_category_id = ic.id');
		$this->db->where('ic.id', $id);
		$this->db->group_by('ic.id');
		$result = $this->db->get();
		if ($result->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}
}