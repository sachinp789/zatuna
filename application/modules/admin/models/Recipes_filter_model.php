<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recipes_filter_model extends MY_Model {
	protected $primary_key = 'id';

	var $table = 'recipes,master_recipeingredients,ingredients,users,recipe_categories,admin';
	var $column_order = array('recipes.id', 'recipes.recipe_name', 'recipe_media', 'recipes.preparation_time', 'recipes.cooking_time', 'recipe_categories.cat_name', 'ingredients.name', 'recipes.status', 'admin.first_name', 'admin.last_name', 'recipes.created_at');
	var $column_search = array('recipes.id', 'recipes.recipe_name', 'recipes.preparation_time', 'recipes.cooking_time', 'recipe_categories.cat_name', 'ingredients.name', 'admin.first_name', 'admin.last_name', 'recipes.created_at', 'recipes.updated_at', 'recipe_categories.cat_name', 'recipes.status'); //set column field database for datatable searchable
	var $order = array('recipes.id' => 'asc');

	private function _get_datatables_query() {
		$this->db->select('recipes.id,recipes.recipe_name,recipes.recipe_media,recipes.preparation_time,recipes.cooking_time,recipe_categories.cat_name,recipes.status,ingredients.name,admin.first_name,admin.last_name,recipes.created_at,recipes.updated_at');
		$this->db->from('master_recipeingredients');
		$this->db->join('recipes', 'recipes.id=master_recipeingredients.recipe_id');
		$this->db->join('ingredients', 'master_recipeingredients.ingredient_id=ingredients.id');
		$this->db->join('admin', 'admin.admin_id=recipes.admin_by', 'left');
		$this->db->join('recipe_categories', 'recipe_categories.id=recipes.recipe_category_id', 'left');
		$this->db->group_by('recipes.id');

		if ($this->input->post('fromId') && $this->input->post('fromTo')) {
			$this->db->where('recipes.id BETWEEN "' . trim($this->input->post('fromId')) . '" and "' . trim($this->input->post('fromTo')) . '"');
		}

		if ($this->input->post('FirstName')) {
			$this->db->like('first_name', trim($this->input->post('FirstName')));
		}

		if ($this->input->post('RecipeName')) {
			$this->db->like('recipe_name', trim($this->input->post('RecipeName')));
		}

		if ($this->input->post('PreparationTime')) {
			$this->db->where('preparation_time', trim($this->input->post('PreparationTime')));
		}

		if ($this->input->post('CookingTime')) {
			$this->db->where('cooking_time', trim($this->input->post('CookingTime')));
		}

		if ($this->input->post('IngridentName')) {
			$this->db->where('name', trim($this->input->post('IngridentName')));
		}

		if ($this->input->post('LastName')) {
			$this->db->like('last_name', trim($this->input->post('LastName')));
		}

		/* if($this->input->post('Created_at'))
			        {
			            $this->db->where('recipes.created_at >=', date('Y-m-d 00:00:00',strtotime(trim($this->input->post('Created_at')))));
		*/

		if ($this->input->post('RecipeCat')) {
			$this->db->where('cat_name', trim($this->input->post('RecipeCat')));
		}

		$i = 0;

		foreach ($this->column_search as $item) // loop column
		{
			if ($_POST['search']['value']) // if datatable send POST for search
			{

				if ($i === 0) // first loop
				{
					$this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
					$this->db->like($item, $_POST['search']['value']);
				} else {
					$this->db->or_like($item, $_POST['search']['value']);
				}

				if (count($this->column_search) - 1 == $i) //last loop
				{
					$this->db->group_end();
				}
				//close bracket
			}
			$i++;
		}

		if (isset($_POST['order'])) // here order processing
		{
			$this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
		} else if (isset($this->order)) {
			$order = $this->order;
			$this->db->order_by(key($order), $order[key($order)]);
		}
	}

	public function get_datatables() {
		$this->_get_datatables_query();
		if ($_POST['length'] != 1) {
			$this->db->limit($_POST['length'], $_POST['start']);
		}

		$query = $this->db->get();
		return $query->result();
	}

	public function count_filtered() {
		$this->_get_datatables_query();
		$query = $this->db->get();
		return $query->num_rows();
	}

	public function count_all() {
		$this->db->from('recipes');
		return $this->db->count_all_results();
	}

	public function get_ingredients($recipe_id) {
		$this->db->select('ingredients.name,master_recipeingredients.recipe_id');
		$this->db->from('ingredients');
		$this->db->join('master_recipeingredients', 'master_recipeingredients.ingredient_id= ingredients.id');
		$this->db->where('master_recipeingredients.recipe_id', $recipe_id);
		$query_final = $this->db->get();
		return $query_final->result();
	}
}
