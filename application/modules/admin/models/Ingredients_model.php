<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ingredients_model extends MY_Model {

	protected $primary_key = 'id';

	// Get All Ingredients
	function getIngredients() {
		$query = $this->db->select('ing.id,ing.name,ing.name_ar,ing.name_fr,ing.amount,ing.kcal_grams,ing.protein,ing.fat,carbs')
			->join('ingredient_categories AS ic', 'ing.ingredient_category_id = ic.id', 'JOIN')
			->order_by("ing.name", "asc")
			->get_where('ingredients as ing', array('ing.status' => 0, 'ic.status' => 0))
		//->order_by('name', 'asc')
			->result();
		return $query;
	}

	// Get ingredients categories
	function getIngredientscats() {
		$query = $this->db->select('id,name,name_ar,name_fr')
			->order_by("name", "asc")
			->get_where('ingredient_categories', array('status' => 0))
		//->group_by('name', 'asc')
			->result();
		return $query;
	}

	// Get All Rest Ingredients
	function getRestIngredients($ingids) {
		//echo $ingids;
		$query = $this->db->select('ing.id,ing.name,ing.name_ar,ing.name_fr,ing.amount,ing.kcal_grams,ing.protein,ing.fat,ing.carbs');
		$this->db->from('ingredients as ing');
		$this->db->join('ingredient_categories AS ic', 'ing.ingredient_category_id = ic.id', 'JOIN');
		$this->db->group_start();
		$this->db->where('ing.status', 0);
		$this->db->where('ic.status', 0);
		$this->db->where_not_in('ing.id', $ingids);
		$this->db->group_end();
		$this->db->order_by("ing.name", "asc");
		$result = $this->db->get()->result();
		//echo $this->db->last_query();
		return $result;
	}

	// Get ingredients with category
	function getIngredientslist() {
		$query = $this->db->select('ing.*,ic.name as cat_name');
		//$recipeslist = $this->db->select('*');
		$this->db->from('ingredients AS ing');
		$this->db->join('ingredient_categories AS ic', 'ing.ingredient_category_id = ic.id', 'JOIN');
		//$this->db->where('ing.status',0);

		$result = $this->db->get()->result();
		return $result;
	}

	// Get Ingredients Unit By ID
	function getIngredientUnitbyId($ingID) {
		return $this->db->select('unit.id,unit.unit_name,unit.unit_name_ar,unit.unit_name_fr,unit.unit_name_us,unit.unit_name_us_ar,unit.unit_name_us_fr,iunit.id as ingunit_id')
			->from('ingredient_units as iunit')
			->join('units as unit', 'unit.id = iunit.unit_id')
			->where('iunit.ingredient_id', $ingID)
			->where('unit.status', 1)
			->get()->result();
	}

	// Get Ingredients Unit By ID
	function checkIngredientUnitbyId($ingID, $unitID) {
		$this->db->where('ingredient_id', $ingID);
		$this->db->where('unit_id', $unitID);
		$query = $this->db->get('ingredient_units');
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	// Check ingrdeints exists
	function checkIngredientName($name) {
		$this->db->where('name', $name);
		$query = $this->db->get('ingredients');
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	// Get ingredient name with ID
	function checkIngredient($name) {
		$this->db->select('id,protein,fat,carbs,kcal_grams');
		$this->db->from('ingredients');
		$this->db->where('name', $name);
		$query = $this->db->get();
		return $query->row();
	}

	// Check ingredients exists in dependecy table
	function checkDependency($id = NULL) {
		$this->db->select('ing.*');
		//$recipeslist = $this->db->select('*');
		$this->db->from('ingredients AS ing');
		$this->db->join('master_recipeingredients AS mr', 'mr.ingredient_id = ing.id');
		$this->db->join('ingredient_units AS ingunit', 'ingunit.ingredient_id = ing.id');
		$this->db->where('ing.id', $id);
		$this->db->group_by('ing.id');
		$result = $this->db->get();
		if ($result->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	// Remove ingredints belogs to recipes ID
	function row_deleteRecipeIngredients($id) {
		$this->db->where('recipe_id', $id);
		$this->db->delete('master_recipeingredients');
	}
}
