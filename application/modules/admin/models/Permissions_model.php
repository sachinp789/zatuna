<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Permissions_model extends MY_Model {

    protected $primary_key = 'id';

    // List of permission
    function getPermission(){
    	$this->db->select('*');
    	$this->db->from('permissions');
    	$query = $this->db->get();
    	return $query->result();
    }
   
}