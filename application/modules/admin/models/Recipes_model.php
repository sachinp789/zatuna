<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recipes_model extends MY_Model {

	protected $primary_key = 'id';

	function insert_batch($data) {
		$this->db->insert_batch('master_recipeingredients', $data);
	}

	// Get All Recipes Categories
	function getRecipescat() {
		$query = $this->db->select('id,cat_name,cat_name_ar,cat_name_fr')
			->get_where('recipe_categories', array('status' => 0))
			->result();
		return $query;
	}

	// Get recipes ingredints on recipe _id
	function getRecipeingredients($recipe_id = '') {
		$ingredientsdata = $this->db->select('mr.id,mr.qty,mr.unit,mr.ingredient_id,mr.measureunit_id,mr.finalunit_id,ing.name,ing.name_fr,ing.name_ar,unit.unit_name,unit.unit_name_fr,unit.unit_name_ar,ing.kcal_grams,ing.protein,ing.fat,ing.carbs,mr.measure_amount,mr.protein_amount,mr.fat_amount,mr.carbs_amount');
		//$recipeslist = $this->db->select('*');
		$this->db->from('master_recipeingredients AS mr');
		$this->db->join('ingredients AS ing', 'mr.ingredient_id = ing.id', 'LEFT');
		//$this->db->join('weights AS wt', 'wt.id = mr.measureunit_id', 'LEFT');
		$this->db->join('units AS unit', 'unit.id = mr.finalunit_id', 'LEFT');
		$this->db->where('mr.status', 0);
		$this->db->where('mr.recipe_id', $recipe_id);

		$result = $this->db->get()->result();
		return $result;
	}

	// Get rest infredients of recipes
	function getSelectedingredients($recipe_id) {
		$ingredientsdata = $this->db->select('mr.ingredient_id');
		//$recipeslist = $this->db->select('*');
		$this->db->from('master_recipeingredients AS mr');
		$this->db->join('ingredients AS ing', 'mr.ingredient_id = ing.id');
		$this->db->where('ing.status', 0);
		$this->db->where('mr.recipe_id', $recipe_id);

		$result = $this->db->get()->result();
		return $result;
	}

	// Sub category get
	function getRecipesSubcat($cat_id = '') {
		$this->db->select('id,sub_cate_name');
		$this->db->from('recipe_subcategories');
		$this->db->join('ingredients AS ing', 'mr.ingredient_id = ing.id', 'LEFT');
		$this->db->where('recipe_category_id', $cat_id);
		$query = $this->db->get()->result();
		return $query;
	}

	// All recipies listing of admin added
	function getRecipies() {
		$this->db->select('*');
		$this->db->from('recipes');
		//$this->db->where('admin_by',$this->session->userdata('admin_id'));
		$query = $this->db->get()->result();
		return $query;
	}

	// All recipies listing of user added
	function getUserRecipies() {
		$this->db->select('recp.*,CONCAT(user.firstname," ",user.lastname) AS name');
		$this->db->from('recipes as recp');
		$this->db->join('users AS user', 'recp.user_id = user.id', 'LEFT');
		$this->db->where('recp.user_id <>', 'NULL');
		// $this->db->order_by('recp.id', 'asc');
		$query = $this->db->get()->result();
		return $query;
	}

	// Recipes approve or reject of user
	function approveRejectRecipe($data, $recipe_id, $user_id) {
		$this->db->where('id', $recipe_id);
		$this->db->where('user_id', $user_id);
		if ($this->db->update('recipes', $data)) {
			return 1;
		} else {
			return 0;
		}
	}

	/* // Get master id from recipes ID
		    function getMasterIDRecipes($recipe_id){
		        $this->db->select('master.id as master_id');
		        $this->db->from('master_recipeingredients as master');
		        $this->db->where('master.recipe_id',$recipe_id);
		       // $this->db->order_by('recp.id', 'asc');
		        $query = $this->db->get()->result();
		        return $query;
	*/

	// Get master id from recipes ID
	function exitsRecipesIngredient($recipe_id, $ingredint_id) {
		//$this->db->where('id',$master_id);
		//$this->db->select('id');
		$this->db->where('recipe_id', $recipe_id);
		$this->db->where('ingredient_id', $ingredint_id);
		$query = $this->db->get('master_recipeingredients');
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0) {
			return 1;
		} else {
			return 0;
		}
	}

	function deleteRecipesIngredient($id) {
		$this->db->where('id', $id);
		if ($this->db->delete('master_recipeingredients')) {
			return 1;
		} else {
			return 0;
		}
	}

	function getID($recipe_id, $ingredint_id) {
		$this->db->select('id,qty,unit');
		$this->db->where('recipe_id', $recipe_id);
		$this->db->where('ingredient_id', $ingredint_id);
		$this->db->from('master_recipeingredients');
		//return $this->db->get()->row()->id;
		$query = $this->db->get()->result();
		return $query;
	}

	function find($id = '') {
		return $this->db->select('recipe_media')
			->from('recipes')
			->where('id', $id)
			->get()->row();
	}
}
