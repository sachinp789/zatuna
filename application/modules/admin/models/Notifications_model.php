<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications_model extends MY_Model {

	protected $primary_key = 'id';

	/** Get all notifications **/
	function getNotifications() {
		$this->db->select("*");
		$this->db->from('notifications');
		$query = $this->db->get();
		return $query->result();
	}
	/**
	 * Gets the active notifications.
	 */
	function getActiveNotifications() {
		$this->db->select("id,notify_name");
		$this->db->from('notifications');
		$this->db->where('status', '0');
		$query = $this->db->get();
		return $query->result();
	}

}