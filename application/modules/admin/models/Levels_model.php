<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Levels_model extends MY_Model {

    protected $primary_key = 'id';
    
    // Get All Levels	
    function getLevels(){
    	$query = $this->db->select('id,level_type')
    			->get_where('levels',array('status' => 0))
    			->result();
  		return $query;
    }

    // Get All Levels	
    function getallLevels(){
    	$this->db->select('*');
    	$this->db->from('levels');
    			//->get_where('levels',array('status' => 0))
    	$query = $this->db->get();
  		return $query->result();
    }
}
