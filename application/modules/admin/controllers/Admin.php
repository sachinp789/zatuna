<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	function __construct() {
		//echo base_url();
		parent::__construct();
		lang_switcher($this->session->userdata('siteLang'));
		//var_dump($this->session->userdata('siteLang'));
		//$this->load->library('session');
		$this->load->model('Admin_model');
		$this->load->model('Site_configuration_model');
		$this->load->library('email');
		$this->load->library('session');
		//$this->session->set_userdata('name', 'mayurghsiy');
		//$this->load->library('session');
	}

	function index() {
		//var_dump($this->session->all_userdata());
		if ($this->session->userdata('admin_login') != 1) {
			redirect(base_url() . 'admin/login');
		}
		$this->dashboard();
	}

	function login() {
		if ($this->session->userdata('admin_login') == 1) {
			redirect(base_url('admin/dashboard'));
		}
		if ($_POST) {
			$email = trim($this->input->post('email'));
			$password = trim($this->input->post('password'));

			//echo $email.'-'.$password;exit;
			$this->process_login($email, $password);
		}
		$this->data['title'] = 'User Login';
		$this->load->view('admin/user/login', $this->data);
	}

	function forgot_password() {
		if ($_POST) {
			$email = trim($this->input->post('email'));
			$get_row = $this->db->get_where('admin', array('email' => $email))->row();

			if (count($get_row) > 0) {
				$random_string = $this->random_string_generate();

				if ($this->db->update('admin', array('reset_key' => $random_string))) {

					$emailsettings = site_configuration(); // SMTP Settings
					$configuration = array(
						'protocol' => $emailsettings[0]->smtp_protocol,
						'smtp_host' => $emailsettings[0]->smtp_host,
						'smtp_port' => $emailsettings[0]->smtp_port,
						'smtp_user' => $emailsettings[0]->smtp_username,
						'smtp_pass' => $emailsettings[0]->smtp_password,
						'wordwrap' => TRUE,
						'mailtype' => 'html',
					);

					$this->load->library('email');
					$this->email->set_newline("\r\n");
					$this->email->initialize($configuration);
					$this->email->from('tejas.patel@searchnative.in', 'Zatuna');
					$this->email->to($email);
					$this->email->subject('Reset your zatuna password');
					$message = "<p>This email has been sent as a request to reset our password</p>";
					$message .= "<p><a href='" . base_url() . "admin/reset_password/$random_string'>Click here </a>if you want to reset your password,
                        if not, then ignore</p>";

					$this->email->message($message);
					if ($this->email->send()) {
						$this->session->set_flashdata('success', 'Please check email to reset your password.');
					} else {
						$this->session->set_flashdata('error', $this->email->print_debugger());
						//show_error($this->email->print_debugger());
					}

					redirect(base_url('admin/forgot_password'));
				}
			} else {
				$this->session->set_flashdata('error', 'Email address does not exist.');
				redirect(base_url('admin/forgot_password'));
			}
		}
		// die("DF");
		$this->data['title'] = 'Forgot Password';
		$this->load->view('admin/user/forgot_password', $this->data);
	}

	function profile($id) {
		if ($this->session->userdata('admin_login') != 1) {
			redirect(base_url() . 'admin/login');
		}
		if ($_POST) {
			$admin_id = $this->Admin_model->update($id, array(
				'first_name' => trim($_POST['first_name']),
				'last_name' => trim($_POST['last_name']),
				'email' => trim($_POST['email']),
				'gender' => $_POST['gender'],
				'mobile' => trim($_POST['mobile']),
				'city' => trim($_POST['city']),
				'address' => trim($_POST['address']),
			));
			if ($_FILES['photo']['name'] != '') {
				unlink('uploads/admin_image/' . $id . '.jpg');
				move_uploaded_file($_FILES['photo']['tmp_name'], 'uploads/admin_image/' . $id . '.jpg');
				$this->Admin_model->update($id, array('photo' => $id . '.jpg'));
			}
			$this->flash_notification('Profile is successfully updated.');
			redirect(base_url('admin/dashboard'));
		} else {
			// $user = $this->Admin_model->get($param2);
			$this->data['title'] = $this->lang->line('dash_edit_profile');
			$this->data['page'] = 'profile';
			$this->data['user'] = $this->Admin_model->get($id);
			$this->__template('admin/profile', $this->data);
			// $this->load->view('admin/profile',$this->data);
		}
		//redirect(base_url('admin/dashboard'));
	}

	function process_login($email = '', $password = '') {

		$get_row = $this->db->get_where('admin', array('email' => $email, 'password' => md5($password)));

		//print_r($get_row);exit;

		if ($get_row->num_rows() > 0) {

			$sitelist = $this->Site_configuration_model->getAllRecords();

			$lang = $sitelist[0]->language;
			$this->session->set_userdata('siteLang', $lang); // Site language Default

			$row = $get_row->row();
			$session_data = [
				'admin_login' => 1,
				'admin_id' => $row->admin_id,
				'admin_firstname' => $row->first_name,
				'admin_lastname' => $row->last_name,
			];	
			$this->session->set_userdata($session_data);
			//$this->session->set_userdata("admin_login", "1");
			//$this->session->set_userdata("admin_id", $row->admin_id);
			//$this->session->set_userdata("admin_firstname", $row->first_name);
			//$this->session->set_userdata("admin_lastname", $row->last_name);
			//echo $this->session->userdata('admin_login');exit;
			
			redirect(base_url('admin/dashboard'));
		} else {
			$this->session->set_flashdata('error', 'Invalid email or password');
			redirect(base_url('admin/login'));
		}
	}

	function __hash($str) {
		return hash('md5', $str . config_item('encryption_key'));
	}

	function dashboard() {
		
		if ($this->session->userdata('admin_login') != 1) {
			redirect(base_url() . 'admin/login');
		}

		$sitelist = $this->Site_configuration_model->getAllRecords();

		$this->data['title'] = $this->lang->line('dashboard_title');
		$this->data['page'] = 'dashboard';
		//$this->data['sitelist'] = $sitelist[0];
		$this->data['todolist'] = array();
		$this->__template('admin/user/dashboard', $this->data);
	}

	function logout() {
		$this->session->sess_destroy();
		redirect(base_url('admin/login'));
	}

	function change_password() {
		if ($this->session->userdata('admin_login') != 1) {
			redirect(base_url() . 'admin/login');
		}
		if ($_POST) {
			//die("f");
			$admin_id = $this->Admin_model->update($this->session->userdata('admin_id'), array(
				'password' => md5($_POST['cpassword']))
			);

			$this->flash_notification('Password is successfully change.');
			redirect(base_url('admin/dashboard'));
		} else {
			$this->data['title'] = $this->lang->line('dash_change_pwd');
			$this->data['page'] = 'changepassword';
			$this->__template('admin/changepassword', $this->data);
		}

	}

	function check_password($param = '') {

		$password = $this->input->post('password');
		$data = $this->Admin_model->get_by(array(
			'admin_id' => $this->input->post('userid'),
			'password' => md5($password),
		));
		if ($data) {
			echo "true";
		} else {
			echo "false";
		}
	}

	/*function forgot_password() {
		        $this->load->model('admin/Site_model');
		        if ($_POST) {
		           $record = $this->Site_model->is_user_email_present($_POST['email']);
		            if ($record) {
		                $user_id = $record->admin_id;
		                $random_string = $this->random_string_generate();

		                $this->update_forgot_password_key('', $user_id, md5($random_string));
		                $url = $this->forgot_password_url('', $user_id, $random_string);

		                $this->email->set_newline("\r\n");
		                $this->email->from('tejas.patel@searchnative.in', 'Top doctor');
		                $this->email->to($_POST['email']);
		                $this->email->subject('Reset your top doctor password');
		                $message = "Please find a below password";
		                $message .= "<br/>";
		                $get_record = $this->Site_model->is_user_email_present($record->email);
		                $message .= 'New Password:- '.$random_string;

		                $this->email->message($message);
		                $this->email->send();
		                $this->session->set_flashdata('success', 'Please check email to reset your password.');
		                redirect(base_url('admin/login'));
		            } else {
		                // email is not registered in the system
		                $this->session->set_flashdata('email_not_found', 'Email is not registered in the system.');
		                redirect(base_url('admin/login'));
		            }
		        } else {
		           redirect(base_url('admin/login'));
		        }
	*/

	function random_string_generate() {
		$this->load->helper('string');
		return random_string('alnum', 16);
	}

	function update_forgot_password_key($user_type, $user_id, $key) {
		if ($user_id != '' && $key != '') {
			$this->Site_model->update_forgot_password_key($user_type, $user_id, $key);
		} else {
			redirect(base_url('admin/login'));
		}
	}

	function forgot_password_url($user_type, $user_id, $random_string) {
		$this->load->library('encrypt');
		$base_url = base_url();
		$user_type = hash('md5', $user_type . config_item('encryption_key'));

		return $base_url . 'user/reset_password/' . $user_id . '/' . 'user' . '/' . $random_string;
	}

	// Reset admin password
	function reset_password($token) {

		if ($_POST) {
			if ($token == $_POST['_token']) {
				//update password and reset token and redirect
				if (trim($_POST['password']) == trim($_POST['cpassword'])) {
					$this->db->update('admin',
						[
							'password' => md5($_POST['password']),
							'reset_key' => '',
						],
						['reset_key' => $_POST['_token']]);
					$this->session->set_flashdata('success', 'Password has been successfully updated.');
					redirect(base_url('admin/login'));
				} else {
					$this->session->set_flashdata("error", "Password is mismatched.");
					redirect(base_url() . 'admin/reset_password/' . $token);
				}
			}
		}

		//check entry for token
		$user = $this->db->select()->from('admin')
			->where('reset_key', $token)->get()->result();

		if (count($user) > 0) {
			$this->data['token'] = $token;
			//$this->__templateFront('home/reset_password', $data);
			$this->data['title'] = 'Reset Password';
			$this->load->view('admin/user/reset_password', $this->data);
		} else {
			$this->session->set_flashdata("error", "Link is Expired");
			redirect(base_url() . 'admin/login');
			//   show_404();
		}
	}

	/*function reset_password($user_id = '', $user_type = '', $key = '') {
		        $this->load->model('site/Site_model');
		        if ($_POST) {
		            if ($this->compare_reset_password($_POST['password'], $_POST['confirm_password'])) {
		                $data = array(
		                    'password' => $this->__hash(trim($_POST['password']))
		                );
		                $user_data = $this->Site_model->update_password($user_type, $user_id, $data);
		                $this->Site_model->reset_forgot_password_key($user_data['type'], $user_data['type_id'], $user_data['user_id']);

		                $this->flash_notification('success', 'Password was successully reseted.');
		                redirect(base_url('user/login'));
		            } else {
		                $this->flash_notification('danger', 'Password was mismatched.');
		                redirect(base_url('user/reset_password/' . $user_id . '/' . $user_type . '/' . $key));
		            }
		        }

		        if($user_id && $user_type && $key) {
		            $is_key_present = $this->Site_model->check_for_forgot_password_key($user_type, $key);
		            if ($is_key_present) {
		                $this->data['title'] = 'Forgot Password';
		                $this->load->view('user/user/reset_password', $this->data);
		            } else {
		                show_404();
		            }
		        } else {
		            show_404();
		        }
	*/

	function check_user_type_hash($hash) {
		if (hash('md5', 'user' . config_item('encryption_key')) == $hash) {
			return 'user';
		}

	}

	function compare_reset_password($password, $confirm_password) {

		if (trim($password) == trim($confirm_password)) {
			return TRUE;
		}

		return FALSE;
	}

	/*function email_user_credential($user) {
		        $this->load->model('email/Email_model');
		        $this->load->helper('email/system_email');
		        $subject = 'Top Doctor Login Credentials';
		        $message = 'Hello, ' . $user['first_name'] . ' ' . $user['last_name'];
		        $message .= "<br/>Your email address is registered with Top Doctor. ";
		        $message .= "Now you can access your Top Doctor account by following credentials.";
		        $message .= "<br/>Url: " .base_url().'user/login';
		        $message .= "<br/>Email: " . $user['email'];
		        $message .= "<br/>Password: " . $user['password'];
		        Modules::run('email/email/setemail', $user['email'], $subject, $message, $attachment=array());
		        return;
	*/

	function password_compare_and_update($user_id, $password) {
		$user = $this->User_model->get($user_id);

		if ($user->password !== $password) {
			$password = Modules::run('user/__hash', $password);
			$this->User_model->update($user_id, array(
				'password' => $password,
			));
		}
	}

	function check_user_email($param = '') {

		$email = $this->input->post('email');
		if ($param == '') {
			$data = $this->Admin_model->get_by(array(
				'email' => $email,
			));
			if ($data) {
				echo "false";
			} else {
				echo "true";
			}
		} else {
			$data = $this->Admin_model->get_by(array(
				'admin_id !=' => $this->input->post('userid'),
				'email' => $email,
			));

			if ($data) {
				echo "false";
			} else {
				echo "true";
			}
		}
	}

	function configuration() {
		if ($this->session->userdata('admin_login') != 1) {
			redirect(base_url() . 'admin/login');
		}
		if ($_POST) {

			if (!empty($_FILES['site_logo']['name'])) {

				if (!is_dir(FCPATH . 'uploads/sitelogo/')) {
					$path = FCPATH . 'uploads/sitelogo/';
					mkdir($path, 0777);
				}
				$config['upload_path'] = FCPATH . 'uploads/sitelogo/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = $_FILES['site_logo']['name'];

				//Load upload library and initialize configuration
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('site_logo')) {
					$uploadData = $this->upload->data();
					$sitelogo = $uploadData['file_name'];
				} else {
					$sitelogo = '';
				}
			} else {
				$sitelogo = '';
			}
			//echo $sitelogo;exit;

			//Prepare array of user data
			$siteData = array(
				'title' => trim($this->input->post('site_title')),
				'email' => trim($this->input->post('site_email')),
				'language' => trim($this->input->post('site_language')),
				'phone' => trim($this->input->post('site_phone')),
				'description' => trim($this->input->post('footer_note')),
				'smtp_protocol' => trim($this->input->post('smtp_protocol')),
				'smtp_host' => trim($this->input->post('smtp_host')),
				'smtp_port' => trim($this->input->post('smtp_port')),
				'smtp_username' => trim($this->input->post('smtp_username')),
				'smtp_password' => trim($this->input->post('smtp_password')),
				'logo' => $sitelogo,
			);

			$insertSiteData = $this->Site_configuration_model->insert($siteData);

			//Storing insertion status message.
			if ($insertSiteData) {
				$lang = $this->input->post('site_language');
				$this->session->set_userdata('siteLang', $lang);
				/* $this->session->set_userdata("logo", $sitelogo);
					                $this->session->set_userdata("site_email", $this->input->post('site_email'));
					                $this->session->set_userdata("site_language", $this->input->post('site_language'));
					                $this->session->set_userdata("site_phone", $this->input->post('site_phone'));
					                $this->session->set_userdata("site_description", $this->input->post('footer_note'));
				*/
				$this->flash_notification('Settings have been saved successfully.');
				//$this->session->set_flashdata('success', 'Settings have been saved successfully.');
				redirect(base_url() . 'admin/configuration');

			} else {
				//$this->session->set_flashdata('error', 'Some problems occured, please try again.');
				redirect(base_url() . 'admin/configuration');
			}
		}

		$sitelist = $this->Site_configuration_model->getAllRecords();

		$this->data['title'] = $this->lang->line('site_index');
		$this->data['page'] = 'site_setting';
		$this->data['sites'] = $sitelist;
		$this->__template('admin/site_setting', $this->data);
	}

	function configuration_update($siteID) {

		//echo $this->input->post('smtp_password');exit;

		if ($_POST) {

			if (!empty($_FILES['site_logo']['name'])) {

				if (!is_dir(FCPATH . 'uploads/sitelogo/')) {
					$path = FCPATH . 'uploads/sitelogo/';
					mkdir($path, 0777);
				}
				$config['upload_path'] = FCPATH . 'uploads/sitelogo/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				$config['file_name'] = $_FILES['site_logo']['name'];

				//Load upload library and initialize configuration
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('site_logo')) {
					unlink(FCPATH . 'uploads/sitelogo/' . $this->input->post('prev_logo'));
					$uploadData = $this->upload->data();
					$sitelogo = $uploadData['file_name'];
				} else {
					$sitelogo = '';
				}
			} else {
				$sitelogo = $this->input->post('prev_logo');
			}
			//echo $sitelogo;exit;

			//Prepare array of user data
			$siteupdateData = array(
				'title' => trim($this->input->post('site_title')),
				'email' => trim($this->input->post('site_email')),
				'language' => trim($this->input->post('site_language')),
				'phone' => trim($this->input->post('site_phone')),
				'description' => trim($this->input->post('footer_note')),
				'smtp_protocol' => trim($this->input->post('smtp_protocol')),
				'smtp_host' => trim($this->input->post('smtp_host')),
				'smtp_port' => trim($this->input->post('smtp_port')),
				'smtp_username' => trim($this->input->post('smtp_username')),
				'smtp_password' => $this->input->post('siteemail_pwd') ? $this->input->post('siteemail_pwd') : trim($this->input->post('smtp_password')),
				'logo' => $sitelogo,
				'updated_at' => date('Y-m-d H:i:s'),
			);

			//print_r($siteupdateData);exit;

			$siteupdated = $this->Site_configuration_model->updateRecord($siteID, $siteupdateData);

			//Storing insertion status message.
			if ($siteupdated == 1) {
				$lang = $this->input->post('site_language');
				$this->session->set_userdata('siteLang', $lang);
				/*$this->session->set_userdata("logo", $sitelogo);
					                $this->session->set_userdata("site_email", $this->input->post('site_email'));
					                $this->session->set_userdata("site_language", $this->input->post('site_language'));
					                $this->session->set_userdata("site_phone", $this->input->post('site_phone'));
					                $this->session->set_userdata("site_description", $this->input->post('footer_note'));
				*/
				$this->flash_notification('Settings have been updated successfully.');
				// $this->session->set_flashdata('success', 'Settings have been updated successfully.');
				redirect(base_url() . 'admin/configuration');

			} else {
				// $this->session->set_flashdata('error', 'Some problems occured, please try again.');
				redirect(base_url() . 'admin/configuration');
			}
		}

		$sitelist = $this->Site_configuration_model->getAllRecords();

		$this->data['title'] = $this->lang->line('site_index');
		$this->data['page'] = 'site_setting';
		$this->data['sites'] = $sitelist;
		$this->__template('admin/site_setting', $this->data);
	}
}
