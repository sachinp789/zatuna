<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Userrecipe extends MY_Controller {

    /**
     * Constructor
     */
    function __construct() {
        parent::__construct();
        $this->load->model('admin/Recipes_model');
        if(!$this->session->userdata('admin_id'))
        {
            redirect(base_url().'admin/login');
        }
    }

    /**
     * Index
     */
    function index() {
        $this->data['title'] = 'User Recipes';
        $this->data['page'] = 'userrecipe';
        $this->data['recipes'] = $this->Recipes_model->getUserRecipies();
        
        $this->__template('admin/recipes/userrecipe', $this->data);
    }

    // Delete User Recipe
    function delete_recipe($recipe_id = ''){
        //print_r($recipe_id);exit;
        redirect(base_url().'admin/userrecipe/');
    }
}