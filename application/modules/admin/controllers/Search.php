<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MY_Controller {

	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		lang_switcher($this->session->userdata('siteLang'));
		$this->load->model('admin/Recipes_filter_model');
		if (!$this->session->userdata('admin_id')) {
			redirect(base_url() . 'admin/login');
		}
	}

	/**
	 * Index
	 */
	function index() {
		$this->data['title'] = $this->lang->line('search_index');
		$this->data['page'] = 'search';
		// $this->data['recipes'] = $this->Recipes_model->order_by_column('id');
		$this->__template('admin/recipes/advancerecipe', $this->data);
	}

	public function ajax_list() {
		$list = $this->Recipes_filter_model->get_datatables();
		//echo $this->db->last_query();exit;
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $recipes) {

			$ingredients_names = array();
			$recipes->ingredients_names = array();

			$ingredients = $this->Recipes_filter_model->get_ingredients($recipes->id);

			for ($k = 0; $k < count($ingredients); $k++) {
				if ($ingredients[$k]->recipe_id == $recipes->id) {
					$ingredients_names[] = $ingredients[$k]->name;
				}
			}

			$recipes->ingredients_names = implode(" | ", $ingredients_names);

			$no++;
			$row = array();
			$row[] = $recipes->id;
			$row[] = $recipes->recipe_name;
			$row[] = $recipes->preparation_time;
			$row[] = $recipes->cooking_time;
			$row[] = $recipes->cat_name;
			$row[] = $recipes->ingredients_names;
			/*$row[] = $recipes->first_name;
			$row[] = $recipes->last_name;*/
			$row[] = $recipes->created_at;
			$row[] = $recipes->updated_at;

			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Recipes_filter_model->count_all(),
			"recordsFiltered" => $this->Recipes_filter_model->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
}