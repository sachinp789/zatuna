<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recipesubcategory extends MY_Controller {

	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		lang_switcher($this->session->userdata('siteLang'));
		$this->load->model('admin/Recipe_subcategories_model');
		if (!$this->session->userdata('admin_id')) {
			redirect(base_url() . 'admin/login');
		}
	}

	/**
	 * recipe sub category
	 */

	function index() {

		$this->data['title'] = $this->lang->line('recp_subcatindex');
		$this->data['page'] = 'recipes_subcategory';
		$this->data['categories'] = $this->Recipe_subcategories_model->getallSubCategories();
		$this->__template('admin/category/recipies/subcategory_index', $this->data);
	}

	/**
	 * create recipes sub category
	 */

	function subcategorycreate() {
		if ($_POST) {
			$categories = array(
				'sub_cate_name' => trim($_POST['subcat_name']),
				'sub_cate_name_ar' => trim($_POST['subcat_name_ar']),
				'sub_cate_name_fr' => trim($_POST['subcat_name_fr']),
				'recipe_category_id' => $_POST['recipe_category_name'],
				'status' => $_POST['rcstatus'],
			);
			$this->Recipe_subcategories_model->insert($categories);
			$this->flash_notification('Recipe Subcategory is successfully added.');
			redirect(base_url() . 'admin/recipesubcategory');
		}
		$this->data['title'] = $this->lang->line('recp_subcatadd');
		$this->data['page'] = 'subcatgory_create';
		$this->__template('admin/category/recipies/subcategory_create', $this->data);
	}

	/**
	 * Edit Recipes Sub Category
	 */
	function subcategoryedit($subcatid = "") {
		if ($subcatid) {
			$this->data['title'] = $this->lang->line('recp_subcatedit');
			$this->data['page'] = 'subcategory_edit';
			//$this->data['levels'] = $this->Recipe_categories_model->get($rcat_id);
			$this->data['recpsubcatlist'] = $this->Recipe_subcategories_model->get($subcatid);
			$this->data['param2'] = '';
			$this->__template('admin/category/recipies/subcategory_edit', $this->data);
		}
		redirect(base_url() . 'admin/recipesubcategory');
	}

	/**
	 * Recipe Subcategory Update
	 */

	function subcategoryupdate($subcatid = "") {
		if ($_POST) {

			if ($_POST['rcstatus'] == 1) {

				$checkExists = $this->Recipe_subcategories_model->checkDependency($subcatid); // Check dependency on delete category
				if ($checkExists == 1) {
					$this->session->set_flashdata('error', '* Recipe Subcategory already used in another resource.');
				} else {
					$this->Recipe_subcategories_model->update($subcatid, array(
						'sub_cate_name' => trim($_POST['subcat_name']),
						'sub_cate_name_ar' => trim($_POST['subcat_name_ar']),
						'sub_cate_name_fr' => trim($_POST['subcat_name_fr']),
						'recipe_category_id' => $_POST['recipe_category_name'],
						'status' => $_POST['rcstatus'],
						'updated_at' => date('Y-m-d H:i:s'),
					));
					$this->flash_notification('Recipe Subcategory is successfully updated.');
				}
			} else {

				$this->Recipe_subcategories_model->update($subcatid, array(
					'sub_cate_name' => trim($_POST['subcat_name']),
					'sub_cate_name_ar' => trim($_POST['subcat_name_ar']),
					'sub_cate_name_fr' => trim($_POST['subcat_name_fr']),
					'recipe_category_id' => $_POST['recipe_category_name'],
					'status' => $_POST['rcstatus'],
					'updated_at' => date('Y-m-d H:i:s'),
				));
				$this->flash_notification('Recipe Subcategory is successfully updated.');
			}
		}
		redirect(base_url() . 'admin/recipesubcategory');
	}

	/**
	 * Remove Recipe Sub Category
	 */

	function delete_subcategory($subcatid = "") {
		$checkExists = $this->Recipe_subcategories_model->checkDependency($subcatid); // Check dependency on delete category
		if ($checkExists == 1) {
			$this->session->set_flashdata('error', '* Recipe Subcategory already used in another resource.');
		} else {
			if ($subcatid) {
				$this->Recipe_subcategories_model->delete($subcatid);
				$this->flash_notification('Recipe Subcategory is successfully deleted.');
			}
		}
		redirect(base_url() . 'admin/recipesubcategory');
	}

	/**
	 *  Recipe SubCategory check exists or not
	 */

	function checkSubcategory() {
		//print_r($_POST);exit;
		$isExists = $this->Recipe_subcategories_model->checkRecipeSubcategoryName(trim($_POST['subcat_name']));

		if ($isExists == 1) {
			echo "false";
		} else {
			echo "true";
		}
	}
}