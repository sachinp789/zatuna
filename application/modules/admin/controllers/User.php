<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller {

	function __construct() {
		parent::__construct();
		lang_switcher($this->session->userdata('siteLang'));
		$this->load->model('admin/Users_model');
		//$this->load->helper('language');
		$this->load->helper('text');
		date_default_timezone_set('Asia/Kolkata');
		if ($this->session->userdata('admin_login') != 1) {
			redirect(base_url() . 'admin/login');
		}
	}
	function index() {
		$this->data['title'] = $this->lang->line('user_index');
		$this->data['page'] = "register";
		$this->data['users'] = $this->Users_model->get_all();
		$this->__template('admin/user/user_list', $this->data);
	}

	function dashboard() {
		$this->data['page'] = 'dashboard';
		$this->__template('register/dashboard', $this->data);
	}

	function edit($id) {
		$user = $this->Users_model->get($id);

		if (count($user) > 0) {
			$this->data['title'] = $this->lang->line('user_edit');
			$this->data['page'] = "edit";
			$this->data['user'] = $user;
			$this->__template('admin/user/edit', $this->data);
		} else {
			redirect(base_url('admin/user'));
		}
	}

	function update($id) {
		// echo $id;exit;
		if ($_POST) {

			$user_id = $this->Users_model->update($id, array(
				'firstname' => trim($_POST['first_name']),
				//'lastname'      => $_POST['last_name'],
				'email' => trim($_POST['email']),
				'mobile_no' => trim($_POST['mobile']),
				'address' => trim($_POST['address']),
				//'status' => $_POST['status'],
				'updated_at' => date('Y-m-d H:i:s'),
				'status' => $_POST['rcstatus'],
			));
			$this->flash_notification('Users is successfully updated.');
		}
		redirect(base_url('admin/user'));
	}

	function delete($id) {
		$this->Users_model->delete($id);
		$this->flash_notification("User successfully deleted");
		redirect(base_url() . 'admin/user/', 'refresh');
	}
	function check_user_email($param = '') {
		$email = $this->input->post('email');
		if ($param == '') {
			$data = $this->Users_model->get_by(array('email' => $email));
			if ($data) {
				echo "false";
			} else {
				echo "true";
			}
		} else {
			$data = $this->Users_model->get_by(array(
				'id !=' => $this->input->post('userid'),
				'email' => $email,
			));
			if ($data) {
				echo "false";
			} else {
				echo "true";
			}
		}
	}
	// Get Lat-Long using zipcode
	/*function latlong($latitude="",$langtitude=""){
	        $zipcode=$_POST['zip'];
	        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
	        $details=file_get_contents($url);
	        $result = json_decode($details,true);
	        $lat=$result['results'][0]['geometry']['location']['lat'];
	        $lng=$result['results'][0]['geometry']['location']['lng'];
	        echo $latlong=json_encode(array('lat'=>$lat,'lng'=>$lng));
*/

	function update_status() {
		$status = $_POST['status'];
		$id = $_POST['id'];
		$student_id = $this->User_master_model->update($id, array('status' => $status));
	}

	/**
	 *  Get ajax all users
	 */
	function getusers() {
		$this->db->select('id, firstname,lastname,device_token');
		$this->db->order_by('created_at', 'DESC');
		$query = $this->db->get('users');
		echo json_encode($query->result());
	}

}
