<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ingredients extends MY_Controller {

	function __construct() {
		parent::__construct();
		lang_switcher($this->session->userdata('siteLang'));
		$this->load->model('admin/Ingredients_model');
		$this->load->model('admin/Ingredient_categories_model');
		//$this->load->model('admin/Units_model');
		$this->load->helper('language');
		$this->load->helper('text');
		date_default_timezone_set('Asia/Kolkata');
		if ($this->session->userdata('admin_login') != 1) {
			redirect(base_url() . 'admin/login');
		}
	}
	function index() {
		$this->data['title'] = $this->lang->line('ing_index');
		$this->data['page'] = "ingredients";
		$this->data['ingredients'] = $this->Ingredients_model->getIngredientslist();
		$this->__template('admin/ingredients/index', $this->data);
	}

	/**
	 * Index action
	 */
	function create() {
		if ($_POST) {

			if (!empty($_FILES['ingredients_media']['name'])) {
				$config['upload_path'] = 'uploads/ingredients/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';
				//$config['file_name'] = $_FILES['ingredients_media']['name'];

				//Load upload library and initialize configuration
				$this->load->library('upload', $config);
				$fileName = preg_replace('/[^A-Za-z0-9.]/', "", $_FILES["ingredients_media"]["name"]);
				$config['file_name'] = time() . '-' . $fileName;
				$this->upload->initialize($config);

				if ($this->upload->do_upload('ingredients_media')) {
					$uploadData = $this->upload->data();
					$picture = $config['upload_path'] . $config['file_name'];
				} else {
					$picture = '';
				}
			} else {
				$picture = '';
			}

			$ingredients_name = $_POST['ingredients_name'];
			$ingredients_name_ar = $_POST['ingredients_name_ar'];
			$ingredients_name_fr = $_POST['ingredients_name_fr'];
			//$ingredients_description = $_POST['ingredients_description'];
			// $ingredients_unit = $_POST['ingredients_unit'];
			$ingredients_cat = $_POST['ingredient_category'];
			$protein = number_format($_POST['ingredients_protein'], 2);
			$fat = number_format($_POST['ingredients_fat'], 2);
			$carbs = number_format($_POST['ingredients_carbs'], 2);
			$amount = $_POST['ingredients_amount'];

			$ingredients = array(
				"name" => trim($ingredients_name),
				"name_ar" => trim($ingredients_name_ar),
				"name_fr" => trim($ingredients_name_fr),
				'description' => trim($_POST['description']),
				'description_ar' => trim($_POST['description_ar']),
				'description_fr' => trim($_POST['description_fr']),
				"protein" => trim($protein),
				"fat" => trim($fat),
				"carbs" => trim($carbs),
				"kcal_grams" => trim($amount),
				"media" => $picture,
				//"description"               =>$ingredients_description,
				//"unit"                      =>$ingredients_unit,
				'ingredient_category_id' => $ingredients_cat,
			);

			$insert_id = $this->Ingredients_model->insert($ingredients);

			// Ingredient Unit add
			if (count($_POST['ing_unit']) > 0) {

				foreach ($_POST['ing_unit'] as $value) {

					$this->db->insert('ingredient_units', array(
						'ingredient_id' => $insert_id,
						'unit_id' => $value,
					));
				}
			}
			$this->flash_notification('Ingredients is successfully added.');
			redirect(base_url() . 'admin/ingredients/');

		}
		$this->data['title'] = $this->lang->line('ing_add');
		$this->data['page'] = 'create';
		$this->__template('admin/ingredients/create', $this->data);
	}

	// Get ingridents from id
	function edit($ingridents_id = '') {
		if ($ingridents_id) {
			$this->data['title'] = $this->lang->line('ing_edit');
			$this->data['page'] = 'ingredients';
			$this->data['ingredients'] = $this->Ingredients_model->get($ingridents_id);
			$this->data['param2'] = '';
			$this->__template('admin/ingredients/edit', $this->data);
		}
	}

	/**
	 * Update ingredients table
	 * @param string $id
	 */
	function update($id = '') {

		if (!empty($_FILES['ingredients_media']['name'])) {
			$config['upload_path'] = 'uploads/ingredients/';
			$config['allowed_types'] = 'jpg|jpeg|png|gif';
			//$config['file_name'] = $_FILES['ingredients_media']['name'];

			//Load upload library and initialize configuration
			$this->load->library('upload', $config);

			$fileName = preg_replace('/[^A-Za-z0-9.]/', "", $_FILES["ingredients_media"]["name"]);
			$config['file_name'] = time() . '-' . $fileName;

			$this->upload->initialize($config);

			if ($this->upload->do_upload('ingredients_media')) {
				$uploadData = $this->upload->data();
				$picture = $config['upload_path'] . $config['file_name'];
			} else {
				$picture = '';
			}
		} else {
			$picture = $_POST['old_image'];
		}

		if ($_POST['is_status'] == 1) {
			$checkExists = $this->Ingredients_model->checkDependency($id); // Check dependency on inactive status
			if ($checkExists == 1) {
				$this->session->set_flashdata('erroring', '* Ingredient already used in another table.');
			} else {
				$this->Ingredients_model->update($id, array(
					'name' => trim($_POST['ingredients_name']),
					"name_ar" => trim($_POST['ingredients_name_ar']),
					"name_fr" => trim($_POST['ingredients_name_fr']),
					'description' => trim($_POST['description']),
					'description_ar' => trim($_POST['description_ar']),
					'description_fr' => trim($_POST['description_fr']),
					//'unit' => $_POST['ingredients_unit'],
					"protein" => number_format($_POST['ingredients_protein'], 2),
					"fat" => number_format($_POST['ingredients_fat'], 2),
					"carbs" => number_format($_POST['ingredients_carbs'], 2),
					'kcal_grams' => trim($_POST['ingredients_amount']),
					'media' => $picture,
					'ingredient_category_id' => $_POST['ingredient_category'],
					"status" => $_POST['is_status'],
					'updated_at' => date('Y-m-d H:i:s'),
				));

				// Ingredient Unit add
				if (count($_POST['ing_unit']) > 0) {

					foreach ($_POST['ing_unit'] as $value) {

						$check = $this->Ingredients_model->checkIngredientUnitbyId($id, $value);

						if ($check == 0) {
							$this->db->insert('ingredient_units', array(
								'ingredient_id' => $id,
								'unit_id' => $value,
							));
						} else {
							continue;
						}
					}
				}

				$this->flash_notification('Ingredients is successfully updated.');
			}
		} else {
			$this->Ingredients_model->update($id, array(
				'name' => trim($_POST['ingredients_name']),
				"name_ar" => trim($_POST['ingredients_name_ar']),
				"name_fr" => trim($_POST['ingredients_name_fr']),
				'description' => trim($_POST['description']),
				'description_ar' => trim($_POST['description_ar']),
				'description_fr' => trim($_POST['description_fr']),
				//'unit' => $_POST['ingredients_unit'],
				"protein" => number_format($_POST['ingredients_protein'], 2),
				"fat" => number_format($_POST['ingredients_fat'], 2),
				"carbs" => number_format($_POST['ingredients_carbs'], 2),
				'kcal_grams' => trim($_POST['ingredients_amount']),
				'media' => $picture,
				'ingredient_category_id' => $_POST['ingredient_category'],
				"status" => $_POST['is_status'],
				'updated_at' => date('Y-m-d H:i:s'),
			));

			// Ingredient Unit add
			if (count($_POST['ing_unit']) > 0) {

				foreach ($_POST['ing_unit'] as $value) {

					$check = $this->Ingredients_model->checkIngredientUnitbyId($id, $value);

					if ($check == 0) {
						$this->db->insert('ingredient_units', array(
							'ingredient_id' => $id,
							'unit_id' => $value,
						));
					} else {
						continue;
					}
				}
			}
			$this->flash_notification('Ingredients is successfully updated.');
		}
		redirect(base_url() . 'admin/ingredients/');
	}

	/**
	 * Delete ingredients details
	 * @param string $ingredient_id
	 *
	 */
	function delete_ingredient($ingredient_id = '') {
		$checkExists = $this->Ingredients_model->checkDependency($ingredient_id); // Check dependency on delete ingredient
		if ($checkExists == 1) {
			$this->session->set_flashdata('erroring', '* Ingredient already used in another table.');
		} else {
			if ($ingredient_id) {
				$ingredient = $this->db->select('media') // Steps images
					->from('ingredients')
					->where('id', $ingredient_id)
					->get()->row();
				unlink(FCPATH . $ingredient->media); // Remove ingredient images
				$this->Ingredients_model->delete($ingredient_id);
				$this->flash_notification('Ingredients is successfully deleted.');
			}
		}
		redirect(base_url() . 'admin/ingredients/');
	}

	// Get Ingridients list
	function getIngredientsList() {
		//echo "gfgg";
		$list = $this->Ingredients_model->getIngredients();
		echo json_encode($list);
	}

	// Update ingridents data
	function updateIngredients($masterID) {

		$updateData = array(
			'ingredient_id' => $_POST['ignname'],
			'qty' => trim($_POST['qty']),
			'measure_amount' => $_POST['totalcalory'],
			'protein_amount' => $_POST['totalprotine'],
			'fat_amount' => $_POST['totalfat'],
			'carbs_amount' => $_POST['totalcarbs'],
			'measure_amount_ounce' => $_POST['totalcalory_ounce'],
			'protein_amount_ounce' => $_POST['totalprotine_ounce'],
			'fat_amount_ounce' => $_POST['totalfat_ounce'],
			'carbs_amount_ounce' => $_POST['totalcarbs_ounce'],
			/*'measureunit_id'=> $_POST['unit_measure']   */
			'finalunit_id' => $_POST['unit_measure'],
		);

		//print_r($updateData);exit;

		$this->db->where('id', $masterID);
		if ($this->db->update('master_recipeingredients', $updateData)) {
			echo "success";
		} else {
			echo "error";
		}
	}

	// Delete Ingridents Unit
	function delete_ingredientunit() {
		$unitID = $_POST['unit_id'];
		$this->db->where('id', $unitID);
		if ($this->db->delete('ingredient_units')) {
			echo $this->db->affected_rows();
		}
	}

	// Import CSV of Ingredients
	function importCSV() {

		if ($_POST) {

			$count = 0;
			$ing_csv = array();
			$ingredientsunitArray = array();
			$fp = fopen($_FILES["file"]["tmp_name"], 'r') or die("can't open file");
			//echo $_FILES['file']['size'];exit;
			if ($_FILES["file"]["size"] > 1) {
				//echo '<pre>';
				while ($csv_line = fgetcsv($fp, 1024)) {
					$count++;
					if ($count == 1) {
						continue;
					} //keep this if condition if you want to remove the first row
					for ($i = 0, $j = count($csv_line); $i < $j; $i++) {

						$ing_csv['name'] = trim($csv_line[0]);
						$ing_csv['name_ar'] = trim($csv_line[1]);
						$ing_csv['name_fr'] = trim($csv_line[2]);
						$ing_csv['protein'] = trim($csv_line[6]);
						$ing_csv['fat'] = trim($csv_line[7]);
						$ing_csv['carbs'] = trim($csv_line[8]);
						$ing_csv['kcal_grams'] = trim($csv_line[9]);
						$ing_csv['status'] = $csv_line[10];
						//$ing_csv['unit'] = $csv_line[11];
						//$ing_csv['unit_ar'] = $csv_line[12];
						//$ing_csv['unit_fr'] = $csv_line[13];

						$check = $this->Ingredient_categories_model->checkCategory($csv_line[11]);
						//$ing_csv['count'] = count($check);
						$ing_csv['category'] = $check->id;

					}

					if (count($check) == 0) {
						$this->db->insert('ingredient_categories', array('name' => trim($csv_line[11]), 'name_ar' => trim($csv_line[12]), 'name_fr' => trim($csv_line[13])));
						$insert_id = $this->db->insert_id();
						$ing_csv['category'] = $insert_id;
					}
					//print_r(count($check));
					$i++;

					$ingcsvdata = array(
						//'id' => $insert_csv['id'] ,
						'name' => $ing_csv['name'],
						'name_ar' => $ing_csv['name_ar'],
						'name_fr' => $ing_csv['name_fr'],
						'protein' => $ing_csv['protein'],
						//'count'   => $ing_csv['count'],
						'fat' => $ing_csv['fat'],
						'carbs' => $ing_csv['carbs'],
						'kcal_grams' => $ing_csv['kcal_grams'],
						'status' => $ing_csv['status'],
						'ingredient_category_id' => $ing_csv['category'],
						//'updated_at' => date('Y-m-d H:i:s')
					);

					$checkAll = $this->db->get_where('ingredients',
						array(
							'name' => $ing_csv['name'],
							//'name_ar' => $ing_csv['name_ar'],
							//'name_fr' => $ing_csv['name_ar'],
						)
					)->row();

					$unitarr = explode('|', $csv_line[3]);
					$unitarr_ar = explode('|', trim($csv_line[4]));
					$unitarr_fr = explode('|', trim($csv_line[5]));

					if (count($checkAll) == 0) {
						//die("D");
						$this->db->insert('ingredients', $ingcsvdata);
						$insert_id = $this->db->insert_id();

						/********************** UNITS ********************/

						for ($i = 0; $i < count($unitarr); $i++) {
							$checkunit = $this->db->select('id')
								->get_where('units', array('unit_name' => $unitarr[$i]))
								->row(); // Check unit exists

							if (count($checkunit) == 0) {
								if (!empty($unitarr[$i])) {

									$this->db->insert('units', array('unit_name' => $unitarr[$i], 'unit_name_ar' => $unitarr_ar[$i], 'unit_name_fr' => $unitarr_fr[$i])); // Units stored
									$unitID = $this->db->insert_id();

									/*$this->db->insert('units', array('unit_name' => $unitarr[$i])); // Units stored
									$unitID = $this->db->insert_id();*/

									$this->db->insert('ingredient_units', array('unit_id' => $unitID, 'ingredient_id' => $insert_id)); // Ingredients_units table stored
								} else {
									continue;
								}
							} else {

								$checkingunit = $this->db->select('id')
									->get_where('ingredient_units', array('unit_id' => $checkunit->id, 'ingredient_id' => $insert_id))
									->row(); // Check unit and ingredient id exists

								if (count($checkingunit) == 0) {
									if (!empty($unitarr[$i])) {
										$this->db->insert('ingredient_units', array('unit_id' => $checkunit->id, 'ingredient_id' => $insert_id)); // Ingredients_units table stored
									}
								} else {
									continue;
								}
							}

						}
						/************************* END *********************/

					} else {
						$this->db->where('id', $checkAll->id);
						$ingcsvdata['updated_at'] = date('Y-m-d H:i:s');
						//print_r($ingcsvdata);
						$this->db->update('ingredients', $ingcsvdata);
					}
					// $this->db->insert('ingredients', $ingcsvdata); // Recipes sub categories
				}

				fclose($fp) or die("can't close file");
				//$data['success']="success";
				$this->flash_notification('CSV import successfully.');
			} else {
				$this->session->set_flashdata("error", "File is empty.");
				//$this->flash_notification('File is empty.');
			}
			redirect(base_url() . 'admin/ingredients');
		}
	}

	/**
	 *  Ingredients check exists or not
	 */

	function checkIngredient() {
		//print_r($_POST);exit;
		$isExists = $this->Ingredients_model->checkIngredientName(trim($_POST['ingredients_name']));

		if ($isExists == 1) {
			echo "false";
		} else {
			echo "true";
		}
	}

}