<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Recipes extends MY_Controller {

	/**
	 * Constructor
	 */
	function __construct() {
		$this->load->library('form_validation');
		parent::__construct();
		lang_switcher($this->session->userdata('siteLang'));
		$this->load->model('admin/Recipes_model');
		$this->load->model('admin/Recipes_filter_model');
		$this->load->model('admin/Ingredients_model');
		$this->load->model('admin/Units_model');
		$this->load->model('admin/Master_recipeingredients_model');
		$this->load->model('admin/Recipes_filter_model');
		$this->load->model('admin/Recipe_categories_model');
		$this->load->model('admin/Recipe_subcategories_model');
		$this->load->library('excel');
		if (!$this->session->userdata('admin_id')) {
			redirect(base_url() . 'admin/login');
		}
	}

	/**
	 * Index
	 */
	function index() {
		$this->data['title'] = $this->lang->line('recp_index');
		$this->data['page'] = 'recipes';
		$this->data['recipes'] = $this->Recipes_model->getRecipies();

		// $this->data['recipes'] = $this->Recipes_model->order_by_column('id');
		$this->__template('admin/recipes/index', $this->data);
	}

	public function recipesajax_list() {
		$list = $this->Recipes_filter_model->get_datatables();
		//echo $this->db->last_query();exit;
		$data = array();
		$no = $_POST['start'];

		foreach ($list as $recipes) {

			$ingredients_names = array();
			$recipes->ingredients_names = array();

			$ingredients = $this->Recipes_filter_model->get_ingredients($recipes->id);

			for ($k = 0; $k < count($ingredients); $k++) {
				if ($ingredients[$k]->recipe_id == $recipes->id) {
					$ingredients_names[] = $ingredients[$k]->name;
				}
			}

			$recipes->ingredients_names = implode(" | ", $ingredients_names);
			if (!empty($recipes->recipe_media)) {
				$img = '<img src=' . $recipes->recipe_media . ' width="50">';
			} else {
				$img = '';
			}
			$no++;

			$row = array();
			$row[] = "<input type='checkbox'  class='deleteRow' value='" . $recipes->id . "'  />#" . $no;
			//$row[] = $no;
			$row[] = $recipes->recipe_name;
			$row[] = $img;
			/*$row[] = $recipes->ingredients_names;*/
			$row[] = $recipes->preparation_time;
			$row[] = ($recipes->status == 0) ? "<i id=" . $recipes->id . " class='status_checks btn btn-success' data=" . $recipes->id . ">Active</i>" : "<i id=" . $recipes->id . " class='status_checks btn btn-danger' data=" . $recipes->id . ">Inactive</i>";
			$row[] = "<a href=" . base_url() . "admin/recipes/edit/" . $recipes->id . " class='btn btn-sm btn-primary'><span><i class='fa fa-pencil' aria-hidden='true'></i>" . $this->lang->line('btnedit') . "</span></a> <a href=" . base_url() . "admin/recipes/delete_recipe/" . $recipes->id . " class='btn btn-sm btn-danger' onclick=\"return confirm('Are you sure want to remove {$recipes->recipe_name} ?');\"><span><i class='fa fa-trash-o' aria-hidden='true'></i>" . $this->lang->line('btnremove') . "</span></a>";
			$data[] = $row;
		}

		$output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $this->Recipes_filter_model->count_all(),
			"recordsFiltered" => $this->Recipes_filter_model->count_filtered(),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}

	/**
	 * Index action
	 */
	function create() {

		if ($_POST) {
			/*echo '<pre>';
				print_r($_POST);
			*/
			/********************* Preparation Images ****************/
			$filesmethod = $_FILES['stepmedia'];
			$filesmethodar = $_FILES['stepmedia_ar'];
			$filesmethodfr = $_FILES['stepmedia_fr'];

			$config = array(
				'upload_path' => FCPATH . 'uploads/recipesteps',
				'allowed_types' => 'jpg|gif|png|jpeg|mp4|avi|3gp',
				'overwrite' => 1,
			);
			$images = array();
			$imagesar = array();
			$imagesfr = array();
			//echo '<pre>';
			for ($i = 1; $i <= count($filesmethod); $i++) {

				if ($filesmethod['name'][$i] != '') {

					$this->load->library('upload', $config);

					$_FILES['stepmedia[]']['name'] = $filesmethod['name'][$i];
					$_FILES['stepmedia[]']['type'] = $filesmethod['type'][$i];
					$_FILES['stepmedia[]']['tmp_name'] = $filesmethod['tmp_name'][$i];
					$_FILES['stepmedia[]']['error'] = $filesmethod['error'][$i];
					$_FILES['stepmedia[]']['size'] = $filesmethod['size'][$i];

					//$fileName = date('dmYhis') . '-' . $filesmethod['name'][$i];
					$fileName = preg_replace('/[^A-Za-z0-9.]/', "", $filesmethod['name'][$i]);

					$images[$i] = 'uploads/recipesteps/' . $fileName;

					$config['file_name'] = $fileName;

					$this->upload->initialize($config);

					if ($this->upload->do_upload('stepmedia[]')) {
						$this->upload->data();
					} else {
						echo $this->upload->display_errors();
					}
				} else {
					$images[$i] = "";
				}
			}

			for ($i = 1; $i <= count($filesmethodar); $i++) {

				if ($filesmethodar['name'][$i] != '') {

					$this->load->library('upload', $config);

					$_FILES['stepmedia_ar[]']['name'] = $filesmethodar['name'][$i];
					$_FILES['stepmedia_ar[]']['type'] = $filesmethodar['type'][$i];
					$_FILES['stepmedia_ar[]']['tmp_name'] = $filesmethodar['tmp_name'][$i];
					$_FILES['stepmedia_ar[]']['error'] = $filesmethodar['error'][$i];
					$_FILES['stepmedia_ar[]']['size'] = $filesmethodar['size'][$i];

					//$fileName = date('dmYhis') . '-' . $filesmethod['name'][$i];
					$fileName = preg_replace('/[^A-Za-z0-9.]/', "", $filesmethodar['name'][$i]);

					$imagesar[$i] = 'uploads/recipesteps/' . $fileName;

					$config['file_name'] = $fileName;

					$this->upload->initialize($config);

					if ($this->upload->do_upload('stepmedia_ar[]')) {
						$this->upload->data();
					} else {
						echo $this->upload->display_errors();
					}
				} else {
					$imagesar[$i] = "";
				}
			}

			for ($i = 1; $i <= count($filesmethodfr); $i++) {

				if ($filesmethodfr['name'][$i] != '') {

					$this->load->library('upload', $config);

					$_FILES['stepmedia_fr[]']['name'] = $filesmethodfr['name'][$i];
					$_FILES['stepmedia_fr[]']['type'] = $filesmethodfr['type'][$i];
					$_FILES['stepmedia_fr[]']['tmp_name'] = $filesmethodfr['tmp_name'][$i];
					$_FILES['stepmedia_fr[]']['error'] = $filesmethodfr['error'][$i];
					$_FILES['stepmedia_fr[]']['size'] = $filesmethodfr['size'][$i];

					//$fileName = date('dmYhis') . '-' . $filesmethod['name'][$i];
					$fileName = preg_replace('/[^A-Za-z0-9.]/', "", $filesmethodfr['name'][$i]);

					$imagesfr[$i] = 'uploads/recipesteps/' . $fileName;

					$config['file_name'] = $fileName;

					$this->upload->initialize($config);

					if ($this->upload->do_upload('stepmedia_fr[]')) {
						$this->upload->data();
					} else {
						echo $this->upload->display_errors();
					}
				} else {
					$imagesfr[$i] = "";
				}
			}
			/**************************** END *****************/

			/*********************** Recipes Images *************/
			$files = $_FILES['recipe_media'];
			$config1 = array(
				'upload_path' => FCPATH . 'uploads/recipes',
				'allowed_types' => 'jpg|gif|png|jpeg|mp4|avi',
				'overwrite' => 1,
			);
			if ($files['name'][0] != '') {
				/********************* Recipes ****************/

				$this->load->library('upload', $config1);

				$recipe_image = array();

				foreach ($files['name'] as $key => $image) {
// die("DF");
					$_FILES['recipe_media[]']['name'] = $files['name'][$key];
					$_FILES['recipe_media[]']['type'] = $files['type'][$key];
					$_FILES['recipe_media[]']['tmp_name'] = $files['tmp_name'][$key];
					$_FILES['recipe_media[]']['error'] = $files['error'][$key];
					$_FILES['recipe_media[]']['size'] = $files['size'][$key];

					//$file = mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $image);

					//print_r($file);
					//

					//echo $new . '<br/>';

					$fileName = preg_replace('/[^A-Za-z0-9.]/', "", $_FILES["recipe_media[]"]["name"]);
					//echo $fileName;

					$recipe_image[] = $fileName;

					$config1['file_name'] = $fileName;

					$this->upload->initialize($config1);

					if ($this->upload->do_upload('recipe_media[]')) {
						$this->upload->data();
					} else {
						return false;
					}
				}
			}
			/************************** END *********************/
			//exit;
			$recipes_title = trim($_POST['recipe_name']);
			$recipes_title_ar = trim($_POST['recipe_name_ar']);
			$recipes_title_fr = trim($_POST['recipe_name_fr']);
			$recipe_description = trim($_POST['recipe_description']);
			$recipe_description_ar = trim($_POST['recipe_description_ar']);
			$recipe_description_fr = trim($_POST['recipe_description_fr']);
			// $recipe_method = $_POST['recipe_method'];
			//$recipe_image = '';
			$recipe_prep_time = trim($_POST['preparation_time']);
			$cooking_time = trim($_POST['cooking_time']);
			$serving = trim($_POST['serving']);
			$recipe_notes = trim($_POST['notes']);
			//$recipe_calories = $_POST['calories'];
			$rec_cat = $_POST['recipe_category'];
			$rec_subcat = $_POST['recipe_subcat'];
			$rec_stitle = trim($_POST['source_name']);
			$rec_slink = trim($_POST['source_link']);
			$level = $_POST['level'] ? $_POST['level'] : 1;
			$featured = $_POST['is_featured'] ? 1 : 0;

			$recipes = array(
				"recipe_name" => $recipes_title,
				"recipe_name_ar" => $recipes_title_ar,
				"recipe_name_fr" => $recipes_title_fr,
				"recipe_media" => base_url() . 'uploads/recipes/' . $recipe_image[0],
				//"recipe_method"         =>$recipe_method,
				"preparation_time" => $recipe_prep_time,
				"cooking_time" => $cooking_time,
				"recipe_description" => $recipe_description,
				"recipe_description_ar" => $recipe_description_ar,
				"recipe_description_fr" => $recipe_description_fr,
				"notes" => $recipe_notes,
				"serving" => $serving,
				//"calories"              =>$recipe_calories,
				"admin_by" => $this->session->userdata('admin_id'),
				"is_featured" => $featured,
				"recipe_category_id" => $rec_cat,
				"recipe_subcategory_id" => $rec_subcat,
				"source_title" => $rec_stitle,
				"source_link" => $rec_slink,
				"level_id" => $level,
			);

			$recipie_id = $this->Recipes_model->insert($recipes);

			/********************* Recipes Steps Store ************/
			$steps = array(
				"recipe_id" => $recipie_id,
			);

			for ($i = 1; $i <= count($_POST['txtstep']); $i++) {

				$steps['step_no'] = trim($_POST['txtstep'][$i]);
				$steps['step_description'] = trim($_POST['preparation_method'][$i]);
				$steps['step_description_ar'] = trim($_POST['preparation_method_ar'][$i]);
				$steps['step_description_fr'] = trim($_POST['preparation_method_fr'][$i]);
				$steps['image'] = $images[$i];
				$steps['image_ar'] = $imagesar[$i];
				$steps['image_fr'] = $imagesfr[$i];
				$this->db->insert('recipe_steps', $steps);
			}

			/*********************** END ***************************/

			/**************** Ingredients Master table stored *****/
			$ingredients = array(
				"recipe_id" => $recipie_id,
			);

			for ($i = 1; $i <= count($_POST['ingname']); $i++) {

				$checkID = $this->Recipes_model->exitsRecipesIngredient($recipie_id, $_POST['ingname'][$i]);

				if ($checkID == 1) {
					continue;
				} else {
					$ingredients['ingredient_id'] = $_POST['ingname'][$i];
					$ingredients['qty'] = trim($_POST['ingmaterial'][$i]);
					//$ingredients['unit'] = $_POST['ingunit'][$i];
					//$ingredients['measureunit_id'] = $_POST['unitname'][$i];
					$ingredients['finalunit_id'] = $_POST['unitname'][$i];
					$ingredients['measure_amount'] = $_POST['final_calories'][$i];
					$ingredients['protein_amount'] = $_POST['protine_calories'][$i];
					$ingredients['fat_amount'] = $_POST['fat_calories'][$i];
					$ingredients['carbs_amount'] = $_POST['carbs_calories'][$i];
					$ingredients['measure_amount_ounce'] = $_POST['final_calories_ounce'][$i];
					$ingredients['protein_amount_ounce'] = $_POST['protine_calories_ounce'][$i];
					$ingredients['fat_amount_ounce'] = $_POST['fat_calories_ounce'][$i];
					$ingredients['carbs_amount_ounce'] = $_POST['carbs_calories_ounce'][$i];
					$this->db->insert('master_recipeingredients', $ingredients);
				}
			}
			/******************** END **************************/

			/*********************** Calculate total calory stored in reicpes table ****/

			$totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$recipie_id}")->row()->maxid;

			$totalcalory_ounce = $this->db->query("SELECT SUM(TRUNCATE(measure_amount_ounce,2)) AS maxounceid FROM master_recipeingredients WHERE recipe_id = {$recipie_id}")->row()->maxounceid;

			$caldata = array('total_calory' => round($totalcalory, 2), 'total_calory_ounce' => round($totalcalory_ounce, 2));

			$this->db->where('id', $recipie_id);
			$this->db->update('recipes', $caldata);
			/********************************* END ***************************************/

			$this->flash_notification('Recipe is successfully added.');
			redirect(base_url() . 'admin/recipes/');

		}

		$this->data['title'] = $this->lang->line('recp_add');
		$this->data['page'] = 'create';
		$this->data['units'] = $this->getUnits();
		$this->__template('admin/recipes/create', $this->data);

	}

	// Get recipes from id
	function edit($recipe_id = '') {

		if ($recipe_id) {
			$this->data['title'] = $this->lang->line('recp_edit');
			$this->data['page'] = 'recipes';
			$this->data['recipes'] = $this->Recipes_model->get($recipe_id);
			$this->data['recp_ing'] = $this->Recipes_model->getRecipeingredients($recipe_id);
			$this->data['resting'] = $this->Recipes_model->getSelectedingredients($recipe_id);
			$this->data['recipescats'] = $this->Recipes_model->getRecipescat();
			$this->data['units'] = $this->getUnits();
			//$this->data['param2'] = '';
			$this->__template('admin/recipes/edit', $this->data);
		}

		//redirect('quiz');
	}

	/**
	 * Update recipes table , master_recipesingrdients
	 * @param string $id
	 */
	function update($id = '') {

		$fullpath = base_url() . 'uploads/recipes/';
		//echo $fullpath;exit;
		if ($_POST) {
			$ingredients = array();
			$this->Recipes_model->update($id, array(
				'recipe_name' => trim($_POST['title']),
				'recipe_name_ar' => trim($_POST['title_ar']),
				'recipe_name_fr' => trim($_POST['title_fr']),
				'recipe_description' => trim($_POST['recipe_description']),
				'recipe_description_ar' => trim($_POST['recipe_description_ar']),
				'recipe_description_fr' => trim($_POST['recipe_description_fr']),
				//'recipe_method'         => $_POST['method'],
				'preparation_time' => trim($_POST['prepration_time']),
				'cooking_time' => trim($_POST['cooking_time']),
				'serving' => trim($_POST['serving']),
				'notes' => trim($_POST['notes']),
				'recipe_category_id' => $_POST['recipe_category'],
				"recipe_subcategory_id" => $_POST['recipe_subcat'],
				"source_title" => trim($_POST['source_name']),
				"source_link" => trim($_POST['source_link']),
				//'calories'              => $_POST['calories'],
				//"admin_by"              => $this->session->userdata('admin_id'),
				"is_featured" => $_POST['is_featured'] ? 1 : 0,
				"status" => $_POST['is_status'],
				'level_id' => $_POST['level'] ? $_POST['level'] : 1,
				'updated_at' => date('Y-m-d H:i:s'),
			));

			/********************* Preparation Images ****************/
			$filesmethod = $_FILES['stepmedia'];
			$filesmethodar = $_FILES['stepmedia_ar'];
			$filesmethodfr = $_FILES['stepmedia_fr'];

			$config2 = array(
				'upload_path' => FCPATH . 'uploads/recipesteps',
				'allowed_types' => 'jpg|gif|png|jpeg|mp4|avi|3gp',
				'overwrite' => 1,
			);
			$images = array();
			$imagesar = array();
			$imagesfr = array();
			//echo '<pre>';
			for ($i = 0; $i < count($filesmethod); $i++) {

				if ($filesmethod['name'][$i] != '') {

					$this->load->library('upload', $config2);

					$_FILES['stepmedia[]']['name'] = $filesmethod['name'][$i];
					$_FILES['stepmedia[]']['type'] = $filesmethod['type'][$i];
					$_FILES['stepmedia[]']['tmp_name'] = $filesmethod['tmp_name'][$i];
					$_FILES['stepmedia[]']['error'] = $filesmethod['error'][$i];
					$_FILES['stepmedia[]']['size'] = $filesmethod['size'][$i];

					//$fileName = date('dmYhis') . '-' . $filesmethod['name'][$i];
					$fileName = preg_replace('/[^A-Za-z0-9.]/', "", $filesmethod['name'][$i]);

					$images[$i] = 'uploads/recipesteps/' . $fileName;

					$config2['file_name'] = $fileName;

					$this->upload->initialize($config2);

					if ($this->upload->do_upload('stepmedia[]')) {
						$this->upload->data();
					} else {
						echo $this->upload->display_errors();
					}
				} else {
					$images[$i] = "";
				}
			}

			for ($i = 0; $i < count($filesmethodar); $i++) {

				if ($filesmethodar['name'][$i] != '') {

					$this->load->library('upload', $config2);

					$_FILES['stepmedia_ar[]']['name'] = $filesmethodar['name'][$i];
					$_FILES['stepmedia_ar[]']['type'] = $filesmethodar['type'][$i];
					$_FILES['stepmedia_ar[]']['tmp_name'] = $filesmethodar['tmp_name'][$i];
					$_FILES['stepmedia_ar[]']['error'] = $filesmethodar['error'][$i];
					$_FILES['stepmedia_ar[]']['size'] = $filesmethodar['size'][$i];

					//$fileName = date('dmYhis') . '-' . $filesmethod['name'][$i];
					$fileName = preg_replace('/[^A-Za-z0-9.]/', "", $filesmethodar['name'][$i]);

					$imagesar[$i] = 'uploads/recipesteps/' . $fileName;

					$$config2['file_name'] = $fileName;

					$this->upload->initialize($config2);

					if ($this->upload->do_upload('stepmedia_ar[]')) {
						$this->upload->data();
					} else {
						echo $this->upload->display_errors();
					}
				} else {
					$imagesar[$i] = "";
				}
			}

			for ($i = 0; $i < count($filesmethodfr); $i++) {

				if ($filesmethodfr['name'][$i] != '') {

					$this->load->library('upload', $config2);

					$_FILES['stepmedia_fr[]']['name'] = $filesmethodfr['name'][$i];
					$_FILES['stepmedia_fr[]']['type'] = $filesmethodfr['type'][$i];
					$_FILES['stepmedia_fr[]']['tmp_name'] = $filesmethodfr['tmp_name'][$i];
					$_FILES['stepmedia_fr[]']['error'] = $filesmethodfr['error'][$i];
					$_FILES['stepmedia_fr[]']['size'] = $filesmethodfr['size'][$i];

					//$fileName = date('dmYhis') . '-' . $filesmethod['name'][$i];
					$fileName = preg_replace('/[^A-Za-z0-9.]/', "", $filesmethodfr['name'][$i]);

					$imagesfr[$i] = 'uploads/recipesteps/' . $fileName;

					$$config2['file_name'] = $fileName;

					$this->upload->initialize($config2);

					if ($this->upload->do_upload('stepmedia_fr[]')) {
						$this->upload->data();
					} else {
						echo $this->upload->display_errors();
					}
				} else {
					$imagesfr[$i] = "";
				}
			}
			//print_r($imagesfr);exit;
			/**************************** END *****************/

			$number_of_files = sizeof($_FILES['recipe_media']['tmp_name']);

			if (!empty($_FILES['recipe_media']['name'][0])) {

				$uploads = '';
				for ($i = 0; $i < $number_of_files; $i++) {

					$file_ext = end(explode(".", $_FILES['recipe_media']['name'][$i]));
					//echo $file_ext;
					$uploads .= $files . date('dmYhis') . '.' . $file_ext;
					$config[] = $i . date('dmYhis') . '.' . $file_ext;

					move_uploaded_file($_FILES['recipe_media']['tmp_name'][$i], FCPATH . 'uploads/recipes/' . $config[$i]);
				}
				$files = implode(',', $config);
				$this->Recipes_model->update($id, array('recipe_media' => $fullpath . $files));

				/********************* Recipes Steps Store ************/
				$steps = array(
					"recipe_id" => $id,
				);

				for ($i = 0; $i < count($_POST['txtstep']); $i++) {
					if (!empty($_POST['txtstep'][$i])) {
						$steps['step_no'] = trim($_POST['txtstep'][$i]);
						$steps['step_description'] = trim($_POST['preparation_method'][$i]);
						$steps['step_description_ar'] = trim($_POST['preparation_method_ar'][$i]);
						$steps['step_description_fr'] = trim($_POST['preparation_method_fr'][$i]);
						$steps['image'] = $images[$i];
						$steps['image_ar'] = $imagesar[$i];
						$steps['image_fr'] = $imagesfr[$i];
						//$steps['image'] = 'uploads/recipesteps/' . $images[$i];
						$this->db->insert('recipe_steps', $steps);
					}
				}

				/*********************** END ***************************/
				$ingredients = array(
					"recipe_id" => $id,
				);
				/**************** Ingredients Master table stored *****/
				for ($i = 0; $i < count($_POST['ingname']); $i++) {
					if (!empty($_POST['ingname'][$i])) {

						$checkID = $this->Recipes_model->exitsRecipesIngredient($id, $_POST['ingname'][$i]);

						if ($checkID == 1) {
							continue;
						} else {
							$ingredients['ingredient_id'] = $_POST['ingname'][$i];
							$ingredients['qty'] = trim($_POST['ingmaterial'][$i]);
							/*$ingredients['unit'] = $_POST['ingunit'][$i];*/
							//$ingredients['measureunit_id'] = $_POST['unitname'][$i];
							$ingredients['finalunit_id'] = $_POST['unitname'][$i];
							$ingredients['measure_amount'] = $_POST['final_calories'][$i];
							$ingredients['protein_amount'] = $_POST['protine_calories'][$i];
							$ingredients['fat_amount'] = $_POST['fat_calories'][$i];
							$ingredients['carbs_amount'] = $_POST['carbs_calories'][$i];

							$ingredients['measure_amount_ounce'] = $_POST['final_calories_ounce'][$i];
							$ingredients['protein_amount_ounce'] = $_POST['protine_calories_ounce'][$i];
							$ingredients['fat_amount_ounce'] = $_POST['fat_calories_ounce'][$i];
							$ingredients['carbs_amount_ounce'] = $_POST['carbs_calories_ounce'][$i];
							$this->db->insert('master_recipeingredients', $ingredients);
						}
					} else {
						continue;
					}
				}

				$totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$id}")->row()->maxid;

				$totalcalory_ounce = $this->db->query("SELECT SUM(TRUNCATE(measure_amount_ounce,2)) AS maxcounceid FROM master_recipeingredients WHERE recipe_id = {$id}")->row()->maxcounceid;

				$caldata = array('total_calory' => round($totalcalory, 2), 'total_calory_ounce' => round($totalcalory_ounce, 2));

				$this->db->where('id', $id);
				$this->db->update('recipes', $caldata);
				/******************** END **************************/
			} else {

				/*$oldfiles = implode(',', $_POST['old_image']);
                $this->Recipes_model->update($id,array('recipe_media' => $oldfiles));*/

				/********************* Recipes Steps Store ************/
				$steps = array(
					"recipe_id" => $id,
				);

				for ($i = 0; $i < count($_POST['txtstep']); $i++) {
					/*if (!empty($_POST['preparation_method'][$i]) || !empty($_POST['preparation_method_ar'][$i]) || !empty($_POST['preparation_method_fr'][$i])) {
						$this->session->set_flashdata('error', 'Enter step number');
						redirect(base_url("admin/recipes/edit/{$id}"));
					}*/
					if (!empty($_POST['txtstep'][$i])) {
						$steps['step_no'] = trim($_POST['txtstep'][$i]);
						$steps['step_description'] = trim($_POST['preparation_method'][$i]);
						$steps['step_description_ar'] = trim($_POST['preparation_method_ar'][$i]);
						$steps['step_description_fr'] = trim($_POST['preparation_method_fr'][$i]);
						$steps['image'] = $images[$i];
						$steps['image_ar'] = $imagesar[$i];
						$steps['image_fr'] = $imagesfr[$i];
						//$steps['image'] = 'uploads/recipesteps/' . $images[$i];
						$this->db->insert('recipe_steps', $steps);
					}
				}

				/*********************** END ***************************/
				$ingredients = array(
					"recipe_id" => $id,
				);
				/**************** Ingredients Master table stored *****/

				for ($i = 0; $i < count($_POST['ingname']); $i++) {

					/*if (!empty($_POST['ingmaterial'][$i])) {
						$this->session->set_flashdata('error', 'Select ingredient and unit');
						redirect(base_url("admin/recipes/edit/{$id}"));
					}*/

					if (!empty($_POST['ingname'][$i])) {

						$checkID = $this->Recipes_model->exitsRecipesIngredient($id, $_POST['ingname'][$i]);

						if ($checkID == 1) {
							continue;
						} else {
							$ingredients['ingredient_id'] = $_POST['ingname'][$i];
							$ingredients['qty'] = trim($_POST['ingmaterial'][$i]);
							/*$ingredients['unit'] = $_POST['ingunit'][$i];*/
							/*$ingredients['measureunit_id'] = $_POST['unitname'][$i];*/
							$ingredients['finalunit_id'] = $_POST['unitname'][$i];
							$ingredients['measure_amount'] = $_POST['final_calories'][$i];
							$ingredients['protein_amount'] = $_POST['protine_calories'][$i];
							$ingredients['fat_amount'] = $_POST['fat_calories'][$i];
							$ingredients['carbs_amount'] = $_POST['carbs_calories'][$i];

							$ingredients['measure_amount_ounce'] = $_POST['final_calories_ounce'][$i];
							$ingredients['protein_amount_ounce'] = $_POST['protine_calories_ounce'][$i];
							$ingredients['fat_amount_ounce'] = $_POST['fat_calories_ounce'][$i];
							$ingredients['carbs_amount_ounce'] = $_POST['carbs_calories_ounce'][$i];
							$this->db->insert('master_recipeingredients', $ingredients);
						}
					} else {
						continue;
					}
				}
				$totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$id}")->row()->maxid;

				$totalcalory_ounce = $this->db->query("SELECT SUM(TRUNCATE(measure_amount_ounce,2)) AS maxcounceid FROM master_recipeingredients WHERE recipe_id = {$id}")->row()->maxcounceid;

				$caldata = array('total_calory' => round($totalcalory, 2), 'total_calory_ounce' => round($totalcalory_ounce, 2));

				$this->db->where('id', $id);
				$this->db->update('recipes', $caldata);
				/******************** END **************************/
			}
			$this->flash_notification('Recipe is successfully updated.');
		}

		redirect(base_url() . 'admin/recipes/');
	}

	/**
	 * Delete recipe details
	 * @param string $recipe_id
	 *
	 */
	function delete_recipe($recipe_id = '') {
		if ($recipe_id) {

			$image = $this->Recipes_model->find($recipe_id); // Recipes image
			$images = explode("/", $image->recipe_media);

			$steps = $this->db->select('image') // Steps images
				->from('recipe_steps')
				->where('recipe_id', $recipe_id)
				->get()->row();

			$this->db->trans_start();
			$this->db->query("DELETE FROM recipes WHERE id='$recipe_id';");
			unlink(FCPATH . 'uploads/recipes/' . $images[6]); // Remove recipe image
			unlink(FCPATH . $steps->image); // Remove steps images
			$this->db->trans_complete();
			$this->flash_notification('Recipe is successfully deleted.');
		}
		redirect(base_url() . 'admin/recipes/');
	}

	// Get recipes sub category
	function get_subcategory($recipecat = null) {

		$this->db->select('id, sub_cate_name,sub_cate_name_ar,sub_cate_name_fr');
		if ($recipecat != NULL) {

			$this->db->where('recipe_category_id', $recipecat);
			$this->db->where('status', 0);
			$query = $this->db->get('recipe_subcategories');
			echo json_encode($query->result());
		} else {
			return FALSE;
		}
	}

	/**
	 * Gets the featured recipes.
	 *
	 * @param      blooean  $featured  The featured
	 *
	 * @return     array  The featured recipes.
	 */
	function get_featured_recipes($featured = null) {
		$this->db->select('id, recipe_name,recipe_name_ar,recipe_name_fr');
		if ($featured != NULL) {

			$this->db->where('is_featured', 1);
			$this->db->where('status', 0);
			$query = $this->db->get('recipes');
			echo json_encode($query->result());
		} else {
			return FALSE;
		}
	}

	/**
	 * Gets the mostlike recipes by user .
	 *
	 * @param      integer  $recipe  The recipe
	 */
	function get_mostlike_recipes($recipe = null) {
		$catid = $this->Recipes_model->get_by(array('id' => $recipe));

		$this->db->select('user.id,user.firstname as likeuser,user.lastname,user.device_token');
		$this->db->join('users as user', 'user.id = uf.user_id', 'LEFT');
		$this->db->where('uf.recipe_category_id', $catid->recipe_category_id);
		$this->db->group_by('uf.user_id');
		$this->db->order_by('count(uf.user_id)', 'DESC');
		$mostlike = $this->db->get('user_favourites as uf')->result();

		echo json_encode($mostlike);
	}
	/**
	 * Gets the mostrated recipes.
	 *
	 * @param      integer  $recipe  The recipe
	 */
	function get_mostrated_recipes($recipe = null) {
		$catid = $this->Recipes_model->get_by(array('id' => $recipe));

		$this->db->select('user.id,user.firstname as likeuser,user.lastname,user.device_token');
		$this->db->join('users as user', 'user.id = rt.user_id', 'LEFT');
		$this->db->where('rt.recipe_category_id', $catid->recipe_category_id);
		$this->db->group_by('rt.user_id');
		$this->db->order_by('count(rt.user_id)', 'DESC');
		$mostrated = $this->db->get('ratings as rt')->result();

		echo json_encode($mostrated);
	}

	// User recipe approve
	function approveUserRecipe() {

		$recID = $_POST['recid'];
		$userid = $_POST['userid'];
		$data = array(
			'isapprove' => $_POST['status'],
		); // Status update with 0 to 1

		$success = $this->Recipes_model->approveRejectRecipe($data, $recID, $userid);
		echo $success;
	}

	// Remove Recipes Ingredients
	function removeRecipeIngredient($materID) {
		$deleted = $this->Recipes_model->deleteRecipesIngredient($materID);

		if ($deleted == 1) {
			echo json_encode(array('msg' => 'success'));
		} else {
			echo array('msg' => 'error');
		}
	}

	// Remove Recipes Step No
	function removeRecipeStep($stepID) {
		//echo $stepID;exit;
		$deleted = $this->db->delete('recipe_steps', array('id' => $stepID));
		if ($deleted) {
			echo json_encode(array('msg' => 'success'));
		} else {
			echo array('msg' => 'error');
		}
	}

	// Update Recipes List
	function updateRecipeList($stepID) {

		/********************* Preparation Images ****************/
		$config = array(
			'upload_path' => FCPATH . 'uploads/recipesteps',
			'allowed_types' => 'jpg|gif|png|jpeg|mp4|avi',
			'overwrite' => 1,
		);

		if ($_POST['fileold'] == 'undefined') {
			$fileName = date('dmYhis') . '-' . $_FILES['file']['name'];
			//echo $fileName;exit;
			$config['file_name'] = $fileName;
			$imgname = 'uploads/recipesteps/' . $fileName;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ($this->upload->do_upload('file')) {
				echo $this->upload->display_errors();
				$this->upload->data();
				unlink(FCPATH . $_POST['fileexists']);
			} else {
				echo $this->upload->display_errors();
				//return false;
			}} else {
			$imgname = $_POST['fileold'];
		}
		if ($_POST['fileold_ar'] == 'undefined') {
			$fileName = date('dmYhis') . '-' . $_FILES['file_ar']['name'];
			//echo $fileName;exit;
			$config['file_name'] = $fileName;
			$imgname_ar = 'uploads/recipesteps/' . $fileName;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ($this->upload->do_upload('file_ar')) {
				echo $this->upload->display_errors();
				$this->upload->data();
				unlink(FCPATH . $_POST['fileexists_ar']);
			} else {
				echo $this->upload->display_errors();
				//return false;
			}} else {
			$imgname_ar = $_POST['fileold_ar'];
		}
		if ($_POST['fileold_fr'] == 'undefined') {
			$fileName = date('dmYhis') . '-' . $_FILES['file_fr']['name'];
			//echo $fileName;exit;
			$config['file_name'] = $fileName;
			$imgname_fr = 'uploads/recipesteps/' . $fileName;

			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			if ($this->upload->do_upload('file_fr')) {
				echo $this->upload->display_errors();
				$this->upload->data();
				unlink(FCPATH . $_POST['fileexists_fr']);
			} else {
				echo $this->upload->display_errors();
				//return false;
			}} else {
			$imgname_fr = $_POST['fileold_fr'];
		}

		/**************************** END *****************/
		$updateList = array(
			'step_description' => trim($_POST['description']),
			'step_description_ar' => trim($_POST['description_ar']),
			'step_description_fr' => trim($_POST['description_fr']),
			'image' => $imgname,
			'image_ar' => $imgname_ar,
			'image_fr' => $imgname_fr,
			'updated_at' => date('Y-m-d H:i:s'),
		);

		$this->db->where('id', $stepID);
		if ($this->db->update('recipe_steps', $updateList)) {
			echo json_encode(array('msg' => 'success'));
		} else {
			echo array('msg' => 'error');
		}

	}

	/************************* GET UNITS **********************/
	function getUnits() {
		$result = $this->db->select('id,metric_name')
			->from('weights')
			->order_by('id', 'asc')
			->group_by('metric_name')
			->get()->result();
		return $result;
	}

	function getUnitslist($inGID) {
		$result = $this->db->select('un.id,un.unit_name,un.unit_name_ar,un.unit_name_fr,ig.kcal_grams,ig.protein,ig.fat,ig.carbs')
			->from('units as un')
			->join('ingredient_units as iu', 'iu.unit_id = un.id')
			->join('ingredients as ig', 'ig.id = iu.ingredient_id', 'LEFT')
			->where('iu.ingredient_id', $inGID)
			->where('un.status', 1)
		//->order_by('un.id','asc')
		//->group_by('un.unit_name')
			->get()->result();

		echo json_encode($result);
	}
	/****************************** END **************************/

	/**
	 * Gets the step content.
	 *
	 * @param      <integer>  $stepID  The step id
	 */
	function getStepContent($stepID) {
		//echo $stepID;exit;
		$result = $this->db->select('step_description,step_description_ar,step_description_fr')
			->from('recipe_steps')
			->where('id', $stepID)
			->get()->result();
		//echo $this->db->last_query();exit;
		echo json_encode($result);
	}
	/***************************** END *************************/

	/**
	 * Reicpes status update
	 */

	function update_status() {
		$status = $_POST['status'];
		$id = $_POST['id'];

		$this->db->where("id", $id);
		$this->db->update("recipes", array('status' => $status));
	}

	/**
	 *  Recipes CSV Import
	 */
	function recipeImport() {
		if ($_POST) {
			$count = 0;
			$ingredientsunitArray = array();
			$fp = fopen($_FILES["file"]["tmp_name"], 'r') or die("can't open file");
			$k = 0;

			if ($_FILES["file"]["size"] > 1) {
				//echo '<pre>';
				while ($csv_line = fgetcsv($fp, 1024)) {
					$count++;
					if ($count == 1) {
						continue;
					}
					$k++; //keep this if condition if you want to remove the first row
					for ($i = 1, $j = count($csv_line); $i < $j; $i++) {

						$recipes = array();

						$recipes['recipe_name'] = trim($csv_line[0]);
						$recipes['recipe_name_ar'] = trim($csv_line[1]);
						$recipes['recipe_name_fr'] = trim($csv_line[2]);
						$recipes['recipe_description'] = trim($csv_line[4]);
						$recipes['recipe_description_ar'] = trim($csv_line[5]);
						$recipes['recipe_description_fr'] = trim($csv_line[6]);
						$recipes['serving'] = trim($csv_line[8]);
						$recipes['preparation_time'] = trim($csv_line[10]);
						$recipes['cooking_time'] = trim($csv_line[11]);
						$recipes['is_featured'] = $csv_line[12];
						//$recipes[$i]['recipe_category_id'] = $csv_line[13];

						$methods = explode(";", $csv_line[7]);
						$ingredients = explode(";", $csv_line[9]);

						$check = $this->Recipe_categories_model->checkCategory(trim($csv_line[13]));
						//$ing_csv['count'] = count($check);
						$recipes['recipe_category_id'] = $check->id;

						$recipes['source_title'] = $csv_line[14];
						$recipes['source_link'] = $csv_line[15];

					}

					$recipecsvdata = array(
						//'id' => $insert_csv['id'] ,
						'recipe_name' => trim($recipes['recipe_name']),
						'recipe_name_ar' => trim($recipes['recipe_name_ar']),
						'recipe_name_fr' => trim($recipes['recipe_name_fr']),
						'recipe_description' => trim($recipes['recipe_description']),
						//'count'   => $ing_csv['count'],
						'recipe_description_ar' => trim($recipes['recipe_description_ar']),
						'recipe_description_fr' => trim($recipes['recipe_description_fr']),
						'serving' => trim($recipes['serving']),
						'preparation_time' => trim($recipes['preparation_time']),
						'cooking_time' => trim($recipes['cooking_time']),
						'is_featured' => trim($recipes['is_featured']),
						'recipe_category_id' => $recipes['recipe_category_id'],
						'source_title' => trim($recipes['source_title']),
						'source_link' => trim($recipes['source_link']),
						//'updated_at' => date('Y-m-d H:i:s')
					);

					$checkAll = $this->db->get_where('recipes',
						array(
							'recipe_name' => trim($recipes['recipe_name']),
						)
					)->row(); // Check reicpes name exists

					if (count($checkAll) == 0) {
						// Insert in reicpes table
						if (!empty($csv_line[3])) {

							$filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($csv_line[3])));

							$type = explode(".", $filename);
							if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg'))) {
								$img_destitation_path = getcwd() . "/uploads/recipes/" . date('Y') . '-' . $filename;
								if (!copy(trim($csv_line[3]), $img_destitation_path)) {
									$recipecsvdata['recipe_media'] = '';
								} else {
									$recipecsvdata['recipe_media'] = base_url('uploads/recipes') . '/' . date('Y') . '-' . $filename;
								}
							} else {

								$recipecsvdata['recipe_media'] = '';
							}

						}
						$this->db->insert('recipes', $recipecsvdata);
						$insert_id = $this->db->insert_id(); // Get last id

						if (count($methods) >= 1) {

							foreach ($methods as $key => $value) {

								$stepsdata = explode("|", $value);
								if (!empty($stepsdata[4])) {
									$filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($stepsdata[4])));
									$type = explode(".", $filename);
									if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
										$img_destitation_path = getcwd() . "/uploads/recipesteps/" . date('Y') . '-' . $filename;
										if (!copy(trim($stepsdata[4]), $img_destitation_path)) {
											$stepsdata['img_video'] = '';
										} else {
											$stepsdata['img_video'] = 'uploads/recipesteps/' . date('Y') . '-' . $filename;
										}
									} else {

										$stepsdata['img_video'] = '';
									}
								}

								if (count($stepsdata) > 1) {
									$steps = array(
										'recipe_id' => $insert_id,
										'step_no' => trim($stepsdata[0]),
										'step_description' => trim($stepsdata[1]),
										'step_description_ar' => trim($stepsdata[2]),
										'step_description_fr' => trim($stepsdata[3]),
										'image' => $stepsdata['img_video'],
										//'image'	=> $stepsdata[1]
									);
									$this->db->insert('recipe_steps', $steps);
								}
							}

						}

						//print_r($ingredients);

						if (count($ingredients) >= 1 && !empty($ingredients[0])) {
							$masterArray = array();
							foreach ($ingredients as $key => $value) {
								$ingdata = explode("|", $value);
								//print_r($key);
								if (count($ingdata) > 1) {
									$checking = $this->Ingredients_model->checkIngredient(trim($ingdata[0]));
									$checking->qty = $ingdata[1];

									$masterArray[$key]['recipe_id'] = $insert_id;
									$masterArray[$key]['ingredient_id'] = $checking->id;
									$masterArray[$key]['qty'] = $checking->qty;

									if (count($checking) > 0) {
										$checkunit = $this->Units_model->checkUnit(trim($ingdata[2]));
										$masterArray[$key]['finalunit_id'] = $checkunit->id;
										$kcal[$key] = $this->calculateCalory('kcal', $checking->qty, $checking->kcal_grams, $checkunit->unit_gram);
										$protein[$key] = $this->calculateCalory('protein', $checking->qty, $checking->protein, $checkunit->unit_gram);
										$fat[$key] = $this->calculateCalory('fat', $checking->qty, $checking->fat, $checkunit->unit_gram);
										$carbs[$key] = $this->calculateCalory('carbs', $checking->qty, $checking->carbs, $checkunit->unit_gram);
										$masterArray[$key]['measure_amount'] = $kcal[$key]['total_calory'];
										$masterArray[$key]['protein_amount'] = $protein[$key]['total_protein'];
										$masterArray[$key]['fat_amount'] = $fat[$key]['total_fat'];
										$masterArray[$key]['carbs_amount'] = $carbs[$key]['total_carbs'];

									}
								}
							}
							$this->db->insert_batch('master_recipeingredients', $masterArray);
							/************************* Total calory stored **********/
							$totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$insert_id}")->row()->maxid;

							$caldata = array('total_calory' => $totalcalory);

							$this->db->where('id', $insert_id);
							$this->db->update('recipes', $caldata);
							/******************************** END *********************/
						}

					} else {
						$recipesSteps = array();

						if (!empty($csv_line[3])) {

							$filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($csv_line[3])));

							$type = explode(".", $filename);
							if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
								$img_destitation_path = getcwd() . "/uploads/recipes/" . date('Y') . '-' . $filename;
								if (!copy(trim($csv_line[3]), $img_destitation_path)) {
									$recipecsvdata['recipe_media'] = '';
								} else {
									$recipecsvdata['recipe_media'] = base_url('uploads/recipes') . '/' . date('Y') . '-' . $filename;
								}
							} else {

								$recipecsvdata['recipe_media'] = '';
							}

						}

					}

				}
			}
		}
	}

	/**
	 * Removes a html character.
	 *
	 * @param      <string>  $string  The string
	 */
	function removeHtmlCharacter($string = NULL) {
		return strip_tags($string);
	}
	/**
	 * Removes a character.
	 *
	 * @return     <string>  ( string )
	 */
	function removeCharacter($string = NULL) {
		return preg_replace("/[^0-9]/", "", $c);
	}

	/**
	 *  Recipes excel Import
	 */
	function recipeImportExcel() {

		if ($_POST) {

			$objPHPExcel = PHPExcel_IOFactory::load($_FILES["file"]["tmp_name"]);
			$sheet = $objPHPExcel->getSheet(0);
			$highestColumn = $sheet->getHighestColumn();
			$highestRow = $sheet->getHighestRow();
			$headings = $sheet->rangeToArray('A1:' . $highestColumn . 1,
				NULL,
				TRUE,
				FALSE);

			if ($highestRow > 1) {
				for ($row = 2; $row <= $highestRow; $row++) {

					//  Read a row of data into an array
					$rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row,
						NULL,
						TRUE,
						FALSE);
					$rowData[0] = array_combine($headings[0], $rowData[0]);

					if (count($rowData) > 0) {
						$recipes = array();

						/*$this->form_validation->set_rules('name', 'reicipe name', 'required|alpha');
							$this->form_validation->set_rules('media', 'reicipe image', 'required');
							$this->form_validation->set_rules('description', 'reicipe description', 'required');

							if ($this->form_validation->run() == FALSE) {
								$this->session->set_flashdata('error', validation_errors());
								redirect(base_url() . 'admin/recipes');
						*/

						$recipes['recipe_name'] = $this->removeHtmlCharacter(preg_replace('#<script(.*?)>(.*?)</script>#is', '', trim($rowData[0]['name'])));
						$recipes['recipe_name_ar'] = $this->removeHtmlCharacter(trim($rowData[0]['name_ar']));
						$recipes['recipe_name_fr'] = $this->removeHtmlCharacter(trim($rowData[0]['name_fr']));
						$recipes['recipe_description'] = $this->removeHtmlCharacter(trim($rowData[0]['description']));
						$recipes['recipe_description_ar'] = $this->removeHtmlCharacter(trim($rowData[0]['description_ar']));
						$recipes['recipe_description_fr'] = $this->removeHtmlCharacter(trim($rowData[0]['description_fr']));
						$recipes['serving'] = $this->removeHtmlCharacter(preg_replace("/[^0-9]/", "", trim($rowData[0]['serving'])));
						$recipes['preparation_time'] = $this->removeHtmlCharacter(preg_replace("/[^0-9]/", "", trim($rowData[0]['preparation_time'])));
						$recipes['cooking_time'] = $this->removeHtmlCharacter(preg_replace("/[^0-9]/", "", trim($rowData[0]['cooking_time'])));
						$recipes['is_featured'] = $this->removeHtmlCharacter(trim($rowData[0]['is_featured']));
						//$recipes[$i]['recipe_category_id'] = $csv_line[13];

						$methods = explode(";", $this->removeHtmlCharacter($rowData[0]['method']));
						$ingredients = explode(";", $this->removeHtmlCharacter($rowData[0]['ingredients']));

						$check = $this->Recipe_categories_model->checkCategory($this->removeHtmlCharacter(trim($rowData[0]['recipe_category']))); // check category

						$checkSubcategory = $this->Recipe_subcategories_model->checkSubCategory($this->removeHtmlCharacter(trim($rowData[0]['sub_category']))); //check subcategory

						if (count($check) == 0) {
							$this->db->insert('recipe_categories', array('cat_name' => $this->removeHtmlCharacter($rowData[0]['recipe_category'])));
							$check->id = $this->db->insert_id(); // Get last id

							$this->db->insert('recipe_subcategories', array('sub_cate_name' => $this->removeHtmlCharacter($rowData[0]['sub_category']), 'recipe_category_id' => $check->id));
							$checkSubcategory->id = $this->db->insert_id(); // Get last id
						} else {

							if (count($checkSubcategory) == 0) {
								$this->db->insert('recipe_subcategories', array('sub_cate_name' => $this->removeHtmlCharacter($rowData[0]['sub_category']), 'recipe_category_id' => $check->id));
								$checkSubcategory->id = $this->db->insert_id(); // Get last i
							}
						}
						//$ing_csv['count'] = count($check);
						$recipes['recipe_category_id'] = $check->id;
						$recipes['recipe_subcategory_id'] = $checkSubcategory->id;

						$recipes['source_title'] = $this->removeHtmlCharacter($rowData[0]['source_title']);
						$recipes['source_link'] = $this->removeHtmlCharacter($rowData[0]['source_link']);

						//echo '<pre>';
						$checkAll = $this->db->get_where('recipes',
							array(
								'recipe_name' => trim($recipes['recipe_name']),
							)
						)->row(); // Check reicpes name exists

						if (!$checkAll) {
							// Insert in reicpes table
							if (!empty($this->removeHtmlCharacter($rowData[0]['media']))) {

								$filename = preg_replace('/[^A-Za-z0-9.]/', "", $this->removeHtmlCharacter(trim(basename($rowData[0]['media']))));

								$type = explode(".", $filename);
								if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg'))) {
									$img_destitation_path = getcwd() . "/uploads/recipes/" . date('Y') . '-' . $filename;
									if (!copy($this->removeHtmlCharacter(trim($rowData[0]['media'])), $img_destitation_path)) {
										$recipes['recipe_media'] = '';
									} else {
										$recipes['recipe_media'] = base_url('uploads/recipes') . '/' . date('Y') . '-' . $filename;
									}
								} else {

									$recipes['recipe_media'] = '';
								}

							}
							//echo '<pre>';
							//print_r(json_encode($recipes));
							//exit;
							$this->db->insert('recipes', $recipes);
							$insert_id = $this->db->insert_id(); // Get last id

							if (count($methods) >= 1) {

								foreach ($methods as $key => $value) {

									$stepsdata = explode("|", $value);

									if (!empty($stepsdata[4])) {
										$filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($stepsdata[4])));
										$type = explode(".", $filename);
										if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
											$img_destitation_path = getcwd() . "/uploads/recipesteps/" . date('Y') . '-' . $filename;
											if (!copy(trim($stepsdata[4]), $img_destitation_path)) {
												$stepsdata['img_video'] = '';
											} else {
												$stepsdata['img_video'] = 'uploads/recipesteps/' . date('Y') . '-' . $filename;
											}
										} else {

											$stepsdata['img_video'] = '';
										}
									}
									if (count($stepsdata) > 1) {
										$steps = array(
											'recipe_id' => $insert_id,
											'step_no' => trim($stepsdata[0]),
											'step_description' => trim($stepsdata[1]),
											'step_description_ar' => trim($stepsdata[2]),
											'step_description_fr' => trim($stepsdata[3]),
											'image' => $stepsdata['img_video'],
											//'image'	=> $stepsdata[1]
										);
										//print_r($steps);
										$this->db->insert('recipe_steps', $steps);
									}
								}

							}

							if (count($ingredients) >= 1 && !empty($ingredients[0])) {
								$masterArray = array();
								foreach ($ingredients as $key => $value) {
									$ingdata = explode("|", $value);
									//print_r($key);
									if (count($ingdata) > 1) {
										$checking = $this->Ingredients_model->checkIngredient(trim($ingdata[0]));
										if (count($checking) == 0) {
											$this->db->insert('ingredients', array('name' => $ingdata[0]));
											$ing_id = $this->db->insert_id(); // Get last id
											$checking->id = $ing_id;
										}
										$checking->qty = $ingdata[1];

										$masterArray[$key]['recipe_id'] = $insert_id;
										$masterArray[$key]['ingredient_id'] = $checking->id;
										$masterArray[$key]['qty'] = $checking->qty;

										if (count($checking) > 0) {
											$checkunit = $this->Units_model->checkUnit(trim($ingdata[2]));

											if (count($checkunit) == 0) {
												$this->db->insert('units', array('unit_name' => $ingdata[2]));
												$unit_id = $this->db->insert_id(); // Get last id
												$checkunit->id = $unit_id;
												//$checkunit->unit_gram = 0;
											}
											$masterArray[$key]['finalunit_id'] = $checkunit->id;
											$kcal[$key] = $this->calculateCalory('kcal', $checking->qty, $checking->kcal_grams, $checkunit->unit_gram);
											$protein[$key] = $this->calculateCalory('protein', $checking->qty, $checking->protein, $checkunit->unit_gram);
											$fat[$key] = $this->calculateCalory('fat', $checking->qty, $checking->fat, $checkunit->unit_gram);
											$carbs[$key] = $this->calculateCalory('carbs', $checking->qty, $checking->carbs, $checkunit->unit_gram);
											$masterArray[$key]['measure_amount'] = $kcal[$key]['total_calory'];
											$masterArray[$key]['protein_amount'] = $protein[$key]['total_protein'];
											$masterArray[$key]['fat_amount'] = $fat[$key]['total_fat'];
											$masterArray[$key]['carbs_amount'] = $carbs[$key]['total_carbs'];
											$masterArray[$key]['measure_amount_ounce'] = $kcal[$key]['total_calory_ounce'];
											$masterArray[$key]['protein_amount_ounce'] = $protein[$key]['total_protein_ounce'];
											$masterArray[$key]['fat_amount_ounce'] = $fat[$key]['total_fat_ounce'];
											$masterArray[$key]['carbs_amount_ounce'] = $carbs[$key]['total_carbs_ounce'];

										}
									}
								}
								$this->db->insert_batch('master_recipeingredients', $masterArray);
								/************************* Total calory stored **********/
								$totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$insert_id}")->row()->maxid;

								$totalcalory_ounce = $this->db->query("SELECT SUM(measure_amount_ounce) AS maxid FROM master_recipeingredients WHERE recipe_id = {$insert_id}")->row()->maxid;

								$caldata = array('total_calory' => round($totalcalory, 2), 'total_calory_ounce' => round($totalcalory_ounce, 2));

								$this->db->where('id', $insert_id);
								$this->db->update('recipes', $caldata);
								/******************************** END *********************/
							}

						} else {
							// update
							if (!empty($this->removeHtmlCharacter($rowData[0]['media']))) {

								$filename = preg_replace('/[^A-Za-z0-9.]/', "", $this->removeHtmlCharacter(trim(basename($rowData[0]['media']))));

								$type = explode(".", $filename);
								if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
									$img_destitation_path = getcwd() . "/uploads/recipes/" . date('Y') . '-' . $filename;
									if (!copy($this->removeHtmlCharacter(trim($rowData[0]['media'])), $img_destitation_path)) {
										$recipes['recipe_media'] = '';
									} else {
										$recipes['recipe_media'] = base_url('uploads/recipes') . '/' . date('Y') . '-' . $filename;
									}
								} else {

									$recipes['recipe_media'] = '';
								}

							}
							// Update recipes table
							$this->db->where('id', $checkAll->id);
							$recipes['updated_at'] = date('Y-m-d H:i:s');
							//print_r($ingcsvdata);
							$this->db->update('recipes', $recipes);

							//delete all step which belongs to current recipe
							//delete all ingredients which belongs to recipes
							$deleted = $this->row_deletesteps($checkAll->id);
							$ingdeleted = $this->Ingredients_model->row_deleteRecipeIngredients($checkAll->id);

							if (count($methods) >= 1) {

								foreach ($methods as $key => $value) {

									$stepsdata = explode("|", $value);
									if (!empty($stepsdata[4])) {
										$filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($stepsdata[4])));
										$type = explode(".", $filename);
										if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
											$img_destitation_path = getcwd() . "/uploads/recipesteps/" . date('Y') . '-' . $filename;
											if (!copy(trim($stepsdata[4]), $img_destitation_path)) {
												$stepsdata['img_video'] = '';
											} else {
												$stepsdata['img_video'] = 'uploads/recipesteps/' . date('Y') . '-' . $filename;
											}
										} else {

											$stepsdata['img_video'] = '';
										}
									}

									if (count($stepsdata) > 1) {
										$steps = array(
											'recipe_id' => $checkAll->id,
											'step_no' => trim($stepsdata[0]),
											'step_description' => trim($stepsdata[1]),
											'step_description_ar' => trim($stepsdata[2]),
											'step_description_fr' => trim($stepsdata[3]),
											'image' => $stepsdata['img_video'],
										);
										//print_r($steps);
										$this->db->insert('recipe_steps', $steps);
									}
								}

							}

							if (count($ingredients) >= 1 && !empty($ingredients[0])) {
								$masterArray = array();
								foreach ($ingredients as $key => $value) {
									$ingdata = explode("|", $value);
									//print_r($key);
									if (count($ingdata) > 1) {
										$checking = $this->Ingredients_model->checkIngredient(trim($ingdata[0]));
										if (count($checking) == 0) {
											$this->db->insert('ingredients', array('name' => $ingdata[0]));
											$ing_id = $this->db->insert_id(); // Get last id
											$checking->id = $ing_id;
										}
										$checking->qty = $ingdata[1];

										$masterArray[$key]['recipe_id'] = $checkAll->id;
										$masterArray[$key]['ingredient_id'] = $checking->id;
										$masterArray[$key]['qty'] = $checking->qty;

										if (count($checking) > 0) {
											$checkunit = $this->Units_model->checkUnit(trim($ingdata[2]));

											if (count($checkunit) == 0) {
												$this->db->insert('units', array('unit_name' => $ingdata[2]));
												$unit_id = $this->db->insert_id(); // Get last id
												//echo $unit_id;
												$checkunit->id = $unit_id;
												//$checkunit->unit_gram = 0;
											}
											$masterArray[$key]['finalunit_id'] = $checkunit->id;
											$kcal[$key] = $this->calculateCalory('kcal', $checking->qty, $checking->kcal_grams, $checkunit->unit_gram);
											$protein[$key] = $this->calculateCalory('protein', $checking->qty, $checking->protein, $checkunit->unit_gram);
											$fat[$key] = $this->calculateCalory('fat', $checking->qty, $checking->fat, $checkunit->unit_gram);
											$carbs[$key] = $this->calculateCalory('carbs', $checking->qty, $checking->carbs, $checkunit->unit_gram);
											$masterArray[$key]['measure_amount'] = $kcal[$key]['total_calory'];
											$masterArray[$key]['protein_amount'] = $protein[$key]['total_protein'];
											$masterArray[$key]['fat_amount'] = $fat[$key]['total_fat'];
											$masterArray[$key]['carbs_amount'] = $carbs[$key]['total_carbs'];
											$masterArray[$key]['measure_amount_ounce'] = $kcal[$key]['total_calory_ounce'];
											$masterArray[$key]['protein_amount_ounce'] = $protein[$key]['total_protein_ounce'];
											$masterArray[$key]['fat_amount_ounce'] = $fat[$key]['total_fat_ounce'];
											$masterArray[$key]['carbs_amount_ounce'] = $carbs[$key]['total_carbs_ounce'];

										}
									}
								}
								$this->db->insert_batch('master_recipeingredients', $masterArray);
								/************************* Total calory stored **********/
								$totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$checkAll->id}")->row()->maxid;

								$totalcalory_ounce = $this->db->query("SELECT SUM(measure_amount_ounce) AS maxid FROM master_recipeingredients WHERE recipe_id = {$checkAll->id}")->row()->maxid;

								$caldata = array('total_calory' => round($totalcalory, 2), 'total_calory_ounce' => round($totalcalory_ounce, 2));

								$this->db->where('id', $checkAll->id);
								$this->db->update('recipes', $caldata);
								/******************************** END *********************/
							}
						}
						$this->flash_notification('File import successfully.');
					}
				}
			} else {
				$this->session->set_flashdata("error", "* File is empty.");
			}
		}
		redirect(base_url() . 'admin/recipes');
	}
	/**
	 * Steps remove of recipes
	 *
	 * @param      integer  $id     The identifier
	 */
	function row_deletesteps($id) {
		$this->db->where('recipe_id', $id);
		$this->db->delete('recipe_steps');
	}

	/**
	 * Calculates the calory.
	 *
	 * @param      string  $qty       The qty
	 * @param      string  $ingdata   The ingdata
	 * @param      string  $unitdata  The unitdata
	 */
	public function calculateCalory($name = '', $qty = '', $ingdata = '', $unitdata = '') {

		$nutrientGrams = $qty * $unitdata;

		if ($name == 'kcal') {
			$nutrientPerServing = ($ingdata * $nutrientGrams) / 100.0;
			$nutrientPerServing_ounce = $nutrientPerServing / 28.35;

		} else if ($name == 'fat') {
			$nutrientPerServingfat = ($ingdata * $nutrientGrams) / 100.0;
			$nutrientPerServingfat_ounce = $nutrientPerServingfat / 28.35;

		} else if ($name == 'carbs') {
			$nutrientPerServingcarbs = ($ingdata * $nutrientGrams) / 100.0;
			$nutrientPerServingcarbs_ounce = $nutrientPerServingcarbs / 28.35;

		} else if ($name == 'protein') {
			$nutrientPerServingprotine = ($ingdata * $nutrientGrams) / 100.0;
			$nutrientPerServingprotine_ounce = $nutrientPerServingprotine / 28.35;
		}

		$calories = array(
			'total_calory' => round($nutrientPerServing, 2),
			'total_protein' => round($nutrientPerServingprotine, 2),
			'total_fat' => round($nutrientPerServingfat, 2),
			'total_carbs' => round($nutrientPerServingcarbs, 2),
			'total_calory_ounce' => round($nutrientPerServing_ounce, 2),
			'total_protein_ounce' => round($nutrientPerServingprotine_ounce, 2),
			'total_fat_ounce' => round($nutrientPerServingfat_ounce, 2),
			'total_carbs_ounce' => round($nutrientPerServingcarbs_ounce, 2),
		);

		return $calories;
	}

	/**
	 *  Remove bulk recipes delete ajax
	 */
	function deleteRecipes() {
		$data_ids = $_REQUEST['data_ids'];
		$data_id_array = explode(",", $data_ids);

		if (!empty($data_id_array)) {
			foreach ($data_id_array as $id) {
				$this->db->query("DELETE FROM recipes WHERE id='$id';");
			}
		}
	}

	/**
	 *  Recipes excel Import
	 */
	function recipeImportExcel_01032017() {

		if ($_POST) {

			$objPHPExcel = PHPExcel_IOFactory::load($_FILES["file"]["tmp_name"]);

			$cell_collection = $objPHPExcel->getActiveSheet()->getCellCollection();

			foreach ($cell_collection as $cell) {
				$column = $objPHPExcel->getActiveSheet()->getCell($cell)->getColumn();
				$row = $objPHPExcel->getActiveSheet()->getCell($cell)->getRow();
				$data_value = $objPHPExcel->getActiveSheet()->getCell($cell)->getValue();
				//header will/should be in row 1 only.
				if ($row == 1) {
					$header[$row][$column] = $data_value;
				} else {
					$arr_data[$row][$column] = $data_value;
				}
			}

			if (count($arr_data) > 0) {

				foreach ($arr_data as $key => $value) {

					//print_r($value);
					$recipes = array();

					$recipes['recipe_name'] = $this->removeHtmlCharacter(preg_replace('#<script(.*?)>(.*?)</script>#is', '', trim($value[A])));
					$recipes['recipe_name_ar'] = $this->removeHtmlCharacter(trim($value[B]));
					$recipes['recipe_name_fr'] = $this->removeHtmlCharacter(trim($value[C]));
					$recipes['recipe_description'] = $this->removeHtmlCharacter(trim($value[E]));
					$recipes['recipe_description_ar'] = $this->removeHtmlCharacter(trim($value[F]));
					$recipes['recipe_description_fr'] = $this->removeHtmlCharacter(trim($value[G]));
					$recipes['serving'] = $this->removeHtmlCharacter(preg_replace("/[^0-9]/", "", trim($value[I])));
					$recipes['preparation_time'] = $this->removeHtmlCharacter(preg_replace("/[^0-9]/", "", trim($value[K])));
					$recipes['cooking_time'] = $this->removeHtmlCharacter(preg_replace("/[^0-9]/", "", trim($value[L])));
					$recipes['is_featured'] = $this->removeHtmlCharacter(trim($value[M]));
					//$recipes[$i]['recipe_category_id'] = $csv_line[13];

					$methods = explode(";", $this->removeHtmlCharacter($value[H]));
					$ingredients = explode(";", $this->removeHtmlCharacter($value[J]));

					$check = $this->Recipe_categories_model->checkCategory($this->removeHtmlCharacter(trim($value[N]))); // check category

					$checkSubcategory = $this->Recipe_subcategories_model->checkSubCategory($this->removeHtmlCharacter(trim($value[O]))); //check subcategory

					if (count($check) == 0) {
						$this->db->insert('recipe_categories', array('cat_name' => $this->removeHtmlCharacter($value[N])));
						$check->id = $this->db->insert_id(); // Get last id

						$this->db->insert('recipe_subcategories', array('sub_cate_name' => $this->removeHtmlCharacter($value[O]), 'recipe_category_id' => $check->id));
						$checkSubcategory->id = $this->db->insert_id(); // Get last id
					} else {

						if (count($checkSubcategory) == 0) {
							$this->db->insert('recipe_subcategories', array('sub_cate_name' => $this->removeHtmlCharacter($value[O]), 'recipe_category_id' => $check->id));
							$checkSubcategory->id = $this->db->insert_id(); // Get last i
						}
					}
					//$ing_csv['count'] = count($check);
					$recipes['recipe_category_id'] = $check->id;
					$recipes['recipe_subcategory_id'] = $checkSubcategory->id;

					$recipes['source_title'] = $this->removeHtmlCharacter($value[P]);
					$recipes['source_link'] = $this->removeHtmlCharacter($value[Q]);

					//echo '<pre>';
					$checkAll = $this->db->get_where('recipes',
						array(
							'recipe_name' => trim($recipes['recipe_name']),
						)
					)->row(); // Check reicpes name exists

					if (!$checkAll) {
						// Insert in reicpes table
						if (!empty($this->removeHtmlCharacter($value[D]))) {

							$filename = preg_replace('/[^A-Za-z0-9.]/', "", $this->removeHtmlCharacter(trim(basename($value[D]))));

							$type = explode(".", $filename);
							if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg'))) {
								$img_destitation_path = getcwd() . "/uploads/recipes/" . date('Y') . '-' . $filename;
								if (!copy($this->removeHtmlCharacter(trim($value[D])), $img_destitation_path)) {
									$recipes['recipe_media'] = '';
								} else {
									$recipes['recipe_media'] = base_url('uploads/recipes') . '/' . date('Y') . '-' . $filename;
								}
							} else {

								$recipes['recipe_media'] = '';
							}

						}

						$this->db->insert('recipes', $recipes);
						$insert_id = $this->db->insert_id(); // Get last id

						if (count($methods) >= 1) {

							foreach ($methods as $key => $value) {

								$stepsdata = explode("|", $value);

								if (!empty($stepsdata[4])) {
									$filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($stepsdata[4])));
									$type = explode(".", $filename);
									if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
										$img_destitation_path = getcwd() . "/uploads/recipesteps/" . date('Y') . '-' . $filename;
										if (!copy(trim($stepsdata[4]), $img_destitation_path)) {
											$stepsdata['img_video'] = '';
										} else {
											$stepsdata['img_video'] = 'uploads/recipesteps/' . date('Y') . '-' . $filename;
										}
									} else {

										$stepsdata['img_video'] = '';
									}
								}
								if (count($stepsdata) > 1) {
									$steps = array(
										'recipe_id' => $insert_id,
										'step_no' => trim($stepsdata[0]),
										'step_description' => trim($stepsdata[1]),
										'step_description_ar' => trim($stepsdata[2]),
										'step_description_fr' => trim($stepsdata[3]),
										'image' => $stepsdata['img_video'],
										//'image'	=> $stepsdata[1]
									);
									//print_r($steps);
									$this->db->insert('recipe_steps', $steps);
								}
							}

						}

						if (count($ingredients) >= 1 && !empty($ingredients[0])) {
							$masterArray = array();
							foreach ($ingredients as $key => $value) {
								$ingdata = explode("|", $value);
								//print_r($key);
								if (count($ingdata) > 1) {
									$checking = $this->Ingredients_model->checkIngredient(trim($ingdata[0]));
									if (count($checking) == 0) {
										$this->db->insert('ingredients', array('name' => $ingdata[0]));
										$ing_id = $this->db->insert_id(); // Get last id
										$checking->id = $ing_id;
									}
									$checking->qty = $ingdata[1];

									$masterArray[$key]['recipe_id'] = $insert_id;
									$masterArray[$key]['ingredient_id'] = $checking->id;
									$masterArray[$key]['qty'] = $checking->qty;

									if (count($checking) > 0) {
										$checkunit = $this->Units_model->checkUnit(trim($ingdata[2]));

										if (count($checkunit) == 0) {
											$this->db->insert('units', array('unit_name' => $ingdata[2]));
											$unit_id = $this->db->insert_id(); // Get last id
											$checkunit->id = $unit_id;
											//$checkunit->unit_gram = 0;
										}
										$masterArray[$key]['finalunit_id'] = $checkunit->id;
										$kcal[$key] = $this->calculateCalory('kcal', $checking->qty, $checking->kcal_grams, $checkunit->unit_gram);
										$protein[$key] = $this->calculateCalory('protein', $checking->qty, $checking->protein, $checkunit->unit_gram);
										$fat[$key] = $this->calculateCalory('fat', $checking->qty, $checking->fat, $checkunit->unit_gram);
										$carbs[$key] = $this->calculateCalory('carbs', $checking->qty, $checking->carbs, $checkunit->unit_gram);
										$masterArray[$key]['measure_amount'] = $kcal[$key]['total_calory'];
										$masterArray[$key]['protein_amount'] = $protein[$key]['total_protein'];
										$masterArray[$key]['fat_amount'] = $fat[$key]['total_fat'];
										$masterArray[$key]['carbs_amount'] = $carbs[$key]['total_carbs'];
									}
								}
							}
							$this->db->insert_batch('master_recipeingredients', $masterArray);
							/************************* Total calory stored **********/
							$totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$insert_id}")->row()->maxid;

							$caldata = array('total_calory' => $totalcalory);

							$this->db->where('id', $insert_id);
							$this->db->update('recipes', $caldata);
							/******************************** END *********************/
						}

					} else {
						// update
						if (!empty($this->removeHtmlCharacter($value[D]))) {

							$filename = preg_replace('/[^A-Za-z0-9.]/', "", $this->removeHtmlCharacter(trim(basename($value[D]))));

							$type = explode(".", $filename);
							if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg'))) {
								$img_destitation_path = getcwd() . "/uploads/recipes/" . date('Y') . '-' . $filename;
								if (!copy($this->removeHtmlCharacter(trim($value[D])), $img_destitation_path)) {
									$recipes['recipe_media'] = '';
								} else {
									$recipes['recipe_media'] = base_url('uploads/recipes') . '/' . date('Y') . '-' . $filename;
								}
							} else {

								$recipes['recipe_media'] = '';
							}

						}
						// Update recipes table
						$this->db->where('id', $checkAll->id);
						$recipes['updated_at'] = date('Y-m-d H:i:s');
						//print_r($ingcsvdata);
						$this->db->update('recipes', $recipes);

						//delete all step which belongs to current recipe
						//delete all ingredients which belongs to recipes
						$deleted = $this->row_deletesteps($checkAll->id);
						$ingdeleted = $this->Ingredients_model->row_deleteRecipeIngredients($checkAll->id);

						if (count($methods) >= 1) {

							foreach ($methods as $key => $value) {

								$stepsdata = explode("|", $value);
								if (!empty($stepsdata[4])) {
									$filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($stepsdata[4])));
									$type = explode(".", $filename);
									if (in_array($type['1'], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
										$img_destitation_path = getcwd() . "/uploads/recipesteps/" . date('Y') . '-' . $filename;
										if (!copy(trim($stepsdata[4]), $img_destitation_path)) {
											$stepsdata['img_video'] = '';
										} else {
											$stepsdata['img_video'] = 'uploads/recipesteps/' . date('Y') . '-' . $filename;
										}
									} else {

										$stepsdata['img_video'] = '';
									}
								}

								if (count($stepsdata) > 1) {
									$steps = array(
										'recipe_id' => $checkAll->id,
										'step_no' => trim($stepsdata[0]),
										'step_description' => trim($stepsdata[1]),
										'step_description_ar' => trim($stepsdata[2]),
										'step_description_fr' => trim($stepsdata[3]),
										'image' => $stepsdata['img_video'],
									);
									//print_r($steps);
									$this->db->insert('recipe_steps', $steps);
								}
							}

						}

						if (count($ingredients) >= 1 && !empty($ingredients[0])) {
							$masterArray = array();
							foreach ($ingredients as $key => $value) {
								$ingdata = explode("|", $value);
								//print_r($key);
								if (count($ingdata) > 1) {
									$checking = $this->Ingredients_model->checkIngredient(trim($ingdata[0]));
									if (count($checking) == 0) {
										$this->db->insert('ingredients', array('name' => $ingdata[0]));
										$ing_id = $this->db->insert_id(); // Get last id
										$checking->id = $ing_id;
									}
									$checking->qty = $ingdata[1];

									$masterArray[$key]['recipe_id'] = $checkAll->id;
									$masterArray[$key]['ingredient_id'] = $checking->id;
									$masterArray[$key]['qty'] = $checking->qty;

									if (count($checking) > 0) {
										$checkunit = $this->Units_model->checkUnit(trim($ingdata[2]));

										if (count($checkunit) == 0) {
											$this->db->insert('units', array('unit_name' => $ingdata[2]));
											$unit_id = $this->db->insert_id(); // Get last id
											//echo $unit_id;
											$checkunit->id = $unit_id;
											//$checkunit->unit_gram = 0;
										}
										$masterArray[$key]['finalunit_id'] = $checkunit->id;
										$kcal[$key] = $this->calculateCalory('kcal', $checking->qty, $checking->kcal_grams, $checkunit->unit_gram);
										$protein[$key] = $this->calculateCalory('protein', $checking->qty, $checking->protein, $checkunit->unit_gram);
										$fat[$key] = $this->calculateCalory('fat', $checking->qty, $checking->fat, $checkunit->unit_gram);
										$carbs[$key] = $this->calculateCalory('carbs', $checking->qty, $checking->carbs, $checkunit->unit_gram);
										$masterArray[$key]['measure_amount'] = $kcal[$key]['total_calory'];
										$masterArray[$key]['protein_amount'] = $protein[$key]['total_protein'];
										$masterArray[$key]['fat_amount'] = $fat[$key]['total_fat'];
										$masterArray[$key]['carbs_amount'] = $carbs[$key]['total_carbs'];

									}
								}
							}
							$this->db->insert_batch('master_recipeingredients', $masterArray);
							/************************* Total calory stored **********/
							$totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$checkAll->id}")->row()->maxid;

							$caldata = array('total_calory' => $totalcalory);

							$this->db->where('id', $checkAll->id);
							$this->db->update('recipes', $caldata);
							/******************************** END *********************/
						}
					}
				}
				$this->flash_notification('File import successfully.');
			} else {
				$this->session->set_flashdata("error", "* File is empty.");
			}
		}
		redirect(base_url() . 'admin/recipes');
	}

	function change_dateformat() {
		error_reporting(0);
		$date = $_POST['getdate'];
		$days = $_POST['days'];
		$days = $days . " days";
		$date = date_create($_POST['getdate']);
		date_add($date, date_interval_create_from_date_string($days));
		$date_final = date_format($date, "Y-m-d");
		echo date_formats($date_final);

	}
}