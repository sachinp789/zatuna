<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Csvimport extends MY_Controller {

    /**
     * Constructor
     */
    function __construct() {
        parent::__construct();
        $this->load->model('admin/Recipe_categories_model');
        $this->load->model('admin/Ingredient_categories_model');
        //$this->load->library('csvimport');
        if(!$this->session->userdata('admin_id'))
        {
            redirect(base_url().'admin/login');
        }
    }

    function index(){
        $this->data['title'] = 'Import Category';
        $this->data['page'] = 'import';
        $this->__template('admin/import/index', $this->data);
    }
    // Import recipes and ingredient category
    function csvImport(){

        $config['upload_path'] = FCPATH.'uploads/csv';
        $config['allowed_types'] = 'csv';
        $config['max_size'] = '1000';
 
        $count=0;
        $recp_csv = array();
        $ing_csv = array();
        $fp = fopen($_FILES['csv_file']['tmp_name'],'r') or die("can't open file");

        //echo "dsdds".empty($_FILES);
        //print_r($_FILES['csv_file']);exit;

        if($_FILES["csv_file"]["size"] > 1){
          
        while($csv_line = fgetcsv($fp,1024))
        {
            $count++;
            if($count == 1)
            {
                continue;
            }//keep this if condition if you want to remove the first row
            for($i = 0, $j = count($csv_line); $i < $j; $i++)
            {
                //$insert_csv['id'] = $csv_line[0];//remove if you want to have primary key,
                $recp_csv['cat_name'] = $csv_line[0];
                $recp_csv['status'] = $csv_line[1];

                $ing_csv['name'] = $csv_line[2];
                $ing_csv['status'] = $csv_line[3];
            }
            $i++;
            $recpdata = array(
                //'id' => $insert_csv['id'] ,
                'cat_name' => $recp_csv['cat_name'],
                'status' => $recp_csv['status']
            );

            $ingdata = array(
                //'id' => $insert_csv['id'] ,
                'name' => $ing_csv['name'],
                'status' => $ing_csv['status']
            );

           $this->db->insert('recipe_categories', $recpdata); // Recipes categories
           $this->db->insert('ingredient_categories', $ingdata); // Ingredients categories
            }
         fclose($fp) or die("can't close file");
         //$data['success']="success";
         $this->flash_notification('CSV import successfully.'); 
        }
        else{
          $this->flash_notification('File is empty.');   
        }    
        redirect(base_url().'admin/csvimport/');
    }

    // Recipies sub caregories import
    function recipeSubcategory(){

        $count=0;
        $recp_sub_csv = array();
        $fp = fopen($_FILES['recipe_csv_file']['tmp_name'],'r') or die("can't open file");

        //echo "dsdds".empty($_FILES);
        //print_r($_FILES['csv_file']);exit;

        if($_FILES["recipe_csv_file"]["size"] > 1){
          
        while($csv_line = fgetcsv($fp,1024))
        {
            $count++;
            if($count == 1)
            {
                continue;
            }//keep this if condition if you want to remove the first row
            for($i = 0, $j = count($csv_line); $i < $j; $i++)
            {
                //$insert_csv['id'] = $csv_line[0];//remove if you want to have primary key,
                $recp_sub_csv['sub_cat_name'] = $csv_line[0];
                $recp_sub_csv['recp_cat_id'] = $csv_line[1];
                $recp_sub_csv['active'] = $csv_line[2];
            }
            $i++;
            $recpsubcatdata = array(
                //'id' => $insert_csv['id'] ,
                'sub_cate_name' => $recp_sub_csv['sub_cat_name'],
                'recipe_category_id'    => $recp_sub_csv['recp_cat_id'],
                'status' => $recp_sub_csv['active']
            );

           $this->db->insert('recipe_subcategories', $recpsubcatdata); // Recipes sub categories
            }
         fclose($fp) or die("can't close file");
         //$data['success']="success";
         $this->flash_notification('CSV import successfully.'); 
        }
        else{
          $this->flash_notification('File is empty.');   
        }    
        redirect(base_url().'admin/csvimport/');
    }   
}