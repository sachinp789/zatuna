<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Notification extends MY_Controller {

	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		lang_switcher($this->session->userdata('siteLang'));
		//$this->load->helper('pushnotification_helper');
		$this->load->model('admin/Notifications_model');
		$this->load->model('admin/Users_model');
		lang_switcher($this->session->userdata('siteLang'));
		if (!$this->session->userdata('admin_id')) {
			redirect(base_url() . 'admin/login');
		}
	}

	/**
	 * Index
	 */
	function index() {
		$this->data['title'] = $this->lang->line('page_title_index');
		$this->data['page'] = 'notification';
		$this->data['notifications'] = $this->Notifications_model->getNotifications();
		// $this->data['recipes'] = $this->Recipes_model->order_by_column('id');
		$this->__template('admin/notifications/index', $this->data);
	}
	/**
	 * Create Notification
	 */
	function create() {

		if ($_POST) {

			//Check whether user upload picture
			/*if (!empty($_FILES['device_media']['name'])) {
				$config['upload_path'] = 'uploads/notifications/';
				$config['allowed_types'] = 'jpg|jpeg|png|gif';

				$fileName = preg_replace('/[^A-Za-z0-9.]/', "", $_FILES['device_media']['name']); // Name replace

				$config['file_name'] = $fileName;

				//Load upload library and initialize configuration
				$this->load->library('upload', $config);
				$this->upload->initialize($config);

				if ($this->upload->do_upload('device_media')) {
					$uploadData = $this->upload->data();
					$picture = $uploadData['file_name'];
				} else {
					$picture = '';
				}
			} else {
				$picture = '';
			}*/

			//Prepare array of user data
			$notificationData = array(
				'notify_name' => $this->input->post('device_title'),
				'status' => $this->input->post('notification_status'),
			);

			//Pass user data to model
			$insertNotificationData = $this->Notifications_model->insert($notificationData);

			//Storing insertion status message.
			if ($insertNotificationData) {
				$this->flash_notification('Notification is successfully added.');
				redirect(base_url() . 'admin/notification/');
			}
		}

		$this->data['title'] = $this->lang->line('page_title_add');
		$this->data['page'] = 'create';
		$this->__template('admin/notifications/create', $this->data);
	}

	// Get notification list from id
	function edit($notify_id = '') {

		if ($notify_id) {
			$this->data['title'] = $this->lang->line('page_title_edit');
			$this->data['page'] = 'edit';
			$this->data['notify'] = $this->Notifications_model->get($notify_id);
			$this->__template('admin/notifications/edit', $this->data);
		}
	}

	// Updated notification
	function update($notify_id = '') {

		if ($_POST) {
			//Prepare array of user data
			$notificationData = array(
				'notify_name' => $this->input->post('device_title'),
				'status' => $this->input->post('notification_status'),
			);

			//Pass user data to model
			$updateNotificationData = $this->Notifications_model->update($notify_id, $notificationData);

			//Storing insertion status message.
			if ($updateNotificationData) {
				$this->flash_notification('Notification is successfully updated.');
				redirect(base_url() . 'admin/notification/');
			}
		}
	}
	/**
	 * Delete Notification
	 */
	function delete_notification($notify_id = '') {
		if ($notify_id) {
			$this->Notifications_model->delete($notify_id);
			$this->flash_notification('Notification is successfully deleted.');
		}
		redirect(base_url() . 'admin/notification/');
	}

	/**
	 * Display push notification form
	 */
	function send() {

		$this->data['title'] = $this->lang->line('page_send_form');
		$this->data['notifications'] = $this->Notifications_model->getActiveNotifications();
		$this->data['page'] = 'send';
		$this->__template('admin/notifications/send', $this->data);
	}
	/**
	 *  Send notification
	 */
	function sendnotification() {
		if ($_POST) {

			$duration = $this->input->post('time_interval');
			$messagebody = $this->input->post('message');
			$case = $this->input->post('select_activity'); // Activity

			//	$token = $this->db
			$currenttime = date("Y-m-d H:i:s");
			//echo $duration;exit;
			$message = array();

			$tokens = $this->getallTokens();

			switch ($case) {
			case 'last_loggedin':
				if (count($tokens) > 0) {
					$title = 'Your last logged in activity since 15 days.';
					foreach ($tokens as $key => $value) {
						//echo $duration . '<br/>' . $value->last_updated;
						$difference = $this->checkDate($duration, $value->last_updated);

						if ($difference == 1) {
							$message[] = $this->sendPushNotification($value->device_token, $messagebody, $title);
							$this->flash_notification('Notification is send successfully.');
						}
						//$this->flash_notification('No notification send.');
					}
				}
				break;

			case 'special':
				if (isset($_POST['userslist'])) {
					$title = 'Your special recipes are awaiting.';

					foreach ($_POST['userslist'] as $key => $value) {

						$usercheck = $this->Users_model->get_by(array('id' => $value));

						if ($usercheck->device_token) {
							$this->sendPushNotification($usercheck->device_token, $messagebody, $title);
							$this->flash_notification('Notification is send successfully.');
						}
						//$this->flash_notification('No notification send.');
					}
				}
				break;

			default:
				# code...
				break;
			}
			/*
				<input type="number" step="1" min="1" max="" name="quantity" value="1" title="Qty" class="input-text qty text" size="4" pattern="[0-9]*" inputmode="numeric">
			*/
			/*if (in_array(0, $message)) {
					$this->flash_notification('Notification is send successfully.');
				} else {
					$this->flash_notification('No notification send.');
			*/
			redirect(base_url() . 'admin/notification/');
		}
	}
	/**
	 * Push notifcation send code in iphone
	 *
	 * @param      string  $token  The token
	 *
	 * @return     string  ( json )
	 */
	function sendPushNotification($token, $message = NULL, $title = NULL) {
		//$ci = &get_instance(); // CI instance
		$passphrase = '';
		$messagealert = '';
		if (empty($message)) {
			$messagealert = "Your last logged in activity since 15 days.";
		} else {
			$messagealert = $message;
		}
		$ctx = stream_context_create();

		stream_context_set_option($ctx, 'ssl', 'local_cert', FCPATH . 'zatuna_dev.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		// LIVE :- gateway.push.apple.com:2195
		// Staging :- gateway.sandbox.push.apple.com:2195

		$fp = stream_socket_client(
			'ssl://gateway.sandbox.push.apple.com:2195', $error, $errstr, 30, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp) {
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		}

		// Create the payload body
		$body['aps'] = array(
			'alert' => array(
				'title' => $title,
				'body' => $messagealert,
			),
			'sound' => 'default',
		);

		//$body['body'] =
		/*$body['aps'] = array(
			'alert' => $messagealert,
			'sound' => 'default',
		);*/
		//$body['image'] = base_url() . 'uploads/notifications' . $messages[0]->media;
		$payload = json_encode($body);
		$msg = chr(0) . pack('n', 32) . pack('H*', $token) . pack('n', strlen($payload)) . $payload;
		$result = fwrite($fp, $msg, strlen($msg));

		if (!$result) {
			return 0;
		} else {
			return 1;
		}
		fclose($fp);
	}

	/**
	 * Get list of tokens
	 */
	function getallTokens() {
		return $this->db->get('devicetokens')->result();
		//return $this->db->get('devicetokens');
	}

	/*Difference between time*/
	function dateDiff($start, $end) {
		$start_ts = strtotime($start);
		$end_ts = strtotime($end);
		$diff = $end_ts - $start_ts;
		return round($diff / 86400);
	}
	/**
	 *  Check current date is less than previous date
	 */
	function checkDate($inputdate, $existdate) {
		$idate = strtotime($inputdate);
		$exdate = strtotime($existdate);

		//echo $idate . '==' . $exdate;exit;

		if ($exdate < $idate) {
			return 1;
		} else {
			return 0;
		}
	}

}
