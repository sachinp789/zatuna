<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Levels extends MY_Controller {

	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		$this->load->model('admin/Levels_model');
		if (!$this->session->userdata('admin_id')) {
			redirect(base_url() . 'admin/login');
		}
	}

	/**
	 * Index
	 */
	function index() {
		$this->data['title'] = 'Level';
		$this->data['page'] = 'level';
		$this->data['levels'] = $this->Levels_model->getallLevels();
		$this->__template('admin/level/index', $this->data);
	}

	/**
	 * Create action
	 */
	function create() {
		if ($_POST) {

			$levels = array(
				'level_type' => trim($_POST['type']),
				'level_type_ar' => trim($_POST['type_ar']),
				'level_type_fr' => trim($_POST['type_fr']),
				'status' => $_POST['lvstatus'],
			);
			$this->Levels_model->insert($levels);
			$this->flash_notification('Level is successfully added.');
			redirect(base_url() . 'admin/levels/');
		}
		$this->data['title'] = 'Add levels';
		$this->data['page'] = 'create';
		$this->__template('admin/level/create', $this->data);

	}

	// Edit Level
	function edit($level_id = '') {
		if ($level_id) {
			$this->data['title'] = 'Edit Levels';
			$this->data['page'] = 'levels';
			$this->data['levelslist'] = $this->Levels_model->get($level_id);
			$this->data['param2'] = '';
			$this->__template('admin/level/edit', $this->data);
		}
		redirect(base_url() . 'admin/levels/');
	}

	/**
	 * Update level
	 * @param string $id
	 */
	function update($levelid = '') {
		if ($_POST) {

			$this->Levels_model->update($levelid, array(
				'level_type' => trim($_POST['type']),
				'level_type_ar' => trim($_POST['type_ar']),
				'level_type_fr' => trim($_POST['type_fr']),
				'status' => $_POST['lvstatus'],
				'updated_at' => date('Y-m-d H:i:s'),
			));
			$this->flash_notification('Level is successfully updated.');
		}
		redirect(base_url() . 'admin/levels/');
	}

	/**
	 * Delete recipe details
	 * @param string $recipe_id
	 *
	 */
	function delete_level($level_id = '') {
		if ($level_id) {
			//echo $level_id;exit;
			$this->Levels_model->delete($level_id);
			$this->flash_notification('level is successfully deleted.');
		}
		redirect(base_url() . 'admin/levels/');
	}

	/*** Level status update ***/
	function update_status() {
		$status = $_POST['status'];
		$id = $_POST['id'];
		$this->Levels_model->update($id, array('status' => $status));
	}

}