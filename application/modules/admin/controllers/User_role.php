<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User_role extends MY_Controller {

	function __construct(){
		 parent::__construct();
		 $this->load->model('admin/User_roles_model');		
         $this->load->model('admin/Permissions_model');      
	}

	// Listing of user with permission
	function index() {

		$rolesKey = array();
		$this->data['title'] = "User Role";
        $this->data['page'] = "role";
        $this->data['users'] = $this->User_roles_model->getRolesPermission();
        //$usersroles = $this->User_roles_model->getRolesPermission();
        /*$names = array();
        foreach ($usersroles as $key => $value) {
            
                $names[] = $this->User_roles_model->getUniquename($value->role_name);
        }
        $this->data['users'] = $names;*/

        $this->__template('admin/user/role', $this->data);
	}

	// Create user roles
	function create() {  

        if($_POST)
        {   

            for($i=0;$i<count($_POST['roles']);$i++){

            	$roles = array(
                    "role_name"      =>$_POST['role_name'],
                    "permission_id"  =>$_POST['roles'][$i]
                );
                $this->User_roles_model->insert($roles);
            }
            $this->flash_notification('User role is successfully added.');
            redirect(base_url('admin/user_role'));
        }
    }

    // User role deleted
    function delete_role($user_id){
        $Where = explode('|',urldecode($user_id));
        $this->User_roles_model->deleteRole($Where);
        $this->flash_notification("User role removed successfully.");
        redirect(base_url() . 'admin/user_role/', 'refresh');
    }

    // User role update
    function update($user_id){
            $users = explode('|',urldecode($user_id));
            $roles = array();

            if($_POST) {
            $i=0;
            $perms = array();
            /*$perms = array("Mac", "NT", "Irix", "Linux");
            print_r($perms);exit;*/
            foreach ($_POST['roles'] as $key => $value) {
                   
                   $exists = $this->User_roles_model->checkPermission($users,$_POST['roles']);

                   $perms[$i] = $exists[$i]['permission_id'];
                   
                   if(in_array($value, $perms)){
                        $rolesupdate = array(
                            "role_name"      =>$_POST['role_name'],
                            "permission_id"  =>$value,
                            'updated_at'     => date('Y-m-d H:i:s')
                        );
                        $this->User_roles_model->updateRolePermission($rolesupdate,$users);
                   }
                   else{
                      $roles = array(
                            "role_name"      =>$_POST['role_name'],
                            "permission_id"  =>$value,
                            'updated_at'     => date('Y-m-d H:i:s')
                        );
                      $this->User_roles_model->insert($roles); 
                   }

            $i++;
            }
            $this->flash_notification('User role successfully updated.');
        }
        redirect(base_url('admin/user_role'));
    }
}