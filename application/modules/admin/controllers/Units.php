<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Units extends MY_Controller {

	function __construct() {
		parent::__construct();
		lang_switcher($this->session->userdata('siteLang'));
		$this->load->model('admin/Units_model');
		$this->load->model('admin/Ingredients_model');
		//$this->load->helper('language');
		$this->load->helper('text');
		date_default_timezone_set('Asia/Kolkata');
		if ($this->session->userdata('admin_login') != 1) {
			redirect(base_url() . 'admin/login');
		}
	}
	/*********** Stored Procedure **************/
	function add() {

		$this->db->trans_start();
		$sql = "CALL addUnit('data',@insert_id)";
		$this->db->query($sql);
		$query = $this->db->query("SELECT @insert_id as my_id");
		$this->db->trans_complete();
		$query = $query->row();
		echo $query->my_id;

		/*$my_id = 0;
			foreach ($query->result_array() as $row) {
				$my_id = $row['my_id'];
		*/

		//	print_r($my_id);
	}
	/*********************** END *******************/
	// List of units
	function index() {
		$this->data['title'] = $this->lang->line('unit_index');
		$this->data['page'] = "unit";
		$this->data['unit_list'] = $this->Units_model->getUnitList();
		$this->__template('admin/unit/index', $this->data);
	}

	// Add Units
	function create() {
		if ($_POST) {

			$units = array(
				"unit_name" => trim($_POST['unit_name']),
				'unit_name_ar' => trim($_POST['unit_name_ar']),
				'unit_name_fr' => trim($_POST['unit_name_fr']),
				'unit_gram' => trim($_POST['unit_amount']),
				"unit_name_us" => trim($_POST['unit_name_us']),
				'unit_name_us_ar' => trim($_POST['unit_name_us_ar']),
				'unit_name_us_fr' => trim($_POST['unit_name_us_fr']),
				'unit_ounce' => trim($_POST['unit_amount_us']),
				"status" => $_POST['unit_status'],
			);

			//print_r($units);exit;

			if ($this->Units_model->insert($units)):
				$this->flash_notification('Unit is successfully added.');
			else:
				$this->flash_notification('Error while unit creating.');
			endif;
			redirect(base_url() . 'admin/units');
		}
		$this->data['title'] = $this->lang->line('unit_add');
		$this->data['page'] = "addunit";
		$this->__template('admin/unit/create', $this->data);
	}

	// Edit List of Units
	function edit($unitid) {
		if ($unitid) {
			$this->data['title'] = $this->lang->line('unit_edit');
			$this->data['page'] = 'editunit';
			$this->data['units'] = $this->Units_model->get($unitid);
			$this->__template('admin/unit/edit', $this->data);
		}
	}

	// Update unit data
	function update($unitid) {

		if ($_POST['unit_status'] == 0) {
			$checkExists = $this->Units_model->checkDependency($unitid); // Check dependency on inactive status
			if ($checkExists == 1) {
				$this->session->set_flashdata('error', '* Unit already used in another table.');
			} else {
				$this->Units_model->update($unitid, array(
					'unit_name' => trim($_POST['unit_name']),
					'unit_name_ar' => trim($_POST['unit_name_ar']),
					'unit_name_fr' => trim($_POST['unit_name_fr']),
					'unit_gram' => trim($_POST['unit_amount']),
					"unit_name_us" => trim($_POST['unit_name_us']),
					'unit_name_us_ar' => trim($_POST['unit_name_us_ar']),
					'unit_name_us_fr' => trim($_POST['unit_name_us_fr']),
					'unit_ounce' => trim($_POST['unit_amount_us']),
					'status' => $_POST['unit_status'],
					'updated_at' => date('Y-m-d H:i:s'),
				));
				$this->flash_notification('Unit is successfully updated.');
			}
		} else {
			$this->Units_model->update($unitid, array(
				'unit_name' => trim($_POST['unit_name']),
				'unit_name_ar' => trim($_POST['unit_name_ar']),
				'unit_name_fr' => trim($_POST['unit_name_fr']),
				'unit_gram' => trim($_POST['unit_amount']),
				"unit_name_us" => trim($_POST['unit_name_us']),
				'unit_name_us_ar' => trim($_POST['unit_name_us_ar']),
				'unit_name_us_fr' => trim($_POST['unit_name_us_fr']),
				'unit_ounce' => trim($_POST['unit_amount_us']),
				'status' => $_POST['unit_status'],
				'updated_at' => date('Y-m-d H:i:s'),
			));
			$this->flash_notification('Unit is successfully updated.');
		}
		redirect(base_url() . 'admin/units');
	}

	/**
	 * Delete unit
	 * @param string $unit_id
	 *
	 */
	function delete_unit($unit_id = '') {
		$checkExists = $this->Units_model->checkDependency($unit_id); // Check dependency on delete unit

		if ($checkExists == 1) {
			$this->session->set_flashdata('error', '* Unit already used in another table.');
		} else {
			if ($unit_id) {
				//echo $level_id;exit;
				$this->Units_model->delete($unit_id);
				$this->flash_notification('Unit is successfully deleted.');
			}
		}

		redirect(base_url() . 'admin/units/');
	}

	// Get ingridents unit by ajax call in recipes add
	function getIngUnits($id) {
		$unitList = $this->Ingredients_model->getIngredientUnitbyId($id);
		echo json_encode($unitList);
	}

	// Get unit amount
	function getUnitamount($id) {
		$unitValue = $this->Units_model->get($id);
		echo json_encode($unitValue);
	}

	/**
	 *  Unit check exists or not
	 */

	function checkUnit() {
		//print_r($_POST);exit;
		$isExists = $this->Units_model->checkUnitName(trim($_POST['unit_name']));

		if ($isExists == 1) {
			echo "false";
		} else {
			echo "true";
		}
	}

	/**
	 *  Unit check exists or not in US metric
	 */

	function checkUnitUS() {
		//print_r($_POST);exit;
		$isExists = $this->Units_model->checkUnitName(trim($_POST['unit_name_us']));

		if ($isExists == 1) {
			echo "false";
		} else {
			echo "true";
		}
	}

}