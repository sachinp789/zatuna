<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recipecategory extends MY_Controller {

	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		lang_switcher($this->session->userdata('siteLang'));
		$this->load->model('admin/Recipe_categories_model');
		//$this->load->model('admin/Recipe_subcategories_model');
		if (!$this->session->userdata('admin_id')) {
			redirect(base_url() . 'admin/login');
		}
	}

	/**
	 * Index
	 */
	function index() {
		$this->data['title'] = $this->lang->line('recp_catindex');
		$this->data['page'] = 'recipes_category';
		$this->data['categories'] = $this->Recipe_categories_model->getallCategories();
		$this->__template('admin/category/recipies/index', $this->data);
	}

	/*** Recipe_categories_model status update ***/
	function update_status() {
		$status = $_POST['status'];
		$id = $_POST['id'];
		$this->Recipe_categories_model->update($id, array('status' => $status));
	}

	/**
	 * Create action
	 */
	function create() {
		if ($_POST) {
			$categories = array(
				'cat_name' => trim($_POST['cat_name']),
				'cat_name_ar' => trim($_POST['cat_name_ar']),
				'cat_name_fr' => trim($_POST['cat_name_fr']),
				'status' => $_POST['rcstatus'],
			);
			$this->Recipe_categories_model->insert($categories);
			$this->flash_notification('Recipe Category is successfully added.');
			redirect(base_url() . 'admin/recipecategory/');
		}
		$this->data['title'] = $this->lang->line('recp_catadd');
		$this->data['page'] = 'create';
		$this->__template('admin/category/recipies/create', $this->data);

	}

	// Edit Recipe Category
	function edit($rcat_id = '') {
		if ($rcat_id) {
			$this->data['title'] = $this->lang->line('recp_catedit');
			$this->data['page'] = 'Recipe category';
			//$this->data['levels'] = $this->Recipe_categories_model->get($rcat_id);
			$this->data['recpcatlist'] = $this->Recipe_categories_model->get($rcat_id);
			$this->data['param2'] = '';
			$this->__template('admin/category/recipies/edit', $this->data);
		}
		redirect(base_url() . 'admin/recipecategory/');
	}

	/**
	 * Update Recipe Category
	 * @param string $rcatid
	 */
	function update($rcatid = '') {
		if ($_POST) {
			if ($_POST['rcstatus'] == 1) {
				$checkExists = $this->Recipe_categories_model->checkDependency($rcatid); // Check dependency on delete category
				if ($checkExists == 1) {
					$this->session->set_flashdata('error', '* Recipe Category already used in another table.');
				} else {
					$this->Recipe_categories_model->update($rcatid, array(
						'cat_name' => trim($_POST['cat_name']),
						'cat_name_ar' => trim($_POST['cat_name_ar']),
						'cat_name_fr' => trim($_POST['cat_name_fr']),
						'status' => $_POST['rcstatus'],
						'updated_at' => date('Y-m-d H:i:s'),
					));
					$this->flash_notification('Recipe Category is successfully updated.');
				}
			} else {
				$this->Recipe_categories_model->update($rcatid, array(
					'cat_name' => trim($_POST['cat_name']),
					'cat_name_ar' => trim($_POST['cat_name_ar']),
					'cat_name_fr' => trim($_POST['cat_name_fr']),
					'status' => $_POST['rcstatus'],
					'updated_at' => date('Y-m-d H:i:s'),
				));
				$this->flash_notification('Recipe Category is successfully updated.');
			}
		}
		redirect(base_url() . 'admin/recipecategory/');
	}

	/**
	 * Delete recipe category
	 * @param string $reccat_id
	 *
	 */
	function delete_recpcat($reccat_id = '') {
		$checkExists = $this->Recipe_categories_model->checkDependency($reccat_id); // Check dependency on delete category
		if ($checkExists == 1) {
			$this->session->set_flashdata('error', '* Recipe Category already used in another table.');
		} else {
			if ($reccat_id) {
				$this->Recipe_categories_model->delete($reccat_id);
				$this->flash_notification('Recipe Category is successfully deleted.');
			}

		}
		redirect(base_url() . 'admin/recipecategory/');
	}

	/**
	 *  Recipe Category check exists or not
	 */

	function checkCategory() {
		//print_r($_POST);exit;
		$isExists = $this->Recipe_categories_model->checkRecipeCategoryName(trim($_POST['cat_name']));

		if ($isExists == 1) {
			echo "false";
		} else {
			echo "true";
		}
	}

}