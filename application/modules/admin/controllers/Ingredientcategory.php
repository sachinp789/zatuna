<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ingredientcategory extends MY_Controller {

	/**
	 * Constructor
	 */
	function __construct() {
		parent::__construct();
		lang_switcher($this->session->userdata('siteLang'));
		$this->load->model('admin/Ingredient_categories_model');
		if (!$this->session->userdata('admin_id')) {
			redirect(base_url() . 'admin/login');
		}
	}

	/**
	 * Index
	 */
	function index() {
		$this->data['title'] = $this->lang->line('ing_catindex');
		$this->data['page'] = 'ingredient_category';
		$this->data['categories'] = $this->Ingredient_categories_model->getallCategories();
		$this->__template('admin/category/ingredients/index', $this->data);
	}

	/*** Recipe_categories_model status update ***/
	function update_status() {
		$status = $_POST['status'];
		$id = $_POST['id'];
		$this->Ingredient_categories_model->update($id, array('status' => $status));
	}

	/**
	 * Create action
	 */
	function create() {
		if ($_POST) {
			$ingredients = array(
				'name' => trim($_POST['cat_name']),
				'name_ar' => trim($_POST['cat_name_ar']),
				'name_fr' => trim($_POST['cat_name_fr']),
				'status' => $_POST['ingstatus'],
			);
			$this->Ingredient_categories_model->insert($ingredients);
			$this->flash_notification('Ingredient Category is successfully added.');
			redirect(base_url() . 'admin/ingredientcategory/');
		}
		$this->data['title'] = $this->lang->line('ing_catadd');
		$this->data['page'] = 'create';
		$this->__template('admin/category/ingredients/create', $this->data);

	}

	// Edit Ingredient Category
	function edit($ingcat_id = '') {
		if ($ingcat_id) {
			$this->data['title'] = $this->lang->line('ing_catedit');
			$this->data['page'] = 'Ingredient category';
			$this->data['ingcatlist'] = $this->Ingredient_categories_model->get($ingcat_id);
			$this->data['param2'] = '';
			$this->__template('admin/category/ingredients/edit', $this->data);
		}
		redirect(base_url() . 'admin/ingredientcategory/');
	}

	/**
	 * Update Ingredient Category
	 * @param string $ingcat_id
	 */
	function update($ingcat_id = '') {

		if ($_POST['ingstatus'] == 1) {
			$checkExists = $this->Ingredient_categories_model->checkDependency($ingcat_id); // Check dependency on inactive status
			if ($checkExists == 1) {
				$this->session->set_flashdata('error', '* Ingredient category already used in another table.');
			} else {
				$this->Ingredient_categories_model->update($ingcat_id, array(
					'name' => trim($_POST['cat_name']),
					'name_ar' => trim($_POST['cat_name_ar']),
					'name_fr' => trim($_POST['cat_name_fr']),
					'status' => $_POST['ingstatus'],
					'updated_at' => date('Y-m-d H:i:s'),
				));
				$this->flash_notification('Unit is successfully updated.');
			}
		} else {
			$this->Ingredient_categories_model->update($ingcat_id, array(
				'name' => trim($_POST['cat_name']),
				'name_ar' => trim($_POST['cat_name_ar']),
				'name_fr' => trim($_POST['cat_name_fr']),
				'status' => $_POST['ingstatus'],
				'updated_at' => date('Y-m-d H:i:s'),
			));
			$this->flash_notification('Ingredient Category is successfully updated.');
		}
		redirect(base_url() . 'admin/ingredientcategory/');
	}

	/**
	 * Delete Ingredient category
	 * @param string $ingcat_id
	 *
	 */
	function delete_ingcat($ingcat_id = '') {

		$checkExists = $this->Ingredient_categories_model->checkDependency($ingcat_id); // Check dependency on delete category
		if ($checkExists == 1) {
			$this->session->set_flashdata('error', '* Ingredient category already used in another table.');
		} else {
			if ($ingcat_id) {
				//echo $recipe_id;exit;
				$this->Ingredient_categories_model->delete($ingcat_id);
				$this->flash_notification('Ingredient Category is successfully deleted.');
			}
		}

		redirect(base_url() . 'admin/ingredientcategory/');
	}

	/**
	 *  Ingredients category check exists or not
	 */

	function checkIngredientCategory() {
		//print_r($_POST);exit;
		$isExists = $this->Ingredient_categories_model->checkIngCategory(trim($_POST['ingredients_name']));

		if ($isExists == 1) {
			echo "false";
		} else {
			echo "true";
		}
	}
}