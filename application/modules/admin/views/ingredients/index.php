<section class="content">
<div class=box>
<div class="box-body">
<form action="<?php echo base_url(); ?>admin/ingredients/importCSV" method="post" name="upload_excel" enctype="multipart/form-data" id="csvform">
    <div class="col-md-12">
        <div class="form-group">
            <label class="col-sm-2 control-label">Import CSV</label>
            <div class="col-sm-4">
                <input type="file" name="file" id="file" class="form-control" required><br/>
                <?php if ($this->session->flashdata('error')) {?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php }?>
            </div>
            <div class="col-sm-2">
                <input type="submit" name="import" class="btn btn-default">
            </div>
            <div class="col-sm-3">
                 <?php if (file_exists(FCPATH . 'sample-ingredient.csv')): ?>
                     <a href="<?php echo base_url(); ?>sample-ingredient.csv" download="">Download Sample
                    </a>
                <?php endif;?>
            </div>
            </div>
    </div>
</form>
</div>
<div class="clear"></div>
<hr>
<div class=box-body>
 <?php if ($this->session->flashdata('erroring')) {?>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('erroring'); ?></div>
            <?php }?>
<p>
<a href="<?php echo base_url() ?>admin/ingredients/create" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $this->lang->line('btncreate'); ?></a>
</p>
<!-- <a data-placement="top" data-toggle="tooltip" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_ingredients_create');" href="#" class="links"><i class="fa fa-plus"></i>Ingredients</a> -->

<div id="getresponse">
<table id="datatable-list-14" class="table table-striped table-bordered table-responsive" cellspacing=0 width=100%>
<thead>
    <tr>
        <th>ID</th>
        <th>Ingredient Name</th>
        <th>Media</th>
        <th>Category</th>
        <th>Visible Front</th>
        <th>Action</th>
    </tr>
</thead>
<tbody>
    <?php
foreach ($ingredients as $row):

?>
        <tr>
            <td><?php echo $row->id; ?></td>
            <td><?php echo $row->name; ?></td>
            <?php if (!empty($row->media)): ?>
            <td><img src="<?php echo base_url() . '/' . $row->media ?>" width="50"/></td>
            <?php else: ?>
            <td><img src="<?php echo base_url(); ?>uploads/ingredients/default_food.jpeg" width="50"/></td>
            <?php endif;?>
            <td><?php echo $row->cat_name; ?></td>
            <td><?php echo ($row->status == 0) ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>' ?></td>
            <td class="menu-action">
                 <a href="<?php echo base_url() . 'admin/ingredients/edit/' . $row->id ?>" class="btn btn-sm btn-primary"><span><i class="fa fa-pencil" aria-hidden="true"></i> <?php echo $this->lang->line('btnedit'); ?></span></a>

                 <a href="<?php echo base_url(); ?>admin/ingredients/delete_ingredient/<?php echo $row->id; ?>" class="btn btn-sm btn-danger" onclick='return confirm("Are you sure want to remove <?php echo $row->name ?>?");'><span ><i class="fa fa-trash-o" aria-hidden="true"></i> <?php echo $this->lang->line('btnremove'); ?></span></a>
               <!--  <a data-placement="top" data-toggle="tooltip" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_ingredients_edit/<?php echo $row->id; ?>');" href="#"><span class="label label-primary mr6 mb6"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</span></a>
                <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>admin/ingredients/delete_ingredient/<?php echo $row->id; ?>');"  data-toggle="tooltip" data-placement="top" ><span class="label label-danger mr6 mb6"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</span></a> -->
            </td>

        </tr>
    <?php endforeach;?>
</tbody>
</table>
</div>
<!-- End .panel -->
</div>
<!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
       // var table = $('#datatable-list-14').DataTable();

        $('#datatable-list-14').DataTable( {
          "searching": true,
          "lengthMenu": [[5,10,25, 50, -1], [5,10,25, 50, "All"]],
          "pagingType": "full_numbers",
          'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */

            }]
        } );
        //$("#datatable-list-14 tr th:nth(1)").removeClass("sorting")
    })
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#csvform").validate({
            rules: {
                file:{    required: true,extension: "csv"
                        },
            },
            messages: {
                file: {   required: "Please select csv file",extension:"Please upload file with .csv extension"
                },
            }
        });
    });
    $(function() {
    setTimeout(function() {
        $(".alert-danger").hide('blind', {}, 500)
    }, 5000);
});
</script>
<!-- End #content

