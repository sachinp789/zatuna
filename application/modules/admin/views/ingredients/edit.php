<?php
$this->load->model('admin/Ingredients_model');
$this->load->model('admin/Units_model');
$unitslist = $this->Ingredients_model->getIngredientUnitbyId($ingredients->id);

/*$ingredients=$this->Ingredients_model->get($param2);*/
//print_r($ingredients);exit;
//$ingdata = $this->Ingredients_model->getIngredients();
$ingcats = $this->Ingredients_model->getIngredientscats();
$ingunits = $this->Units_model->getUnitListByStatus();
?>
<section class="content">
<div class=row>

<div class=col-lg-12>
        <!-- col-lg-12 start here -->
<div class="box box-primary">
    <div class=box-body>

    <?php echo form_open(base_url() . 'admin/ingredients/update/' . $ingredients->id, array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'ingredientsform1', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>
    <div class="padded">
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line('name'); ?><span style="color:red">*</span></label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="ingredients_name" value="<?php echo $ingredients->name ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line('name_ar'); ?><span style="color:red"></span></label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="ingredients_name_ar" value="<?php echo $ingredients->name_ar ?>"/>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line('name_fr'); ?><span style="color:red"></span></label>
            <div class="col-sm-6">
                <input type="text" class="form-control" name="ingredients_name_fr" value="<?php echo $ingredients->name_fr ?>"/>
            </div>
        </div>
        <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('description'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <textarea name="description" class="form-control" id="description"><?php echo $ingredients->description ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('description_ar'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <textarea name="description_ar" class="form-control" id="description_ar"><?php echo $ingredients->description_ar ?></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('description_fr'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <textarea name="description_fr" class="form-control" id="description_fr"><?php echo $ingredients->description_fr ?></textarea>
                </div>
            </div>
        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line('ingredient_category'); ?><span style="color:red">*</span></label>
            <div class="col-sm-6">
               <select name="ingredient_category" id="ingredient_category" class="form-control">
                <option value="">Select Category</option>
                <?php
$cats = '';
foreach ($ingcats as $key => $value):
	if ($value->id == $ingredients->ingredient_category_id) {
		$select = "selected='selected'";
	} else { $select = "";}
	$cats .= "<option value=$value->id $select>";
	if ($this->session->userdata('siteLang') == 'ar') {
		$cats .= $value->name_ar;
	} else if ($this->session->userdata('siteLang') == 'fr') {
	$cats .= $value->name_fr;
} else {
	$cats .= $value->name;
}
$cats .= "</option>";
endforeach;
echo $cats;
?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo ucwords("Media Upload"); ?><span style="color:red"></span></label>
            <div class="col-sm-6">
                <input type="file" class="form-control" name="ingredients_media" id="ingredients_media" />
            </div>
            <input type="hidden" name="old_image" value="<?php echo $ingredients->media ?>">
        </div>
         <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line('nutrition_unit'); ?><span style="color:red">*</span></label>

            <div class="col-sm-6">
            <div class="table-responsive">
            <table class="table table-striped" id="tblremoveunit">
            <thead>
            <tr>
                <th>Unit</th>
                <th>#</th>
            </tr>
            </thead>
            <tbody>
                <?php
foreach ($unitslist as $value) {
	$units[] = $value->id;
	?>
                    <tr id="item_<?php echo $value->id; ?>">
                        <td id="ingname_<?php echo $value->id; ?>">
                        <?php
if ($this->session->userdata('siteLang') == 'ar') {
		echo $value->unit_name_ar;
	} else if ($this->session->userdata('siteLang') == 'fr') {
		echo $value->unit_name_fr;
	} else {
		echo $value->unit_name;
	}
	?></td>
                        <td>
                        <a onclick="remove_unit(<?php echo $value->ingunit_id; ?>,<?php echo $value->id ?>)" id="remove_unit_<?php echo $value->id; ?>"><span class="glyphicon glyphicon-remove"></span></a>
                        </td>
                    </tr>
                <?php
}
?>
            </tbody>
        </table>
        <hr>
        </div>
            <?php
if (count($ingunits) == 0):
?>
                <a href="<?php echo base_url() ?>/admin/units/create" class="btn btn-xs btn-default">Add Unit</a>
                <?php
endif;
?>
                <?php
foreach ($ingunits as $key => $value):
	if (in_array($value->id, $units)) {
		$checked = "checked='checked'";
		$disabled = "disabled='true'";
	} else {
		$checked = "";
		$disabled = "";
	}
	?>
															    <div class="col-md-4" id="unitsdata">
															        <div class="checkbox">
															          <label><input type="checkbox" name="ing_unit[]" value="<?php echo $value->id ?>" <?php echo $checked; ?> id="newunit_<?php echo $value->id ?>" <?php echo $disabled; ?>><?php if ($this->session->userdata('siteLang') == 'ar') {echo $value->unit_name_ar;} else if ($this->session->userdata('siteLang') == 'fr') {echo $value->unit_name_fr;} else {echo $value->unit_name;}?>
</label>
                    </div>
                </div>
                <?php
endforeach;
?>
        <label for="ing_unit[]" class="error"></label>
        </div>
        </div>
        <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('protein'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="ingredients_protein" id="ingredients_protein" value="<?php echo $ingredients->protein ?>" />
                </div>
        </div>
        <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('fat'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="ingredients_fat" id="ingredients_fat" value="<?php echo $ingredients->fat ?>"/>
                </div>
        </div>
        <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('carbs'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="ingredients_carbs" id="ingredients_carbs" value="<?php echo $ingredients->carbs ?>"/>
                </div>
        </div>
         <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('kcal'); ?><span style="color:red">*</span></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="ingredients_amount" value="<?php echo $ingredients->kcal_grams ?>"/>
                </div>
        </div>

       <!--  <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo ucwords("description"); ?><span style="color:red">*</span></label>
            <div class="col-sm-6">
                <textarea name="ingredients_description" class="form-control" rows="3" id="ingredients_description"><?php echo $ingredients->description ?></textarea>
            </div>
        </div> -->
       <!--  <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo ucwords("unit"); ?><span style="color:red">*</span></label>
            <div class="col-sm-6">
            <input type="text" class="form-control" name="ingredients_unit" value="<?php echo $ingredients->unit ?>"/>
            </div>
        </div> -->

        <div class="form-group">
            <label class="col-sm-3 control-label"><?php echo $this->lang->line('status'); ?><span style="color:red"></span></label>
            <div class="col-sm-6">
                <label for="active" class="control-label">
                <input type="radio" name="is_status" <?php echo ($ingredients->status == 0) ? "checked='checked'" : "" ?> value="0" id="active"/> <?php echo $this->lang->line('status_name_act'); ?> </label>
                <label for="inactive" class="control-label">
                <input type="radio" name="is_status" <?php echo ($ingredients->status == 1) ? "checked='checked'" : "" ?> value="1" id="inactive"/> <?php echo $this->lang->line('status_name_dact'); ?> </label>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btnupdate'); ?></button>
                <a href="<?php echo base_url('admin/ingredients'); ?>" class="btn btn-default"><?php echo $this->lang->line('btncancel'); ?></a>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>

    </div>
    </div>
    <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
</section>
<!-- End #content -->
<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, 'Please enter a valid number.');
        jQuery.validator.addMethod(
            "maxfilesize",
            function (value, element) {
                return this.optional(element) || (element.files && element.files[0]
                                       && element.files[0].size < 1024 * 1024 * 5);
            },
            'The file size can not exceed 2MB.'
        );

        $('#ingredients_protein').keypress(function(event) {
          if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
             event.preventDefault();
          }
        });
         $('#ingredients_fat').keypress(function(event) {
          if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
             event.preventDefault();
          }
        });
          $('#ingredients_carbs').keypress(function(event) {
          if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
             event.preventDefault();
          }
        });

        $("#ingredientsform1").validate({
            rules: {
                ingredients_name:{    required: true,
                           //character: true,
                        },
                ingredients_amount:{number:true},
                ingredient_category: {   required: true
                        },
                ingredients_media:{extension: 'gif|png|jpg|jpeg',maxfilesize:true},
                "ing_unit[]":{
                        required:true,
                        minlength:1
               },
            },
            messages: {
                ingredients_name: {   required: "Enter ingredient name",
                            character: "Enter ingredient name",
                        },
                /*ingredients_description: {   required: "Enter description"},*/
                ingredient_category: {   required: "Select ingredient category",
                },

                "ing_unit[]":{required:"Please select at least one unit."},
                highlight: function(label) {
                  $(label).closest('.form-group').addClass('error');
                },
            }
        });

    });

 /*************************** Remove Ingredients Unit ****************/
 function remove_unit(no,unitid){
       if(confirm('Are you sure want to remove?')){

        $.ajax({
            type: "POST",
            url: "<?php echo base_url() . 'admin/ingredients/delete_ingredientunit' ?>",
            dataType:"json",
            data:"unit_id="+no,
            success: function(response) {
                if(response == 1){
                    $('#tblremoveunit').load(document.URL + ' #tblremoveunit');
                    $('#newunit_'+unitid).removeAttr('checked');
                    $('#newunit_'+unitid).removeAttr('disabled');
                }
            }
          });
       }
}
/********************************* END *********************************/
</script>