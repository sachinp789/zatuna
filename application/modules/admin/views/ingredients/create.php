<!-- Start .row -->
<?php
$this->load->model('admin/Ingredients_model');
$this->load->model('admin/Units_model');
//echo $this->session->userdata('siteLang');
//$ingdata = $this->Ingredients_model->getIngredients();
$ingcats = $this->Ingredients_model->getIngredientscats();
$ingunits = $this->Units_model->getUnitListByStatus();
?>
<section class="content">
<div class=row>

<div class=col-lg-12>
        <!-- col-lg-12 start here -->
<div class="box box-primary">
    <div class=box-body>
       <?php echo form_open(base_url() . 'admin/ingredients/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'ingredientsform', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>
        <div class="padded">
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('name'); ?><span style="color:red">*</span></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="ingredients_name" value=""/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('name_ar'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="ingredients_name_ar" value=""/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('name_fr'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="ingredients_name_fr" value=""/>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('description'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <textarea name="description" class="form-control" id="description"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('description_ar'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <textarea name="description_ar" class="form-control" id="description_ar"></textarea>
                </div>
            </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('description_fr'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <textarea name="description_fr" class="form-control" id="description_fr"></textarea>
                </div>
            </div>
             <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('ingredient_category'); ?><span style="color:red">*</span></label>
                <div class="col-sm-6">
                   <select name="ingredient_category" id="ingredient_category" class="form-control">
                    <option value="">Select Category</option>
                    <?php
$cats = '';
foreach ($ingcats as $key => $value):
	$cats .= "<option value=$value->id>";
	if ($this->session->userdata('siteLang') == 'ar') {
		$cats .= $value->name_ar;
	} else if ($this->session->userdata('siteLang') == 'fr') {
	$cats .= $value->name_fr;
} else {
	$cats .= $value->name;
}
$cats .= "</option>";
endforeach;
echo $cats;
?>
                    </select>
                </div>
            </div>
           <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo ucwords("Media Upload"); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <input type="file" class="form-control" name="ingredients_media" id="ingredients_media" />
                </div>
            </div>
            <!-- <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo ucwords("description"); ?><span style="color:red">*</span></label>
                <div class="col-sm-6">
                    <textarea name="ingredients_description" class="form-control" rows="3" id="ingredients_description"></textarea>
                </div>
            </div> -->
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('nutrition_unit'); ?><span style="color:red">*</span></label>
                <div class="col-sm-6 units">
                    <?php
if (count($ingunits) == 0):
?>
                    <a href="<?php echo base_url() ?>/admin/units/create" class="btn btn-xs btn-default">Add Unit</a>
                    <?php
endif;
?>
                    <?php
foreach ($ingunits as $value):
?>
                    <div class="col-md-4">
                        <div class="checkbox">
                          <label>
                          <input type="checkbox" name="ing_unit[]" value="<?php echo $value->id ?>">
                          <?php if ($this->session->userdata('siteLang') == 'ar') {echo $value->unit_name_ar;} else if ($this->session->userdata('siteLang') == 'fr') {echo $value->unit_name_fr;} else {echo trim($value->unit_name);}?>
                            </label>
                        </div>
                    </div>
                    <?php
endforeach;
?>
            <label for="ing_unit[]" class="error"></label>
            </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('protein'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="ingredients_protein" id="ingredients_protein"/>
                </div>
            </div>
             <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('fat'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="ingredients_fat" id="ingredients_fat"/>
                </div>
            </div>
             <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('carbs'); ?><span style="color:red"></span></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="ingredients_carbs" id="ingredients_carbs"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo $this->lang->line('kcal'); ?><span style="color:red">*</span></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="ingredients_amount" id="ingredients_amount"/>
                </div>
            </div>

           <!--  <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo ucwords("unit"); ?><span style="color:red">*</span></label>
                <div class="col-sm-6">
                <input type="text" class="form-control" name="ingredients_unit" value=""/>
                </div>
            </div> -->
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btnadd'); ?></button>
                    <a href="<?php echo base_url('admin/ingredients'); ?>" class="btn btn-default"><?php echo $this->lang->line('btncancel'); ?></a>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
</section>
<!-- End #content -->
<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, 'Please enter a valid number.');
        jQuery.validator.addMethod(
            "maxfilesize",
            function (value, element) {
                return this.optional(element) || (element.files && element.files[0]
                                       && element.files[0].size < 1024 * 1024 * 5);
            },
            'The file size can not exceed 2MB.'
        );

        $('#ingredients_protein').keypress(function(event) {
          if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
             event.preventDefault();
          }
        });
         $('#ingredients_fat').keypress(function(event) {
          if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
             event.preventDefault();
          }
        });
          $('#ingredients_carbs').keypress(function(event) {
          if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
             event.preventDefault();
          }
        });

        $("#ingredientsform").validate({
            rules: {
                ingredients_name:{    required: true,
                           character: true,
                           remote: {
                                url: "<?php echo base_url(); ?>admin/ingredients/checkIngredient",
                                type: "post",
                                data: {
                                    ingredients_name: function () {
                                        return $('#ingredientsform :input[name="ingredients_name"]').val();
                                    }
                                }
                            }
                        },
                ingredients_amount:{required:true,number:true},
               /* ingredients_unit: { required: true},  */
                ingredient_category: {   required: true
                        },
                ingredients_media:{extension: 'gif|png|jpg|jpeg',maxfilesize:true},
               "ing_unit[]":{
                        required:true,
                        minlength:1
               },
            },
            messages: {
                ingredients_name: {   required: "Enter ingredient name",
                            character: "Enter ingredient name",
                            remote: "Ingredient already in use"
                        },
                //ingredients_description: {   required: "Enter description"},
                ingredient_category: {   required: "Select ingredient category",
                },
                "ing_unit[]":{required:"Please select at least one unit."},
                ingredients_amount:{required: "Please enter kcal(grams)"},
                highlight: function(label) {
                  $(label).closest('.form-group').addClass('error');
                },
            }
        });
    });
</script>