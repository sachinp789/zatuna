<section class="content">
<!-- Start .row -->
<div class="row">
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="box box-primary">
            <div class="box-body">
                    <?php echo form_open(base_url() . 'admin/change_password', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'user-changepwd-form', 'target' => '_top', "enctype" => "multipart/form-data")); ?>
                    <input type="hidden" name="txtuserid" id="txtuserid" value="<?php echo $this->session->userdata('admin_id'); ?>">
                    <div class="padded">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('ch_pwd'); ?><span style="color:red">*</span></label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="password" id="password"/>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('ch_newpwd'); ?><span style="color:red">*</span></label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="newpassword" id="newpassword"/>
                            </div>
                        </div>
                         <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('ch_confpwd'); ?><span style="color:red">*</span></label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" name="cpassword" id="cpassword"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary" ><?php echo $this->lang->line('btnupdate'); ?></button>
                                <a href="<?php echo base_url('admin/dashboard'); ?>" class="btn btn-default"><?php echo $this->lang->line('btncancel'); ?></a>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {

    $("#password").keypress(function (evt) {
          var keycode = evt.charCode || evt.keyCode;
          if (keycode  == 32) {
            return false;
          }
    });
    $("#newpassword").keypress(function (evt) {
          var keycode = evt.charCode || evt.keyCode;
          if (keycode  == 32) {
            return false;
          }
    });
    $("#cpassword").keypress(function (evt) {
          var keycode = evt.charCode || evt.keyCode;
          if (keycode  == 32) {
            return false;
          }
    });

    $("#user-changepwd-form").validate({
           rules: {
                password: {
                            required: true,
                            remote: {
                                url: "<?php echo base_url(); ?>admin/check_password",
                                type: "post",
                                data: {
                                     password: function () {
                                        return $("#password").val();
                                    },
                                    userid: function () {
                                          return $("#txtuserid").val();
                                    },
                                }
                            }
                        },
                newpassword: "required",
                cpassword: {
                            required:true,
                            equalTo : "#newpassword"
                        },
                },
            messages: {
                password: {
                    required: "Enter password",
                    remote: "Password not match with our system",
                },
                newpassword: "Enter new password",
                cpassword: {
                    required:"Enter Confirm password",
                    equalTo:"New password and confirm password not match"

                },
            }
        });
    });
    /*$().ready(function () {
        $("#user-create-form").validate({
            rules: {
                password: {
                            required: true,
                            remote: {
                                url: "<?php echo base_url(); ?>admin/check_password",
                                type: "post",
                                data: {
                                     password: function () {
                                        return $("#password").val();
                                    },
                                    userid: function () {
                                          return $("#txtuserid").val();
                                    },
                                }
                            }
                        },
                newpassword: "required",
                cpassword: {
                            required:true,
                            equalTo : "#newpassword"
                        },
                },
            messages: {
                password: {
                    required: "Enter password",
                    remote: "Password not match with our system",
                },
                newpassword: "Enter new password",
                cpassword: {
                    required:"Enter Confirm password",
                    equalTo:"New password and confirm password not match"

                },
            }
        });
    });*/
//$("#myModalLabel2").text('Change Password');
</script>