<section class="content">
<!-- Start .row -->
<div class="row">
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="box box-primary">
            <div class="box-body">
                    <?php echo form_open(base_url() . 'admin/profile/' . $user->admin_id, array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'user-profile-form', 'target' => '_top', "enctype" => "multipart/form-data")); ?>
                    <input type="hidden" name="txtuserid" id="txtuserid" value="<?php echo $user->admin_id; ?>">
                    <div class="padded">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('admin_fname'); ?><span style="color:red">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="first_name" id="first_name"
                                       value="<?php echo $user->first_name; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('admin_lname'); ?><span style="color:red">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="last_name" id="last_name"
                                       value="<?php echo $user->last_name; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('admin_email'); ?><span style="color:red">*</span></label>
                            <div class="col-sm-6">
                                <input type="email" class="form-control" name="email"
                                       autocomplete="off" id="email" value="<?php echo $user->email ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('admin_gender'); ?><span style="color:red">*</span></label>
                            <div class="col-sm-6">
                                <select id="gender" name="gender" class="form-control">
                                    <option value="Male"
                                            <?php if ($user->gender == 'Male') {
	echo 'selected';
}
?>>Male</option>
                                    <option value="Female"
                                            <?php if ($user->gender == 'Female') {
	echo 'selected';
}
?>>Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('admin_mobile'); ?><span style="color:red">*</span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="mobile" id="mobile"
                                       value="<?php echo $user->mobile; ?>" maxlength="15"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('admin_city'); ?><span style="color:red"></span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="city" id="city"
                                       value="<?php echo $user->city; ?>"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('admin_address'); ?><span style="color:red"></span></label>
                            <div class="col-sm-6">
                                <textarea id="address" class="form-control" name="address"><?php echo $user->address; ?></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('admin_photo'); ?></label>
                            <div class="col-sm-6">
                                <input type="file" name="photo" id="photo" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary" ><?php echo $this->lang->line('btnupdate'); ?></button>
                                <a href="<?php echo base_url('admin/dashboard'); ?>" class="btn btn-default"><?php echo $this->lang->line('btncancel'); ?></a>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value);
        }, 'Please enter a valid number.');
        jQuery.validator.addMethod("phoneno", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 &&
            phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
        }, "<br />Please specify a valid phone number");

        jQuery.validator.addMethod("email", function(value, element)
        {
        return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
        }, "Please enter a valid email address.");

       $("#user-profile-form").validate({
            rules: {
                first_name: {character:true,required:true},
                last_name: {character:true,required:true},
                email: {
                            //required: true,
                            email: true
                            /*remote: {
                                url: "<?php echo base_url(); ?>admin/check_user_email/profile",
                                type: "post",
                                data: {
                                     email: function () {
                                        return $("#email").val();
                                    },
                                    userid: function () {
                                          return $("#txtuserid").val();
                                    },
                                }
                            }*/
                        },
                mobile: {number:true,required:true},
                city: {character:true},
                photo: {accept: "image/*"},
            },
            messages: {
                first_name: {character:"Enter valid first name"},
                last_name: {character:"Enter valid last name"},
                email: {
                    required: "Enter email id",
                    email: "Enter valid email id",
                    remote: "Email id already exists",
                },
                mobile: {maxlength:"Please enter valid mobile number"},
                city: {character:"Please enter valid city"},
                photo:{accept:"Please select valid photo"},
            }
        });
    });
$("#myModalLabel2").text('Profile');
</script>