<section class="content">
<div class=box>
        <div class="box-body">
            <div class=panel-body>
                 <?php if ($this->session->flashdata('error')) {?>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php }?>
                <p><a href="<?php echo base_url() ?>admin/recipesubcategory/subcategorycreate" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $this->lang->line('btncreate'); ?></a></p>

                <!-- <a data-placement="top" data-toggle="tooltip" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_category_recipies_create');" href="#" class="links"><i class="fa fa-plus"></i>Recipe Category</a> -->

                <div id="getresponse">

                <table id="datatable-list-16" class="table table-striped table-bordered table-responsive" cellspacing=0 width=100%>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Name_AR</th>
                            <th>Name_FR</th>
                            <th>Category</th>
                            <th>Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>

                    <tbody>
                    <?php
foreach ($categories as $row):
?>
                    <tr>
                    <td><?php echo $row->id ?></td>
                    <td><?php echo $row->sub_cate_name ?></td>
                    <td><?php echo $row->sub_cate_name_ar ?></td>
                    <td><?php echo $row->sub_cate_name_fr ?></td>
                    <td><?php echo $row->cat_name ?></td>
                    <td><?php echo ($row->status == 0) ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>' ?></td>
                    <td class="menu-action">

                    <a href="<?php echo base_url() . 'admin/recipesubcategory/subcategoryedit/' . $row->id ?>" class="btn btn-sm btn-primary"><span><i class="fa fa-pencil" aria-hidden="true"></i> <?php echo $this->lang->line('btnedit'); ?></span></a>

                    <a href="<?php echo base_url(); ?>admin/recipesubcategory/delete_subcategory/<?php echo $row->id; ?>" class="btn btn-sm btn-danger" onclick='return confirm("Are you sure want to remove <?php echo $row->sub_cate_name ?>?");'><span ><i class="fa fa-trash-o" aria-hidden="true"></i> <?php echo $this->lang->line('btnremove'); ?></span></a>

                    </td>
                    </tr>
                    <?php
endforeach;
?>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
</section>
<!-- End #content -->
<script type="text/javascript">
    $(document).ready(function(){
        $('#datatable-list-16').DataTable( {
          "searching": true,
          "lengthMenu": [[5,10,25, 50, -1], [5,10,25, 50, "All"]],
          "pagingType": "full_numbers",
          'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }]
        } );
    })
     $(function() {
      setTimeout(function() {
        $(".alert-danger").hide('blind', {}, 500)
    }, 5000);
    });
</script>