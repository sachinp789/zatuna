<!-- Start .row -->
<section class="content">
<div class=row>

    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class="box box-primary">
        <div class="box-body">

            <?php echo form_open(base_url() . 'admin/recipecategory/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'recpcatform', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>
            <div class="padded">
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('recp_name'); ?><span style="color:red">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="cat_name" value=""/>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('recp_name_ar'); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="cat_name_ar" value=""/>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('recp_name_fr'); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="cat_name_fr" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('status'); ?></label>
                    <div class="col-sm-6">
                        <label for="active" class="control-label">
                            <input type="radio" name="rcstatus" value="0" id="active" checked/> <?php echo $this->lang->line('status_name_act'); ?> </label>
                        <label for="inactive" class="control-label">
                            <input type="radio" name="rcstatus" value="1" id="inactive"/> <?php echo $this->lang->line('status_name_dact'); ?> </label>
                        </div>
                    <!-- <div class="col-sm-6">
                        <select name="rcstatus" class="form-control">
                            <option value="0">Active</option>
                            <option value="1">Inactive</option>
                        </select>
                    </div> -->
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btnadd'); ?></button>
                        <a href="<?php echo base_url('admin/recipecategory'); ?>" class="btn btn-default"><?php echo $this->lang->line('btncancel'); ?></a>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
</section>
<!-- End #content -->
<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, 'Please enter a valid number.');

        $("#recpcatform").validate({
            rules: {
                cat_name:{    required: true,
                              remote: {
                                url: "<?php echo base_url(); ?>admin/recipecategory/checkCategory",
                                type: "post",
                                data: {
                                    cat_name: function () {
                                        return $('#recpcatform :input[name="cat_name"]').val();
                                    }
                                }
                            }
                        },
            },
            messages: {
                cat_name: {   required: "Enter recipe category",remote:"Recipe category already in use"
                },
            }
        });
    });
</script>