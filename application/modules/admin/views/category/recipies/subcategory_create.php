<!-- Start .row -->
<?php
$this->load->model('admin/Recipe_categories_model');
$categories = $this->Recipe_categories_model->getallActiveCategories();
?>
<section class="content">
<div class=row>

    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class="box box-primary">
        <div class="box-body">

            <?php echo form_open(base_url() . 'admin/recipesubcategory/subcategorycreate/', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'recpsubcatform', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>
            <div class="padded">
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('recp_cat'); ?><span style="color:red">*</span></label>
                    <div class="col-sm-6">
                    <select class="form-control" name="recipe_category_name">
                    <option value="">Select Category</option>
                    <?php
$subcat = "";
foreach ($categories as $value) {
	$subcat .= "<option value=$value->id>";
	if ($this->session->userdata('siteLang') == 'ar') {
		$subcat .= $value->cat_name_ar;
	} else if ($this->session->userdata('siteLang') == 'fr') {
		$subcat .= $value->cat_name_fr;
	} else {
		$subcat .= $value->cat_name;
	}
	$subcat .= "</option>";
}
echo $subcat;
?>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('recp_subcat'); ?><span style="color:red">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="subcat_name" value=""/>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('recp_subcat_ar'); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="subcat_name_ar" value=""/>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('recp_subcat_fr'); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="subcat_name_fr" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('status'); ?></label>
                    <div class="col-sm-6">
                        <label for="active" class="control-label">
                            <input type="radio" name="rcstatus" value="0" id="active" checked/> <?php echo $this->lang->line('status_name_act'); ?> </label>
                        <label for="inactive" class="control-label">
                            <input type="radio" name="rcstatus" value="1" id="inactive"/> <?php echo $this->lang->line('status_name_dact'); ?> </label>
                        </div>
                    <!-- <div class="col-sm-6">
                        <select name="rcstatus" class="form-control">
                            <option value="0">Active</option>
                            <option value="1">Inactive</option>
                        </select>
                    </div> -->
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btnadd'); ?></button>
                        <a href="<?php echo base_url('admin/recipesubcategory/'); ?>" class="btn btn-default"><?php echo $this->lang->line('btncancel'); ?></a>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
</section>
<!-- End #content -->
<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, 'Please enter a valid number.');

        $("#recpsubcatform").validate({
            rules: {
                recipe_category_name:{  required: true,
                                },
                subcat_name:{required:true,character:true,
                            remote: {
                                        url: "<?php echo base_url(); ?>admin/recipesubcategory/checkSubcategory",
                                        type: "post",
                                        data: {
                                            subcat_name: function () {
                                                return $('#recpsubcatform :input[name="subcat_name"]').val();
                                            }
                                        }
                                    }
                },
                subcat_name_ar:{character:true},
                subcat_name_fr:{character:true}
            },
            messages: {
                recipe_category_name: {   required: "Select recipe category"
                },
                subcat_name:{required:"Enter sub category",character:"Please enter valid category name",remote:"Recipe Subcategory already in use"},
                subcat_name_ar:{character:"Please enter valid category name"},
                subcat_name_fr:{character:"Please enter valid category name"},
            }
        });
    });
</script>