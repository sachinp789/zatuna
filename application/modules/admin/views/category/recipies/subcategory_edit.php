<?php
$this->load->model('admin/Recipe_categories_model');
$recpcatlist = $this->Recipe_categories_model->getCategories();
?>
<section class="content">
<!-- Start .row -->
<div class=row>
    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class="box box-primary">
            <div class=box-body>
            <?php echo form_open(base_url() . 'admin/recipesubcategory/subcategoryupdate/' . $recpsubcatlist->id, array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'editrecpsubcat', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>
            <div class="padded">
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('recp_cat'); ?><span style="color:red">*</span></label>
                    <div class="col-sm-6">
                    <select class="form-control" name="recipe_category_name">
                    <option value="">Select Category</option>
                    <?php
$subcat = "";
$select = "";
foreach ($recpcatlist as $value) {
	if ($value->id == $recpsubcatlist->recipe_category_id) {
		$select = "selected='selected'";
	} else {
		$select = "";
	}
	$subcat .= "<option value=$value->id $select>";
	if ($this->session->userdata('siteLang') == 'ar') {
		$subcat .= $value->cat_name_ar;
	} else if ($this->session->userdata('siteLang') == 'fr') {
		$subcat .= $value->cat_name_fr;
	} else {
		$subcat .= $value->cat_name;
	}
	$subcat .= "</option>";
}
echo $subcat;
?>
                    </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('recp_subcat'); ?><span style="color:red">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="subcat_name" value="<?php echo $recpsubcatlist->sub_cate_name ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('recp_subcat_ar'); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="subcat_name_ar" value="<?php echo $recpsubcatlist->sub_cate_name_ar ?>"/>
                    </div>
                </div>
                 <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('recp_subcat_fr'); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="subcat_name_fr" value="<?php echo $recpsubcatlist->sub_cate_name_fr ?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('status'); ?></label>
                     <div class="col-sm-6">
                        <label for="active" class="control-label">
                        <input type="radio" name="rcstatus" <?php echo ($recpsubcatlist->status == 0) ? "checked='checked'" : "" ?> value="0" id="active"/> <?php echo $this->lang->line('status_name_act'); ?> </label>
                        <label for="inactive" class="control-label">
                        <input type="radio" name="rcstatus" <?php echo ($recpsubcatlist->status == 1) ? "checked='checked'" : "" ?> value="1" id="inactive"/> <?php echo $this->lang->line('status_name_dact'); ?> </label>
                    </div>
                   <!--  <div class="col-sm-6">
                        <select name="rcstatus" class="form-control">
                            <option value="0" <?php echo ($recpcatlist->status == 0) ? 'selected="selected"' : ''; ?>>Active</option>
                            <option value="1" <?php echo ($recpcatlist->status == 1) ? 'selected="selected"' : ''; ?>>Inactive</option>
                        </select>
                    </div> -->
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btnupdate'); ?></button>
                        <a href="<?php echo base_url('admin/recipesubcategory'); ?>" class="btn btn-default"><?php echo $this->lang->line('btncancel'); ?></a>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
</section>
<!-- End #content -->

<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, 'Please enter a valid number.');

        $("#editrecpsubcat").validate({
            rules: {
                recipe_category_name:{required: true},
                subcat_name:{required:true,character:true},
                /*subcat_name_ar:{character:true},
                subcat_name_fr:{character:true}*/
            },
            messages: {
                recipe_category_name: {   required: "Select recipe category",
                },
                subcat_name:{required:"Enter sub category",character:"Please enter valid category name"},
                /*subcat_name_ar:{character:"Please enter valid category name"},
                subcat_name_fr:{character:"Please enter valid category name"},*/
            }
        });
    });
</script>