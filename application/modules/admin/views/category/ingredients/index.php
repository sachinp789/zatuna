<section class="content">
<div class=box>

    <div class=box-body>
     <?php if ($this->session->flashdata('error')) {?>
                <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
            <?php }?>
                            <p><a href="<?php echo base_url() ?>admin/ingredientcategory/create" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $this->lang->line('btncreate'); ?></a></p>
                           <!--  <a data-placement="top" data-toggle="tooltip" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_category_ingredients_create');" href="#" class="links"><i class="fa fa-plus"></i>Ingredient Category</a> -->

                            <div id="getresponse">

                            <table id="datatable-list-15" class="table table-striped table-bordered table-responsive" cellspacing=0 width=100%>
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Name_AR</th>
                                        <th>Name_FR</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                <?php
foreach ($categories as $row):
?>
                                <tr>
                                <td><?php echo $row->id ?></td>
                                <td><?php echo $row->name ?></td>
                                <td><?php echo $row->name_ar ?></td>
                                <td><?php echo $row->name_fr ?></td>
                                <td><?php echo ($row->status == 0) ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>' ?></td>
                                <td class="menu-action">
                                <a href="<?php echo base_url() . 'admin/ingredientcategory/edit/' . $row->id ?>" class="btn btn-sm btn-primary"><span><i class="fa fa-pencil" aria-hidden="true"></i> <?php echo $this->lang->line('btnedit'); ?></span></a>

                                <a href="<?php echo base_url(); ?>admin/ingredientcategory/delete_ingcat/<?php echo $row->id; ?>" class="btn btn-sm btn-danger" onclick='return confirm("Are you sure want to remove <?php echo $row->name ?>?");'><span ><i class="fa fa-trash-o" aria-hidden="true"></i> <?php echo $this->lang->line('btnremove'); ?></span></a>

                               <!--  <a data-placement="top" data-toggle="tooltip" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_category_ingredients_edit/<?php echo $row->id; ?>');" href="#"><span class="label label-primary mr6 mb6"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</span></a>
                                <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>admin/ingredientcategory/delete_ingcat/<?php echo $row->id; ?>');"  data-toggle="tooltip" data-placement="top" ><span class="label label-danger mr6 mb6"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</span></a> -->
                                </td>
                                </tr>
                                <?php
endforeach;
?>
                                </tbody>
                            </table>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
</section>
<!-- End #content -->
<script type="text/javascript">
 $(document).on('click','.status_checks',function(){
  var status = ($(this).hasClass("btn-success")) ? '1' : '0';
  var msg = (status =='0')? 'Inactive' : 'Active';

  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);
    url = "<?php echo base_url(); ?>admin/ingredientcategory/update_status/";
    $.ajax({
      type:"POST",
      url: url,
      data: {id:$(current_element).attr('data'),status:status},
      success: function(data)
      {
       // console.log(data);return false;
        location.reload();
      }
    });
  }

});
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#datatable-list-15').DataTable( {
          "searching": true,
          "lengthMenu": [[5,10,25, 50, -1], [5,10,25, 50, "All"]],
          'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }]
        } );
    })
    $(function() {
    setTimeout(function() {
        $(".alert-danger").hide('blind', {}, 500)
    }, 5000);
});
</script>