<!-- Start .row -->
<div class=row>

    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class=" panel-default">
            <div class=panel-body>
                <div class="tabs mb20">
                    <ul id="import-tab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#quiz-details" data-toggle="tab" aria-expanded="true">Import Category</a>
                        </li>
                    </ul>
                    <div id="import-tab-content" class="tab-content">
                        <div class="tab-pane fade active in" id="quiz-details">
                            <div class="panel-default">
                                <div class="panel-body">

                                    <div class="box-content">

                                        <?php echo form_open(base_url() . 'admin/csvimport/csvImport/', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'csvform', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>
                                        <div class="padded">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"><?php echo ucwords("CSV import"); ?><span style="color:red">*</span></label>

                                                <div class="col-sm-4">
                                                    <input type="file" name="csv_file" id="csv_file" class="file"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-6">
                                                    <button type="submit" class="btn btn-info vd_bg-green">UPLOAD</button>
                                                </div>
                                            </div>
                                        </div>
                                        <?php echo form_close(); ?>
                                        <hr>
                                        <?php echo form_open(base_url() . 'admin/csvimport/recipeSubcategory/', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'csvform1', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>
                                        <div class="padded">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"><?php echo ucwords("recipe subcategory import"); ?><span style="color:red">*</span></label>

                                                <div class="col-sm-4">
                                                    <input type="file" name="recipe_csv_file" id="recipe_csv_file" class="file"/>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-6">
                                                    <button type="submit" class="btn btn-info vd_bg-green">UPLOAD</button>
                                                </div>
                                            </div>
                                        </div>
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- tab content -->

                    </div>

                </div>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->

<script type="text/javascript">
    $(document).ready(function () {
        $("#csvform").validate({
            rules: {
                csv_file:{    required: true
                              extension: "csv"
                        },
            },
            messages: {
                csv_file: {   required: "Please select csv file",
                },
            }
        });
        $("#csvform1").validate({
            rules: {
                recipe_csv_file:{    required: true
                        },
            },
            messages: {
                recipe_csv_file: {   required: "Please select csv file",
                },
            }
        });
    });
</script>