<!-- Start .row -->
<!-- Start .row -->
<section class="content">
<div class=box>                      
        <div class="box-body">
            <div class=panel-body>
                            <p><a href="<?php echo base_url(); ?>admin/levels/create" class="btn btn-primary"><i class="fa fa-plus"></i> Create</a></p>

                            <!-- <a data-placement="top" data-toggle="tooltip" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_level_create');" href="#" class="links"><i class="fa fa-plus"></i>Level</a> -->
                            
                <div id="getresponse">
                  
                <table id="datatable-list-13" class="table table-striped table-bordered table-responsive" cellspacing=0 width=100%>
                    <thead>
                        <tr>
                            <th>ID</th>			
                            <th>Type</th>
                            <th>Status</th>		
                            <th>Action</th>	
                        </tr>
                    </thead>

                    <tbody>
                    <?php
                    foreach ($levels as $row):    
                    ?> 
                    <tr>   
                    <td><?php echo $row->id?></td>
                    <td><?php echo $row->level_type?></td>
                    <td>
                    <?php echo ($row->status == 0) ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>' ?>    
                    <!-- <i data="<?php echo $row->id;?>" class="status_checks btn
                        <?php echo ($row->status)?'btn-danger': 'btn-success'?>"><?php echo ($row->status)? 'Inactive' : 'Active'?></i> --></td>
                    <td class="menu-action">
                     <a href="<?php echo base_url() . 'admin/levels/edit/'. $row->id ?>" class="btn btn-sm btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i> Edit</a>
                     <a href="<?php echo base_url(); ?>admin/levels/delete_level/<?php echo $row->id; ?>" class="btn btn-sm btn-danger" onclick='return confirm("Are you sure want to remove <?php echo $row->level_type ?>?");'><span ><i class="fa fa-trash-o" aria-hidden="true"></i> Remove</span></a>
                    <!-- <a data-placement="top" data-toggle="tooltip" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_level_edit/<?php echo $row->id; ; ?>');" href="#"><span class="label label-primary mr6 mb6"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</span></a>
                    <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>admin/levels/delete_level/<?php echo $row->id; ?>');"  data-toggle="tooltip" data-placement="top" ><span class="label label-danger mr6 mb6"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</span></a> -->
                    </td>
                    </tr>
                    <?php
                    endforeach;
                    ?>        															
                    </tbody>
                </table>
              
                </div>
            </div>
        </div>
        <!-- End .panel -->
</div>
<!-- End .row -->
</div>
</section>
<script type="text/javascript">
 $(document).on('click','.status_checks',function(){
  var status = ($(this).hasClass("btn-success")) ? '1' : '0';
  var msg = (status =='0')? 'Inactive' : 'Active';

  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);
    url = "<?php echo base_url(); ?>admin/levels/update_status/";
    $.ajax({
      type:"POST",
      url: url,
      data: {id:$(current_element).attr('data'),status:status},
      success: function(data)
      {   
       // console.log(data);return false;
        location.reload();
      }
    });
  }      
});

$(document).ready(function() {
    $('#datatable-list-13').DataTable({
      "lengthMenu": [[5,10,25, 50, -1], [5,10,25, 50, "All"]],
      'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }]
    });
} );

</script>