<!-- Start .row -->
<section class="content">
<div class=row>                      

    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class="box box-primary">
           <div class="box-body">  
            <?php echo form_open(base_url() . 'admin/levels/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'levelform', 'target' => '_top','enctype'=>'multipart/form-data')); ?>
            <div class="padded">
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo ucwords("level type"); ?><span style="color:red">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="type" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo ucwords("level type_ar"); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="type_ar" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo ucwords("level type_fr"); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="type_fr" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo ucwords("status"); ?></label>
                        <div class="col-sm-6">
                        <label for="active" class="control-label">
                            <input type="radio" name="lvstatus" value="0" id="active" checked/> Active </label>
                        <label for="inactive" class="control-label">
                            <input type="radio" name="lvstatus" value="1" id="inactive"/> Inactive </label>
                        </div>
                    <!-- <div class="col-sm-6">
                        <select name="lvstatus" class="form-control">
                            <option value="0">Active</option>
                            <option value="1">Inactive</option>
                        </select>
                    </div> -->
                </div> 
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-primary">Add</button>
                        <a href="<?php echo base_url('admin/levels'); ?>" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </div>    
            <?php echo form_close(); ?>  
        </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End #content -->
</section>
<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, 'Please enter a valid number.');

        $("#levelform").validate({
            rules: {
                type:{    required: true
                        },
            },
            messages: {
                type: {   required: "Enter level type",
                },
            }
        });
    });
</script>