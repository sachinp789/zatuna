<?php
//$this->load->model('admin/Levels_model');
//$levelslist = $this->Levels_model->get($param2);
?>
<section class="content">
<!-- Start .row -->
<div class=row>                      
    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class="box box-primary">
        <div class=box-body>
               
        <?php echo form_open(base_url() . 'admin/levels/update/'.$levelslist->id, array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'editlevelform1', 'target' => '_top','enctype'=>'multipart/form-data')); ?>
        <div class="padded">
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo ucwords("level type"); ?><span style="color:red">*</span></label>
                <div class="col-sm-6">
                    <input type="text" class="form-control" name="type" value="<?php echo $levelslist->level_type?>"/>
                </div>
            </div> 
            <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo ucwords("level type_ar"); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="type_ar" value="<?php echo $levelslist->level_type_ar?>"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo ucwords("level type_fr"); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="type_fr" value="<?php echo $levelslist->level_type_fr?>"/>
                    </div>
                </div>
            <div class="form-group">
                <label class="col-sm-3 control-label"><?php echo ucwords("status"); ?></label>
                <div class="col-sm-6">
                    <label for="active" class="control-label">
                    <input type="radio" name="rcstatus" <?php echo ($levelslist->status == 0) ? "checked='checked'" : ""?> value="0" id="active"/> Active </label>
                    <label for="inactive" class="control-label">
                    <input type="radio" name="rcstatus" <?php echo ($levelslist->status == 1) ? "checked='checked'" : ""?> value="1" id="inactive"/> Inactive </label>
                </div>
            </div> 
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-6">
                    <button type="submit" class="btn btn-primary">Update</button>
                    <a href="<?php echo base_url('admin/levels'); ?>" class="btn btn-default">Cancel</a>
                </div>
            </div>
        </div>   
        <?php echo form_close(); ?>  
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</section>
<!-- End #content -->

<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, 'Please enter a valid number.');

        $("#editlevelform1").validate({
            rules: {
                type:{    required: true
                        },
            },
            messages: {
                type: {   required: "Enter level type",
                },
            }
        });
    });
</script>