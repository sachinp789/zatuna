<!-- Start .row -->
<!-- Start .row -->
<?php ?>
<div class=row>                      

    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class="panel-default toggle panelMove panelClose panelRefresh">
            <div class=panel-body>
                <div class="tabs mb20">
                    <ul id="import-tab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#project-list" data-toggle="tab" aria-expanded="true">Ingredient Category</a>
                        </li>                       
                    </ul>
                    <div id="import-tab-content" class="tab-content">
                        <div class="tab-pane fade active in" id="project-list">
                            
                            <a data-placement="top" data-toggle="tooltip" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_category_ingredients_create');" href="#" class="links"><i class="fa fa-plus"></i>Ingredient Category</a>
                            
                            <div id="getresponse">
                              
                            <table id="datatable-list" class="table table-striped table-bordered table-responsive" cellspacing=0 width=100%>
                                <thead>
                                    <tr>
                                        <th>No</th>			
                                        <th>Name</th>
                                        <th>Status</th>		
                                        <th>Action</th>	
                                    </tr>
                                </thead>

                                <tbody>
                                <?php
                                foreach ($categories as $row):    
                                ?> 
                                <tr>   
                                <td></td>
                                <td><?php echo $row->name?></td>
                                <td><i data="<?php echo $row->id;?>" class="status_checks btn
                                    <?php echo ($row->status)?'btn-danger': 'btn-success'?>"><?php echo ($row->status)? 'Inactive' : 'Active'?></i></td>
                                <td class="menu-action">
                                <a data-placement="top" data-toggle="tooltip" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_category_ingredients_edit/<?php echo $row->id; ; ?>');" href="#"><span class="label label-primary mr6 mb6"><i aria-hidden="true" class="fa fa-pencil"></i>Edit</span></a>
                                <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>admin/ingredientcategory/delete_ingcat/<?php echo $row->id; ?>');"  data-toggle="tooltip" data-placement="top" ><span class="label label-danger mr6 mb6"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</span></a>
                                </td>
                                </tr>
                                <?php
                                endforeach;
                                ?>        															
                                </tbody>
                            </table>
                          
                            </div>
                        </div>

                        <!-- tab content -->
                        

                    </div>

                </div>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End contentwrapper -->
</div>
<!-- End #content -->
<script type="text/javascript">
 $(document).on('click','.status_checks',function(){
  var status = ($(this).hasClass("btn-success")) ? '1' : '0';
  var msg = (status =='0')? 'Inactive' : 'Active';

  if(confirm("Are you sure to "+ msg)){
    var current_element = $(this);
    url = "<?php echo base_url(); ?>admin/ingredientcategory/update_status/";
    $.ajax({
      type:"POST",
      url: url,
      data: {id:$(current_element).attr('data'),status:status},
      success: function(data)
      {   
       // console.log(data);return false;
        location.reload();
      }
    });
  }      

});
</script>