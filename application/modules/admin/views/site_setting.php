<section class="content">
<!-- Start .row -->
<div class="row">
    <div class="col-lg-12">
        <!-- col-lg-12 start here -->
        <div class="box box-primary">
            <div class="box-body">
            <h3><?php echo $this->lang->line('site_gen'); ?></h3><hr>
                    <?php
if (count($sites) > 0):
	echo form_open(base_url() . 'admin/configuration_update/' . $sites[0]->id, array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'site-form', 'target' => '_top', "enctype" => "multipart/form-data"));
else:
	echo form_open(base_url() . 'admin/configuration', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'site-form', 'target' => '_top', "enctype" => "multipart/form-data"));
endif;
?>
                    <input type="hidden" name="prev_logo" value="<?php echo $sites[0]->logo ?>">
                    <div class="padded">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('site_title'); ?><span style="color:red"></span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="site_title" id="site_title" value="<?php echo $sites[0]->title ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('site_logo'); ?><span style="color:red"></span></label>
                            <div class="col-sm-6">
                                <input type="file" class="form-control" name="site_logo" id="site_logo"/>
                                <img src="<?php echo (base_url() . 'uploads/sitelogo/' . $sites[0]->logo) ?>" width="50"/>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-sm-3 control-label">Email<span style="color:red"></span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="site_email" id="site_email" value="<?php echo $sites[0]->email ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label">Phone<span style="color:red"></span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="site_phone" id="site_phone" value="<?php echo $sites[0]->phone ?>" />
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('site_description'); ?><span style="color:red"></span></label>
                            <div class="col-sm-6">
                               <textarea name="footer_note" id="footer_note" class="form-control"><?php echo trim($sites[0]->description) ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('site_lang'); ?><span style="color:red"></span></label>
                            <div class="col-sm-6">
                               <select name="site_language" id="site_language" class="form-control">
                                    <option value="en" <?php if ($sites[0]->language == 'en') {
	echo "selected='selected'";
}
?>>English</option>
                                    <option value="fr" <?php if ($sites[0]->language == 'fr') {
	echo "selected='selected'";
}
?>>French</option>
                                    <option value="ar" <?php if ($sites[0]->language == 'ar') {
	echo "selected='selected'";
}
?>>Arabic</option>
                               </select>
                            </div>
                        </div>
                        <h3><?php echo $this->lang->line('site_emailset'); ?></h3>
                        <hr>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('smtp_protocol'); ?><span style="color:red"></span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="smtp_protocol" id="smtp_protocol" value="<?php echo $sites[0]->smtp_protocol ?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('smtp_host'); ?><span style="color:red"></span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="smtp_host" id="smtp_host" value="<?php echo $sites[0]->smtp_host ?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('smtp_port'); ?><span style="color:red"></span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="smtp_port" id="smtp_port" value="<?php echo $sites[0]->smtp_port ?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('smtp_user'); ?><span style="color:red"></span></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="smtp_username" id="smtp_username" value="<?php echo $sites[0]->smtp_username ?>" />
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('smtp_pwd'); ?><span style="color:red"></span></label>
                            <div class="col-sm-6">
                                <input type="hidden" name="siteemail_pwd" value="<?php echo $sites[0]->smtp_password ?>">
                                <input type="password" class="form-control" name="smtp_password" id="smtp_password">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary" ><?php echo $this->lang->line('btnsave'); ?></button>
                               <!--  <a href="<?php echo base_url('admin/dashboard'); ?>" class="btn btn-default">Cancel</a> -->
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
     jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value); // /^[0-9 ]+$/
        }, 'Please enter a valid number.');
     jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
     jQuery.validator.addMethod("email", function(value, element)
        {
        return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
        }, "Please enter a valid email address.");
    $("#site-form").validate({
           rules: {
                site_title: {character:true,required:true},
                site_email: {required:true,email: true},
                site_phone: {number:true},
                smtp_port: {
                    number: true,
                },
                site_logo: {
                      required: false,
                      extension: 'gif|png|jpg|jpeg',
                    },
                },
            messages: {
                site_title: {required:"Enter site title",character:"Enter valid site title"},
                site_phone: {required:"Enter site phone",number:"Enter valid site phone"},
                site_email: {
                    required: "Enter site email",
                    email: "Enter valid email address"
                },
                smtp_port: "Enter valid port number",
                site_logo:{
                   extension:"Only gif|png|jpg|jpeg file is allowed!"
                },
            }
        });
    });
</script>