<section class="content">
<div class=row>
    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class="box box-primary">
           <div class="box-body">
            <?php echo form_open(base_url() . 'admin/notification/sendnotification/', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'sendnotifications', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>
            <div class="padded">
                <div class="form-group">
                    <label class="col-sm-3 control-label">Activity<span style="color:red">*</span></label>
                    <div class="col-sm-6">

                    <select name="select_activity" id="select_activity" class="form-control">
                    <option value="">-- Select Activity --</option>
                        <option value="last_loggedin">Last loggedin</option>
                        <option value="special">Special Recipe</option>
                    </select>
                    </div>
                </div>

                <div class="form-group time" style="display:none">
                    <label class="col-sm-3 control-label">Time interval<span style="color:red">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" id="time_interval" name="time_interval" class="form-control">
                    </div>
                </div>

                <div class="form-group featured" style="display:none">
                    <label class="col-sm-3 control-label">Recipes<span style="color:red">*</span></label>
                    <div class="col-sm-6">
                       <select name="featured_recipe" id="featured_recipe" class="form-control">
                       </select>
                    </div>
                </div>

                <div class="form-group filter" style="display:none">
                    <label class="col-sm-3 control-label">Filter By<span style="color:red">*</span></label>
                    <div class="col-sm-6">
                       <select name="filter" id="filter" class="form-control">
                            <option value="">-- Select Filter --</option>
                            <option value="all">All</option>
                            <option value="like">Most Like</option>
                            <option value="rating">Rating</option>
                       </select>
                    </div>
                </div>

                <div class="getresponse">
                    <table id="users" class="table table-striped table-bordered table-responsive" style="width: 80%;margin: 0 auto">

                    </table>
                </div>

                <div class="getresponse-like">
                    <table id="userslike" class="table table-striped table-bordered table-responsive">

                    </table>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Message<span style="color:red">*</span></label>
                    <div class="col-sm-6">
                    <textarea name="message" class="form-control"></textarea>
                    </div>
                </div>

                <div id="form-group">

                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-primary" name="btnnotify"><?php echo $this->lang->line('btnnotify'); ?></button>
                        <a href="<?php echo base_url('admin/notification'); ?>" class="btn btn-default"><?php echo $this->lang->line('btncancel'); ?></a>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End #content -->
</section>
<script type="text/javascript">
$(document).ready(function () {


$("#select_activity").on('change',function(){

        if($(this).val()=='last_loggedin'){
            $('.time').show();
            $('.filter').hide();
            $('.featured').hide();
            $('.getresponse').hide();
           // $('.userslike').hide();
            $('#userslike').empty();
        }else{
            $('.featured').hide();
            $('.time').hide();
            $('.filter').hide();
            $('.getresponse').hide();
            //$('.userslike').hide();
            $('#userslike').empty();
        }

        if($(this).val() == 'special'){
            //get_users()
            $('.getresponse').show();
            $('.featured').show();
            $('.userslike').hide();
            var featured = 1;
            var lang = "<?php echo $this->session->userdata('siteLang'); ?>";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('admin/recipes/get_featured_recipes'); ?>/"+featured,
                dataType: "json",
                success: function(data){
                     $('#featured_recipe')
                    .find('option')
                    .remove()
                    .end()
                    .append('<option value="">-- Select Recipe --</option>');
                     $.each(data, function (key,value){
                        //console.log(value);
                         var opt = $('<option />'); // here we're creating a new select option with for each city
                         opt.val(value.id);
                         if(lang == 'ar'){
                         opt.text(value.recipe_name_ar);
                         }
                         else if(lang == 'fr'){
                         opt.text(value.recipe_name_fr);
                         }
                         else{
                         opt.text(value.recipe_name);
                         }
                         $('#featured_recipe').append(opt);
                     });
                },
            });
        }else{$('.featured').hide();$('.userslike').hide();}
$('.getresponse-like').hide();
});

$("#featured_recipe").on('change',function(){
  if($(this).val() != ''){
        $('.filter').show();
        $('.getresponse').hide();
        $('#userslike').empty();
        $("#filter option:first").attr('selected', true);
       // $('#users').empty();
  }
  else{
     $('.getresponse').show();
     $('.filter').hide();
     $('#userslike').empty();
  }
  $('.getresponse-like').hide();
});

$("#filter").on('change',function(){

        var recipe = $("#featured_recipe").val();

        if($(this).val() == 'all'){
            $('.getresponse-like').show();
                $.ajax({
                method: "POST",
                url: "<?php echo base_url('admin/user/getusers'); ?>",
                dataType: "json",
                success: function (data) {
                    var jsonObj = data; //For storing the response data in jsonObj variable.
                    var strHTML = '<tr><th><input type="checkbox" id="usermost" onclick="checkLikeUser()">#</th><th>Name</th><th>Token</th></tr>';
                    $(jsonObj).each(function(){
                        strHTML+='<tr>';
                        var row = $(this)[0]; //Extracting out row object one by one.
                        //console.log(row.firstname);
                        strHTML += '<td><input type="checkbox" value="'+row.id+'" class="mostusers" name="userslist[]"></td><td>'+ row.firstname  +' '+row.lastname +'</td><td>'+ row.device_token  + '</td>'; //For creating the html part of the table
                        strHTML+='</tr>';
                    });
                    // strHTML +='</tr>';
                    $('#userslike').empty().append(strHTML);//To append the html part into the table
                },
                error:function()
                {
                    alert('some error occurred');
                }
            });
        }
        else if($(this).val() == 'like'){
          $('.getresponse-like').show();
           $.ajax({
                type: "POST",
                url: "<?php echo base_url('admin/recipes/get_mostlike_recipes'); ?>/"+recipe,
                dataType: "json",
                success: function(data){
                    //console.log(data);return false;
                    var strHTML = '<tr><th><input type="checkbox" id="usermost" onclick="checkLikeUser()">#</th><th>Name</th><th>Token</th></tr>';
                    $(data).each(function(){
                        strHTML+='<tr>';
                        var row = $(this)[0]; //Extracting out row object one by one.

                        strHTML += '<td><input type="checkbox" value="'+row.id+'" class="mostusers" name="userslist[]"></td><td>'+ row.likeuser +' '+row.lastname+'</td><td>'+ row.device_token + '</td>'; //For creating the html part of the table
                        strHTML+='</tr>';
                    });
                    // strHTML +='</tr>';
                    $('#userslike').empty().append(strHTML);//To append the html part into the table
                },
            });
        }
        else if($(this).val() == 'rating'){
            $('.getresponse-like').show();
             $.ajax({
                type: "POST",
                url: "<?php echo base_url('admin/recipes/get_mostrated_recipes'); ?>/"+recipe,
                dataType: "json",
                success: function(data){
                   if(data.length === 0){
                    //alert(1);
                   }
                   else{

                    var strHTML = '<tr><th><input type="checkbox" id="usermost" onclick="checkLikeUser()">#</th><th>Name</th><th>Token</th></tr>';
                    $(data).each(function(){
                        strHTML+='<tr>';
                        var row = $(this)[0]; //Extracting out row object one by one.

                        strHTML += '<td><input type="checkbox" value="'+row.id+'" class="mostusers" name="userslist[]"></td><td>'+ row.likeuser + ' '+row.lastname+'</td><td>'+ row.device_token + '</td>'; //For creating the html part of the table
                        strHTML+='</tr>';
                    });
                    // strHTML +='</tr>';
                    $('#userslike').empty().append(strHTML);//To append the html part into the table
                }},
            });
        }
        else{
              $('.getresponse-like').hide();
             $('#userslike').empty();
        }
});

/*function get_users(){
    $.ajax({
            method: "POST",
            url: "<?php echo base_url('admin/user/getusers'); ?>",
            dataType: "json",
            success: function (data) {
                var jsonObj = data; //For storing the response data in jsonObj variable.
                var strHTML = '<tr><td colspan="3"><input type="checkbox" onclick="checkAll();" id="usernotify"> All</td></tr>';
                $(jsonObj).each(function(){
                    strHTML+='<tr>';
                    var row = $(this)[0]; //Extracting out row object one by one.
                    //console.log(row.firstname);
                    strHTML += '<td><input type="checkbox" value="'+row.id+'" class="sendusers"></td><td>'+ row.firstname + '</td><td>'+ row.lastname + '</td>'; //For creating the html part of the table
                    strHTML+='</tr>';
                });
                // strHTML +='</tr>';
                $('#users').empty().append(strHTML);//To append the html part into the table
            },
            error:function()
            {
                alert('some error occurred');
            }
        });
}*/

$("#sendnotifications").validate({
    rules: {
        select_activity:{
                    required: true,
                },
        time_interval:{
            required:true,
        },
        featured_recipe:{required:true},
        filter:{required:true},
        message:{
            maxlength: 140,
        }
    },
    messages: {
        select_activity: { required: "Select activity"
        },
        time_interval: { required: "Select time interval"
        },
        featured_recipe:{required:"Select Recipes"},
        filter:{required:"Select Filter option"}
    }
});

});

function checkAll(){ // All users checked

     $("#usernotify").on('change',function() { // bulk checked
        var status = this.checked;
        $(".sendusers").each( function() {
            $(this).prop("checked",status);
        });

    });
}

function checkLikeUser(){ // All users checked

     $("#usermost").on('change',function() { // bulk checked
        var status = this.checked;
        $(".mostusers").each( function() {
            $(this).prop("checked",status);
        });

    });
}

$( function() {
        $( "#time_interval" ).datepicker({
            dateFormat: 'yy-mm-dd'
        });
});

</script>

<!--
// Most like

    SELECT users.firstname as likeuser FROM user_favourites AS UL LEFT JOIN recipes AS REC ON REC.id = UL.recipe_id
LEFT JOIN users ON users.id = UL.user_id
WHERE REC.recipe_subcategory_id =3 GROUP BY users.id
-->