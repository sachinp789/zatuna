<section class="content">
<div class=row>
    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class="box box-primary">
           <div class="box-body">
            <?php echo form_open(base_url() . 'admin/notification/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'notifications', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>
            <div class="padded">
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('page_title_name'); ?><span style="color:red">*</span></label>
                    <div class="col-sm-6">
                    <input type="text" name="device_title" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('status'); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                            <label for="active" class="control-label">
                            <input type="radio" name="notification_status" value="0" id="active" checked/> <?php echo $this->lang->line('status_name_act'); ?> </label>
                            <label for="inactive" class="control-label">
                            <input type="radio" name="notification_status" value="1" id="inactive"/> <?php echo $this->lang->line('status_name_dact'); ?> </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-primary" name="btnadd"><?php echo $this->lang->line('btnadd'); ?></button>
                        <a href="<?php echo base_url('admin/notification'); ?>" class="btn btn-default"><?php echo $this->lang->line('btncancel'); ?></a>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End #content -->
</section>
<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, 'Please enter a valid number.');

        $("#notifications").validate({
            rules: {
                device_title:{
                            required: true,
                            character:true,
                        },
                device_text:{required:true},
                device_media:{required:true},
            },
            messages: {
                unit_name: { required: "Enter title",character: "Enter valid title"
                },
            }
        });
    });
</script>