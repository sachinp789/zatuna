<?php
$this->load->model('admin/Permissions_model'); 
$permissions = $this->Permissions_model->getPermission();
?>
<!-- Start .row -->
<div class=row>                      
    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body"> 

                <div class="box-content">     
                    <div class="">
                        <span style="color:red">* <?php echo "is " . ucwords("mandatory field"); ?></span> 
                    </div>                                    
                    <?php echo form_open(base_url() . 'admin/user_role/create', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'user-role-form', 'target' => '_top','onsubmit'=>'return validate_form()')); ?>
                    <div class="padded">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Role Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="role_name" id="role_name"
                                       value=""/>
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="col-sm-4 control-label">Permission</label>
                            <div class="col-sm-8">
                            <?php
                            foreach ($permissions as $key => $value) {
                            ?>
                            <div class="checkbox">
                                <label class="control-label">
                                <input type="checkbox" name="roles[]" class="form-group" value="<?php echo $value->id;?>">
                                <?php echo $value->permission_type;?>
                                <span class="glyphicon glyphicon-question-sign" title="<?php echo $value->description ?>"></span>
                                </label>
                            </div>
                            <?php
                            }
                            ?>
                            </div>
                        </div>												
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-info vd_bg-green" ><?php echo ucwords("add"); ?></button>
                            </div>
                        </div>
                    </div>
                    </form>               
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
   $(document).ready(function () {
        $("#user-role-form").validate({
            rules: {
                role_name: "required",
            },
            messages: {
                role_name: "Pleas enter role name",
            }
        });
    });
   function validate_form()
    {
    valid = true;

    if($('input[type=checkbox]:checked').length == 0)
    {
        alert ("Please select at least one permission." );
        valid = false;
    }

    return valid;
    }
</script>