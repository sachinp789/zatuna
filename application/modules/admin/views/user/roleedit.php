<?php
$this->load->model('admin/User_roles_model');
$this->load->model('admin/Permissions_model'); 

$userroles = $this->User_roles_model->getRolesPermission(urldecode($param2));
$permissions = $this->Permissions_model->getPermission();
?>
<!-- Start .row -->
<div class=row>                      
    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class="panel-default">
            <div class="panel-body"> 

                <div class="box-content">     
                    <div class="">
                        <span style="color:red">* <?php echo "is " . ucwords("mandatory field"); ?></span> 
                    </div>                                    
                    <?php echo form_open(base_url() . 'admin/user_role/update/'.urldecode($param2), array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'user-role-update', 'target' => '_top')); ?>
                    <div class="padded">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Role Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" name="role_name" id="role_name"
                                       value="<?php echo $userroles[0]->role_name?>"/>
                            </div>
                        </div>

                         <div class="form-group">
                            <label class="col-sm-4 control-label">Permission</label>
                            <div class="col-sm-8">
                            <?php
                            $checked = array();
                            foreach ($permissions as $key => $value){
                                foreach ($userroles as $row => $role){
                                    if($role->permission_id == $value->id){
                                        $checked[$key] = "checked='checked'";
                                }
                            }
                            ?>  
                            <div class="checkbox">
                                <label class="control-label">
                                <input type="checkbox" name="roles[<?php echo $value->id;?>]" class="form-group" <?php echo $checked[$key]?> value="<?php echo $value->id;?>">
                                <?php echo $value->permission_type;?>
                                <span class="glyphicon glyphicon-question-sign" title="<?php echo $value->description ?>"></span>
                                </label>
                            </div>
                            <?php
                            }
                            ?>
                            </div>
                        </div>
                        <input type="hidden" class="form-control" name="rolenm" id="role_name"
                                       value="<?php echo $userroles[0]->role_name?>"/>									
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <button type="submit" class="btn btn-info vd_bg-green" ><?php echo ucwords("update"); ?></button>
                            </div>
                        </div>
                    </div>
                    </form>               
                    </div>                
                </div>
            </div>
        </div>
    </div>
</div>