<!DOCTYPE html>
<!--[if lt IE 8]><html class="no-js lt-ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class=no-js>
    <!--<![endif]-->
    <html class=no-js>

        <head>
            <meta charset=utf-8>
            <title>Admin Login</title>
            <meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1">
            <meta name=author content="">
            <meta name=description content="">
            <meta name=keywords content="">
            <meta name=application-name content="">
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel=stylesheet type=text/css>
            <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel=stylesheet type=text/css>
            <link href='<?php echo base_url(); ?>assets/dist/admin/adminlte.min.css' rel='stylesheet' media='screen'>
            <link href='<?php echo base_url(); ?>assets/dist/admin/lib.min.css' rel='stylesheet' media='screen'>
            <link href='<?php echo base_url(); ?>assets/dist/admin/app.min.css' rel='stylesheet' media='screen'>
            <style type="text/css">
                label.error {
                        color: #ed7a53;
                    }
            </style>
        </head>

        <body class="login-page">
            <div class="conatiner">
                <div class="login-box">
                    <div class="login-logo"><b>Admin Panel</b></div>
                    <div class="login-box-body">
                        <p class="login-box-msg">Sign in to start your session</p>
                        <?php if ($this->session->flashdata('error')) {?>
                            <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                        <?php }?>
                         <?php if ($this->session->flashdata('success')) {?>
                            <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                        <?php }?>
                        <form method="post" accept-charset="utf-8" id="login-form" role="form">

                            <div class="form-group">
                                    <label for="email">Email</label>
                                    <input name="email" id="email" class="form-control" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" name="password" id="password" class="form-control" placeholder="Password">

                            </div>

                            <div class="row">
                                <div class="col-xs-4">
                                    <button type="submit"  class="btn btn-primary btn-block btn-flat">Sign In</button>
                                </div>
                                <div class="col-xs-8">
                                    <a id="forgot-password-link" class="pull-right" href="<?php echo base_url(); ?>admin/forgot_password" data-toggle="modal">Forgot password ?</a>
                                </div>
                            </div>
                        </form>
                    </div>
                    <br/>
                     <div class=footer>
                        <p class=text-center>Copyrights &copy; <?php echo date('Y'); ?> <a href="<?php echo base_url() ?>" class="color-blue strong">Zatuna</a>. All rights reserved.</p>
                    </div>
            </div>
        </div>
    </body>
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
   <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
   <script type="text/javascript">
   $(document).ready(function () {
        $("#login-form").validate({
            rules: {
                email:{    required: true,email:true
                           //character: true,
                    },
                password:{required:true,}
            },
            messages: {
                email:{   required: "Please enter email",email:"Please enter valid email"},
                password:{   required: "Please enter password"},
            }
        });
   });
</script>

</html>