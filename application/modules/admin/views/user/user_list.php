<section class="content">
<div class=box>
        <div class="box-body">
                <table id="datatable-list-12" class="table table-striped table-bordered table-responsive" cellspacing=0 width=100%>
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <!-- <th>Last Name</th> -->
                            <th>Email</th>
                            <th>Mobile</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $user) {?>
                            <tr>
                                <td><?php echo $user->id; ?></td>
                                <td><?php echo $user->firstname; ?></td>
                                <!-- <td><?php echo $user->lastname; ?></td> -->
                                <td><?php echo $user->email; ?></td>
                                <td><?php echo $user->mobile_no; ?></td>
                                <td><?php echo ($user->status == 0) ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>' ?></td>
                                <td>
                                <a href="<?php echo base_url() . 'admin/user/edit/' . $user->id ?>" class="btn btn-sm btn-primary"><span><i class="fa fa-pencil" aria-hidden="true"></i> <?php echo $this->lang->line('btnedit'); ?></span></a>
                                 <a href="<?php echo base_url(); ?>admin/user/delete/<?php echo $user->id; ?>" class="btn btn-sm btn-danger" onclick='return confirm("Are you sure want to remove <?php echo $user->firstname ?>?");'><span><i class="fa fa-trash-o" aria-hidden="true"></i> <?php echo $this->lang->line('btnremove'); ?></span></a>
                                    <!-- <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_user_edit/<?php echo $user->id; ?>');" data-toggle="modal"><span class="label label-primary mr6 mb6"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</span></a>

                                    <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>user/delete/<?php echo $user->id; ?>');" data-toggle="modal" ><span class="label label-danger mr6 mb6"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</span></a>  -->
                                </td>
                            </tr>
                        <?php }?>
                    </tbody>
                </table>
    </div>
</div>

</div>
<!-- End .row -->
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable-list-12').DataTable({
             //"pageLength": 5,
             "lengthMenu": [[5,10,25, 50, -1], [5,10,25, 50, "All"]],
             "pagingType": "full_numbers",
             'aoColumnDefs': [{
                'bSortable': false,
                'aTargets': [-1] /* 1st one, start by the right */
            }]
           // "dom": '<"top"i>pf<"bottom"lrt><"clear">'
        });
} );
</script>