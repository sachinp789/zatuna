<!-- Start .row -->
<?php 
$this->load->model('admin/User_roles_model');
$roles = array();
foreach ($users as $key => $value){
        $roles[] = $this->User_roles_model->getUniqueRole($value->role_name);
}
//echo '<pre>';
//print_r($roles);exit;

?>
<div class=row> 
    <div class="col-lg-12">
        <div class=" panel-default toggle panelMove panelClose panelRefresh">
            <div class=panel-body>
                <a href="#" class="links" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_user_rolecreate');" data-toggle="modal"><i class="fa fa-plus"></i> Add User Role</a>
                <table id="datatable-list" class="table table-striped table-bordered table-responsive" cellspacing=0 width=100%>
                        <thead>
                        <tr>
                            <!-- <th>No</th> -->
                            <th>Role</th>
                            <th>Permission</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php 
                        foreach ($users as $key => $value) :
                            //print_r(count($roles[$key]));
                        ?>   
                        <tr>
                           <!--  <td><?php echo $value->id?></td> -->
                            <td><?php echo $value->role_name?></td>
                            <td>
                            <?php 
                            if(count($roles[$key]) > 1){
                                foreach ($roles[$key] as $rk => $permission){

                                    $ids[$rk] = $permission->id;
                                    $abc[$rk] = $permission->permission_type;
                                }
                           /* echo '<pre>';
                            print_r($abc);
                           // exit;    */
                            echo implode("<strong>|</strong>", $abc);
                            }
                            else{
                            echo $value->permission_type;   
                            }
                            ?>
                            </td>
                            <td>
                            <?php
                            if(count($roles[$key]) > 1){
                                $roleID = implode("|", $ids);
                            }else{$roleID = $value->id;}
                            ?>
                            <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_user_roleedit/<?php echo $roleID ?>');" data-toggle="modal"><span class="label label-primary mr6 mb6"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</span></a>

                            <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>admin/user_role/delete_role/<?php echo $roleID; ?>');" data-toggle="modal" ><span class="label label-danger mr6 mb6"><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</span>
                            </a> 
                            </td>
                        </tr>
                        <?php
                        endforeach;    
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- End .panel -->
    </div>
</div>

</div>
<!-- End .row -->
</div>
<!-- End contentwrapper -->

