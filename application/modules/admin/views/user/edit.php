<?php
/*$roles = $this->db->where_not_in('role_name', array('Student', 'Professor'))->order_by('role_name', 'ASC')->get('role')->result();
$this->load->model('user/Role_model');*/
//$this->load->model('admin/Users_model');
//$user = $this->Users_model->get($param2);
?>
<section class="content">
<!-- Start .row -->
<div class=row>
    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class="box box-primary">
            <div class="box-body">
                    <?php echo form_open(base_url() . 'admin/user/update/' . $user->id, array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'autocomplete' => 'off', 'id' => 'user-update-form', 'target' => '_top')); ?>
                    <input type="hidden" name="txtuserid" id="txtuserid" value="<?php echo $user->id; ?>">
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('user_fname'); ?></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="first_name" id="first_name"
                                       value="<?php echo $user->firstname; ?>"/>
                            </div>
                        </div>
                       <!--  <div class="form-group">
                            <label class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="last_name" id="last_name"
                                       value="<?php echo $user->lastname; ?>"/>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('user_email'); ?></label>
                            <div class="col-sm-6">
                                <input type="email" class="form-control" name="email"
                                       autocomplete="off" id="email" value="<?php echo $user->email ?>" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('user_pwd'); ?></label>
                            <div class="col-sm-6">
                                <input type="password" class="form-control" value="<?php echo $user->password ?>" name="password" id="password" readonly/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('user_gender'); ?></label>
                            <div class="col-sm-6">
                                <select id="gender" name="gender" class="form-control">
                                    <option value="Male"
                                            <?php if ($user->gender == 'Male') {
	echo 'selected';
}
?>>Male</option>
                                    <option value="Female"
                                            <?php if ($user->gender == 'Female') {
	echo 'selected';
}
?>>Female</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('user_mobile'); ?></label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" name="mobile" id="mobile"
                                       value="<?php echo $user->mobile_no; ?>" maxlength="15"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('user_address'); ?></label>
                            <div class="col-sm-6">
                                <textarea id="address" class="form-control" name="address"><?php echo $user->address; ?></textarea>
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label class="col-sm-4 control-label">Role</label>
                            <div class="col-sm-8">
                                <select id="role" class="form-control" name="role">
                                    <option value="">Select</option>
                                    <?php foreach ($roles as $role) {
	?>
                                        <option value="<?php echo $role->role_id; ?>"
                                                <?php if ($user->role_id == $role->role_id) {
		echo 'selected';
	}
	?>><?php echo $role->role_name; ?></option>
                                    <?php }?>
                                </select>
                            </div>
                        </div> -->
                        <div class="form-group">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('status'); ?></label>
                            <div class="col-sm-6">
                                <label for="active" class="control-label">
                                    <input type="radio" name="rcstatus" value="0" id="active" <?php echo ($user->status == 0) ? "checked='checked'" : "" ?>/> <?php echo $this->lang->line('status_name_act'); ?> </label>
                                <label for="inactive" class="control-label">
                                    <input type="radio" name="rcstatus" value="1" <?php echo ($user->status == 1) ? "checked='checked'" : "" ?> id="inactive"/> <?php echo $this->lang->line('status_name_dact'); ?> </label>
                            </div>
                            <!-- <div class="col-sm-6">
                                <select id="role" class="form-control" name="status">
                                    <option value="">Select</option>
                                    <option value="0"
                                            <?php if ($user->status == 0) {
	echo 'selected';
}
?>>Active</option>
                                    <option value="1"
                                            <?php if ($user->status == 1) {
	echo 'selected';
}
?>>Inactive</option>
                                </select>
                            </div>	 -->
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-3 col-sm-6">
                                <button type="submit" class="btn btn-primary" ><?php echo $this->lang->line('btnupdate'); ?></button>
                                <a href="<?php echo base_url('admin/user'); ?>" class="btn btn-default"><?php echo $this->lang->line('btncancel'); ?></a>
                            </div>
                        </div>
                 </form>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9-+]+$/.test(value); // /^[0-9 ]+$/
        }, 'Please enter a valid number.');
        /*jQuery.validator.addMethod("phoneno", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length >= 0 &&
            phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
        }, "<br />Please specify a valid phone number");*/
        /*jQuery.validator.addMethod('customphone', function (value, element) {
            return this.optional(element) || /^(\+91-|\+91|0)?\d{15}$/.test(value);
        }, "Please enter a valid phone number");*/
         jQuery.validator.addMethod("email", function(value, element)
        {
        return this.optional(element) || /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z.]{2,5}$/i.test(value);
        }, "Please enter a valid email address.");

        $("#user-update-form").validate({
            rules: {
                first_name: {required:true},
                last_name: {character:true},
                email: {
                            required: true,
                            email: true,
                            /*remote: {
                                url: "<?php echo base_url(); ?>admin/user/check_user_email",
                                type: "post",
                                data: {
                                    email: function () {
                                        return $("#email").val();
                                    },
                                    userid: function () {
                                          return $("#txtuserid").val();
                                    },
                                }
                            }*/
                        },
               /* mobile: {number:true},*/
            },
            messages: {
                first_name: {character:"Please enter valid first name"},
                last_name: {character:"Please enter valid last name"},
                email: {
                    required: "Enter email id",
                    email: "Enter valid email id",
                    remote: "Email id already exists",
                },
                /*mobile: {maxlength:"Please enter valid mobile number"},*/
            }
        });
    });
</script>