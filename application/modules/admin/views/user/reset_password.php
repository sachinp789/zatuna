<!DOCTYPE html>
<!--[if lt IE 8]><html class="no-js lt-ie8"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class=no-js>

        <head>
            <meta charset=utf-8>
            <title>Reset Password</title>
            <meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1">
            <meta name=author content="">
            <meta name=description content="">
            <meta name=keywords content="">
            <meta name=application-name content="">
            <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel=stylesheet type=text/css>
            <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel=stylesheet type=text/css>
            <link href='<?php echo base_url(); ?>assets/dist/admin/adminlte.min.css' rel='stylesheet' media='screen'>
            <link href='<?php echo base_url(); ?>assets/dist/admin/lib.min.css' rel='stylesheet' media='screen'>
            <link href='<?php echo base_url(); ?>assets/dist/admin/app.min.css' rel='stylesheet' media='screen'>
        </head>

        <body class="login-page">
            <div class="conatiner">
                <div class="login-box">
                    <div class="login-box-body">
                    <h3 class="box-title">Reset Password</h3>
                        <?php if($this->session->flashdata('error')){ ?>
                            <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                        <?php } ?>
                        <?php if($this->session->flashdata('success')){ ?>
                            <div class="alert alert-success"><?php echo $this->session->flashdata('success'); ?></div>
                        <?php } ?>
                        <form method="post" accept-charset="utf-8" id="login-form" role="form" method="post">
                        <input type="hidden" name="_token" value="<?php echo $token;?>">    
                            <div class="form-group">
                                    <label for="password">Password</label>
                                    <input type="password" name="password" id="password" class="form-control" placeholder="Password" required>     
                            </div>

                            <div class="form-group">
                                    <label for="cpassword">Confirm Password</label>
                                    <input type="password" name="cpassword" id="cpassword" class="form-control" placeholder="Confirm Password" required>     
                            </div>
                            
                            <div class="row">
                                <div class="col-xs-4">
                                    <button type="submit"  class="btn btn-primary btn-block btn-flat">Reset</button>
                                </div>
                                 <div class="col-xs-4">
                                    <a href="<?php echo base_url('admin/login');?>" class="btn btn-primary btn-block btn-flat">Back</a>
                                </div>
                            </div>
                        </form> 
                    </div>
                    <br/>
                     <div class=footer>
                        <p class=text-center>Copyrights &copy; 2016 <a href="#" class="color-blue strong" target=_blank>Zatuna</a>. All rights reserved.</p>
                    </div>
            </div>
        </div>
    </body>
</html>