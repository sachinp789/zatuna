<?php
$this->load->model('admin/Recipe_categories_model');
$recpcatlist = $this->Recipe_categories_model->get($param2);
?>
<!-- Start .row -->
<div class=row>                      

    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class=" panel-default">
            <div class=panel-body>
                <div class="tabs mb20">
                    <ul id="import-tab" class="nav nav-tabs">
                        <li class="active">
                            <a href="#quiz-details" data-toggle="tab" aria-expanded="true">Edit Recipe Category</a>
                        </li>                       
                    </ul>
                    <div id="import-tab-content" class="tab-content">
                        <div class="tab-pane fade active in" id="quiz-details">
                            <div class="panel-default">
                                <div class="panel-body"> 

                                    <div class="box-content">  

                                        <?php echo form_open(base_url() . 'admin/recipecategory/update/'.$recpcatlist->id, array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'editrecpcat', 'target' => '_top','enctype'=>'multipart/form-data')); ?>
                                        <div class="padded">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"><?php echo ucwords("category name"); ?><span style="color:red">*</span></label>
                                                <div class="col-sm-6">
                                                    <input type="text" class="form-control" name="cat_name" value="<?php echo $recpcatlist->cat_name?>"/>
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label"><?php echo ucwords("status"); ?></label>
                                                <div class="col-sm-6">
                                                    <select name="rcstatus" class="form-control">
                                                        <option value="0" <?php echo ($recpcatlist->status == 0)? 'selected="selected"':'';?>>Active</option>
                                                        <option value="1" <?php echo ($recpcatlist->status == 1)? 'selected="selected"':'';?>>Inactive</option>
                                                    </select>
                                                </div>
                                            </div> 
                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-6">
                                                    <button type="submit" class="btn btn-info vd_bg-green">Update</button>
                                                </div>
                                            </div>
                                        </div>   
                                        <?php echo form_close(); ?>  
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- tab content -->
                      
                    </div>

                </div>
            </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End contentwrapper -->
</div>
<!-- End #content -->

<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, 'Please enter a valid number.');

        $("#editrecpcat").validate({
            rules: {
                cat_name:{    required: true
                        },
            },
            messages: {
                cat_name: {   required: "Enter recipe category",
                },
            }
        });
    });
</script>