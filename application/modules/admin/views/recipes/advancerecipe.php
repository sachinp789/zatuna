<?php
$Ingridents = $this->db->select('name,name_ar,name_fr')->from('ingredients')->order_by('name', 'asc')->get()->result();
$Recipecategories = $this->db->select('cat_name,cat_name_ar,cat_name_fr')->from('recipe_categories')->order_by('cat_name', 'asc')->get()->result();
?>
<section class="content">
<div class=box>
<!-- col-lg-12 start here -->
<div class="box-body">
<form id="form-filter" class="form-horizontal">
    <div class="col-sm-4">
        <div class="form-group">
            <label for="country" class="col-sm-5 control-label"><?php echo $this->lang->line('id'); ?></label>
            <div class="col-sm-7">
                 <input type="text" name="fromId" id="fromId" class="form-control" placeholder="From">
                 <input type="text" name="fromTo" id="fromTo" class="form-control" placeholder="To">
            </div>
        </div>
        <div class="form-group">
            <label for="RecipeName" class="col-sm-5 control-label"><?php echo $this->lang->line('recp_name_search'); ?></label>
            <div class="col-sm-7">
                 <input type="text" placeholder="Recipe Name" class="form-control" id="RecipeName">
            </div>
        </div>

    </div>
    <div class="col-sm-4">
         <div class="form-group">
            <label for="PreparationTime" class="col-sm-5 control-label"><?php echo $this->lang->line('prep_time_search'); ?></label>
            <div class="col-sm-7">
                <input type="text" placeholder="Preparation Time" class="form-control" id="PreparationTime">
            </div>
        </div>
        <div class="form-group">
            <label for="CookingTime" class="col-sm-5 control-label"><?php echo $this->lang->line('cook_time_search'); ?></label>
            <div class="col-sm-7">
                <input type="text" placeholder="Cooking Time" class="form-control" id="CookingTime">
            </div>
        </div>
        <div class="form-group">
            <label for="IngridentName" class="col-sm-5 control-label"><?php echo $this->lang->line('ingname_search'); ?></label>
            <div class="col-sm-7">
            <?php
$select = '<select id="IngridentName" class="form-control">';
$select .= '<option value="">--Select Ingredient--</option>';
foreach ($Ingridents as $value) {
	$select .= '<option value="' . $value->name . '">';
	if ($this->session->userdata('siteLang') == 'ar') {
		//$select.= '<option value="'.$value->name_ar.'">';
		$select .= $value->name_ar;
	} else if ($this->session->userdata('siteLang') == 'fr') {
		//$select.= '<option value="'.$value->name_fr.'">';
		$select .= $value->name_fr;
	} else {
		$select .= $value->name;
	}
	$select .= '</option>';
}
$select .= '</select>';
echo $select;
?>
            </div>
        </div>
    </div>
    <div class="col-sm-4">
        <div class="form-group">
            <label for="RecipeCategory" class="col-sm-5 control-label"><?php echo $this->lang->line('recp_cat_search'); ?></label>
            <div class="col-sm-7">
            <?php
$select = '<select id="RecipeCategory" class="form-control">';
$select .= '<option value="">--Select Recipe Category--</option>';
foreach ($Recipecategories as $value) {
	$select .= '<option value="' . $value->cat_name . '">';
	//$select.= '<option value="'.$value->cat_name.'">';
	if ($this->session->userdata('siteLang') == 'ar') {
		// $select.= '<option value="'.$value->cat_name_ar.'">';
		$select .= $value->cat_name_ar;
	} else if ($this->session->userdata('siteLang') == 'fr') {
		//$select.= '<option value="'.$value->cat_name_fr.'">';
		$select .= $value->cat_name_fr;
	} else {
		//$select.= '<option value="'.$value->cat_name.'">';
		$select .= $value->cat_name;
	}
	$select .= '</option>';
}
$select .= '</select>';
echo $select;
?>
            </div>
        </div>
         <div class="form-group">
            <label for="LastName" class="col-sm-5 control-label"></label>
            <div class="col-sm-7">
                <button type="button" id="btn-filter" class="btn btn-primary"><?php echo $this->lang->line('btnfilter'); ?></button>
                <button type="button" id="btn-reset" class="btn btn-default"><?php echo $this->lang->line('btnreset'); ?></button>
            </div>
        </div>
    </div>
    <!-- <div class="col-sm-4">
         <div class="form-group">
            <label for="FirstName" class="col-sm-5 control-label">First Name</label>
            <div class="col-sm-7">
                <input type="text" placeholder="First Name" class="form-control" id="FirstName">
            </div>
        </div>
        <div class="form-group">
            <label for="LastName" class="col-sm-5 control-label">Last Name</label>
            <div class="col-sm-7">
                <input type="text" placeholder="Last Name" class="form-control" id="LastName">
            </div>
        </div>

        <div class="form-group">
            <label for="LastName" class="col-sm-5 control-label"></label>
            <div class="col-sm-7">
                <button type="button" id="btn-filter" class="btn btn-primary">Filter</button>
                <button type="button" id="btn-reset" class="btn btn-default">Reset</button>
            </div>
        </div>
    </div> -->
</form>
</div>
<div class="clear"></div>
<hr>
<div class="box-body">
<div id="getresponse">
    <table id="table-example" class="table table-hover table-striped table-bordered table-responsive example datatableList display" cellspacing="0" width="100%">
        <thead>
                <tr>
                    <th>ID</th>
                    <th>Recipe Name</th>
                    <th>Preparation Time</th>
                    <th>Cooking Time</th>
                    <th>Recipe Category</th>
                    <th>Ingredient Name</th>
                    <!-- <th>Firstname</th>
                    <th>Lastname</th> -->
                    <th>Created Date</th>
                    <th>Modify Date</th>
                </tr>
        </thead>
        <!-- <thead>
                    <tr>
                    <td>
                         <div class="w98p">
                        <input type="text" name="fromId" id="fromId" class="form-control" placeholder="From"></div>
                         <div class="w98p">
                       <input type="text" name="fromTo" id="fromTo" class="form-control" placeholder="To"></div>
                    </td>

                    <td><div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="Recipe Name" class="form-control" id="RecipeName">
                        </div>
                    </div></td>
                    <td><div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="Preparation Time" class="form-control" id="PreparationTime">
                        </div>
                    </div></td>
                     <td><div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="Cooking Time" class="form-control" id="CookingTime">
                        </div>
                    </div></td>
                    <td><div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="Ingrident Name" class="form-control" id="IngridentName">
                        </div>
                    </div></td>
                     <td><div class="col-sm-4">
                        <div class="w98p">
                                    <input type="text" placeholder="First Name" class="form-control" id="FirstName">
                        </div>
                    </div></td>
                    <td><div class="col-sm-4">
                        <div class="w98p">
                            <input type="text" placeholder="Last Name" class="form-control" id="LastName">
                        </div>
                    </div></td>
                    <td><div class="col-sm-4">
                        <div class="w98p">
                            <input type="date" placeholder="Created Date" class="form-control datepicker" id="Created_at" value="<?php echo set_value('date'); ?>">

                        </div>
                    </div></td>
                </tr>
                </thead> -->
        <!-- <tfoot>
            <tr>
                    <th>ID</th>
                    <th>Recipe Name</th>
                    <th>Preparation Time</th>
                    <th>Cooking Time</th>
                    <th>Ingrident Name</th>
                    <th>Firstname</th>
                    <th>Lastname</th>
                    <th>Created Date</th>
            </tr>
        </tfoot> -->
    </table>
    </form>
</div>
</div>
</div>
</div>
<!-- End .panel -->
</div>
<!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
</section>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script> -->
<!-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>  -->

<script type="text/javascript">
$(document).ready(function() {

  table = $('#table-example').DataTable({

        "processing": false, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "stateSave": true,
        "mark": true,
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('admin/search/ajax_list') ?>",
            "type": "POST",
            "data": function ( data ) {
                //data.country = $('#country').val();
                data.fromId = $('#fromId').val();
                data.fromTo = $('#fromTo').val();
                data.RecipeName = $('#RecipeName').val();
                data.PreparationTime = $('#PreparationTime').val();
                data.CookingTime = $('#CookingTime').val();
               /* data.Firstname = $('#Firstname').val();
                data.LastName = $('#LastName').val();*/
                data.IngridentName = $('#IngridentName').val();
                data.Created_at = $('#Created_at').val();
                data.RecipeCat = $('#RecipeCategory').val();
                //data.LastName = $('#LastName').val();
                //data.address = $('#address').val();
            }
        },

        initComplete: function() {
        $('#form-filter input[type="text"]').unbind();
            $('#form-filter input[type="text"]').bind('keyup', function(e) {
                if(e.keyCode == 13) {
                     table.page( 'next' ).draw( 'page' );
                     table.ajax.reload(null,false);
                }
            });
        },
            "aoColumnDefs" : [ {
            'bSortable' : true,
            'aTargets' : [ 1 ]
        } ],

        "dom": '<"top"i>fp<"bottom"lrt><"clear">',
        "searching": false,
        "paging": true,
        "pagingType": "full_numbers"

        //Set column definition initialisation properties.
        /*"columnDefs": [
        {
            "targets": [ 0 ], //first column / numbering column
            "orderable": false, //set not orderable
        },
        ],*/

    });


    $('#btn-filter').click(function(){ //button filter event click
        table.page( 'next' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });

    $('#Created_at').change(function(){
        //button filter event click
        table.page( 'next' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });

    $("#IngridentName").change(function(){
        table.page( 'next' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });

    $("#RecipeCategory").change(function(){
        table.page( 'next' ).draw( 'page' );
        table.ajax.reload(null,false);  //just reload table
    });

    $('#btn-reset').click(function(){ //button reset event click
        $('#form-filter')[0].reset();
        table.ajax.reload(null,false);  //just reload table
    });

});
</script>