<!-- Start .row -->
<?php
$this->load->model('admin/Ingredients_model');
$this->load->model('admin/Recipes_model');
$this->load->model('admin/Levels_model');
$ingdata = $this->Ingredients_model->getIngredients();
$recipescats = $this->Recipes_model->getRecipescat();
$levels = $this->Levels_model->getLevels();
?>
<section class="content">
<div class=row>

<div class=col-lg-12>
        <!-- col-lg-12 start here -->
<div class="box box-primary">
            <div class="box-body">

                <?php echo form_open_multipart(base_url() . 'admin/recipes/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'recipeform', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>
                <div class="padded">
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('name'); ?><span style="color:red">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="recipe_name" value="" placeholder="Name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('name_ar'); ?><span style="color:red"></span></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="recipe_name_ar" value="" placeholder="Name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('name_fr'); ?><span style="color:red"></span></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="recipe_name_fr" value="" placeholder="Name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('media'); ?><span style="color:red">*</span></label>
                        <div class="col-sm-6">
                            <input type="file" class="form-control" name="recipe_media[]" id="recipe_media" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('description'); ?><span style="color:red">*</span></label>
                        <div class="col-sm-6">
                            <textarea name="recipe_description" class="form-control" rows="3" placeholder="Description"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('description_ar'); ?><span style="color:red"></span></label>
                        <div class="col-sm-6">
                            <textarea name="recipe_description_ar" class="form-control" rows="3" placeholder="Description"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('description_fr'); ?><span style="color:red"></span></label>
                        <div class="col-sm-6">
                            <textarea name="recipe_description_fr" class="form-control" rows="3" placeholder="Description"></textarea>
                        </div>
                    </div>
                   <!--  <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo ucwords("method"); ?><span style="color:red">*</span></label>
                        <div class="col-sm-6">
                            <textarea name="recipe_method" class="form-control" rows="6" placeholder="Preparation Method"></textarea>
                        </div>
                    </div> -->

                    <div class="form-group method">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('method'); ?><span style="color:red">*</span></label>
                            <div class="col-sm-6 prepmethod">
                            <div class="col-sm-12" style="padding:0">
                            <input type="number" name="txtstep[1]" id="txtstep" class="form-control required" placeholder="Step No">
                            <br/>
                            <label class="control-label"><?php echo $this->lang->line('img_vid'); ?></label>
                            <input type="file" name="stepmedia[1]" class="form-control" placeholder="Image/Video" id="stepmedia1" onchange="checkfile(1,'en')">
                             <label class="control-label"><?php echo $this->lang->line('img_vid'); ?>_ar</label>
                            <input type="file" name="stepmedia_ar[1]" class="form-control" placeholder="Image/Video" id="stepmediaar1" onchange="checkfile(1,'ar')">
                             <label class="control-label"><?php echo $this->lang->line('img_vid'); ?>_fr</label>
                             <input type="file" name="stepmedia_fr[1]" class="form-control" placeholder="Image/Video" id="stepmediafr1" onchange="checkfile(1,'fr')">
                            <label></label>
                            <textarea name="preparation_method[1]" id="preparation_method" class="form-control required" rows="3" placeholder="Preparation Description"></textarea>
                            <textarea name="preparation_method_ar[1]" id="preparation_method_ar" class="form-control" rows="3" placeholder="AR_Preparation Description"></textarea>
                            <textarea name="preparation_method_fr[1]" id="preparation_method_fr" class="form-control" rows="3" placeholder="FR_Preparation Description"></textarea>
                            <button class="btn btn-info btn-xs insert"><i class="fa fa-plus"></i></</button>
                            </div>
                            </div>
                    </div>

                   <!--  <input type="button" class="add-row" value="Add Row">
                     <table>
                            <thead>
                                <tr>
                                    <th>Select</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><input type="checkbox" name="record"></td>
                                    <td>Peter Parker</td>
                                    <td>peterparker@mail.com</td>
                                </tr>
                            </tbody>
                        </table>

                    <script type="text/javascript">
                         $(".add-row").click(function(){
                                var markup = "<tr><td><input type='checkbox' name='record'></td><td></td><td></td></tr>";
                                $("table tbody").append(markup);
                            });
                    </script> -->
                     <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('serving'); ?><span style="color:red">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="serving" value="" id="serving" placeholder="Serving"/>
                        </div>
                    </div>

                    <div class="form-group ing">
                            <label class="col-sm-3 control-label"><?php echo $this->lang->line('ingredients'); ?><span style="color:red">*</span></label>
                            <div class="col-sm-6 ing2">
                            <?php if (count($ingdata) == 0): ?>
                            <a href="<?php echo base_url() ?>admin/ingredients/create" class="btn btn-xs btn-default"><span>Add Ingredients</span></a>
                            <?php else: ?>
                            <div class="col-sm-12" style="padding:0">
                            <div class="col-sm-4" style="padding:0">
                            <span><?php echo $this->lang->line('ingredients'); ?></span>
                            <select name="ingname[1]" id="ingname" class="form-control required" onchange="get_units(this.value,1)" id="ingname_1">
                            <option value="">Select Ingredients</option>
                            <?php
$ing = '';
foreach ($ingdata as $key => $value):
	$ing .= "<option value=$value->id>";
	if ($this->session->userdata('siteLang') == 'ar') {
		$ing .= $value->name_ar;
	} else if ($this->session->userdata('siteLang') == 'fr') {
	$ing .= $value->name_fr;
} else {
	$ing .= $value->name;
}
$ing .= "</option>";
endforeach;
echo $ing;
?>
                            </select>
                            <?php
$ingvalue = '';
foreach ($ingdata as $key => $value):
	$ingvalue .= "<input type=hidden id=ing_value_{$value->id} value={$value->kcal_grams}>";
	$ingvalue .= "<input type=hidden id=protine_value_{$value->id} value={$value->protein}>";
	$ingvalue .= "<input type=hidden id=fat_value_{$value->id} value={$value->fat}>";
	$ingvalue .= "<input type=hidden id=carbs_value_{$value->id} value={$value->carbs}>";
	//$ingvalue.="<input type=hidden id=ingfinal_price_{$value->id} name=ingfinal_price[$value->id]>";
endforeach;
echo $ingvalue;
?>
                            <input type="hidden" id="ingfinal_price_1" name="ingfinal_price[1]">
                            <input type="hidden" id="protinefinal_price_1" name="protinefinal_price[1]">
                            <input type="hidden" id="fatfinal_price_1" name="fatfinal_price[1]">
                            <input type="hidden" id="carbsfinal_price_1" name="carbsfinal_price[1]">
                            </div>
                            <div class="col-sm-3" style="padding:0">
                            <span><?php echo $this->lang->line('qty'); ?></span>
                            <input type="text" name="ingmaterial[1]" class="form-control required" placeholder="Quantity" id="ingmaterial_1" onkeypress="return (event.charCode >= 48 && event.charCode <= 57) ||
   event.charCode == 46 || event.charCode == 0 " onkeyup="Calfunction(0,1)">
                            </div>
                            <div class="col-sm-3" style="padding:0">
                            <span><?php echo $this->lang->line('unit'); ?></span>
                            <select name="unitname[1]" id="unitname_1" class="form-control required" onchange="calculate_calories(this.value,1)">
                                <option value="">Select Unit</option>
                                <?php
/* $unit = '';
foreach ($units as $key => $value) :
$unit .= "<option value=$value->id>";
$unit .= $value->metric_name;
$unit .= "</option>";
endforeach;
echo $unit; */
?>
                            </select>
                            <!-- <input type="text" name="ingunit[]" class="form-control" id="ingunit" placeholder="Unit"> -->
                            </div>
                            <input type="hidden" name="final_calories[1]" id="final_calories_1">
                            <input type="hidden" name="final_calories_ounce[1]" id="final_calories_ounce_1">

                            <input type="hidden" name="protine_calories[1]" id="protine_calories_1">
                            <input type="hidden" name="protine_calories_ounce[1]" id="protine_calories_ounce_1">

                            <input type="hidden" name="fat_calories[1]" id="fat_calories_1">
                            <input type="hidden" name="fat_calories_ounce[1]" id="fat_calories_ounce_1">

                            <input type="hidden" name="carbs_calories[1]" id="carbs_calories_1">
                            <input type="hidden" name="carbs_calories_ounce[1]" id="carbs_calories_ounce_1">

                            <div class="col-sm-2" style="padding:0;">
                            <button class="btn btn-info btn-xs add" style="margin:30% 10%"><i class="fa fa-plus"></i></</button>
                            </div>
                            </div>
                            <?php endif;?>
                            </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('prep_time'); ?><span style="color:red">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="preparation_time" value="" placeholder="Minutes"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('cook_time'); ?><span style="color:red">*</span></label>
                        <div class="col-sm-6">
                            <input type="text" class="form-control" name="cooking_time" value="" placeholder="Minutes" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('notes'); ?><span style="color:red"></span></label>
                        <div class="col-sm-6">
                            <textarea name="notes" class="form-control" rows="3" placeholder="Notes"></textarea>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo ucwords("calories"); ?><span style="color:red">*</span></label>
                        <div class="col-sm-6">
                        <input type="text" class="form-control" name="calories" value="" placeholder="Calories"/>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('featured'); ?><span style="color:red"></span></label>
                        <div class="col-sm-6">
                            <input type="checkbox" name="is_featured"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('recipe_category'); ?><span style="color:red">*</span></label>
                        <div class="col-sm-6">
                           <select name="recipe_category" id="recipe_category" class="form-control">
                            <option value="">Select Category</option>
                            <?php
$recp = '';
foreach ($recipescats as $key => $value):
	$recp .= "<option value=$value->id>";
	if ($this->session->userdata('siteLang') == 'en') {
		$recp .= $value->cat_name;
	}
	if ($this->session->userdata('siteLang') == 'ar') {
		$recp .= $value->cat_name_ar;
	}
	if ($this->session->userdata('siteLang') == 'fr') {
		$recp .= $value->cat_name_fr;
	}
	//$recp .= $value->cat_name;
	$recp .= "</option>";
endforeach;
echo $recp;
?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('sub_category'); ?><span style="color:red">*</span></label>
                    <div class="col-sm-6">
                        <select name='recipe_subcat' id='recipe_subcat' class="form-control">
                            <option value="">Select Subcategory</option>
                        </select>
                    </div>
                    </div>

                    <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('source_title'); ?><span style="color:red">*</span></label>
                    <div class="col-sm-6">
                       <input type="text" name="source_name" class="form-control">
                    </div>
                    </div>

                    <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('source_link'); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" name="source_link" class="form-control">
                    </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label"><?php echo $this->lang->line('level'); ?><span style="color:red"></span></label>
                        <div class="col-sm-6">
                           <select name="level" id="level" class="form-control">
                            <option value="">Select Level</option>
                            <?php
$level = '';
foreach ($levels as $key => $value):
	$level .= "<option value=$value->id>";
	$level .= $value->level_type;
	$level .= "</option>";
endforeach;
echo $level;
?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-6">
                            <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btnadd'); ?></button>
                            <a href="<?php echo base_url('admin/recipes'); ?>" class="btn btn-default"><?php echo $this->lang->line('btncancel'); ?></a>
                        </div>
                    </div>
                </div>
                <?php echo form_close(); ?>
        </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
</section>
<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>/assets/css/jquery-ui-timepicker-addon.css">
<script src="https://rawgithub.com/trentrichardson/jQuery-Timepicker-Addon/master/jquery-ui-timepicker-addon.js"></script> -->
<!-- End #content -->
<script type="text/javascript">
function checkfile(x,lang=null){
    var a=0;
    var ext = $('#stepmedia'+x).val().split('.').pop().toLowerCase();
    var ext_ar = $('#stepmediaar'+x).val().split('.').pop().toLowerCase();
    var ext_fr = $('#stepmediafr'+x).val().split('.').pop().toLowerCase();

    if(lang == 'en'){
        if ($.inArray(ext, ['gif','png','jpg','jpeg','mp4']) == -1){
        alert('Format Must Be JPG, JPEG, PNG or GIF or MP4')
        $('#stepmedia'+x).val('');
        a=0;
        }else{
        var media = $("#stepmedia"+x).prop("files")[0];
       // console.log(media.name)
        if (media.size > 8000000){
        $('#stepmedia'+x).val('');
        alert('Maximum File Size Limit is 4MB.');
        //$('#error2').slideDown("slow");
        a=0;
        }else{
        a=1;
        $('#error2').slideUp("slow");
        }
        $('#error1').slideUp("slow");
        if (a==1){
        $('input:submit').attr('disabled',false);
        }
    }
    }
    else if(lang == 'ar'){
         if ($.inArray(ext_ar, ['gif','png','jpg','jpeg','mp4']) == -1){
        alert('Format Must Be JPG, JPEG, PNG or GIF or MP4')
        $('#stepmediaar'+x).val('');
        a=0;
        }else{
        var media = $("#stepmediaar"+x).prop("files")[0];
       // console.log(media.name)
        if (media.size > 8000000){
        $('#stepmediaar'+x).val('');
        alert('Maximum File Size Limit is 4MB.');
        //$('#error2').slideDown("slow");
        a=0;
        }else{
        a=1;
        $('#error2').slideUp("slow");
        }
        $('#error1').slideUp("slow");
        if (a==1){
        $('input:submit').attr('disabled',false);
        }
    }
    }
    else if(lang == 'fr'){
         if ($.inArray(ext_fr, ['gif','png','jpg','jpeg','mp4']) == -1){
        alert('Format Must Be JPG, JPEG, PNG or GIF or MP4')
        $('#stepmediafr'+x).val('');
        a=0;
        }else{
        var media = $("#stepmediafr"+x).prop("files")[0];
       // console.log(media.name)
        if (media.size > 8000000){
        $('#stepmediafr'+x).val('');
        alert('Maximum File Size Limit is 4MB.');
        //$('#error2').slideDown("slow");
        a=0;
        }else{
        a=1;
        $('#error2').slideUp("slow");
        }
        $('#error1').slideUp("slow");
        if (a==1){
        $('input:submit').attr('disabled',false);
        }
    }
    }
}

    $(document).ready(function () {
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, 'Please enter a valid number.');
        jQuery.validator.addMethod(
            "maxfilesize",
            function (value, element) {
                return this.optional(element) || (element.files && element.files[0]
                                       && element.files[0].size < 1024 * 1024 * 5);
            },
            'The file size can not exceed 2MB.'
        );

       /* jQuery.validator.addMethod("requiredUrl", function(value, element, param) {
            if(value != ''){console.log(value);
            var expression = /^(http:\/\/|https:\/\/)?((([\w-]+\.)+[\w-]+)|localhost)(\/[\w- .\/?%&=]*)?/i;
            return expression.test(value);
            }
        }, jQuery.validator.messages.url);*/

        $('select[name="ingname"]').each(function() {
            $(this).rules('add', {
                required: true
            });
        });
        $('input[name="ingmaterial"]').each(function() {
            $(this).rules('add', {
                required: true
            });
        });
        $('select[name="unitname"]').each(function() {
            $(this).rules('add', {
                required: true
            });
        });

        $('input[name="txtstep"]').each(function() {
            $(this).rules('add', {
                required: true
            });
        });

        $("#recipeform").validate({
            rules: {
                recipe_name:{    required: true,
                           character: true,
                        },
                'recipe_media[]': {
                      required: true,
                      extension: 'gif|png|jpg|jpeg',
                      maxfilesize:true
                    },
                preparation_time: {
                            required: true,
                            number: true
                        },
                cooking_time: {   required: true,number: true
                        },
                recipe_description: {   required: true
                        },
                recipe_method: {   required: true
                        },
               /* 'ingname[]': {   required: true
                        },
                'ingmaterial[]': {   required: true
                        },
                'unitname[]': {   required: true
                        },*/
                serving: {
                            number: true,
                            required: true
                        },

                calories: {   required: true
                        },
                recipe_category: {   required: true
                        },
                recipe_subcat:{ required: true
                        },
               // source_name:{required:true},
                source_link:{url: true},
               /* 'txtstep[]': {   required: true
                },*/

            },
            messages: {
                recipe_name: {   required: "Enter recipe name",
                            character: "Enter recipe name",
                        },
                preparation_time: {   required: "Enter preparation time",
                                    minlength: " preparation time must contain at least {1} number",
                        },
                cooking_time: {   required: "Enter cooking time",
                        },
                recipe_description: {   required: "Enter recipe description",
                },
                recipe_method: {   required: "Enter recipe method",
                        },
                serving: {   required: "Enter number of serving",
                        },

                calories: {   required: "Enter calories",
                        },
                recipe_category: {   required: "Select recipe category",
                        },
                recipe_subcat: {
                        required: "Select recipe sub category",
                        },
                //source_name:{required:"Enter source title"},
               // source_link:{required:"Enter source link"},
                'recipe_media[]':{
                   required : "Please upload recipe image",
                   extension:"Only gif|png|jpg|jpeg file is allowed!",
                   maxfilesize:"The file size can not exceed 5MB."
                },
               /* 'ingname[]':{
                   required : "Please select ingredient"
                },
                'ingmaterial[]': {   required: "Enter quantity"
                        },
                'unitname[]': {   required: "Select unit"
                        },*/
                /*'txtstep[]':{
                   required : "Please define steps"
                }*/
            }
        });


    var max_fields      = 100; //maximum input boxes allowed
    var wrapper         = $(".ing2"); //Fields wrapper
    var add_button      = $(".add"); //Add button ID

    var x = 1; //initlal text box count

    /*$("#ingmaterial_"+x).keypress(function(event) {
      if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
         event.preventDefault();
      }
    });*/

    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append("<div class=col-sm-12 style=padding:0><div class=col-sm-4 style=padding:0><span><?php echo $this->lang->line('ingredients') ?></span> <select name=ingname["+x+"] id=ingname_"+x+" class=form-control required onchange=get_units(this.value,"+x+")><option value=>Select Ingredients</option><?php echo $ing ?></select><?php echo $ingvalue ?><input type=hidden id=ingfinal_price_"+x+" name=ingfinal_price["+x+"]><input type=hidden id=protinefinal_price_"+x+" name=protinefinal_price["+x+"]><input type=hidden id=fatfinal_price_"+x+" name=fatfinal_price["+x+"]><input type=hidden id=carbsfinal_price_"+x+" name=carbsfinal_price["+x+"]></div><div class=col-sm-3 style=padding:0><span><?php echo $this->lang->line('qty') ?></span> <input type=text name=ingmaterial["+x+"] class=form-control required placeholder=Quantity id=ingmaterial_"+x+" onkeypress='return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 46 || event.charCode == 0' onkeyup=Calfunction(0,"+x+")></div><div class=col-sm-3 style=padding:0><span><?php echo $this->lang->line('unit') ?></span> <select name=unitname["+x+"] id=unitname_"+x+" class=form-control required onchange=calculate_calories(this.value,"+x+")><option value=>Select Unit</option><?php //echo $unit ?></select></div><input type=hidden name=final_calories["+x+"] id=final_calories_"+x+"> <input type=hidden name=final_calories_ounce["+x+"] id=final_calories_ounce_"+x+"><input type=hidden name=protine_calories["+x+"] id=protine_calories_"+x+"> <input type=hidden name=protine_calories_ounce["+x+"] id=protine_calories_ounce_"+x+"><input type=hidden name=fat_calories["+x+"] id=fat_calories_"+x+"><input type=hidden name=fat_calories_ounce["+x+"] id=fat_calories_ounce_"+x+"><input type=hidden name=carbs_calories["+x+"] id=carbs_calories_"+x+"><input type=hidden name=carbs_calories_ounce["+x+"] id=carbs_calories_ounce_"+x+"><div class=col-sm-2 style=padding:0><a href='#' class='btn btn-xs btn-danger removeing' style='margin: 30% 10%;'><i class='fa fa-close'></i></a></div></div>"); //add input box

            /* $("#ingmaterial_"+x).keypress(function(event) {
              if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                 event.preventDefault();
              }
            });*/
            //$("#ingname_2 option:nth(2)").remove();
        }
    });

    $(wrapper).on("click",".removeing", function(e){ //user click on remove text
       // console.log(x);
        //console.log($(this).parent('div'));return false;
        e.preventDefault(); $(this).parent('div').parent('div').remove(); x--;
    });

    /*********************** Preparation Method ***********/

    var max_fieldss      = 100; //maximum input boxes allowed
    var wrappermethod   = $(".prepmethod"); //Fields wrapper
    var insert_button   = $(".insert"); //Add button ID
   // var lang='en'; var ar_lang='ar'; var fr_lang='fr';
    var y = 1; //initlal text box count

    $(insert_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(y < max_fieldss){ //max input box allowed
            y++; //text box increment
            $(wrappermethod).append('<div class="col-sm-12" style="padding:0"><input type="number" name="txtstep['+y+']" id="txtstep_'+y+'" class="form-control required" placeholder="Step No"><br/><label class="control-label"><?php echo $this->lang->line("img_vid") ?></label> <input type="file" name="stepmedia['+y+']" id="stepmedia'+y+'" class="form-control" placeholder="Image/Video" onchange=checkfile('+y+',"en")><label class="control-label"><?php echo $this->lang->line("img_vid") ?>_ar</label><input type="file" name="stepmedia_ar['+y+']" id="stepmediaar'+y+'" class="form-control" placeholder="Image/Video" onchange=checkfile('+y+',"ar")> <label class="control-label"><?php echo $this->lang->line("img_vid") ?>_fr</label><input type="file" name="stepmedia_fr['+y+']" id="stepmediafr'+y+'" class="form-control" placeholder="Image/Video" onchange=checkfile('+y+',"fr")><label></label> <textarea name="preparation_method['+y+']" id="preparation_method_'+y+'" class="form-control required" rows="3" placeholder="Preparation Description"></textarea><textarea name="preparation_method_ar['+y+']" id="preparation_method_ar" class="form-control" rows="3" placeholder="AR_Preparation Description"></textarea><textarea name="preparation_method_fr['+y+']" id="preparation_method_fr" class="form-control" rows="3" placeholder="FR_Preparation Description"></textarea><a href="#" class="btn btn-xs btn-danger remove"><i class="fa fa-close"></i></a> <a class="btn btn-info btn-xs insertmore"><i class="fa fa-plus"></i></</a></div>'); //add input box
        }
    });

    $(wrappermethod).on("click",".remove", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); y--;
    });

    $(wrappermethod).on("click",".insertmore", function(e){
        if(y < max_fieldss){ //max input box allowed
            y++; //text box increment
            $(wrappermethod).append('<div class="col-sm-12" style="padding:0"><input type="number" name="txtstep['+y+']" id="txtstep_'+y+'" class="form-control required" placeholder="Step No"><br/><label class="control-label"><?php echo $this->lang->line("img_vid") ?></label> <input type="file" name="stepmedia['+y+']" id="stepmedia'+y+'" class="form-control" placeholder="Image/Video" onchange=checkfile('+y+',"en")><label class="control-label"><?php echo $this->lang->line("img_vid") ?>_ar</label><input type="file" name="stepmedia_ar['+y+']" id="stepmediaar'+y+'" class="form-control" placeholder="Image/Video" onchange=checkfile('+y+',"ar")> <label class="control-label"><?php echo $this->lang->line("img_vid") ?>_fr</label><input type="file" name="stepmedia_fr['+y+']" id="stepmediafr'+y+'" class="form-control" placeholder="Image/Video" onchange=checkfile('+y+',"fr")><label></label> <textarea name="preparation_method['+y+']" id="preparation_method_'+y+'" class="form-control required" rows="3" placeholder="Preparation Description"></textarea><textarea name="preparation_method_ar['+y+']" id="preparation_method_ar" class="form-control" rows="3" placeholder="AR_Preparation Description"></textarea><textarea name="preparation_method_fr['+y+']" id="preparation_method_fr" class="form-control" rows="3" placeholder="FR_Preparation Description"></textarea><a href="#" class="btn btn-xs btn-danger remove"><i class="fa fa-close"></i></a> <a class="btn btn-info btn-xs insertmore"><i class="fa fa-plus"></i></</a></div>');
        }
    });


    /**************************** END ******************************/

    // Recipes category selection
    $('#recipe_category').change(function(){

        var recipecat = $(this).val();
        var lang = "<?php echo $this->session->userdata('siteLang'); ?>";
        $.ajax({
            type: "POST",
            url: "<?php echo base_url('admin/recipes/get_subcategory'); ?>/"+recipecat,
            dataType: "json",
            success: function(data){
                 $('#recipe_subcat')
                .find('option')
                .remove()
                .end()
                .append('<option value="">Select Subcategory</option>');
                 $.each(data, function (key,value){
                    //console.log(value);
                     var opt = $('<option />'); // here we're creating a new select option with for each city
                     opt.val(value.id);
                     if(lang == 'ar'){
                     opt.text(value.sub_cate_name_ar);
                     }
                     else if(lang == 'fr'){
                     opt.text(value.sub_cate_name_fr);
                     }
                     else{
                     opt.text(value.sub_cate_name);
                     }
                     $('#recipe_subcat').append(opt);
                 });
            },
        });
    });

    $("#ingunit").keypress(function(event){
        var inputValue = event.charCode;
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)){
            event.preventDefault();
        }
    });
});


function Calfunction(unit,x){
    var unit = $("#unitname_"+x).val();
    if(unit == ''){
        calculate_calories(0,x)
    }
    else{
    calculate_calories(unit,x);
    }
}

function get_units(id,x){
        //calculate_calories()
       // return false;
        //alert(id);return false;
      // var catId = $("select#recipe_category option:selected").attr('value');
       var qty = $("#ingmaterial_"+x).val();
       var finalprice = $("#ing_value_"+id).val();
       var protine = $("#protine_value_"+id).val();
       var fat = $("#fat_value_"+id).val();
       var carbs = $("#carbs_value_"+id).val();
       //console.log("#ing_value_"+id);
       var lang = "<?php echo $this->session->userdata('siteLang'); ?>";
       $("#ingfinal_price_"+x).val(finalprice);
       $("#protinefinal_price_"+x).val(protine);
       $("#fatfinal_price_"+x).val(fat);
       $("#carbsfinal_price_"+x).val(carbs);
       //alert($("#ing_value_"+id).val());return false;
       $.ajax({
            type: "POST",
            url: "<?php echo base_url('admin/units/getIngUnits'); ?>/"+id,
            dataType: "json",
            success: function(data){
                // console.log(data);return false;
                 $('#unitname_'+x)
                .find('option')
                .remove()
                .end()
                .append('<option value="">Select Unit</option>');
                 $.each(data, function (key,value){
                     var opt = $('<option />'); // here we're creating a new select option with for each city
                     opt.val(value.id);
                     if(lang == 'ar'){
                     opt.text(value.unit_name_ar);
                     }
                     else if(lang == 'fr'){
                     opt.text(value.unit_name_fr);
                     }
                     else{
                     opt.text(value.unit_name);
                     }
                     //opt.select(selected);
                     $('#unitname_'+x).append(opt);
                     //$('#unitname option[value='+subcatId+']').val(subcatId).attr('selected', true);
                 });
            },
        });
}

function calculate_calories(id,x){ //alert(id+x)
      // var catId = $("select#recipe_category option:selected").attr('value');
    var quantity = parseFloat($("#ingmaterial_"+x).val());
    var nutrientPer100g = parseFloat($("#ingfinal_price_"+x).val());
    var protinePer100g = parseFloat($("#protinefinal_price_"+x).val());
    var fatPer100g = parseFloat($("#fatfinal_price_"+x).val());
    var carbsPer100g = parseFloat($("#carbsfinal_price_"+x).val());
    //var gramsPerServing = $("#serving").val(); // id

    $.ajax({
            type: "POST",
            url: "<?php echo base_url('admin/units/getUnitamount'); ?>/"+id,
            dataType: "json",
            success: function(data){

                /************************ Gram calculation ************/
                var nutrientGrams = quantity * (data.unit_gram);
                //alert(quantity * data.unit_gram);
                var nutrientPerServing = (nutrientPer100g * nutrientGrams) / 100.0;
                var nutrientPerServingprotine = (protinePer100g * nutrientGrams) / 100.0;
                var nutrientPerServingfat= (fatPer100g * nutrientGrams) / 100.0;
                var nutrientPerServingcarbs = (carbsPer100g * nutrientGrams) / 100.0;
              //  var nutrientGrams = (quantity * data.unit_gram) * nutrientPerServing;
                $("#final_calories_"+x).val(nutrientPerServing.toFixed(2));
                $("#protine_calories_"+x).val(nutrientPerServingprotine.toFixed(2));
                $("#fat_calories_"+x).val(nutrientPerServingfat.toFixed(2));
                $("#carbs_calories_"+x).val(nutrientPerServingcarbs.toFixed(2));

                /************************* END **************************/

                /******************* Ounce Calculation *****************/
                //var nutrientOunce = quantity * (data.unit_ounce);
                var nutrientPerServing_ounce = (nutrientPerServing) / 28.35;
                var nutrientPerServingprotine_ounce = (nutrientPerServingprotine) / 28.35;
                var nutrientPerServingfat_ounce = (nutrientPerServingfat) / 28.35;
                var nutrientPerServingcarbs_ounce = (nutrientPerServingcarbs) / 28.35;

                $("#final_calories_ounce_"+x).val(nutrientPerServing_ounce.toFixed(2));
                $("#protine_calories_ounce_"+x).val(nutrientPerServingprotine_ounce.toFixed(2));
                $("#fat_calories_ounce_"+x).val(nutrientPerServingfat_ounce.toFixed(2));
                $("#carbs_calories_ounce_"+x).val(nutrientPerServingcarbs_ounce.toFixed(2));

                /********************** END *****************************/

                //return Math.floor(nutrientGrams);
                //console.log(data.unit_gram);return false;
            }
    });
}
</script>