<section class="content">
<div class=box>                      
<div class="box-body">
<div id="getresponse">
    <table id="datatable-list-17" class="table table-striped table-bordered table-responsive" cellspacing=0 width=100%>
        <thead>
            <tr>
                <th>ID</th>			
                <th>Recipe Name</th>
                <th>Media</th>
                <th>Preparation Time</th>
                <th>User</th>
                <th>Action</th>	
              
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($recipes as $row):
                ?>
                <tr>
                    <td><?php echo $row->user_id; ?></td>	
                    <td><?php echo $row->recipe_name; ?></td>
                    <?php if(!is_null($row->recipe_media)):?>
                    <td><img src="<?php echo $row->recipe_media?>" width="50"/></td>
                    <?php else:?>
                    <td><span>No Image</span></td>
                    <?php endif;?>  
                    <td><?php echo $row->preparation_time; ?></td>
                    <td><?php echo $row->name; ?></td>	
                    <td>
                    <a href="javascript:void(0);" id="status<?php echo $row->id ?>" data-fullText="<?php echo $row->isapprove;?>" onclick="isApproveReject(<?php echo $row->id.','.$row->user_id?>);" class="btn <?php echo ($row->isapprove == 0)?'btn-success':'btn-danger'?>"><?php echo ($row->isapprove == 0)? 'Approve' : 'Reject'?></a>                           
                    </td>
                    
                </tr>
            <?php endforeach; ?>															
        </tbody>
    </table>
</div>
</div>
<!-- col-lg-12 end here -->
</div>
</section>
<!-- End .row -->
<script type="text/javascript">
function isApproveReject(recipeID,userID){
    var stats = $('#status'+recipeID).data('fulltext');
    var finalstatus = (stats == 0) ? 1 : 0;
    //alert(stats);return false;
    var msg = (stats == 0)? 'Approve' : 'Reject';

    var r=confirm("Do you want to "+msg+" recipe?");
    url = "<?php echo base_url(); ?>admin/recipes/approveUserRecipe/";
    if(r==true){
    $.ajax({
          type:"POST",
          url: url,
          data: {recid:recipeID,userid:userID,status:finalstatus},
          success: function(data)
          {
            if(data == 1){
                location.reload();
            }
            location.reload();
          }
        });
    }
}    
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#datatable-list-17').DataTable( {
          "searching": true
        } );  
    })
</script>