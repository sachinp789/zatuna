<section class="content">
<div class=box>
<!-- col-lg-12 start here -->

<div class="box-body">

<form action="<?php echo base_url(); ?>admin/recipes/recipeImportExcel" method="post" name="upload_excel" enctype="multipart/form-data" id="xlsform">
    <div class="col-md-12">
        <div class="form-group">
            <label class="col-sm-2 control-label">Recipe Import</label>
            <div class="col-sm-4">
                <input type="file" name="file" id="file" class="form-control" required><br/>
                <label class="control-label"><span style="color:red">*</span>(Use jpg,gif,png for image and .mp4 for video)</label>
                <?php if ($this->session->flashdata('error')) {?>
                    <div class="alert alert-danger"><?php echo $this->session->flashdata('error'); ?></div>
                <?php }?>
            </div>
            <div class="col-sm-2">
                <input type="submit" name="import" class="btn btn-default">
            </div>
            <div class="col-sm-3">
            <?php if (file_exists(FCPATH . 'sample.xls')): ?>
                <a href="<?php echo base_url(); ?>sample.xls" download>Download Sample
                </a>
            <?php endif;?>
            </div>
        </div>
    </div>
</form>
</div>
<hr>
<div class=panel-body>
<p><a href="<?php echo base_url(); ?>admin/recipes/create" class="btn btn-primary"><i class="fa fa-plus"></i> <?php echo $this->lang->line('btncreate'); ?></a></p>
<!-- <a data-placement="top" data-toggle="tooltip" onclick="showAjaxModal('<?php echo base_url(); ?>modal/popup/admin_recipes_create');" href="#" class="links"><i class="fa fa-plus"></i>Recipes</a> -->

<div id="getresponse">
    <table id="datatable-list-2" class="table table-striped table-bordered table-responsive" cellspacing=0 width=100%>
        <thead>
            <tr>
                <th><input type="checkbox"  id="bulkDelete"  /> <button id="deleteTriger">Delete</button></th>
                <!-- <th>ID</th> -->
                <th>Name</th>
                <th>Media</th>
                <!-- <th>Ingredients</th> -->
                <th>Preparation Time</th>
                <th>Visible Front</th>
                <th>Action</th>
            </tr>
        </thead>
        <!-- <tbody>
        <?php
foreach ($recipes as $row):
?>
                <tr>
                    <td><?php echo $row->id; ?></td>
                    <td><?php echo $row->recipe_name; ?></td>
                    <?php if (!is_null($row->recipe_media)): ?>
                    <td><img src="<?php echo $row->recipe_media ?>" width="50"/></td>
                    <?php else: ?>
                    <td><img src="<?php echo base_url() ?>uploads/recipes/default_food.jpeg" width="50"/></td>
                    <?php endif;?>
                    <td><?php echo $row->preparation_time; ?></td>
                    <td>
                    <?php if ($row->status == 0) {?>
                        <i id="<?php echo $row->id; ?>" class="status_checks btn btn-success" data="<?php echo $row->id; ?>">Active</i>
                        <?php } else {?>
                        <i id="<?php echo $row->id; ?>" class="status_checks btn btn-danger" data="<?php echo $row->id; ?>">Inactive</i>
                        <?php }?>
                        </td>
                    <td class="menu-action">

                        <a href="<?php echo base_url() . 'admin/recipes/edit/' . $row->id ?>" class="btn btn-sm btn-primary"><span><i class="fa fa-pencil" aria-hidden="true"></i> <?php echo $this->lang->line('btnedit'); ?></span></a>

                         <a href="<?php echo base_url(); ?>admin/recipes/delete_recipe/<?php echo $row->id; ?>" class="btn btn-sm btn-danger" onclick='return confirm("Are you sure want to remove <?php echo $row->recipe_name ?>?");'><span><i class="fa fa-trash-o" aria-hidden="true"></i> <?php echo $this->lang->line('btnremove'); ?></span></a>
                    </td>

                </tr>
            <?php endforeach;?>
        </tbody> -->
    </table>

</div>
</div>

<!-- tab content -->
</div>
<!-- End .panel -->
</div>
<!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
</section>
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.material.min.css"> -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/r/dt/jq-2.1.4,jszip-2.5.0,pdfmake-0.1.18,dt-1.10.9,af-2.0.0,b-1.0.3,b-colvis-1.0.3,b-html5-1.0.3,b-print-1.0.3,se-1.0.1/datatables.min.js"></script> Recipes Export options

 /* "dom": 'lBfrtip',
        "buttons": [
        {
        extend: 'collection',
        text: 'Export',
        buttons: [
        'excel'
        ]
        }


 -->
<script type="text/javascript">
$(document).ready(function() {

  table = $('#datatable-list-2').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "stateSave": true,
        "mark": true,
        /*'className': 'dt-body-center',
        "render": function (data, type, full, meta){
             return '<input type="checkbox" name="id[]" value="' + $('<div/>').text(data).html() + '">';
         },*/
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('admin/Recipes/recipesajax_list') ?>",
            "type": "POST",
        },
            "aoColumnDefs" : [ {
            'bSortable' : false,
            "orderable": false,
            'aTargets' : [ 0,2,4,5 ]
        } ],

        /*"dom": '<"top"i>fp<"bottom"lrt><"clear">',*/
        "searching": true,
        "paging": true,
        "lengthMenu": [[10], [10]],
        "pagingType": "full_numbers"
    });

    /******************************* AJAX BULK DELETE RECIPES ******************/
    $("#bulkDelete").on('click',function() { // bulk checked
        var status = this.checked;
        $(".deleteRow").each( function() {
            $(this).prop("checked",status);
        });
        /* var rows = table.rows({ 'search': 'applied' }).nodes();
      // Check/uncheck checkboxes for all rows in the table
      $('input[type="checkbox"]', rows).prop('checked', this.checked);*/
    });

     // Handle click on checkbox to set state of "Select all" control
   /*$('#datatable-list-2 tbody').on('change', 'input[type="checkbox"]', function(){
      // If checkbox is not checked
      if(!this.checked){
         var el = $('#bulkDelete').get(0);
         // If "Select all" control is checked and has 'indeterminate' property
         if(el && el.checked && ('indeterminate' in el)){
            // Set visual state of "Select all" control
            // as 'indeterminate'
            el.indeterminate = true;
         }
      }
   });*/

    $('#deleteTriger').on("click", function(event){ // triggering delete one by one
        if( $('.deleteRow:checked').length > 0 ){  // at-least one checkbox checked
            var ids = [];
            $('.deleteRow').each(function(){
                if($(this).is(':checked')) {
                    ids.push($(this).val());
                }
            });
            var WRN_PROFILE_DELETE = "Are you sure you want to delete this row?";
            var check = confirm(WRN_PROFILE_DELETE);
            if(check == true){
                var ids_string = ids.toString();  // array to string conversion
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('admin/Recipes/deleteRecipes') ?>",
                    data: {data_ids:ids_string},
                    success: function(result) {
                         //console.log(result);
                        table.draw(); // redrawing datatable
                    },
                    async:false
                });
            }
        }
    });

    /************************************* END ********************************/
    jQuery.validator.addMethod(
        "xlsxOrxls",
        function(value, element) {
            return value.match(/\.xlsx?$/);
        },
        "<br/><label class='error'><b>Not a valid file format.</b></label>"
    );
    $("#xlsform").validate({
            rules: {
                file:{required: true,xlsxOrxls: true},
            },
            messages: {
                file: {   required: "Please select xsl or xslx file",xlsxOrxls:"Please upload file with xsl or xlsx extension"
                },
            }
    });
});
$(function() {
      setTimeout(function() {
        $(".alert-danger").hide('blind', {}, 500)
    }, 5000);
    });
</script>
<script type="text/javascript">

   $(document).on('click','.status_checks',function(){ //alert()
      var status = ($(this).hasClass("btn-success")) ? '1' : '0';
     // alert(status);
      var msg = (status=='0')? 'Active' : 'Deactive';
      if(confirm("Are you sure to "+ msg+" ?")){
        var current_element = $(this).attr('id');
        var url_status = "<?php echo base_url(); ?>admin/recipes/update_status/";
        $.ajax({
          type:"POST",
          url: url_status,
          data: {id:current_element,status:status},
          success: function(data)
          {
            location.reload();
          }
        });
      }
    });

</script>