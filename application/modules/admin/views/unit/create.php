<section class="content">
<div class=row>
    <div class=col-lg-12>
        <!-- col-lg-12 start here -->
        <div class="box box-primary">
           <div class="box-body">
            <?php echo form_open(base_url() . 'admin/units/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'role' => 'form', 'id' => 'unitform', 'target' => '_top', 'enctype' => 'multipart/form-data')); ?>
            <div class="padded">
                <h3>Metric System</h3><hr>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('unit_name'); ?><span style="color:red">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="unit_name" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('unit_name_ar'); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="unit_name_ar" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('unit_name_fr'); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="unit_name_fr" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('amount'); ?><span style="color:red">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="unit_amount" value=""/>
                    </div>
                </div>
            </div>
            <hr>
            <div class="padded">
            <h3>U.S Metric System</h3><hr>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('unit_name'); ?><span style="color:red">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="unit_name_us" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('unit_name_ar'); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="unit_name_us_ar" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('unit_name_fr'); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="unit_name_us_fr" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('amount_us'); ?><span style="color:red">*</span></label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="unit_amount_us" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-3 control-label"><?php echo $this->lang->line('status'); ?><span style="color:red"></span></label>
                    <div class="col-sm-6">
                            <label for="active" class="control-label">
                            <input type="radio" name="unit_status" value="1" id="active" checked/> <?php echo $this->lang->line('status_name_act'); ?> </label>
                            <label for="inactive" class="control-label">
                            <input type="radio" name="unit_status" value="0" id="inactive"/> <?php echo $this->lang->line('status_name_dact'); ?> </label>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-6">
                        <button type="submit" class="btn btn-primary"><?php echo $this->lang->line('btnadd'); ?></button>
                        <a href="<?php echo base_url('admin/units'); ?>" class="btn btn-default"><?php echo $this->lang->line('btncancel'); ?></a>
                    </div>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
        </div>
        <!-- End .panel -->
    </div>
    <!-- col-lg-12 end here -->
</div>
<!-- End .row -->
</div>
<!-- End #content -->
</section>
<script type="text/javascript">
    $(document).ready(function () {
        jQuery.validator.addMethod("character", function (value, element) {
            return this.optional(element) || /^[A-z ]+$/.test(value);
        }, 'Please enter a valid character.');
        jQuery.validator.addMethod("number", function (value, element) {
            return this.optional(element) || /^[0-9 ]+$/.test(value);
        }, 'Please enter a valid number.');

        $("#unitform").validate({
            rules: {
                unit_name:{ required: true,character:true,
                        remote: {
                            url: "<?php echo base_url(); ?>admin/units/checkUnit",
                            type: "post",
                            data: {
                                unit_name: function () {
                                    return $('#unitform :input[name="unit_name"]').val();
                                }
                            }
                        }
                },
                unit_amount:{required:true},
                unit_name_us:{required:true,character:true,
                        remote: {
                            url: "<?php echo base_url(); ?>admin/units/checkUnitUS",
                            type: "post",
                            data: {
                                unit_name_us: function () {
                                    return $('#unitform :input[name="unit_name_us"]').val();
                                }
                            }
                        }
                },
                unit_amount_us:{required:true},
            },
            messages: {
                unit_name: { required: "Enter unit name",character: "Enter unit name",remote: "Unit already in use"
                },
                unit_name_us:{required:"Enter unit name",character: "Enter unit name",remote: "Unit already in use"},
                unit_amount_us:{required:"Entr unit amount"},
                unit_amount:{required:"Entr unit amount"}
            }
        });
    });
</script>