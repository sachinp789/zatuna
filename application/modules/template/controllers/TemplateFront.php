<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TemplateFront extends MY_Controller {
    
    function __constuct() {
        parent::__constuct();
    }

    function render($page_name='', $data = array()) {
        $this->load->view('header', $data);
        $this->load->view($page_name);
        $this->load->view('footer');
    }
    
    function modal($page_name, $data) {
        $this->load->view($page_name, $data);
    }
}