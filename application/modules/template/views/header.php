<?php $siteLang=$this->session->userdata('site_lang'); ?>
<!DOCTYPE html>
<html lang="en">
<head>  
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<link rel="icon" type="image/ico" href="<?php echo base_url(); ?>images/favicon.ico">
<title><?php echo $this->lang->line('title'); ?></title>


<!-- Latest compiled and minified CSS -->

<link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/normal.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/media.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/custom.css">

<?php if($this->session->userdata('site_lang') == 'arabic'){ ?>
<link rel="stylesheet" href="//cdn.rawgit.com/morteza/bootstrap-rtl/v3.3.4/dist/css/bootstrap-rtl.min.css">
<?php } ?>
  <script>
    if (/mobile/i.test(navigator.userAgent)) document.documentElement.className += ' w-mobile';
  </script>

<!-- <link rel="stylesheet" href="//cdn.rawgit.com/morteza/bootstrap-rtl/v3.3.4/dist/css/bootstrap-rtl.min.css"> -->
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      
      <!--[if lt IE 9]>
      <script src = "https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src = "https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
</head>
<body class="<?php if($siteLang=='arabic') echo 'arabic-lang'; ?>">
<!--///////////////////////////////////////////////////////
       // Header - menu
       //////////////////////////////////////////////////////////-->
<div class="fix-header" id="home">
  <div class="container">
    <div class="w-nav" data-collapse="medium" data-animation="default" data-duration="400"></div>
  </div>
</div>
<div class="fixed-header">
  <div class="container" ng-app="app" ng-controller="MainCtrl as main">
    <div class="w-row">

     <!--///////////////////////////////////////////////////////
     // Logo section 
     //////////////////////////////////////////////////////////-->


      <div class="w-col w-col-3 logo flip pull-left">
        <a href="<?php echo base_url();?>"><img class="logo img-responsive" src="<?php echo base_url(); ?>images/logo.png" alt="Zatuna"></a>
      </div>

      <!--///////////////////////////////////////////////////////
     // End Logo section 
     //////////////////////////////////////////////////////////-->

      <div class="w-col w-col-9 flip pull-right">

     <!--///////////////////////////////////////////////////////
     // Menu section 
     //////////////////////////////////////////////////////////-->


          <div class="w-nav navbar" data-collapse="medium" data-animation="default" data-duration="400" data-contain="1">
              <div class="w-container nav">
                <nav class="w-nav-menu nav-menu flip pull-right" role="navigation">
                  <ul class="list-inline">
                          <li>
                              <a class="w-nav-link menu-li" href="<?php echo base_url(); ?>home"><?php echo $this->lang->line('menuHome'); ?></a>
                          </li>
                          <li class="footer-menu-divider w-nav-link menu-li"> <i class="border3left"></i>
                           <!-- <img src="images/divider.png"> fa fa-ellipsis-v -->
                           </li>
                          <li>
                              <a class="w-nav-link menu-li"  href="#"><?php echo $this->lang->line('menuDoctors'); ?></a>
                          </li>
                          <li class="footer-menu-divider w-nav-link menu-li"> <i class="border3left"></i>
                           </li>
                          <li>
                              <a class="w-nav-link menu-li"  href="#"><?php echo $this->lang->line('menuContactUs'); ?></a>
                          </li>
                          <li class="footer-menu-divider w-nav-link menu-li"> <i class="border3left"></i>
                           </li>
                           <?php if($this->session->userdata('user_login')==1) {?>
                          <li><div class="dropdown">
                              <a class="w-nav-link menu-li signIn-link" href="#"><i class="fa fa-user"></i><?php echo $this->session->userdata('user_firstname').' '. $this->session->userdata('user_lastname'); ?></a>
                              <ul class="dropdown-menu">
                                  <li><a class="w-nav-link"  href="<?php echo base_url(); ?>home/logout">Logout</a></li>
                                </ul>
                              </div>
                            </li>
                           <?php } else{ ?>
                          <li>
                              <a class="w-nav-link menu-li signIn-link" id="signinopen"  href="javascript:;" data-toggle="modal" data-target="#loginModal">
                              <i class="fa fa-user"></i> <?php echo $this->lang->line('menuSignIn'); ?></a>                               
                              <span class="or-text w-nav-link menu-li"><?php echo $this->lang->line('menuOR'); ?></span>                              
                               <a class="w-nav-link menu-li signIn-link" id="signupopen"  href="javascript:void(0);" data-toggle="modal" data-target="#loginModal"><?php echo $this->lang->line('menuRegister'); ?></a>        
                               <span class="p5l"> </span>                    
                          </li><?php } ?>
                          <li class="footer-menu-divider w-nav-link menu-li" style="max-width: 940px;"> <i class="border3left"></i>
                           </li>
                          <li>
                            <div class="dropdown">
                                <select onchange="javascript:window.location.href='<?php echo base_url(); ?>LanguageSwitcher/switchLang/'+this.value;" class="w-nav-link menu-li signIn-link change-lng">
                                <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; ?>>English </option>
                                <option value="arabic" <?php if($this->session->userdata('site_lang') == 'arabic') echo 'selected="selected"'; ?>>Arabic</option>   
                                </select>
                                <span class="down arrow"><i class="fa fa-angle-down"></i> </span>
                            </div>
                          </li>
                          
                  </ul>
                </nav>
                <div class="w-nav-button"> <span class="title"><?php echo $this->lang->line('menuMenu'); ?></span>
                  <div class="w-icon-nav-menu"></div>
                </div>
              </div>
          </div>


        <!--///////////////////////////////////////////////////////
     // End Menu section 
     //////////////////////////////////////////////////////////-->


      </div>
    </div>
  </div>
</div>
<script src="<?php echo base_url(); ?>js/jquery.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?php echo base_url(); ?>css/jquery.rateyo.min.css">
<script src="<?php echo base_url(); ?>js/jquery.rateyo.min.js"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCsGO502jUgum9RSHoErTQkvsdgg9g01Y4" type="text/javascript"></script>