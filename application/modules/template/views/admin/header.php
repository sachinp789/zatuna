<?php
$sitelist = site_configuration();
?>
<!DOCTYPE html>
<html class=no-js>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $sitelist[0]->title; ?></title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Import google fonts - Heading first/ text second -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,700" rel=stylesheet type=text/css>
        <link href="http://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel=stylesheet type=text/css>
        <link href='<?php echo base_url(); ?>assets/dist/admin/adminlte.min.css' rel='stylesheet' media='screen'>
        <link href='<?php echo base_url(); ?>assets/dist/admin/lib.min.css' rel='stylesheet' media='screen'>
        <!-- Css files -->
        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/event_calendar/eventCalendar.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/event_calendar/eventCalendar_theme_responsive.css"/> -->
        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/jquery.mCustomScrollbar.min.css"/> -->
        <!-- <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.min.css"/>  -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/plugins.css"/>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/custom.css"/>
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
       <!--  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css"> -->

        <!-- JS Files -->
        <script src="<?php echo base_url(); ?>assets/js/jquery-2.1.1.min.js"></script>
        <script src='<?php echo base_url(); ?>assets/dist/admin/adminlte.min.js'></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>

        <!-- Fav and touch icons -->
        <link rel=apple-touch-icon-precomposed sizes=144x144 href=<?php echo base_url(); ?>assets/img/ico/apple-touch-icon-144-precomposed.png>
        <link rel=apple-touch-icon-precomposed sizes=114x114 href=<?php echo base_url(); ?>assets/img/ico/apple-touch-icon-114-precomposed.png>
        <link rel=apple-touch-icon-precomposed sizes=72x72 href=<?php echo base_url(); ?>assets/img/ico/apple-touch-icon-72-precomposed.png>
        <link rel=apple-touch-icon-precomposed href=<?php echo base_url(); ?>assets/img/ico/apple-touch-icon-57-precomposed.png>
        <link rel=icon href=<?php echo base_url(); ?>assets/img/ico/favicon.ico type=image/png>
        <!-- Windows8 touch icon ( http://www.buildmypinnedsite.com/ )-->
        <meta name=msapplication-TileColor content="#3399cc">
        <script>
            var base_url = '<?php echo base_url(); ?>';
            $(window).load(function() {
                // Animate loader off screen
                $(".se-pre-con").fadeOut("slow");;
            });
        </script>
<style>
body {top: 0 !important;}
.save
{
    display:none;
}
.no-js #loader { display: none;  }
.js #loader { display: block; position: absolute; left: 100px; top: 0; }
.se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url(<?php echo base_url(); ?>images/preloader.gif) center no-repeat #fff;
}
</style>
</head>

<body class="skin-black fixed <?php echo $this->router->fetch_method(); ?>" style="min-height: 100%" >
<div class="se-pre-con"></div>
<div class=wrapper>
<header class="main-header">
    <a class="logo" href="<?php echo base_url(); ?>">
    <?php
if (count($sitelist) > 0): ?>
     <img src="<?php echo base_url('uploads/sitelogo') . '/' . $sitelist[0]->logo ?>" alt="logo" width="50">
    <?php else: ?>
    <img src="<?php echo base_url('uploads/sitelogo/default.png') ?>" alt="logo" width="50">
    <?php endif;?>
    <span><b>Zatuna</b></span>
   </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo base_url(); ?>uploads/admin_image/<?php echo $this->session->userdata('admin_id') . '.jpg'; ?>" alt="" class="user-image">
                        <span class="hidden-xs"><?php echo $this->session->userdata('admin_firstname') . ' ' . $this->session->userdata('admin_lastname'); ?></span>
                    </a>
                    <ul class="dropdown-menu">
                            <li>
                            <a href="<?php echo base_url(); ?>admin/profile/<?php echo $this->session->userdata('admin_id'); ?>">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                            <?php echo $this->lang->line('dash_edit_profile'); ?>
                            </a>
                            </li>
                            <li>
                            <a href="<?php echo base_url(); ?>admin/change_password">
                                    <i class="fa fa-user" aria-hidden="true"></i>
                            <?php echo $this->lang->line('dash_change_pwd'); ?></a>
                            </li>
                            <li><a href="<?php echo base_url(); ?>admin/logout"><i class="fa fa-sign-out" aria-hidden="true"></i><?php echo $this->lang->line('logout_link'); ?></a>
                            </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

<!-- <header class="main-header">
    <a class="logo" href="<?php echo base_url() . 'admin/dashboard'; ?>">
    <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="logo" width="100">
</a>

nav class="navbar navbar-static-top" role="navigation">
<div id="navbar-no-collapse" class="navbar-custom-menu">
<ul class="nav navbar-right usernav">

<li class=dropdown>
 <a href=# class="dropdown-toggle avatar" data-toggle=dropdown><img src="<?php echo base_url(); ?>uploads/admin_image/<?php echo $this->session->userdata('admin_id') . '.jpg'; ?>" alt="" class="image" width="50">
    <span class=txt><?php echo $this->session->userdata('admin_firstname') . ' ' . $this->session->userdata('admin_lastname'); ?></span> <b class=caret></b>
</a>
<?php ?>
  <ul class="dropdown-menu right">
    <li class=menu>
        <ul>
            <li>
                <a href="<?php echo base_url() . 'admin/dashboard'; ?>"><i class="fa fa-dashboard" aria-hidden="true"></i>Home</a>
            </li>
            <li>

            <a href="<?php echo base_url(); ?>admin/profile/<?php echo $this->session->userdata('admin_id'); ?>">
                    <i class="fa fa-user" aria-hidden="true"></i>
            Edit profile
            </a>
            </li>
            <li>
            <a href="<?php echo base_url(); ?>admin/change_password/<?php echo $this->session->userdata('admin_id'); ?>">
                    <i class="fa fa-user" aria-hidden="true"></i>
            Change Password</a>
            </li>
            <li><a href="<?php echo base_url(); ?>admin/logout"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
            </li>
        </ul>
    </li>
</ul>
</li>

</ul>
</div>
</nav> -->

</header>
<!-- #wrapper --><!--Sidebar background-->
<div id=sidebarbg class="hidden-lg hidden-md hidden-sm hidden-xs"></div>
<!--Sidebar content-->
<aside class="main-sidebar">
<!-- End search -->
<!-- Start .sidebar-inner -->
<section class="sidebar">
    <ul class="sidebar-menu">
        <?php $pages = ['register', 'level', 'unit', 'notification']; // 'role'
?>
        <li>
            <a href="<?php echo base_url() ?>">
                <i class="fa fa-home"></i> <?php echo $this->lang->line('home'); ?> </a>
        </li>
        <li class="treeview">
            <a href="#" class="<?php echo exapnd_not_expand_menu($page, $pages); ?>"><i class="fa fa-cog"></i>
                <span><?php echo $this->lang->line('basic_mngmt'); ?></span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>
                <ul class="treeview-menu"<?php echo navigation_show_hide_ul($page, $pages); ?>>

                    <li id="link-register">
                        <a href="<?php echo base_url(); ?>admin/user">
                            <i class="glyphicon glyphicon-user"></i>
                            <span class="txt"><?php echo $this->lang->line('basic_user'); ?></span>
                        </a>
                    </li>

                   <!--  <li>
                        <a id="link-role" href="<?php echo base_url(); ?>admin/user_role">
                            <i class="glyphicon glyphicon-user"></i>
                            <span class="txt">User Roles</span>
                        </a>
                    </li> -->

                    <!--  <li id="link-level">
                        <a href="<?php echo base_url(); ?>admin/levels">
                            <i class="glyphicon glyphicon-level-up"></i>
                            <span class="txt">Level</span>
                        </a>
                    </li> -->

                    <li id="link-unit">
                        <a href="<?php echo base_url(); ?>admin/units">
                            <i class="glyphicon glyphicon-level-up"></i>
                            <span class="txt"><?php echo $this->lang->line('basic_unit'); ?></span>
                        </a>
                    </li>

                    <li id="link-notification">
                        <a href="<?php echo base_url(); ?>admin/notification">
                            <i class="glyphicon glyphicon-level-up"></i>
                            <span class="txt"><?php echo $this->lang->line('page_header_title'); ?></span>
                        </a>
                    </li>

                   <!--  <li>
                        <a id="link-location4" href="<?php echo base_url(); ?>admin">
                            <i class="glyphicon glyphicon-paperclip"></i>
                            <span class="txt">Rule</span>
                        </a>
                    </li> -->
                </ul>
            </li>
                <!-- <?php $pages2 = ['import'];
?>
                <a href="<?php echo base_url(); ?>" class="<?php echo exapnd_not_expand_menu($page, $pages2); ?>"><i class="icomoon-icon-arrow-down-2 s16 hasDrop"></i><i class="s16 fa fa-chain"></i>
                <span class="txt">Categories</span></a>

                <ul <?php echo navigation_show_hide_ul($page, $pages2); ?>>
                    <li>
                        <a id="link-import" href="<?php echo base_url(); ?>admin/csvimport">
                            <i class="glyphicon glyphicon-import"></i>
                            <span class="txt">Import Categories</span>
                        </a>
                    </li>
                </ul> -->
                <li class="treeview">
                <?php $pages4 = ['ingredients', 'ingredient_category'];
?>

                <a href="<?php echo base_url(); ?>" class="<?php echo exapnd_not_expand_menu($page, $pages4); ?>"><i class="icomoon-icon-arrow-down-2 s16 hasDrop"></i><i class="glyphicon glyphicon-apple sidebar-icon"></i>
                <span><?php echo $this->lang->line('basic_ingredients'); ?></span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>

                 <ul class="treeview-menu"<?php echo navigation_show_hide_ul($page, $pages4); ?>>
                     <li id="link-ingredient_category">
                         <a href="<?php echo base_url(); ?>admin/ingredientcategory">
                            <i class="glyphicon glyphicon-apple sidebar-icon"></i>
                            <span class="txt"><?php echo $this->lang->line('basic_ingredients_category'); ?></span>
                        </a>
                    </li>
                     <li id="link-ingredients">
                        <a href="<?php echo base_url(); ?>admin/ingredients">
                            <i class="glyphicon glyphicon-apple sidebar-icon"></i>
                            <span class="txt"><?php echo $this->lang->line('basic_ingredients_child'); ?></span>
                        </a>
                    </li>
                </ul>
            </li>

             <li class="treeview">
                <?php $pages3 = ['recipes', 'userrecipe', 'recipes_category', 'search', 'recipes_subcategory'];
?>

                <a href="<?php echo base_url(); ?>" class="<?php echo exapnd_not_expand_menu($page, $pages3); ?>"><i class="icomoon-icon-arrow-down-2 s16 hasDrop"></i><i class="glyphicon glyphicon-list-alt"></i>
                <span><?php echo $this->lang->line('basic_recpdirectory'); ?></span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>

                <ul class="treeview-menu"<?php echo navigation_show_hide_ul($page, $pages3); ?>>
                    <li id="link-recipes_category">
                         <a  href="<?php echo base_url(); ?>admin/recipecategory">
                            <i class="glyphicon glyphicon-list-alt"></i>
                            <span class="txt"><?php echo $this->lang->line('basic_recpcat'); ?></span>
                        </a>
                    </li>
                    <li id="link-recipes_subcategory">
                         <a  href="<?php echo base_url(); ?>admin/recipesubcategory">
                            <i class="glyphicon glyphicon-list-alt"></i>
                            <span class="txt"><?php echo $this->lang->line('basic_recpsubcat'); ?></span>
                        </a>
                    </li>
                     <li id="link-recipes">
                        <a href="<?php echo base_url(); ?>admin/recipes">
                            <i class="glyphicon glyphicon-list-alt"></i>
                            <span class="txt"><?php echo $this->lang->line('basic_recipes'); ?></span>
                        </a>
                    </li>
                    <li id="link-search">
                        <a href="<?php echo base_url(); ?>admin/search">
                            <i class="glyphicon glyphicon-list-alt"></i>
                            <span class="txt"><?php echo $this->lang->line('basic_advsearch'); ?></span>
                        </a>
                    </li>
                   <!--  <li id="link-userrecipe">
                        <a  href="<?php echo base_url(); ?>admin/userrecipe">
                            <i class="glyphicon glyphicon-list-alt"></i>
                            <span class="txt">User Recipes</span>
                        </a>
                    </li> -->
                </ul>
            </li>

            <li class="treeview">
                <?php $pages5 = ['site_setting'];?>
                <a href="<?php echo base_url(); ?>" class="<?php echo exapnd_not_expand_menu($page, $pages5); ?>"><i class="icomoon-icon-arrow-down-2 s16 hasDrop"></i><i class="fa fa-cogs"></i>
                <span><?php echo $this->lang->line('basic_setting'); ?></span>
                <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                </span>
                </a>

                <ul class="treeview-menu"<?php echo navigation_show_hide_ul($page, $pages5); ?>>
                     <li id="link-ingredients">
                        <a href="<?php echo base_url() ?>admin/configuration">
                            <i class="fa fa-cog"></i>
                            <span class="txt"><?php echo $this->lang->line('basic_configuration'); ?></span>
                        </a>
                    </li>

                </ul>
            </li>
    </ul>
</section>
</aside>
<!-- End #right-sidebar --><!--Body content-->
<div class="content-wrapper">
<!--Content wrapper-->
<section class="content-header">
<h1><?php echo $title; ?></h1>
<?php echo create_breadcrumb(); ?>
<?php echo set_active_menu($page); ?>
</section>
<!-- End  / heading-->