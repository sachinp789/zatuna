<?php 
$siteLang=$this->session->userdata('site_lang');
$this->load->model('home/Location_master_model');
$this->load->model('home/Specialty_master_model');

$location=$this->Location_master_model->get_all('name_en');
$speciality=$this->Specialty_master_model->get_all('name_en');

/* Get average Rating Query*/
$this->db->select('(reputation + clinic + availability + approachability + technology)/5 as average_score ,date_created,comment,user_master.first_name as UFName ,user_master.last_name as ULName,doctor_master_en.first_name as doctorFname,doctor_master_en.last_name as doctorLname,doctor_details.photo ');
$this->db->limit(2, 0);
$this->db->order_by('average_score', 'desc');
$this->db->join('user_master','user_master.user_id=doctors_rating.user_id');
$this->db->join('doctor_details','doctor_details.doctor_id=doctors_rating.doctor_id');
$this->db->join('doctor_master_en','doctor_master_en.doctor_id=doctors_rating.doctor_id');
$average_score=$this->db->get('doctors_rating')->result();

function timeAgo($time_ago){
  $cur_time   = time();
  $time_elapsed   = $cur_time - $time_ago;
  $seconds  = $time_elapsed ;
  $minutes  = round($time_elapsed / 60 );
  $hours    = round($time_elapsed / 3600);
  $days     = round($time_elapsed / 86400 );
  $weeks    = round($time_elapsed / 604800);
  $months   = round($time_elapsed / 2600640 );
  $years    = round($time_elapsed / 31207680 );
  if($seconds <= 60){echo "$seconds seconds ago";}
  else if($minutes <=60){
    if($minutes==1){echo "one minute ago";}else{echo "$minutes minutes ago";}
  }else if($hours <=24){
    if($hours==1){echo "an hour ago";}else{echo "$hours hours ago";}
  }else if($days <= 7){
    if($days==1){echo "$days day ago";}else{echo "$days days ago";}
  }else if($weeks <= 4.3){
    if($weeks==1){echo "1 week ago";}else{echo "$weeks weeks ago";}
  }else if($months <=12){
    if($months==1){echo "1 month ago";}else{echo "$months months ago";}
  }else{
    if($years==1){echo "one year ago";}else{echo "$years years ago";}
  }
}
?>

<!--///////////////////////////////////////////////////////
       // Search & Rate Your Doctor
       //////////////////////////////////////////////////////////-->
<div class="doctor-det-frm">
  <section class="text-center clearfix">
  <!-- <img src="images/doctors-banner.jpg" class="img-responsive"> -->
      <div class="container">
          <div class="w-row">
            <div class="w-col w-col-3 text-right">
               <div class="search-links-det">
                <ul class="tab-link-searchbox clearfix">
                  <li class="active" id="Doctors">
                    <a href="javascript:void(0)"  class="doctors-link"><?php echo $this->lang->line('homeSearchDoctors'); ?></a>
                  </li>
                  <li id="Hospital">
                    <a href="javascript:void(0)" class="hostpitals-link"><?php echo $this->lang->line('homeSearchHostpitals'); ?></a>
                  </li>
                  <li id="Clinic" >
                    <a href="javascript:void(0)" class="clinics-link"><?php echo $this->lang->line('homeSearchClinic'); ?></a>
                  </li>
                  <li  id="Lab">
                    <a href="javascript:void(0)" class="labs-link"><?php echo $this->lang->line('homeSearchLab'); ?></a>
                  </li>
                </ul>
                </div>
            </div>
          
            <div class="w-col w-col-9 text-left search-links-view">
              <form class = "form-horizontal" role = "form" action="<?php echo base_url(); ?>search" method="post">
                <input type="hidden" value="Doctors" name="typeofsearch" class="typeofsearch">   
                    <div class = "form-group location">
                      <div class = "col-sm-6 pull-left col-xs-12">
                         <select class = "form-control dropdown" name="location">
                           <option value=""><?php echo $this->lang->line('homeSearchlocation'); ?></option>
                           <?php foreach ($location as $row_location) {?>
                             <option value="<?php echo $row_location->location_id; ?>"><?php if($siteLang=='arabic') echo $row_location->name; else echo $row_location->name_en; ?></option>
                           <?php } ?>
                        </select>
                      </div>
                    </div>
                    <div class = "form-group speciality">
                      <div class = "col-sm-6 pull-left col-xs-12">
                         <select class = "form-control dropdown" name="speciality">
                          <option value=""><?php echo $this->lang->line('homeSearchSpeciality'); ?></option>
                          <?php foreach ($speciality as $row_speciality) {?>
                             <option value="<?php echo $row_speciality->specialties_id; ?>"><?php if($siteLang=='arabic') echo $row_speciality->name; else echo $row_speciality->name_en; ?></option>
                          <?php  } ?>
                        </select>
                      </div>
                    </div>
                    <div class = "form-group gender">
                      <div class = "col-sm-6 pull-left col-xs-12">
                         <select class = "form-control dropdown" name="gender">
                           <option value=""><?php echo $this->lang->line('homeSearchGender'); ?></option>
                           <option value="1"><?php echo $this->lang->line('homeSearchGenderMale'); ?></option>
                           <option value="2"><?php echo $this->lang->line('homeSearchGenderFemale'); ?></option>
                        </select>
                      </div>
                    </div>                                        
                     <div class = "form-group searchname">                       
                        <div class = "col-sm-6 pull-left col-xs-12">
                           <input type="text" class="form-control" id="searchname" name="searchname" placeholder="<?php echo $this->lang->line('homeSearchName'); ?>">
                        </div>
                     </div>
                     
                     <div class = "form-group">
                        <div class = "col-sm-6 pull-left col-xs-12">
                                <div class="btn-ex-one pull-right">
                                  <button type="submit" name="searchData" id="searchData" class="ex-btn"><?php echo $this->lang->line('homeSearchBTN'); ?></button>
                                </div>                        
                               </div>
                        </div>                   
                  </form>
                </div>
             </div>          
          </div>
    </section>
</div>

<!--///////////////////////////////////////////////////////
       // ADD TO OUR LIST
       //////////////////////////////////////////////////////////-->
<div class="our-plan-parlex">
  <div class="parlex4-back">
    <div class="container">
      <div class="wrap">
        <div class="our-plans text-center">
          <h2 class="ourplan-heading"><?php echo $this->lang->line('homeAddList'); ?></h2>
          <div class="sepreater"></div>
        </div>
        <div class="w-row">
          <div class="w-col w-col-3 flip pull-left">
            <div class="plan1">
              <div class="plan1-ser1">
                <h4><?php echo $this->lang->line('homeAddDoctor'); ?></h4>
                <p class="plan1-ser1-para"></p>
                <div class="text-center clearfix">
                  <img class="img-responsive nothover" src="<?php echo base_url(); ?>images/add-doctor.png" alt="Add a doctor">
                  <img class="img-responsive hover" src="<?php echo base_url(); ?>images/add-doctor-hover.png" alt="Add a doctor">
                </div>
              </div>
              
            </div>
          </div>
          <div class="w-col w-col-3 flip pull-left">
            <div class="plan1">
              <div class="plan1-ser1">
                <h4><?php echo $this->lang->line('homeAddHospital'); ?></h4>
                <p class="plan1-ser1-para"></p>
                <div class="text-center clearfix">
                  <img class="img-responsive nothover" src="<?php echo base_url(); ?>images/add-hospital.png" alt="Add a hospital">
                  <img class="img-responsive hover" src="<?php echo base_url(); ?>images/add-hospital-hover.png" alt="Add a hospital">
                </div>
              </div>
            </div>
          </div>
          <div class="w-col w-col-3 flip pull-left">
            <div class="plan1 plan3">
              <div class="plan1-ser1 plan3-ser3">
                <h4><?php echo $this->lang->line('homeAddClinic'); ?></h4>
                <p class="plan1-ser1-para"></p>
                <div class="text-center clearfix">
                  <img class="img-responsive nothover" src="<?php echo base_url(); ?>images/add-clinic.png" alt="Add a clinic">
                  <img class="img-responsive hover" src="<?php echo base_url(); ?>images/add-clinic-hover.png" alt="Add a clinic">
                </div>
              </div>              
            </div>
          </div>
          <div class="w-col w-col-3 flip pull-left">
            <div class="plan1 plan4">
              <div class="plan1-ser1 plan4-ser4">
                <h4><?php echo $this->lang->line('homeAddLab'); ?></h4>
                <p class="plan1-ser1-para"></p>
                <div class="text-center clearfix">
                  <img class="img-responsive nothover" src="<?php echo base_url(); ?>images/add-lab.png" alt="Add a lab">
                  <img class="img-responsive hover" src="<?php echo base_url(); ?>images/add-lab-hover.png" alt="Add a lab">                  
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!--///////////////////////////////////////////////////////
       // ARE YOU A DOCTOR?
       //////////////////////////////////////////////////////////-->
<div class="exp service-parlex">
  <div class="who_we_are">
    <div class="container">
      <div class="p20t clearfix">    

          <div class="w-col w-col-6 exp-col2 flip pull-left">
            <div class="col2-div">
              <img class="img-responsive" src="<?php echo base_url(); ?>images/are-you-a-doctor.jpg" alt="Are you a doctor">
            </div>
          </div>
        
          <div class="w-col w-col-6 exp-col2 flip pull-right">
            <div class="col1-div">
              <div class="experinc-box p100t">
                <h3 class="ourplan-heading "><?php echo $this->lang->line('homeRUDoctor'); ?></h3>
                <h4 class="flip text-left"><?php echo $this->lang->line('homeRUDoctor2'); ?></h4>
                <div class="border-buttom"></div>
                <p><?php echo $this->lang->line('homeRUDoctor3'); ?></p>
                <?php if($this->session->userdata('user_login')!=1){?>
                  <div class="buttons">
                      <div class="btn-ex-one">
                        <a class="ex-btn" id="signupopen"  href="javascript:void(0);" data-toggle="modal" data-target="#loginModal"><?php echo $this->lang->line('homeSignUP'); ?></a>
                      </div>                        
                 </div>
                 <?php } ?>
               </div>
            </div>
          </div>       
      </div>   
    </div>
  </div>
</div>

  <!--///////////////////////////////////////////////////////
       // TOPDOCTORS ON THE GO
       //////////////////////////////////////////////////////////-->
<div class="exp service-parlex">
  <div class="exp-back">
    <div class="container">
      <div id="who-v-animation">
      <div class="p70t clearfix">
       
        
          <div class="w-col w-col-6 exp-col1 flip pull-left">
            <div class="col1-div">

              <div class="experinc-box p5t">
                <h3 class="ourplan-heading"><?php echo $this->lang->line('homeContent1'); ?></h3>
                <div class="border-buttom"></div>
                <h5 class="title"><?php echo $this->lang->line('homeContent2'); ?></h5>
               
                  <div class="buttons get-application">
                      <div class="btn-ex-one exp-col2 clear flip pull-left">
                        <a class="" href="#">
                            <img class="img-responsive" src="<?php echo base_url(); ?>images/google-play-btn.png" alt="Google Play">
                        </a>
                      </div>
                 </div>
               </div>
            </div>
          </div>

          <div class="w-col w-col-6 exp-col2 flip pull-right">
            <div class="col2-div">
              <?php if($siteLang=='arabic') $androidImage = 'topdoctors-on-the-go-ar.jpg'; else $androidImage = 'topdoctors-on-the-go.jpg';
                ?>
              <img class="ver-ali-bot img-responsive" src="<?php echo base_url(); ?>images/<?php echo $androidImage;?>" alt="">
            </div>
          </div>

      
      </div>
    </div>
    </div>
  </div>
</div>

<!--///////////////////////////////////////////////////////
       // TOP REVIEWS
       //////////////////////////////////////////////////////////-->

<div class="top-reviews our-plan-parlex">
  <div class="parlex4-back">
    <div class="container">
      <div class="wrap">
        <div class="our-plans text-center">
          <h2 class="ourplan-heading"><?php echo $this->lang->line('homeTopReviews'); ?></h2>
          <div class="sepreater"></div>
        </div>
        <!--<div class="w-row">
          <?php $i=1; foreach ($average_score as $value) {?>
          <div class="w-col w-col-6 flip pull-left">
            <div class="plan1_1">
              <div class="plan1-ser1 clearfix">
                
                  <div class="review-user flip pull-left">
                    <?php if(empty($value->photo)){ ?>
                      <img src="<?php echo base_url(); ?>uploads/user.jpg" class="img-responsive" alt="Review User"><?php } else { ?>
                      <img src="<?php echo base_url(); ?>uploads/doctor_image/<?php echo $value->photo; ?>" class="img-responsive" alt="Review User"><?php } ?>
                  </div>
                  <div class="review-details flip pull-right">
                      <h6 class="flip pull-left"><?php echo $value->UFName .' '. $value->ULName;?> <span class="flip pull-right"><?php echo $value->doctorFname .' '.$value->doctorLname ?></span></h6>
                      <p class="text-left flip"><?php echo $value->comment; ?>
                         <img src="<?php echo base_url(); ?>images/small-user.png" class="img-responsive" alt="user-icon"> <img src="<?php echo base_url(); ?>images/small-user-simple.png" class="img-responsive m5l" alt="user-icon"> 
                      </p>
                        <div class="clearfix">
                            <table class="review-tab-det" width="100%">
                              <tr>
                                <td align="left" class="flip text-left">
                                <?php echo floatval($value->average_score); ?>
                                <input type="hidden" class="rateval<?php echo $i ?>" value="<?php echo floatval($value->average_score); ?>">
                                <div class="rateYo<?php echo $i; ?>"></div>
                                </td>
                                <td align="right" class="flip text-right">
                                    <a href="#">
                                      <img src="<?php echo base_url(); ?>images/link-share-review.png">
                                    </a>
                                </td>
                              </tr>
                              <tr>
                                 <td align="left" class="flip text-left">
                                  <span><?php $curenttime=$value->date_created;
                                  $time_ago =strtotime($curenttime);
                                  echo timeAgo($time_ago);
                                   ?></span>
                                </td>
                                <td align="right" class="flip text-right">
                                  <span class="chat">
                                    <img src="<?php echo base_url(); ?>images/chat-icon.png">
                                  </span>
                                </td>
                              </tr>
                            </table>
                      </div>
                  </div>
              </div>              
            </div>
          </div>
          <script>

            $(function () {
            $(".rateYo<?php echo $i ?>").rateYo({
              "starWidth": "15px",
              "ratedFill": "#053b61",
              "rating" : $('.rateval<?php echo $i ?>').val(),
              "readOnly":true
              })
            }); 
          </script>
          <?php $i++; } ?>
        </div>-->
         <div class="w-row">

          <div class="w-col w-col-6 flip pull-left">
            <div class="plan1_1">
              <div class="plan1-ser1 clearfix">
                  <div class="review-user flip pull-left">
                      <img alt="Review User" class="img-responsive" src="<?php echo base_url(); ?>images/review-user1.png">
                  </div>
                  <div class="review-details flip pull-right">
                      <h6 class="flip pull-left">Monica E. <span class="flip pull-right">For DR ABADI MOHAMMED ADEL EL QADI </span></h6>
                      <p class="text-left flip">
                        Very cool review, Very cool review Very cool review, <br> 
                        Very cool review, Very cool review Very cool review, <br>
                        Very cool review Very cool review <img alt="user-icon" class="img-responsive" src="<?php echo base_url(); ?>images/small-user.png"> <img alt="user-icon" class="img-responsive m5l" src="<?php echo base_url(); ?>images/small-user-simple.png">
                      </p>
                        <div class="clearfix">
                            <table width="100%" class="review-tab-det">
                              <tbody><tr>
                                <td align="left" class="flip text-left">
                                  <img src="<?php echo base_url(); ?>images/rating-star.png">
                                </td>
                                <td align="right" class="flip text-right">
                                    <a href="#">
                                      <img src="<?php echo base_url(); ?>images/link-share-review.png">
                                    </a>
                                </td>
                              </tr>
                              <tr>
                                 <td align="left" class="flip text-left">
                                  <span>1 month ago</span>
                                </td>
                                <td align="right" class="flip text-right">
                                  <span class="chat">
                                    <img src="<?php echo base_url(); ?>images/chat-icon.png">
                                  </span>
                                </td>
                              </tr>
                            </tbody></table>
                      </div>
                  </div>
                  
              </div>              
            </div>
          </div>
          <div class="w-col w-col-6 flip pull-left">
            <div class="plan1_1">
              <div class="plan1-ser1 clearfix">
                  <div class="review-user flip pull-left">
                      <img alt="Review User" class="img-responsive" src="<?php echo base_url(); ?>images/review-user1.png">
                  </div>
                  <div class="review-details flip pull-right">
                      <h6 class="flip pull-left">Monica E. <span class="flip pull-right">For DR ABADI MOHAMMED ADEL EL QADI </span></h6>
                      <p class="text-left flip">
                        Very cool review, Very cool review Very cool review, <br> 
                        Very cool review, Very cool review Very cool review, <br>
                        Very cool review Very cool review <img alt="user-icon" class="img-responsive" src="<?php echo base_url(); ?>images/small-user.png"> <img alt="user-icon" class="img-responsive m5l" src="<?php echo base_url(); ?>images/small-user-simple.png">
                      </p>
                        <div class="clearfix">
                            <table width="100%" class="review-tab-det">
                              <tbody><tr>
                                <td align="left" class="flip text-left">
                                  <img src="<?php echo base_url(); ?>images/rating-star.png">
                                </td>
                                <td align="right" class="flip text-right">
                                    <a href="#">
                                      <img src="<?php echo base_url(); ?>images/link-share-review.png">
                                    </a>
                                </td>
                              </tr>
                              <tr>
                                 <td align="left" class="flip text-left">
                                  <span>1 month ago</span>
                                </td>
                                <td align="right" class="flip text-right">
                                  <span class="chat">
                                    <img src="<?php echo base_url(); ?>images/chat-icon.png">
                                  </span>
                                </td>
                              </tr>
                            </tbody></table>
                      </div>
                  </div>
              </div>              
            </div>
          </div>
         </div> 
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
  var selector = '.tab-link-searchbox > li';
  $(selector).on('click', function(){
    $(selector).removeClass('active');
    var text=$(this).addClass('active');
    var id_attr = $(".tab-link-searchbox > li.active").attr('id');
    $('.typeofsearch').append().val(id_attr);
    if(id_attr=="Doctors"){
      $('.gender').show();      
      $('.searchname').show();
      $('.location').show();
      $('.speciality').show();
    }if(id_attr=="Hospital"){
      $('.gender').hide();   
      $('.speciality').hide();   
      $('.searchname').show();
      $('.location').show();
    }if(id_attr=="Clinic" || id_attr=="Lab"){
      $('.gender').hide();      
      $('.searchname').show();
      $('.location').show();
      $('.speciality').hide();
    }
  });
});
</script>

<script type="text/javascript">
function userController($scope,$http) {
  $scope.users = [];
  $http.get('<?php echo base_url(); ?>home/getlocation').success(function($data){ $scope.users=$data; });
}
</script>