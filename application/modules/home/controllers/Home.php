<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH.'libraries/facebook/facebook.php';

class Home extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->config->load('facebook');
        $this->load->model('home/User_master_model');
        $this->load->library('email');
    }

    function index() {  
        $this->data['title']='TopDoctors';
        $this->__templateFront('home/index', $this->data);
    }

    function login_process() {

        $email=$_POST['loginemail'];
        $password=$_POST['loginpassword'];

        $get_row=$this->db->get_where('user_master',array('email'=>$email,'password'=>hash('md5', $password . config_item('encryption_key')),'status'=>'1'));
        if($get_row->num_rows()>0) {
            $row=$get_row->row();
            $this->session->set_userdata("user_login", "1");
            $this->session->set_userdata("user_id", $row->user_id);
            $this->session->set_userdata("user_firstname", $row->first_name);
            $this->session->set_userdata("user_lastname", $row->last_name);
                redirect(base_url('home'));
        } else {
            if($this->session->userdata('site_lang') == 'arabic'){
                $errorLogin=$this->lang->line('errorLogin');
            }else{
                $errorLogin=$this->lang->line('errorLogin');
            }
            $this->session->set_flashdata('errorLogin',$errorLogin);
            redirect(base_url('home'));
        }
    }

    function signup_process(){
         if($_POST){
                $this->User_master_model->insert(array(
                'first_name'    => $_POST['firstname'],
                'last_name'     => $_POST['lastname'],
                'email'         => $_POST['email'],
                'mobile_number' => $_POST['mobile_no'],
                'password'      => hash('md5', $_POST['c_password'] . config_item('encryption_key')),
                'country_id'    => $_POST['country_id'],
                'address'       => $_POST['address'],
            ));

                $insert_id = $this->db->insert_id();
                if(isset($_POST['c1'])){
                    $this->User_master_model->update($insert_id,array('newslatter'=> $_POST['c1']));
                }
                $this->session->set_userdata("user_login", "1");
                $this->session->set_userdata("user_id", $insert_id);
                $this->session->set_userdata("user_firstname", $_POST['firstname']);
                $this->session->set_userdata("user_lastname", $_POST['lastname']);

                if ($insert_id) {
                $this->email->set_newline("\r\n");
                $this->email->from('tejas.patel@searchnative.in', 'Top doctor');
                $this->email->to($_POST['email']);
                $this->email->subject('Top doctor registration success');
                $data['firstname'] = $_POST['firstname'];
                $data['email']     = $_POST['email'];
                $data['password']  = $_POST['password'];
                $msg = $this->load->view('template/email_template',$data,TRUE);
                $this->email->message($msg);
                $this->email->send();

                /*Email Send for Newsletter*/
                if($_POST['c1']=='1') {
                    $this->email->set_newline("\r\n");
                    $this->email->from('tejas.patel@searchnative.in', 'Top doctor');
                    $this->email->to($_POST['email']);
                    $this->email->subject('Newsletter subscribe for top doctor');
                    $dataln['firstname'] = $_POST['firstname'];
                    $msgnew = $this->load->view('template/newslatter_template',$dataln,TRUE);
                    $this->email->message($msgnew);
                    $this->email->send();
                }
            }

            if($this->session->userdata('site_lang') == 'arabic'){
                $registerSuccess=$this->lang->line('registerSuccess');
            }else{
                $registerSuccess=$this->lang->line('registerSuccess');
            }
            $this->flash_notification($registerSuccess);
            redirect(base_url('home'));
        }

    }

    function logout() {
        $this->session->sess_destroy();
        redirect(base_url('home'));
    }

    function fblogin(){
        $base_url=$this->config->item('base_url'); //Read the baseurl from the config.php file
        //get the Facebook appId and app secret from facebook.php which located in config directory for the creating the object for Facebook class
        $facebook = new Facebook(array(
        'appId'     => $this->config->item('appID'), 
        'secret'    => $this->config->item('appSecret'),
        ));
        
        $user = $facebook->getUser(); // Get the facebook user id 
        
        if($user){
            
            try{
                $user_profile = $facebook->api('/me?fields=id,first_name,last_name,email');  //Get the facebook user profile data
                $user_profile['email'];
                $this->session->set_userdata("user_login", "1");
                $this->session->set_userdata("user_firstname", $user_profile['first_name']);
                $this->session->set_userdata("user_lastname", $user_profile['last_name']);
                redirect(base_url('home'));
                $params = array('next' => $base_url.'fbci/logout');
                
                $ses_user=array('User'=>$user_profile,
                   'logout' =>$facebook->getLogoutUrl($params)   //generating the logout url for facebook 
                );
                $this->session->set_userdata($ses_user);
                header('Location: '.$base_url);
            }catch(FacebookApiException $e){
                error_log($e);
                $user = NULL;
            }  
            redirect(base_url('home'));     
        }   
        redirect(base_url('home'));
    }
    
    function check_user_email($param = '') {
       
        $email = $_POST['email'];
        if ($param == '') {
            $data = $this->User_master_model->get_by(array(
                'email' => $email
            ));
            if ($data) {
                echo "false";
            } else {
                echo "true";
            }
        }
    }

    function forgot_password() {
        $email=$_POST['forgotemail']; 
        if ($_POST) {
           $record = $this->db->get_where('user_master',array('email'=>$email))->row();
           
            if ($record) {
                $user_id = $record->user_id; 
                $random_string = $this->random_string_generate();
               
                $this->update_forgot_password_key('', $user_id, hash('md5',$random_string.config_item('encryption_key')));
                $this->email->set_newline("\r\n");
                $this->email->from('tejas.patel@searchnative.in', 'Top doctor');
                $this->email->to($record->email);
                $this->email->subject('Reset your top doctor password');
                $dataforgot['password'] = $random_string;
                $msgforgotpass=$this->load->view('template/fotgotpass_template',$dataforgot,TRUE);

                $this->email->message($msgforgotpass);
                $this->email->send();

                if($this->session->userdata('site_lang') == 'arabic'){
                    $registerSuccess=$this->lang->line('forgotpasswordsuccess');
                }else{
                    $registerSuccess=$this->lang->line('forgotpasswordsuccess');
                }
                $this->flash_notification($registerSuccess);

                redirect(base_url('home'));
            } else {
                if($this->session->userdata('site_lang') == 'arabic'){
                    $forgotEmailNotFound=$this->lang->line('forgotEmailNotFound');
                }else{
                    $forgotEmailNotFound=$this->lang->line('forgotEmailNotFound');
                }
                $this->session->set_flashdata('forgotEmailNotFound',$forgotEmailNotFound );
                redirect(base_url('home'));
            }
        } else {
           redirect(base_url('home'));
        }
    }

    function random_string_generate() {
        $this->load->helper('string');
        return random_string('alnum', 16);
    }

    function update_forgot_password_key($user_type, $user_id, $key) {
            $this->db->where('user_id', $user_id);
            $this->db->update('user_master', [
                'password' => $key
            ]);
    }
    
    function send_news() {
        $getSubscribe=$this->db->get_where('user_master',array('newslatter'=>1))->result();
        foreach ($getSubscribe as $getSubscribeRow)
        {
            $this->email->set_newline("\r\n");
            $this->email->to($getSubscribeRow->email);
            $this->email->from('tejas.patel@searchnative.in');
            $this->email->subject('@ Top doctor - Newsletter Alert');
            $this->email->message('Hi Here is the info you requested.');
            $this->email->send();
        }
    }
}
