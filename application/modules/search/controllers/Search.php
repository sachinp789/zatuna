<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends MY_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('home/User_master_model');
    }

    function index() {

        $data['title']='TopDoctors';
        $data['locationArray1']=array();
        if(isset($_POST['searchData'])){
            
            $data['location']       =$_POST['location'];
            $data['speciality']     =$_POST['speciality'];
            $data['gender']         =$_POST['gender'];
            $data['searchname']     =$_POST['searchname'];
            $data['typeofsearch']   =$_POST['typeofsearch'];    

            if($data['typeofsearch']=='Doctors'){
                if(!empty($data['location']) || !empty($data['speciality'])){
                    $Dcondition=array('doctor_details.location_id'=>$data['location'],'doctor_details.speciality_id'=>$data['speciality']);
                }elseif(!empty($data['location']) && !empty($data['gender'])){
                    $Dcondition=array('doctor_details.location_id'=>$data['location'],'doctor_details.gender'=>$data['gender']);
                }elseif(!empty($data['location']) && !empty($data['searchname'])){
                    $Dcondition="doctor_details.location_id='".$data['location']."' AND work_master.doctor_id='".$data['typeofsearch']."' OR doctor_master_en.first_name LIKE '%".$data['searchname']."%' OR doctor_master_en.last_name LIKE '%".$data['searchname']."%' ";
                }elseif(!empty($data['location'])){
                    $Dcondition=array('doctor_details.location_id'=>$data['location']);
                }elseif(!empty($data['speciality'])){
                    $Dcondition=array('doctor_details.speciality_id'=>$data['speciality']);
                }elseif(!empty($data['gender'])){
                    $Dcondition=array('doctor_details.gender'=>$data['gender']);
                }elseif(!empty($data['searchname'])){
                    $Dcondition= "doctor_details.doctor_id='".$data['typeofsearch']."' OR doctor_master_en.first_name LIKE '%".$data['searchname']."%' OR doctor_master_en.last_name LIKE '%".$data['searchname']."%' ";
                }else{
                    $Dcondition='';
                }

                $this->db->select('doctor_details.photo,doctor_master_en.first_name as doctorFirstName,doctor_master_en.last_name as doctorLastName,doctor_master_en.address as doctorAddress,doctor_details.phone_number,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude');
                $this->db->join('location_master', 'doctor_details.location_id=location_master.location_id');
                $this->db->join('specialty_master','doctor_details.speciality_id=specialty_master.specialties_id');
                $this->db->join('doctor_master_en', 'doctor_details.doctor_id=doctor_master_en.doctor_id');
                if(!empty($Dcondition)){$this->db->where($Dcondition);}
                $data['locationArray']= $this->db->get('doctor_details')->result();
             
             }elseif ($data['typeofsearch']=='Hospital' || $data['typeofsearch']=='Clinic' ||  $data['typeofsearch']=='Lab') {
                if(!empty($data['location']) && !empty($data['searchname'])){
                    $locationCondition=array('work_master.location_id'=>$data['location'],"work_master.name_en LIKE"=>"%".$data['searchname']."%",'work_master.work_type' =>$data['typeofsearch'],'work_master.status'=>'1');
                }else if(!empty($data['location'])){
                    $locationCondition=array('work_master.location_id'=>$data['location'],'work_master.work_type' =>$data['typeofsearch'],'work_master.status'=>'1');
                }else if(!empty($data['searchname'])){
                    $locationCondition=array("work_master.name_en LIKE"=>"%".$data['searchname']."%",'work_master.work_type' =>$data['typeofsearch'],'work_master.status'=>'1');
                }else{
                    $locationCondition='';
                }
                    $this->db->select('doctor_details.photo,doctor_master_en.first_name as doctorFirstName,doctor_master_en.last_name as doctorLastName,doctor_master_en.address as doctorAddress,doctor_details.phone_number,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude');
                    $this->db->join('doctor_master_en', 'doctor_master_en.doctor_id=doctor_details.doctor_id');
                    $this->db->join('work_master', 'doctor_details.doctor_id=work_master.doctor_id');
                    $this->db->join('location_master', 'doctor_details.location_id = location_master.location_id');
                    if(!empty($locationCondition)){$this->db->where($locationCondition);}
                    $this->db->group_by('doctor_details.doctor_id');
                    $data['locationArray'] = $this->db->get('doctor_details')->result();
                    //echo $this->db->last_query(); die;    
            }elseif(empty($data['typeofsearch'])){
                if(!empty($data['location']) && !empty($data['speciality'])){
                    $condition= array('doctor_details.location_id'=>$data['location'],'doctor_details.speciality_id'=>$data['speciality']);
                }elseif(!empty($data['location']) && !empty($data['gender'])){
                    $condition= array('doctor_details.location_id'=>$data['location'],'doctor_details.gender'=>$data['gender']);
                }elseif(!empty($data['location']) && !empty($data['searchname'])){
                    $condition= "doctor_details.location_id = '".$data['location']."' AND doctor_master_en.first_name LIKE '%".$data['searchname']."%' OR doctor_master_en.last_name LIKE '%".$data['searchname']."%' OR work_master.name_en LIKE '%".$data['searchname']."%' ";
                }elseif(!empty($data['location'])){
                    $condition=array('doctor_details.location_id'=>$data['location']);
                }elseif(!empty($data['speciality'])){
                    $condition= "doctor_details.speciality_id='".$data['speciality']."'";
                }elseif(!empty($data['gender']) ){
                    $condition = "doctor_details.gender='".$data['gender']."'";
                }elseif(!empty($data['searchname'])){
                    $condition = "doctor_master_en.first_name LIKE '%".$data['searchname']."%' OR doctor_master_en.last_name LIKE '%".$data['searchname']."%' OR work_master.name_en LIKE '%".$data['searchname']."%' ";
                }else{
                    $condition='';
                }

                $this->db->select('doctor_details.photo,doctor_master_en.first_name as doctorFirstName,doctor_master_en.last_name as doctorLastName,doctor_master_en.address as doctorAddress,doctor_details.phone_number,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude');
                $this->db->join('location_master', 'doctor_details.location_id=location_master.location_id');
                $this->db->join('specialty_master','doctor_details.speciality_id=specialty_master.specialties_id');
                $this->db->join('doctor_master_en', 'doctor_details.doctor_id=doctor_master_en.doctor_id');
                $this->db->join('work_master','work_master.work_id=doctor_details.work_id');
                if(!empty($condition)){$this->db->where($condition);}
                $data['locationArray']= $this->db->get('doctor_details')->result();
                //echo $this->db->last_query(); die;
            }
        } else if(isset($_POST['specialitySearch'])){    

                $data['searchSpecialityValue']=$_POST['searchSpecialityValue'];

                $condition=array('specialty_master.name_en LIKE'=>'%'.$data['searchSpecialityValue'].'%');
                $this->db->select('doctor_master_en.first_name as doctorFirstName,doctor_master_en.last_name as doctorLastName,doctor_master_en.address as doctorAddress,doctor_details.photo,doctor_details.phone_number,doctor_details.doctor_id,doctor_details.google_map_latitude,doctor_details.google_map_longtude');
                $this->db->join('doctor_details', 'doctor_details.speciality_id=specialty_master.specialties_id');
                $this->db->join('doctor_master_en', 'doctor_details.doctor_id=doctor_master_en.doctor_id');
                $data['locationArray'] = $this->db->get_where('specialty_master', $condition)->result();
                //echo $this->db->last_query();die;    
        }
        $this->db->select('(reputation + clinic + availability + approachability + technology)/5 as featureAverageScore,doctor_master_en.first_name , doctor_master_en.last_name,count(doctors_rating.doctor_id) as doctorReview , doctor_details.doctor_id , doctor_details.photo');
        $this->db->join('doctors_rating','doctor_details.doctor_id=doctors_rating.doctor_id');
        $this->db->join('doctor_master_en','doctor_details.doctor_id=doctor_master_en.doctor_id');
        $this->db->group_by('doctor_details.doctor_id');
        $data['featureArray']=$this->db->get('doctor_details')->result();
        //echo $this->db->last_query();die;
        $this->__templateFront('search/index', $data);
    }


}
