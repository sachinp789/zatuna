<?php 
$siteLang=$this->session->userdata('site_lang');
$this->load->model('home/Location_master_model');
$this->load->model('home/Specialty_master_model');

$locationList=$this->Location_master_model->get_all('name_en');
$specialityList=$this->Specialty_master_model->get_all('name_en');
if(isset($_POST['searchData'])){
    $speciality=$speciality;
    $locationArray=$locationArray;
    $searchSpecialityValue='';
}else{
    if(isset($_POST['specialitySearch'])) {
        $locationArray=$locationArray;
      }else{
        $locationArray=array();
        $searchSpecialityValue='';
      }
    $location='';
    $speciality='';
    $gender='';
    $searchname='';
}
?>

<div class="search-full-box">
  <section class="text-center clearfix">
  <!-- <img src="images/doctors-banner.jpg" class="img-responsive"> -->
      <div class="container">
          <div class="w-row">
            <div class="w-col w-col-12 text-left">
                <form class="form-inline" action="<?php echo base_url();?>search" method="post">
                  <div class="form-group">
                    <span class="flip search-icon"> <i class="fa fa-search"></i></span>
                    <input type="text" name="searchSpecialityValue" class="form-control" id="" placeholder="<?php echo $this->lang->line('searchPlaceHolder'); ?>" value="<?php  echo $searchSpecialityValue; ?>">
                  </div>
                  <div class="form-control-btn-box">
                    <button type="submit" name="specialitySearch" class="btn btn-default search-btn"><?php echo $this->lang->line('searchbtn'); ?></button>                   
                  </div>
                </form>
            </div>
          </div>          
      </div>
  </section>
</div>

<!--///////////////////////////////////////////////////////
       // Filters
       //////////////////////////////////////////////////////////-->
<div class="filters-box m50t">
  <section class="text-center clearfix">
      <div class="container">
          <div class="w-row">
            <div class="w-col w-col-9 text-left">
            <!-- Filter details start  -->
               <div class="filters-details clearfix">
                 <h2 class="flip text-left clearfix"><?php echo $this->lang->line('filterTitle'); ?></h2>
                 <div class="border-divider"></div>
                   <form class="form-horizontal" role="form" action="<?php base_url(); ?>search" method="post">   <input type="hidden" name="typeofsearch">
                    <div class="form-group">
                      <div class = "col-sm-6 pull-left col-xs-12">
                         <select class = "form-control dropdown" name="location">
                           <option value=""><?php echo $this->lang->line('filterLocation'); ?></option>
                           <?php foreach ($locationList as $row_location) {?>
                            <option value="<?php echo $row_location->location_id; ?>" <?php if($location==$row_location->location_id)echo 'selected'; ?>><?php if($siteLang=='arabic') echo $row_location->name; else echo $row_location->name_en; ?></option>
                           <?php } ?>
                        </select>
                      </div>
                  
                      <div class = "col-sm-6 pull-left col-xs-12">
                         <select class = "form-control dropdown" name="speciality">
                           <option value=""><?php echo $this->lang->line('filterSpeciality'); ?></option>
                           <?php foreach ($specialityList as $row_speciality) {?>
                             <option value="<?php echo $row_speciality->specialties_id; ?>" <?php if($speciality==$row_speciality->specialties_id)echo 'selected'; ?>><?php if($siteLang=='arabic') echo $row_speciality->name; else echo $row_speciality->name_en; ?></option>
                          <?php  } ?>
                        </select>
                      </div>
                    </div>
                    <div class = "form-group">
                      <div class = "col-sm-6 pull-left col-xs-12">
                         <select class = "form-control dropdown" name="gender">
                           <option value=""><?php echo $this->lang->line('filterGender'); ?></option>
                           <option value="1" <?php if($gender==1)echo 'selected'; ?>><?php echo $this->lang->line('filterMale'); ?></option>
                           <option value="2" <?php if($gender==2)echo 'selected'; ?>><?php echo $this->lang->line('filterFemale'); ?></option>
                        </select>
                      </div>
                                          
                        <div class = "col-sm-6 pull-left col-xs-12">
                           <input type="text" class="form-control" id="searchname" name="searchname" placeholder="<?php echo $this->lang->line('filterNamePlace'); ?>" value="<?php if($searchname)echo $searchname; ?>">
                        </div>
                     </div>
                     
                     <div class = "form-group no-marginbot">
                        <div class = "col-sm-12 pull-left col-xs-12">                            
                            <div class="flip text-left">
                                <button type="submit" id="searchData" name="searchData" class="btn btn-default search-btn" ><?php echo $this->lang->line('filterSearchBtn'); ?></button>
                            </div>                        
                        </div>                        
                      </div>                   
                  </form>
               </div>
             <!-- Filter details end  -->
             <div class="sorting-det">
                <!-- <form class = "form-horizontal" role = "form">   
                    <div class = "form-group">
                      <div class = "col-sm-6 pull-left col-xs-12">
                         <select class = "form-control dropdown">
                          <option><?php echo $this->lang->line('searchSortBy'); ?></option>
                          <option><?php echo $this->lang->line('searchNearByLocation'); ?></option>
                          <option><?php echo $this->lang->line('searchRatingReview'); ?></option>
                          <option><?php echo $this->lang->line('searchBookMark'); ?></option>
                        </select>
                      </div>
                    </div>
                </form> -->
             </div>   

             <div class="border-divider fullwidth"></div>
             <!-- Filter-show data doctor-details -->
             <div class="filter-show-data clearfix">
                  <form role="form" class="form-horizontal">   
                    <div class="form-group">
                      <?php if(empty($locationArray)) { ?>
                       <div><h1 style="color:#dedede;"><?php echo $this->lang->line('searchNoRecords'); ?></h1></div> 
                      <?php } else{
                        $i=1;
                      foreach ($locationArray as $locationArrayRow) {
                      $this->db->select('(reputation + clinic + availability + approachability + technology)/5 as average_score');
                      $averageScoreRow=$this->db->get_where('doctors_rating',array('doctor_id'=>$locationArrayRow->doctor_id))->row();
                       ?>
                      <div class="col-sm-6 pull-left col-xs-12">
                         <div class="doctor-details">
                            <div class="top-box">
                              <?php if(empty($locationArrayRow->photo)){?>
                              <img src="<?php base_url(); ?>uploads/user.jpg" alt="" height="150" width="150">
                              <?php }else{ ?>
                              <img src="<?php base_url(); ?>uploads/doctor_image/<?php echo $locationArrayRow->photo; ?>" alt="" height="150" width="150"> 
                                <?php } ?>
                                <div class="bookmarks-select">
                                  <img src="images/bookmarks-left.png" alt="">
                                </div>
                            </div>
                            <div class="bottom-box">

                                <h2 class="flip text-left clearfix"><?php echo $locationArrayRow->doctorFirstName." ".$locationArrayRow->doctorLastName;?></h2>
                                <div class="border-divider"></div> 

                                 <div class="review-det">
                                    <div class="flip text-left">
                                      <input type="hidden" class="rateval<?php echo $i ?>" value="<?php echo floatval($averageScoreRow->average_score); ?>">
                                      <div class="rateYo<?php echo $i; ?>"></div>
                                    </div>
                                    <div class="flip text-right">
                                        <span class="title">
                                            <?php $this->db->select('count(*) as doctorReview'); 
                                             $test=$this->db->get_where('doctors_rating',array('doctor_id'=>$locationArrayRow->doctor_id))->row();
                                            echo $test->doctorReview; ?> Reviews
                                        </span>
                                    </div>
                                 </div> 

                                 <div class="address-det">
                                    <div class="w100p clearfix">
                                          <div class=" first-one">
                                          <img src="images/location-icon.png" alt="">
                                      </div>
                                      <div class=" first-one">
                                          <span class="address">
                                              <?php echo $locationArrayRow->doctorAddress;?> 
                                          </span>
                                      </div>  
                                    </div>
                                    
                                    <div class="w100p clearfix">                                    
                                      <div class=" second-one">
                                          <img src="images/mobile-icon.png" alt="">
                                      </div>
                                      <div class=" second-one">
                                          <span class="phone">
                                            <?php echo $locationArrayRow->phone_number;?> 
                                          </span>
                                      </div>
                                    </div>

                                    <div class="w100p clearfix">                                    
                                     <div class="third-one">
                                        <!-- <img src="images/teeth-icon.png" alt=""> -->
                                      </div>                                    
                                     </div> 

                                     <div class="w100p clearfix btn-box"> 
                                         <div class="col-sm-12 pull-left col-xs-12">                            
                                            <div class="flip text-right">      
                                                <!-- <img src="images/bookmarks-plus-icon.png" alt=""> -->
                                                <button class="btn btn-default search-btn" type="search">See More ...</button>
                                            </div>                        
                                        </div>
                                    </div>
                                 </div>                                  

                            </div>
                         </div>
                      </div>
                      <script>
                      $(function () {
                      $(".rateYo<?php echo $i ?>").rateYo({
                        "starWidth": "20px",
                        "ratedFill": "#053b61",
                        "rating" : $('.rateval<?php echo $i ?>').val(),
                        "readOnly":true
                        })
                      }); 
                      </script>
                      <?php $i++; } }?>
                    </div>
                    </form>
             </div>

            </div>

            <div class="w-col w-col-3 text-right">            
              <div class="map-det">
                <a href="javascript:void(0);" class="get-direction-link"><?php echo $this->lang->line('searchGetDirection'); ?></a>
              <div id="map">  </div>  
              </div>
              <script type="text/javascript">
              
              
              var locations = [
              <?php $j=1; foreach ($locationArray as $locationArrayMapRow) { ?>
                [' ',<?php echo $locationArrayMapRow->google_map_latitude;?>,<?php echo $locationArrayMapRow->google_map_longtude;?>, <?php echo $j; ?>],
                <?php $j++; } ?>
              ];
              
              var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 6,
                center: new google.maps.LatLng(<?php echo $locationArrayMapRow->google_map_latitude;?>, <?php echo $locationArrayMapRow->google_map_longtude;?>),
                mapTypeId: google.maps.MapTypeId.ROADMAP
              });
              var infowindow = new google.maps.InfoWindow();
              var marker, i;
              for (i = 0; i < locations.length; i++) { 
                marker = new google.maps.Marker({
                  position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                  map: map
                });
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                  return function() {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                  }
                })(marker, i));
              }
            </script>
              <div class="clearfix"></div>
            <div class="featured-doctor-det clearfix">
               <div class="top">
                  <h3 class="flip text-left clearfix"><?php echo $this->lang->line('searchFeaturedDoctor'); ?></h3>
               </div>
               <div class="bottom">
                 <ul class="clearfix">
                  <?php $j=1; foreach($featureArray as $feaureLocationRow){?>
                   <li class="flip text-left">
                      <div class="img-box">
                        <?php if(empty($feaureLocationRow->photo)){ ?>
                          <img src="<?php echo base_url(); ?>uploads/user.jpg" alt="">
                        <?php }else{?>
                          <img src="<?php echo base_url(); ?>uploads/doctor_image/<?php echo $feaureLocationRow->photo; ?>" alt="">
                          <?php } ?>
                      </div>
                      <div class="details">
                          <h4 class="flip text-left clearfix">
                              <?php echo $feaureLocationRow->first_name .' '. $feaureLocationRow->last_name; ?>
                          </h4>
                          <div class="review-det">
                                    <div class="flip text-left">
                                      <input type="hidden" value="<?php echo floatval($feaureLocationRow->featureAverageScore); ?>" class="rateFeatureVal<?php echo $j;?>">
                                     <div class="rateFeature<?php echo $j; ?>"></div>
                                    </div>
                                    <div class="flip text-right">
                                        <span class="title">
                                          <?php echo $feaureLocationRow->doctorReview; ?> Reviews                                          
                                        </span>
                                    </div> 
                                    <div class="third-one">
                                        <!--<img alt="" src="images/teeth-icon.png">-->
                                    </div>                                   
                          </div>
                      </div>
                   </li>
                   <script>
                      $(function () {
                      $(".rateFeature<?php echo $j; ?>").rateYo({
                        "starWidth": "15px",
                        "ratedFill": "#053b61",
                        "rating" : $('.rateFeatureVal<?php echo $j; ?>').val(),
                        "readOnly":true
                        })
                      }); 
                    </script>
                   <?php $j++; } ?>
                          
                 </ul>
               </div>
            </div>
            </div>
          </div>          
      </div>
  </section>
</div>