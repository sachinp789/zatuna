<?php

class User_shoppingcart_fr_model extends MY_Model {
	
	protected $primary_key = 'id'; // PRIMARY KEY

	function create($data) {
    	$this->db->insert_batch('user_shoppingcart_fr', $data);
	}

	function checkIngredients($data,$recipe_id,$user){
		$text = '';
		$this->db->select('ingredient_fr_id');
		$this->db->where_in('ingredient_fr_id', $data);
		$this->db->where('recipe_fr_id', $recipe_id);
		$this->db->where('user_id', $user);
 		$query = $this->db->get('user_shoppingcart_fr');
		if($query->num_rows() > 0){
			$text = $query->result_array();
		}
		else{
			$text = '';
		}
		return $text;	
	}

	// User shopping list of food glossary
	function getCartlist($user){
		$this->db->select('us.*,ing.name as ingredient_name,ing.media as ingredient_image, ingcat.id as ingredient_category_fr_id,ingcat.name as ingredient_category_name,recp.recipe_name');
		//$recipeslist = $this->db->select('*');
		$this->db->from('user_shoppingcart_fr AS us');
		$this->db->join('recipes_fr AS recp', 'us.recipe_fr_id = recp.id', 'LEFT');
		$this->db->join('ingredients_fr AS ing', 'us.ingredient_fr_id = ing.id', 'LEFT');
		$this->db->join('ingredient_categories_fr AS ingcat', 'ing.ingredient_category_fr_id = ingcat.id', 'LEFT');

		$this->db->where('us.status',0);
		$this->db->where('us.user_id',$user);
		//$this->db->group_by('us.recipe_id');

		$result = $this->db->get()->result();
		return $result;
	}

	// User food glossary remove
	function removeFood($whereArray){
		$this->db->where($whereArray);
		$query = $this->db->delete('user_shoppingcart');
		return $query;
	}
}  