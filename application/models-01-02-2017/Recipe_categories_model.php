<?php

class Recipe_categories_model extends MY_Model {
	
	protected $primary_key = 'id'; // PRIMARY KEY

	/******************* GET ALL CATEGORIES ******************/
	public function get_categories()
    {	
        $this->db->select('*');
        $this->db->from('recipe_categories');
        $this->db->where('status',0);
        $categories = $this->db->get();
        return $categories->result();		    
    }

    /**************** USER DETAILS GET ****************/
    public function get_by_params($where = NULL){
    	$query = $this->db->get_where('recipe_categories',$where);
    	return $query->result();
    }

}  