<?php

class Master_recipeingredients_ar_model extends MY_Model {
	
	protected $primary_key = 'id'; // PRIMARY KEY

    public function get_by_params($where = NULL){
    	$query = $this->db->get_where('master_recipeingredients_ar_model',$where);
    	return $query->result();
    }

}  