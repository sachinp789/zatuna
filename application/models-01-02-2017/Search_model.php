<?php

class Search_model extends MY_Model {

	// Search
	function globalSearch($search, $lang = 'en') {
		$result = array();
		// English
		$ingredientsGlobalSearchResult = $this->recepingredients_GlobalSearch(trim($search));
		return $ingredientsGlobalSearchResult;
	}

	// Search by recipe,ingredient,category name
	function recepingredients_GlobalSearch($search) {
		$this->db->select('master_recipeingredients.id,master_recipeingredients.recipe_id,master_recipeingredients.ingredient_id, master_recipeingredients.qty,recipes.recipe_name,recipes.recipe_name_ar,recipes.recipe_name_fr,recipes.recipe_media,recipes.recipe_method,recipes.preparation_time,recipes.cooking_time,sum(master_recipeingredients.measure_amount) as total_calory,recipes.recipe_description,recipes.serving,recipe_categories.cat_name as recipe_category ,recipe_categories.cat_name_ar as recipe_category_ar,recipe_categories.cat_name_fr as recipe_category_fr');
		$this->db->from('recipes');
		$this->db->join('master_recipeingredients', 'master_recipeingredients.recipe_id = recipes.id', 'left');
		$this->db->join('ingredients', 'ingredients.id = master_recipeingredients.ingredient_id', 'left');
		$this->db->join('recipe_categories', 'recipe_categories.id = recipes.recipe_category_id', 'left');
		$this->db->join('ingredient_categories', 'ingredient_categories.id = ingredients.ingredient_category_id', 'left');
		//$this->db->like('tag_name' , $phrase, 'after');
		$this->db->or_like('recipes.recipe_name', $search);
		$this->db->or_like('ingredients.name', $search);
		$this->db->or_like('recipe_categories.cat_name', $search);
		$this->db->or_like('ingredient_categories.name', $search);
		//$this->db->group_by('master_recipeingredients.recipe_id');
		$this->db->group_by('recipes.recipe_name');
		$query = $this->db->get();
		return $query->result();
	}

	// Search by recipe,ingredient,category name
	function recepingredientsAR_GlobalSearch($search) {
		$this->db->select('master_recipeingredients_ar.id,master_recipeingredients_ar.recipe_fr_id,master_recipeingredients_ar.ingredient_ar_id, master_recipeingredients_ar.qty as nutrition, recipes_ar.recipe_name,recipes_ar_.recipe_media,recipes_ar.recipe_method,recipes_ar.preparation_time,recipes_ar.cooking_time,recipes_ar.recipe_description,recipes_ar.serving,ingredients_ar.name as ingredient_name,ingredients_ar.description as ing_description,ingredients_ar.media as ing_media, recipe_categories_ar.cat_name as recipe_category ,ingredient_categories_ar.name as ing_catename');
		$this->db->from('recipes_ar');
		$this->db->join('master_recipeingredients_ar', 'master_recipeingredients_ar.recipe_ar_id = recipes_ar.id', 'left');
		$this->db->join('ingredients_ar', 'ingredients_ar.id = master_recipeingredients_ar.ingredient_ar_id', 'left');
		$this->db->join('recipe_categories_ar', 'recipe_categories_ar.id = recipes_ar.recipe_category_ar_id', 'left');
		$this->db->join('ingredient_categories_ar', 'ingredient_categories_ar.id = ingredients_ar.ingredient_category_ar_id', 'left');
		//$this->db->like('tag_name' , $phrase, 'after');
		$this->db->or_like('recipes_ar.recipe_name', $search);
		$this->db->or_like('ingredients_ar.name', $search);
		$this->db->or_like('recipe_categories_ar.cat_name', $search);
		$this->db->or_like('ingredient_categories_ar.name', $search);
		//$this->db->group_by('master_recipeingredients.recipe_id');
		$query = $this->db->get();
		return $query->result_array();
	}

	// Search by recipe,ingredient,category name
	function recepingredientsFR_GlobalSearch($search) {
		$this->db->select('master_recipeingredients_fr.id,master_recipeingredients_fr.recipe_fr_id,master_recipeingredients_fr.ingredient_fr_id, master_recipeingredients_fr.qty as nutrition, recipes_fr.recipe_name,recipes_fr.recipe_media,recipes_fr.recipe_method,recipes_fr.preparation_time,recipes_fr.cooking_time,recipes_fr.recipe_description,recipes_fr.serving,ingredients_fr.name as ingredient_name,ingredients_fr.description as ing_description,ingredients_fr.media as ing_media, recipe_categories_fr.cat_name as recipe_category ,ingredient_categories_fr.name as ing_catename');
		$this->db->from('recipes_fr');
		$this->db->join('master_recipeingredients_fr', 'master_recipeingredients_fr.recipe_fr_id = recipes_fr.id', 'left');
		$this->db->join('ingredients_fr', 'ingredients_fr.id = master_recipeingredients_fr.ingredient_fr_id', 'left');
		$this->db->join('recipe_categories_fr', 'recipe_categories_fr.id = recipes_fr.recipe_category_fr_id', 'left');
		$this->db->join('ingredient_categories_fr', 'ingredient_categories_fr.id = ingredients_fr.ingredient_category_fr_id', 'left');
		//$this->db->like('tag_name' , $phrase, 'after');
		$this->db->or_like('recipes.recipe_name', $search);
		$this->db->or_like('ingredients.name', $search);
		$this->db->or_like('recipe_categories.cat_name', $search);
		$this->db->or_like('ingredient_categories.name', $search);
		//$this->db->group_by('master_recipeingredients.recipe_id');
		$query = $this->db->get();
		return $query->result_array();
	}

}