<?php

class Users_model extends MY_Model {
	
	protected $primary_key = 'id'; // PRIMARY KEY

	/******************* GET ALL USERS ******************/
	public function get_users()
    {		
    		$where = array();

    		$id = $this->input->get('user_id');
    		$firstname = $this->input->get('firstname');
    		$lastname = $this->input->get('lastname');
    		$email = $this->input->get('email');

		  	if ($id != '') $where['id'] = $id;
		  	if ($firstname != '') $where['firstname'] = $firstname;
		  	if ($lastname != '') $where['lastname'] = $lastname;
		  	if ($email != '') $where['email'] = $email;

    		if(!empty($where))
    		{
    			$query = $this->get_by_params($where);
            	return $query;

    		}else{	

	            $query = $this->db->get('users')->result();
	            return $query;
    		}
    }

    /**************** USER DETAILS GET ****************/
    public function get_by_params($where = NULL){
    	$query = $this->db->get_where('users',$where);
    	return $query->result();
    }

    /************ FB token check ****************/
    public function check_fb_token($where = NULL){
    	$query = $this->db->get_where('users',$where);
    	return $query->result();
    }

    /*********** Profile Update *****************/
    public function update_profile($user_id, $data)
    {
        $this->db->where('id', $user_id);
        if($this->db->update('users', $data)){
            return 1;
        }
        else{
            return 0;
        }
        //return $this->db->affected_rows();
    }

    // Check oldpassword of user
    public function checkOldPass($user_id,$old_password)
    {
        $this->db->where('id', $user_id);
        $this->db->where('password', $old_password);
        $query = $this->db->get('users');
        if($query->num_rows() > 0)
            return 1;
        else
            return 0;
    }

    // Update New password
    public function saveNewPass($user_id,$new_pass)
    {
        $this->db->where('id', $user_id);
        $this->db->update('users', $new_pass);
        return true;
    }

    // Check where condition
    public function getWhere($emailid){
        $this->db->where('email', $emailid);
        $this->db->from('users');
        $num_res = $this->db->count_all_results();
       // echo $this->db->last_query();exit;
        return $num_res;
    }

    // update password
    public function updatePassword($emailid,$data){
        $this->db->where('email', $emailid);
        $this->db->update('users', $data);
        return $this->db->affected_rows();
    }

    // Store reset password
    public function temp_reset_password($email,$temp_pass){
        $data =array(
        //'email'         =>$email,
        'reset_password'=>$temp_pass
        );
        if($data){
            $this->db->where('email', $email);
            $this->db->update('users', $data);  
            return 1;
        }else{
            return 0;
        }
    }

    // Check code valid or not
    public function is_temp_pass_valid($temp_pass){
        $this->db->where('reset_password', $temp_pass);
        $query = $this->db->get('users');
        if($query->num_rows() == 1){
            return 1;
        }
        else{
            return 0;
        }
    }
}  