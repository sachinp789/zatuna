<?php

class Cookbooks_fr_model extends MY_Model {
	
	protected $primary_key = 'id'; // PRIMARY KEY

	// Insert cookdetails
	function create($data){
		$this->db->insert_batch('cookbooks_fr', $data);
	}

	// User Cookbook List
	function userCookbookList($userID){
		$this->db->select('*');
		$this->db->where('user_id',$userID);
		$query = $this->db->get('cookbooks_fr');
		return $query->result();
	}

	// Cook details for update
	function updateCook($data,$bookID,$userID){
		$this->db->where('id', $bookID);
		$this->db->where('user_id', $userID);
		if($this->db->update('cookbooks_fr', $data)){
			return true;
		}
		else{
			return false;
		}
	}

	// Remove cookBook details
	function removeCookBook($cookID,$userID){
		$this->db->where('id', $cookID);
		$this->db->where('user_id', $userID);
		if($this->db->delete('cookbooks_fr')){
			return true;
		}
		else{
			return false;
		}
	}

	// Get cookbook of user
	function getCookBookUser($bookID,$userID){
		$this->db->select('cbook_image');
		$this->db->where('id', $bookID);
		$this->db->where('user_id', $userID);
		$query = $this->db->get('cookbooks_fr');
		return $query->row();
	}

}  