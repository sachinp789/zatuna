<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Measurements_model extends MY_Model {

    protected $primary_key = 'id';
    
    // Get All Recipes Categories	
    function getUnits($search){
    	return $this->db->select('id,metric_name,metric_value,us_metric,us_value')
        ->like('measurements.metric_name',$search)
        ->get('measurements')
        ->result();
    }
}
