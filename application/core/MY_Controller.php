<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

    public $data = array();

    /**
     * Constructor
     * 
     * @return void
     */
    function __construct() {
        parent::__construct();
        error_reporting(0);
        header('Access-Control-Allow-Origin: *'); 
        $this->data['currency'] = 'currency';

        if ($this->is_user_logged_in()) {
        }
        if($this->session->userdata('std_id'))
        {
         $notification = show_notification($this->session->userdata('std_id'));                            
         $this->session->set_userdata('notifications', $notification);
        }
        if ($this->session->userdata('last_activity')) {
            user_activity();
        }
    }

    /**
     * Check user logged in or not then redirect               
     */
    function login_check_and_redirect_user() {
        $is_user_logged_in = $this->is_user_logged_in();

        if ($is_user_logged_in) {
            redirect(base_url('user/dashboard'));
        } else {
            redirect(base_url('user/login'));
        }
    }

    /**
     * Redirect on login page if user not logged in
     */
    function redirect_if_user_not_logged_in() {
        $is_user_logged_in = $this->is_user_logged_in();

        if (!$is_user_logged_in) {
            redirect(base_url('user/login'));
        }
    }

    /**
     * Check for whether user logged in or not
     * @return boolean
     */
    function is_user_logged_in() {
        $is_user_logged_in = (bool) $this->session->userdata('is_logged_in');

        return $is_user_logged_in;
    }

    /**
     * Template
     * @param string $page_name
     * @param mixed $data
     */
    function __template($page_name, $data) {
        echo Modules::run('template/template/render', $page_name, $data);
    }
    
    function __templateFront($page_name, $data) {
        echo Modules::run('template/templateFront/render', $page_name, $data);
    }

    /**
     * Modal
     * @param string $page_name
     * @param mixed $data
     */
    function __modal($page_name, $data) {
        echo Modules::run('template/template/modal', $page_name, $data);
    }

    /**
     * Set the flash notifacation
     * @param string $message
     */
    function flash_notification($message) {
        $this->session->set_flashdata('flash_message', $message);
    }

    /**
     * Get the login user permission
     */

    /**
     * Email configuration and load library
     */
    function __config_and_load_email_library() {
        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 465,
            'smtp_user' => 'tejas.patel@searchnative.in',
            'smtp_pass' => 'patel@patel',
            'mailtype' => 'html',
            'charset' => 'iso-8859-1'
        );
        
        return $config;
    }

}
