<?php
defined('BASEPATH') or exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Recipe extends REST_Controller
{

    /**
     * Constructor
     *
     *  @return void
     */
    public function __construct()
    {
        parent::__construct();
        lang_switcher($_REQUEST['lang']);
        //echo $this->lang->line('msg');
        //exit;
        $this->load->model('Recipe_categories_model');
        $this->load->model('Recipes_model');
        $this->load->model('User_favourites_model');
        $this->load->model('Recipe_subcategories_model');
        $this->load->model('Ingredients_model');
        $this->load->model('Ratings_model');
        $this->load->model('Master_recipeingredients_model');
        $this->load->model('Master_recipeingredients_ar_model');
        $this->load->model('Master_recipeingredients_fr_model');
        $this->load->model('Measurements_model');
        $this->load->model('Units_model');
        $this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
    }

    // Upload file
    public function uploadFile_post($name, $id, $lang = null)
    {
        $config['upload_path'] = FCPATH . 'uploads/recipes';
        $fullpath = base_url() . 'uploads/recipes/';

        if ($name['name'] != '') {
            $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|zip|rar|jpeg|mp4|mkv|flv|wmv|3gp|webm';
            $this->load->library('upload');

            $file_ext = explode(".", $name['name']);
            $ext_file = strtolower(end($file_ext));
            $config['file_name'] = time() . date('dmYhis') . '.' . $ext_file;
            //now we initialize the upload library
            $this->upload->initialize($config);
            // we retrieve the number of files that were uploaded
            $data = array('recipe_media' => $fullpath . $config['file_name']);

            if ($lang == 'ar') {
                $this->db->where('id', $id);
                $this->db->update('recipes_ar', $data);
                //$this->Recipes_model->update_recipes_id1($id,$data);
            } else if ($lang == 'fr') {
                $this->db->where('id', $id);
                $this->db->update('recipes_fr', $data);
                //$this->Recipes_model->update_recipes_id1($id,$data);
            } else {
                $this->Recipes_model->update_recipes_id1($id, $data);
            }

            $this->upload->do_upload('recipe_media');
            /*$this->upload->do_upload('recipe_media');

        $this->response([
        'status'    => TRUE,
        'data'      => [],
        'message'    => 'Recipe is successfully added!',
        ], REST_Controller::HTTP_OK); // OK (200) Reponse code
         */
        }
    }

    public function do_upload_multiple_files_post($files, $id, $lang = null)
    {

        $config['upload_path'] = FCPATH . 'uploads/userrecipes';
        $fullpath = base_url() . 'uploads/userrecipes/';

        $response = array();

        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $this->load->library('upload');

        foreach ($files as $key => $value) {

            if (!empty($value['name'])) {

                $file_ext = explode(".", $value['name']);
                $name = $key;
                $ext_file = strtolower(end($file_ext));
                $config['file_name'] = rand(0, 10000000000000) . '.' . $ext_file;
                $response[$key] = $fullpath . $config['file_name'];

                $this->upload->initialize($config);

                /*if ($lang == 'ar') {
                $this->db->where('id', $id);
                $this->db->update('recipes_ar', $response);
                //$this->Recipes_model->update_recipes_id1($id,$data);
                } else if ($lang == 'fr') {
                $this->db->where('id', $id);
                $this->db->update('recipes_fr', $response);
                //$this->Recipes_model->update_recipes_id1($id,$data);
                 */
                $this->Recipes_model->update_recipes_id1($id, $response);
                /*}*/

                $this->upload->do_upload($name);
            }
        }
        return $response;
    }

    // Get all recipies categories
    public function recipecategory_get()
    {

        $catlists = $this->Recipe_categories_model->get_categories();

        if (!empty($catlists)) {
            $this->response([
                'status' => true,
                'data' => $catlists,
                'message' => count($catlists) . ' ' . $this->lang->line('msg_found'),
            ], REST_Controller::HTTP_OK); // OK (200) Reponse code
        } else {
            $this->response([
                'status' => false,
                'data' => $catlists,
                'message' => $this->lang->line('msg_notfound'),
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }

    }

    // Get all ingredients categories

    public function ingredientscat_get()
    {

        $tag = $this->get('tag');
        $catlists = $this->Ingredients_model->get_ingredients_cats($tag);

        if (!empty($catlists)) {
            $this->response([
                'status' => true,
                'data' => $catlists,
                'message' => count($catlists) . ' ' . $this->lang->line('msg_found'),
            ], REST_Controller::HTTP_OK); // OK (200) Reponse code
        } else {
            $this->response([
                'status' => false,
                'data' => $catlists,
                'message' => $this->lang->line('msg_notfound'),
            ], REST_Controller::HTTP_OK); // OK (200) Reponse code
        }
    }

    public function recipeslist_get()
    {

        $filter = array();

        $filter['userID'] = $this->get('userid');
        $filter['category'] = $this->get('category');
        $filter['mincalory'] = $this->get('min');
        $filter['maxcolory'] = $this->get('max');
        $filter['pageNo'] = $this->get('page');
        $filter['perPage'] = 10;
        $recipies = $this->Recipes_model->get_recipes_data($filter);
        //print_r($recipies);
        //exit;
        $likedorbookmarkrecipes = $this->Recipes_model->getUserLikeORBookmarkRecipes($filter['userID']);
        $totalLike = $this->User_favourites_model->countRecipeCount(); // Like Count
        //$recipemedia     = $this->Recipes_model->get_recipes_media($userID);
        $ratings = $this->Ratings_model->getRating();
        $usedingred = $this->Ingredients_model->get_usedIngridents($filter['userID']);
        $stepsRecipes = $this->Recipes_model->get_recipesStep();

        foreach ($recipies as $recp) {
            // $i=0;
            /*$ingredients = array();
            $steps = array();
            $likesrecipes = array();
            $bookmarks = array();*/
            /*$rating = array();
            $recp->ingredients = array();
            $recp->steps = array();
             */
            $recp->is_like = 0;
            $recp->userlikecount = 0;
            /*$recp->avg_rating = 0;
            $recp->rating_count = 0;*/

            /*for ($k = 0; $k < count($usedingred); $k++) {
            if ($usedingred[$k]->recipe_id == $recp->recipe_id) {
            $ingredients[] = $usedingred[$k];
            }

            }
            $recp->ingredients = $ingredients;

            for ($k = 0; $k < count($ratings); $k++) {
            if ($ratings[$k]->recipe_id == $recp->recipe_id) {
            $recp->avg_rating = $ratings[$k]->avgRating;
            $recp->rating_count = $ratings[$k]->totalRating;
            }

             */

            /*for ($k = 0; $k < count($stepsRecipes); $k++) {
            if (isset($stepsRecipes[$k]->image)) {
            $stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
            }
            //$stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
            if ($stepsRecipes[$k]->recipe_id == $recp->recipe_id) {
            $steps[] = $stepsRecipes[$k];
            unset($stepsRecipes[$k]->image);
            }

            }
             */
/*
for ($j = 0; $j < count($likedorbookmarkrecipes['bookmarks']); $j++) {
//echo $usedIng[$k]->recipe_id;
if ($likedorbookmarkrecipes['bookmarks'][$j]->recipe_id == $recp->recipe_id) {
$recp->is_bookmark = $likedorbookmarkrecipes['bookmarks'][$j]->is_bookmark;

}

}*/

            for ($i = 0; $i < count($likedorbookmarkrecipes['likes']); $i++) {
                //echo $usedIng[$k]->recipe_id;
                if ($likedorbookmarkrecipes['likes'][$i]->recipe_id == $recp->recipe_id) {
                    $recp->is_like = $likedorbookmarkrecipes['likes'][$i]->is_like;
                }
            }
            foreach ($totalLike as $likerecipe) {
                if ($likerecipe->recipe_id == $recp->recipe_id)
                //array_push($recipecount, $likerecipe->totalcount);
                {
                    $recp->userlikecount = $likerecipe->totalcount;
                }

            }
        }

        $total_rows = $this->Recipes_model->filterRecipeNumRows($filter); // Count total number of rows
        $maxcalory = $this->Recipes_model->maxiMumCalory(); // Maximum Calories Get

        if (!empty($recipies)) {
            $this->response([
                'status' => true,
                'data' => $recipies,
                //'message'    => count($result) . ' data found!',
                'total_records_page' => $total_rows,
                'max_calory' => $maxcalory,
                //'per_page' => $perPage,
                //'data' => $recipies,
                'message' => $this->lang->line('msg_found'),
            ], REST_Controller::HTTP_OK); // OK (200) Reponse code
        } else {
            $this->response([
                'status' => false,
                //'data'            => $recipies,
                'message' => $this->lang->line('msg_notfound'),
            ], REST_Controller::HTTP_OK); // OK (200) Reponse code
        }
    }

    // Previous last seen recipes
    public function getRecipeDetails_get()
    {

        $recipeID = $this->get('recipe_id');
        $userID = $this->get('user_id');
        $recipies = $this->Recipes_model->get_previouesrecipes_data($recipeID);
        $likedorbookmarkrecipes = $this->Recipes_model->getUserLikeORBookmarkRecipesByID($recipeID, $userID);
        //$recipemedia     = $this->Recipes_model->get_recipes_media($recipeID);
        $totalLike = $this->User_favourites_model->countRecipeCount();
        $usedingred = $this->Ingredients_model->get_previous_usedIngridents($recipeID);
        $ratings = $this->Ratings_model->getRating();
        $stepsRecipes = $this->Recipes_model->get_recipesStep();

        foreach ($recipies as $recp) {
            // $i=0;
            $ingredients = array();
            $steps = array();
            $likesrecipes = array();
            $bookmarks = array();
            $rating = array();
            $recp->ingredients = array();
            $recp->steps = array();
            $recp->is_like = 0;
            $recp->is_bookmark = 0;
            $recp->userlikecount = 0;
            $recp->avg_rating = 0;
            $recp->rating_count = 0;

            for ($k = 0; $k < count($usedingred); $k++) {
                if (isset($usedingred[$k]->media)) {
                    $usedingred[$k]->path = base_url() . $usedingred[$k]->media;
                }
                if ($usedingred[$k]->recipe_id == $recp->recipe_id) {
                    $ingredients[] = $usedingred[$k];
                    unset($usedingred[$k]->media);
                }

            }
            $recp->ingredients = $ingredients;

            for ($k = 0; $k < count($ratings); $k++) {
                if ($ratings[$k]->recipe_id == $recp->recipe_id) {
                    $recp->avg_rating = $ratings[$k]->avgRating;
                    $recp->rating_count = $ratings[$k]->totalRating;
                }

            }

            for ($k = 0; $k < count($stepsRecipes); $k++) {
                if (isset($stepsRecipes[$k]->image)) {
                    $stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
                }
                //$stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
                if ($stepsRecipes[$k]->recipe_id == $recp->recipe_id) {
                    $steps[] = $stepsRecipes[$k];
                    unset($stepsRecipes[$k]->image);
                }

            }
            $recp->steps = $steps;

            for ($j = 0; $j < count($likedorbookmarkrecipes['bookmarks']); $j++) {
                //echo $usedIng[$k]->recipe_id;
                if ($likedorbookmarkrecipes['bookmarks'][$j]->recipe_id == $recp->recipe_id) {
                    $recp->is_bookmark = $likedorbookmarkrecipes['bookmarks'][$j]->is_bookmark;

                }

            }

            for ($i = 0; $i < count($likedorbookmarkrecipes['likes']); $i++) {
                //echo $usedIng[$k]->recipe_id;
                if ($likedorbookmarkrecipes['likes'][$i]->recipe_id == $recp->recipe_id) {
                    $recp->is_like = $likedorbookmarkrecipes['likes'][$i]->is_like;
                }
            }

            foreach ($totalLike as $likerecipe) {
                if ($likerecipe->recipe_id == $recp->recipe_id)
                //array_push($recipecount, $likerecipe->totalcount);
                {
                    $recp->userlikecount = $likerecipe->totalcount;
                }

            }
        }

        if (!empty($recipies)) {
            $this->response([
                'status' => true,
                'data' => $recipies,
                'message' => $this->lang->line('msg_found'),
            ], REST_Controller::HTTP_OK); // OK (200) Reponse code
        } else {
            $this->response([
                'status' => false,
                //'data'            => $recipies,
                'message' => $this->lang->line('msg_notfound'),
            ], REST_Controller::HTTP_OK); // OK (200) Reponse code
        }

    }

    // Stored recipies data
    public function saveRecipe_post($lang = 'en')
    {

        //return $this->do_upload_multiple_files_post($_FILES,4,$this->post('lang'));
        //exit;
        $recipe_id = null;
        /*$msg = '';
        if ($this->post('lang') == 'ar') {
        echo "arabic";exit;
        $recipe_id = $this->addReciepiAR_post();
        $msg = 'يضاف صفة بنجاح!';
        } else if ($this->post('lang') == 'fr') {
        echo "french";exit;
        $recipe_id = $this->addReciepiFR_post();
        $msg = 'La recette est ajoutée avec succès!';
        } else {
        $recipe_id = $this->addReciepiEN_post();
        $msg = 'Recipe is successfully added!';
         */

        $recipe_id = $this->addRecipe_post();

        if (!empty($recipe_id)) {

            $this->do_upload_multiple_files_post($_FILES, $recipe_id, $this->post('lang'));
            //$uploadImage = $this->uploadFile_post($_FILES['recipe_media'],$recipe_id,$this->post('lang'));

            $this->response([
                'status' => true,
                'data' => [],
                'message' => $this->lang->line('msg_found'),
            ], REST_Controller::HTTP_OK); // OK (200) Reponse code
        } else {
            $this->response([
                'status' => false,
                'data' => [],
                'message' => $this->lang->line('msg_notfound'),
            ], REST_Controller::HTTP_OK); // OK (200) Reponse code
        }
        //$uploadImage = $this->uploadFile_post($_FILES['recipe_media'],$recipe_id,$this->post('lang'));
    }

    /***************************** Add Recipes ****************/

    public function addRecipe_post()
    {
        $ingarray = array();
        $steps = array();

        if ($_POST) {

            $recipesData = array(
                'recipe_name' => $this->post('recipe_name'),
                'recipe_name_ar' => $this->post('recipe_name_ar'),
                'recipe_name_fr' => $this->post('recipe_name_fr'),
                'recipe_description' => $this->post('recipe_description'),
                'recipe_description_ar' => $this->post('recipe_description_ar'),
                'recipe_description_fr' => $this->post('recipe_description_fr'),
                //'recipe_method' => $this->post('recipe_method'),
                'preparation_time' => $this->post('preparation_time'),
                'cooking_time' => $this->post('cooking_time'),
                'serving' => $this->post('serving'),
                'recipe_category_id' => $this->post('recipe_category'),
                'recipe_subcategory_id' => $this->post('recipe_subcategory'),
                'user_id' => $this->post('user_id'),
            ); // Recipe added in table

            $insertId = $this->Recipes_model->insert($recipesData); // Recipes data stored

            /***************************** Steps Data *************************/
            //$steps = explode(";", $this->post('recipe_steps'));
            /*$arr = [
            [
            "stepno" => "1",
            "desc" => "valore2",
            "desc_ar" => "ar_valore2",
            "desc_fr" => "fr_valore2",
            "image_video" => "/home/sachin/Downloads/Malai-Kofta.jpg",
            "image_video_ar" => "/home/sachin/Downloads/slider3.jpg",
            "image_video_fr" => "fr_valore2",
            ],
            [
            "stepno" => "2",
            "desc" => "valore2",
            "desc_ar" => "ar_valore2",
            "desc_fr" => "fr_valore2",
            "image_video" => "/home/sachin/Downloads/Malai-Kofta.jpg",
            "image_video_ar" => "/home/sachin/Downloads/slider3.jpg",
            "image_video_fr" => "fr_valore2",
            ],

            ];

            $abc = json_encode(array('recipes_steps' => $arr)); */

            $stepsdata = json_decode($this->post('recipes_steps')); // Change to post variable

            for ($i = 0; $i < count($stepsdata); $i++) {

                $steps['step_no'] = $stepsdata[$i]->stepno;
                $steps['recipe_id'] = $insertId;
                $steps['step_description'] = $stepsdata[$i]->desc;
                $steps['step_description_ar'] = $stepsdata[$i]->desc_ar;
                $steps['step_description_fr'] = $stepsdata[$i]->desc_fr;

                if (!empty($stepsdata[$i]->image_video)) {

                    $filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($stepsdata[$i]->image_video)));
                    $type = explode(".", $filename);

                    if (in_array($type[1], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
                        $img_destitation_path = getcwd() . "/uploads/recipesteps/" . time() . '-' . $filename;
                        if (!copy($stepsdata[$i]->image_video, $img_destitation_path)) {
                            $steps['image'] = '';
                        } else {
                            $steps['image'] = 'uploads/recipesteps/' . time() . '-' . $filename;
                        }
                    } else {
                        $steps['image'] = '';
                    }
                    //echo "Full";
                } else {
                    $steps['image'] = '';
                }

                if (!empty($stepsdata[$i]->image_video_ar)) {

                    $filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($stepsdata[$i]->image_video_ar)));
                    $type = explode(".", $filename);

                    if (in_array($type[1], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
                        $img_destitation_path = getcwd() . "/uploads/recipesteps/" . time() . '-' . $filename;
                        if (!copy($stepsdata[$i]->image_video_ar, $img_destitation_path)) {
                            $steps['image_ar'] = '';
                        } else {
                            $steps['image_ar'] = 'uploads/recipesteps/' . time() . '-' . $filename;
                        }
                    } else {
                        $steps['image_ar'] = '';
                    }
                    //echo "Full";
                } else {
                    $steps['image_ar'] = '';
                }

                if (!empty($stepsdata[$i]->image_video_fr)) {

                    $filename = preg_replace('/[^A-Za-z0-9.]/', "", trim(basename($stepsdata[$i]->image_video_fr)));
                    $type = explode(".", $filename);

                    if (in_array($type[1], array('jpg', 'gif', 'png', 'jpeg', 'mp4'))) {
                        $img_destitation_path = getcwd() . "/uploads/recipesteps/" . time() . '-' . $filename;
                        if (!copy($stepsdata[$i]->image_video_fr, $img_destitation_path)) {
                            $steps['image_fr'] = '';
                        } else {
                            $steps['image_fr'] = 'uploads/recipesteps/' . time() . '-' . $filename;
                        }
                    } else {
                        $steps['image_fr'] = '';
                    }
                    //echo "Full";
                } else {
                    $steps['image_fr'] = '';
                }

                $this->db->insert('recipe_steps', $steps);
            }
            /******************************* END *******************************/

            /*************************** Ingredients details **********************/
            $ingrdients = json_decode($this->post('ingredients')); // Convert in array
            for ($i = 0; $i < count($ingrdients); $i++) {

                if (is_numeric($ingrdients[$i]->ingredient_name)) {
                    $ingarray['recipe_id'] = $insertId;

                    if (!is_numeric($ingrdients[$i]->measurement_id)) {
                        $unit_id = $this->Units_model->insert(array(
                            'unit_name' => $ingrdients[$i]->measurement_id)
                        );
                        $ingrdients[$i]->measurement_id = $unit_id;
                    }

                    $ingarray['ingredient_id'] = $ingrdients[$i]->ingredient_name;
                    $ingarray['qty'] = $ingrdients[$i]->qty;
                    $ingarray['finalunit_id'] = $ingrdients[$i]->unit_id;
                    $caldata = $this->calculateCalory($ingrdients[$i]->ingredient_name, $ingrdients[$i]->qty, $ingrdients[$i]->measurement_id);

                    $ingarray['measure_amount'] = $caldata['total_calory'];
                    $ingarray['protein_amount'] = $caldata['total_protein'];
                    $ingarray['fat_amount'] = $caldata['total_fat'];
                    $ingarray['carbs_amount'] = $caldata['total_carbs'];
                    $ingarray['measure_amount_ounce'] = $caldata['total_calory_ounce'];
                    $ingarray['protein_amount_ounce'] = $caldata['total_protein_ounce'];
                    $ingarray['fat_amount_ounce'] = $caldata['total_fat_ounce'];
                    $ingarray['carbs_amount_ounce'] = $caldata['total_carbs_ounce'];

                    $this->Master_recipeingredients_model->insert($ingarray); // Recipes and ingredient master data stored.
                } else {

                    $ing_id[] = $this->Ingredients_model->insert(array(
                        'name' => $ingrdients[$i]->ingredient_name)
                    ); // Ingredient name stored in table

                    if (!is_numeric($ingrdients[$i]->measurement_id)) {
                        $unit_id = $this->Units_model->insert(array(
                            'unit_name' => $ingrdients[$i]->measurement_id)
                        );
                        $ingrdients[$i]->measurement_id = $unit_id;
                    }

                    $ingarray['recipe_id'] = $insertId;
                    $ingarray['ingredient_id'] = $ing_id[$i];
                    $ingarray['finalunit_id'] = $unit_id[$i];
                    $ingarray['qty'] = $ingrdients[$i]->qty;
                    $caldata = $this->calculateCalory($ing_id[$i], $ingrdients[$i]->qty, $ingrdients[$i]->measurement_id);

                    $ingarray['measure_amount'] = $caldata['total_calory'];
                    $ingarray['protein_amount'] = $caldata['total_protein'];
                    $ingarray['fat_amount'] = $caldata['total_fat'];
                    $ingarray['carbs_amount'] = $caldata['total_carbs'];
                    $ingarray['measure_amount_ounce'] = $caldata['total_calory_ounce'];
                    $ingarray['protein_amount_ounce'] = $caldata['total_protein_ounce'];
                    $ingarray['fat_amount_ounce'] = $caldata['total_fat_ounce'];
                    $ingarray['carbs_amount_ounce'] = $caldata['total_carbs_ounce'];

                    $this->Master_recipeingredients_model->insert($ingarray); // Recipes and ingredient master data stored.
                }
            }
            /********************************* END **************************/

            /************************* Total calory stored **********/
            $totalcalory = $this->db->query("SELECT SUM(measure_amount) AS maxid FROM master_recipeingredients WHERE recipe_id = {$insertId}")->row()->maxid;

            $totalcalory_ounce = $this->db->query("SELECT SUM(measure_amount_ounce) AS maxid FROM master_recipeingredients WHERE recipe_id = {$insertId}")->row()->maxid;

            $caldatafinal = array('total_calory' => round($totalcalory, 2), 'total_calory_ounce' => round($totalcalory_ounce, 2));

            $this->db->where('id', $insertId);
            $this->db->update('recipes', $caldatafinal);
            /******************************** END *********************/
        }
        return $insertId;
    }
    /******************************* END **********************/

    /**************************** NEW ***********************/
    public function addReciepiEN_post()
    {
        $insertId = null;

        $ingarray = array();

        $recpdata = [
            'recipe_name' => $this->post('recipe_name'),
            'recipe_description' => $this->post('recipe_description'),
            'recipe_method' => $this->post('recipe_method'),
            'preparation_time' => $this->post('preparation_time'),
            'cooking_time' => $this->post('cooking_time'),
            'serving' => $this->post('serving'),
            'recipe_category_id' => $this->post('recipe_category'),
            'recipe_subcategory_id' => $this->post('recipe_subcategory'),
            'user_id' => $this->post('user_id'),
        ];

        $insertId = $this->Recipes_model->insert($recpdata); // Recipes data stored

        $ingrdients = json_decode($this->post('ingredients')); // Convert in array

        for ($i = 0; $i < count($ingrdients); $i++) {

            if (is_numeric($ingrdients[$i]->ingredient_name)) {
                $ingarray['ingredient_id'] = $ingrdients[$i]->ingredient_name;
                $ingarray['qty'] = $ingrdients[$i]->qty;
                $ingarray['measure_type'] = $ingrdients[$i]->measure_type;
                $ingarray['measure_amount'] = $this->calculateMetric($ingrdients[$i]->measurement_name, $ingrdients[$i]->qty);
                $ingarray['measurement_id'] = $ingrdients[$i]->measurement_id;
                $ingarray['recipe_id'] = $insertId;

                $this->Master_recipeingredients_model->insert($ingarray); // Recipes and ingredient master data stored.
            } else {

                $ing_id[] = $this->Ingredients_model->insert(array(
                    'name' => $ingrdients[$i]->ingredient_name)
                ); // Ingredient name stored in table

                $ingarray['ingredient_id'] = $ing_id[$i];
                $ingarray['qty'] = $ingrdients[$i]->qty;
                $ingarray['measure_type'] = $ingrdients[$i]->measure_type;
                $ingarray['measure_amount'] = $this->calculateMetric($ingrdients[$i]->measurement_name, $ingrdients[$i]->qty);
                $ingarray['measurement_id'] = $ingrdients[$i]->measurement_id;
                $ingarray['recipe_id'] = $insertId;

                $this->Master_recipeingredients_model->insert($ingarray); // Recipes and ingredient master data stored.
            }

        }
/*
if(is_numeric($this->post('ingredient_name'))) {

$recpdata = [
'recipe_name'             => $this->post('recipe_name'),
'recipe_description'     => $this->post('recipe_description'),
'recipe_method'         => $this->post('recipe_method'),
'preparation_time'         => $this->post('preparation_time'),
'cooking_time'             => $this->post('cooking_time'),
'serving'                 => $this->post('serving'),
'recipe_category_id'     => $this->post('recipe_category'),
'recipe_subcategory_id' => $this->post('recipe_subcategory'),
'user_id'                 => $this->post('user_id')
];

$insertId    = $this->Recipes_model->insert($recpdata); //

$masterdata = [
'recipe_id'                => $insertId,
];

for($i=0;$i<count($_POST['ingredient_name']);$i++){
//$total = $this->calculateMetric($this->post('measurement_name'), $this->post('qty')[$i]);
$masterdata['ingredient_id'] = $this->post('ingredient_name')[$i];
// echo $this->post('qty');exit;
$masterdata['qty'] = $this->post('qty')[$i];
$masterdata['measurement_id'] = $this->post('measurement_id')[$i];
$masterdata['measure_amount'] = $this->calculateMetric($this->post('measurement_name'), $this->post('qty')[$i]);
$masterdata['measure_type'] = $this->post('measure_type');
$this->Master_recipeingredients_model->insert($masterdata);
}
//$this->Master_recipeingredients_model->insert($masterdata);    // MasterID
}
else{

// Ingredients details stored
for($i=0;$i<count($this->post('ingredient_name'));$i++)
{
$ing_id[] = $this->Ingredients_model->insert(array(
'name' => $this->post('ingredient_name')[$i])
);
}
// Recipies details stored
$recpdata = [
'recipe_name'             => $this->post('recipe_name'),
'recipe_description'     => $this->post('recipe_description'),
'recipe_method'         => $this->post('recipe_method'),
'preparation_time'         => $this->post('preparation_time'),
'cooking_time'             => $this->post('cooking_time'),
'serving'                 => $this->post('serving'),
'recipe_category_id'     => $this->post('recipe_category'),
'recipe_subcategory_id' => $this->post('recipe_subcategory'),
'user_id'                 => $this->post('user_id')
];

$insertId    = $this->Recipes_model->insert($recpdata); //

// Master tables details
$recpingdata = [
'recipe_id'                => $insertId
];

for($i=0;$i<count($this->post('ingredient_name'));$i++){
$recpingdata['ingredient_id'] = $ing_id[$i];
// echo $this->post('qty');exit;
$recpingdata['qty'] = $this->post('qty')[$i];
$recpingdata['measurement_id'] = $this->post('measurement_id')[$i];
$recpingdata['measure_amount'] = $this->calculateMetric($this->post('measurement_name'), $this->post('qty')[$i]);
$recpingdata['measure_type'] = $this->post('measure_type');
$this->Master_recipeingredients_model->insert($recpingdata);
}
//$this->Master_recipeingredients_model->insert($recpingdata);    // MasterID
}
 */
        //print_r($recpingdata);exit;

        //echo "id :- ".$insertId;exit;
        return $insertId;
    }
    /***************************** END ************************/

    public function getMeasurements_get()
    {
        $tag = $this->get('tag');
        $units = $this->Measurements_model->getUnits($tag);

        if (!empty($units)) {
            $this->response([
                'status' => true,
                'data' => $units,
                'message' => count($units) . ' ' . $this->lang->line('msg_found'),
            ], REST_Controller::HTTP_OK); // OK (200) Reponse code
        } else {
            $this->response([
                'status' => false,
                'data' => [],
                'message' => $this->lang->line('msg_notfound'),
            ], REST_Controller::HTTP_OK); // OK (200) Reponse code
        }
    }
    /**
     * Calculates the calory.
     *
     * @param      string          $param   The parameter
     * @param      integer|string  $param2  The parameter 2
     *
     * @return     integer|string  The calory.
     */
    public function calculateCalory($ingredient = '', $qty = '', $unit = '')
    {
        $total_value = 0;

        $protinecal = 0;
        $carbscal = 0;
        $fat = 0;
        $kcal = 0;

        $calorydata = $this->Ingredients_model->checkIngredient($ingredient);

        if (count($calorydata) > 0) {
            $unitdata = $this->Units_model->getUnitsData($unit);

            $nutrientGrams = $qty * $unitdata->unit_gram;

            $protinecal = ($calorydata->protein * $nutrientGrams) / 100.0;
            $protinecal_ounce = ($protinecal) / 28.35; // Ounce calculation

            $carbscal = ($calorydata->carbs * $nutrientGrams) / 100.0;
            $carbscal_ounce = ($carbscal) / 28.35;

            $fat = ($calorydata->fat * $nutrientGrams) / 100.0;
            $fat_ounce = ($fat) / 28.35;

            $kcal = ($calorydata->kcal_grams * $nutrientGrams) / 100.0;
            $kcal_ounce = ($kcal) / 28.35;

        } else {

            $unitdata = $this->Units_model->getUnitsData($unit);

            $nutrientGrams = $qty * $unitdata->unit_gram;

            $protinecal = ($calorydata->protein * $nutrientGrams) / 100.0;
            $protinecal_ounce = ($protinecal) / 28.35;

            $carbscal = ($calorydata->carbs * $nutrientGrams) / 100.0;
            $carbscal_ounce = ($carbscal) / 28.35;

            $fat = ($calorydata->fat * $nutrientGrams) / 100.0;
            $fat_ounce = ($fat) / 28.35;

            $kcal = ($calorydata->kcal_grams * $nutrientGrams) / 100.0;
            $kcal_ounce = ($kcal) / 28.35;
        }

        $calories = array(
            'total_calory' => $kcal,
            'total_protein' => $protinecal,
            'total_fat' => $fat,
            'total_carbs' => $carbscal,
            'total_calory_ounce' => $kcal_ounce,
            'total_protein_ounce' => $protinecal_ounce,
            'total_fat_ounce' => $fat_ounce,
            'total_carbs_ounce' => $carbscal_ounce,
        );

        return $calories;
    }

    /*************** Calculate total value of food *********/
    public function calculateMetric($param = '', $param2 = '')
    {
        $total_value = 0;
        if ($param == 'gram') {
            $total_value = $param2 * 0.0353;
        } else if ($param == 'kilogram') {
            $total_value = $param2 * 2.2046;
        } else if ($param == 'litre') {
            $total_value = $param2 * 1.0566;
        } else if ($param == 'ml') {
            $total_value = $param2 * 0.0042;
        } else if ($param == 'ounce') {
            $total_value = $param2 * 28.35;
        } else if ($param == 'pounds') {
            $total_value = $param2 * 0.4536;
        } else if ($param == 'quart') {
            $total_value = $param2 * 0.9463;
        } else if ($param == 'cup') {
            $total_value = $param2 * 236.59;
        }

        return $total_value;
    }

    // Get Recipes sub category
    public function recipesSubcategory_get()
    {
        $recipe_cat_id = $this->get('recipe_cat_id');

        $sublists = $this->Recipe_subcategories_model->get_subcategories_by_id($recipe_cat_id);

        if (!empty($sublists)) {
            $this->response([
                'status' => true,
                'data' => $sublists,
                'message' => count($sublists) . ' ' . $this->lang->line('msg_found'),
            ], REST_Controller::HTTP_OK); // OK (200) Reponse code
        } else {
            $this->response([
                'status' => false,
                'data' => [],
                'message' => $this->lang->line('msg_notfound'),
            ], REST_Controller::HTTP_OK); // OK (200) Reponse code
        }
    }

}
