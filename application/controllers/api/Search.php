<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Search extends REST_Controller {

	function __construct() {
		parent::__construct();
		lang_switcher($_REQUEST['lang']);
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
		$this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
		$this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

		//load model
		$this->load->model('Search_model');
		$this->load->model('Ingredients_model');
		$this->load->model('User_favourites_model');
		$this->load->model('Recipes_model');
		$this->load->model('Ratings_model');
	}

	function globalSearch_post() {
		$search = $this->post('search');
		$lang = $this->post('lang');
		$userID = $this->post('user_id');

		if (!empty(trim($search))) {
			$likedorbookmarkrecipes = $this->Recipes_model->getUserLikeORBookmarkRecipes($userID); // Like and Bookmark

			//print_r($likedorbookmarkrecipes);exit;

			$result = $this->Search_model->globalSearch($search, $lang);
			$usedingred = $this->Ingredients_model->get_usedIngridents();
			$totalLike = $this->User_favourites_model->countRecipeCount(); // Like Count
			$stepsRecipes = $this->Recipes_model->get_recipesStep();
			$ratings = $this->Ratings_model->getRating();

			foreach ($result as $recp) {
				// $i=0;
				$ingredients = array();
				$steps = array();
				$rating = array();
				/*$recp->ingredients = array();
						$recp->steps = array();
						$recp->userlikecount = 0;
					*/
				$recp->avg_rating = 0;
				$recp->rating_count = 0;

				/*foreach ($totalLike as $likerecipe) {
							if ($likerecipe->recipe_id == $recp->recipe_id)
							//array_push($recipecount, $likerecipe->totalcount);
							{
								$recp->userlikecount = $likerecipe->totalcount;
							}

						}

						for ($k = 0; $k < count($usedingred); $k++) {
							//print_r($recp);
							if ($usedingred[$k]->recipe_id == $recp->recipe_id) {
								$ingredients[] = $usedingred[$k];
							}
						}
					*/

				for ($k = 0; $k < count($ratings); $k++) {
					if ($ratings[$k]->recipe_id == $recp->recipe_id) {
						$recp->avg_rating = $ratings[$k]->avgRating;
						$recp->rating_count = $ratings[$k]->totalRating;
					}

				}

				/*for ($k = 0; $k < count($stepsRecipes); $k++) {
							if (isset($stepsRecipes[$k]->image)) {
								$stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
							}
							//$stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
							if ($stepsRecipes[$k]->recipe_id == $recp->recipe_id) {
								$steps[] = $stepsRecipes[$k];
								unset($stepsRecipes[$k]->image);
							}

						}
						$recp->steps = $steps;

						for ($i = 0; $i < count($likedorbookmarkrecipes['likes']); $i++) {
							//echo $usedIng[$k]->recipe_id;
							if ($likedorbookmarkrecipes['likes'][$i]->recipe_id == $recp->recipe_id) {
								$recp->is_like = $likedorbookmarkrecipes['likes'][$i]->is_like;
							}
					*/
			}

			if (!empty($result)) {
				$this->response([
					'status' => TRUE,
					'data' => $result,
					'message' => count($result) . ' ' . $this->lang->line('msg_rec_found'),
				], REST_Controller::HTTP_OK);
			} else {
				$this->response([
					'status' => FALSE,
					'data' => array(),
					'message' => $this->lang->line('msg_notfound'),
				], REST_Controller::HTTP_OK);
			}
		} else {
			$this->response([
				'status' => FALSE,
				'data' => array(),
				'message' => $this->lang->line('msg_notfound'),
			], REST_Controller::HTTP_OK);
		}

	}

	function globalSearch_get() {
		$search = $this->get('search');
		$lang = $this->get('lang');

		//echo $lang;exit;

		$result = $this->Search_model->globalSearch($search, $lang);
		$usedingred = $this->Ingredients_model->get_usedIngridents($search);

		foreach ($result as $recp) {
			// $i=0;
			$ingredients = array();
			$recp->ingredients = array();

			for ($k = 0; $k < count($usedingred); $k++) {
				//print_r($recp);
				if ($usedingred[$k]->recipe_id == $recp->recipe_id) {
					$ingredients[] = $usedingred[$k];
				}
			}
			$recp->ingredients = $ingredients;
		}
		//exit;
		if (!empty($result)) {
			$this->response([
				'status' => TRUE,
				'data' => $result,
				'message' => count($result) . ' ' . $this->lang->line('msg_rec_found'),
			], REST_Controller::HTTP_OK);
		} else {
			$this->response([
				'status' => TRUE,
				'message' => $this->lang->line('msg_notfound'),
				'data' => array(),
			], REST_Controller::HTTP_OK);
		}
	}

}