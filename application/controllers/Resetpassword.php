<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Resetpassword extends MY_Controller {

	function index()
    {
       //die("fdf");
    }

    function resetCode($code=NULL){
        //echo $code;exit;
        if ($_POST) {
            if(!empty($_POST['_token'])){
                if ($code == $_POST['_token']) {
                    //update password and reset token and redirect
                    if (trim($_POST['password']) == trim($_POST['cpassword'])) {
                        $this->db->update('users', 
                            [
                                'password' => hash('md5', $_POST['password'] . config_item('encryption_key')), 
                                'reset_password' => ''
                            ], 
                            ['reset_password' => $_POST['_token']]);
                        $this->session->set_flashdata('success','Password has been successfully updated.');
                       redirect(base_url() . 'resetpassword/resetCode');
                    } else {
                        $this->session->set_flashdata("error", "Password is mismatched.");
                        redirect(base_url() . 'resetpassword/resetCode/' . $code);
                    }
                }
            }
            else{
                $this->session->set_flashdata("error", "Password token expired.");
                redirect(base_url() . 'resetpassword/resetCode/');
            }
        }
        
        //check entry for token
        $user = $this->db->select()->from('users')
            ->where('reset_password',$code)->get()->result();
       // echo count($user);exit;    
        if (count($user) > 0) {// die("ss");
            $this->data['token'] = $code;
            //$this->__templateFront('home/reset_password', $data);
            $this->data['title']='Reset Password';
            $this->load->view('reset_password',$this->data);
            //$this->load->view('admin/user/reset_password', $this->data);
        } else {
            $this->data['token'] = $code;
            //$this->__templateFront('home/reset_password', $data);
            $this->data['title']='Reset Password';
            $this->data['key'] = 'expired';
            $this->session->set_flashdata("error", "Link is Expired");
            //redirect(base_url() . 'resetpassword/resetCode/' . $code);
            //redirect(base_url() . 'resetpassword/resetCode/' . $code);
            redirect(base_url() . 'resetpassword/resetCode/');
            //$this->load->view('reset_password',$this->data);
            //$this->load->view('admin/user/reset_password', $code);
         //   show_404();
        }
    	
       /* if($_POST){

            $passdata['codes'] = array(
                    'tempcode'=> $code
            );

            $this->db->where('reset_password', $code);// Check code is valid or not
            $query = $this->db->get('users');

            if($query->num_rows() == 1){
                 $this->load->view('reset_password',$passdata);
            }
            else{
                echo "Code does not match with database.";
                exit;
            }
        }
        else{ 
            $this->load->view('reset_password',$passdata);
        }*/
    	
    }

    function updatePassword(){
    	$tempcode = $_POST['txtcode'];
    	$data = array(
    			'password' => hash('md5', $_POST['newpassword'] . config_item('encryption_key')),
    			'reset_password' => NULL
    			);

    	$this->db->where('reset_password', $tempcode);// Check code is valid or not
	    if($this->db->update('users', $data)) {
        	 echo "Password has benn reset successfully.";
	    	 exit;
	    }
	    else{
	    	 echo "Error while resetting password.";
	    	 exit;
	    }
    }
}