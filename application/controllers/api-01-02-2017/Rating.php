<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Rating extends REST_Controller {

	/**
	 * Constructor
	 *
	 *  @return void
	 */
	function __construct() {
		parent::__construct();
		lang_switcher($_REQUEST['lang']);
		$this->load->model('Ratings_model');
	}

	// Add rating details
	function addRating_post() {

		$ratings = array(
			//'comments' => $this->post('comments'),
			'rating_score' => $this->post('rating_score'),
			'user_id' => $this->post('user_id'),
			'recipe_id' => $this->post('recipe_id'),
		);

		$checkRating = $this->Ratings_model->get_by_params(array('user_id' => $this->post('user_id'), 'recipe_id' => $this->post('recipe_id')));

		if (count($checkRating) > 0) {

			$ratings = $this->Ratings_model->getRating($this->post('recipe_id')); // Rating count and average rating

			foreach ($ratings as $value) {
				$rating = array();
				$value->avg_rating = 0;
				$value->rating_count = 0;

				if ($value->recipe_id == $this->post('recipe_id')) {
					$value->avg_rating = $value->avgRating;
					$value->rating_count = $value->totalRating;
					unset($value->avgRating);
					unset($value->totalRating);
				}
			}
			$this->response([
				'status' => FALSE,
				'ratings' => $ratings,
				'message' => $this->lang->line('rating_already_add'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code

		} else {
			$saved = $this->Ratings_model->insert($ratings); // Rating stored
			$ratings = $this->Ratings_model->getRating($this->post('recipe_id')); // Rating count and average rating

			foreach ($ratings as $value) {
				$rating = array();
				$value->avg_rating = 0;
				$value->rating_count = 0;

				if ($value->recipe_id == $this->post('recipe_id')) {
					$value->avg_rating = $value->avgRating;
					$value->rating_count = $value->totalRating;
					unset($value->avgRating);
					unset($value->totalRating);
				}
			}

			if (!empty($saved)) {
				$this->response([
					'status' => TRUE,
					'ratings' => $ratings,
					'message' => $this->lang->line('rating_add'),
				], REST_Controller::HTTP_OK); // OK (200) Reponse code
			} else {
				$this->response([
					'status' => FALSE,
					'data' => [],
					'message' => $this->lang->line('rating_err'),
				], REST_Controller::HTTP_OK); // OK (200) Reponse code
			}

		}
	}
}