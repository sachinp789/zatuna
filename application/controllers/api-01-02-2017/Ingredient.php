<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ingredient extends REST_Controller {

	function __construct() {
		parent::__construct();
		lang_switcher($_REQUEST['lang']);
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key

        //load model
        $this->load->model('Ingredients_model');
        $this->load->model('Ingredients_ar_model');
        $this->load->model('Ingredients_fr_model');
	}

	// List of ingredients name
	function getIngredients_get(){
		//echo "Gfgg";exit;
		$tag = $this->get('text');
		//$lang = $this->get('lang'); // languages

		$inglist = $this->Ingredients_model->getIngredients($tag);
	
		if(!empty($inglist)){
			$this->response([
			'status'		=> TRUE,
			'data'			=> $inglist,
			'message'		=> count($inglist).' '.$this->lang->line('msg_found')
			],REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
		else{
			$this->response([
			'status'		=> FALSE,
			'data'			=> $inglist,
			'message'		=> $this->lang->line('msg_notfound')
			],REST_Controller::HTTP_OK); // NOT_FOUND (404) being the HTTP response code
		}
	}
}