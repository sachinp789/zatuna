<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class User extends REST_Controller {

	/**
	 * Constructor
	 *
	 *  @return void
	 */
	function __construct() {
		parent::__construct();
		lang_switcher($_REQUEST['lang']);
		//header('Content-type: application/json');
		$this->load->model('Recipes_model');
		$this->load->model('Users_model');
		$this->load->model('User_favourites_model');
		$this->load->model('User_favourites_ar_model');
		$this->load->model('User_favourites_fr_model');
		$this->load->model('Bookmarks_model');
		$this->load->model('Bookmarks_ar_model');
		$this->load->model('Bookmarks_fr_model');
		$this->load->model('Cookbooks_model');
		$this->load->model('Cookbooks_ar_model');
		$this->load->model('Cookbooks_fr_model');
		$this->load->model('Ratings_model');
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
		$this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
		$this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
	}

	function users_get() {

		$users = $this->Users_model->get_users();

		if (!empty($users)) {
			$this->response([
				'status' => TRUE,
				'data' => $users,
				'message' => count($users) . ' ' . $this->lang->line('msg_found'),
			], REST_Controller::HTTP_OK);
		} else {
			$this->response([
				'status' => FALSE,
				'data' => $users,
				'message' => $this->lang->line('msg_notfound'),
			], REST_Controller::HTTP_OK);
		}
	}

	/**
	 * Retrieve the all user using GET method
	 */
	function userregister_get() {

		$fbtoken = $this->get('fb_token');

		if ($fbtoken === NULL || strlen($fbtoken) == 0) {

			$checkemail = $this->Users_model->get_by_params([
				'email' => $this->get('email'),
				'status' => 0,
			]); // Check email exists

			if (count($checkemail) > 0) {
				$this->response(['status' => FALSE, 'message' => 'Email already registered.']);
			} else {
				$data = $this->Users_model->insert([
					'firstname' => $this->get('firstname'),
					'lastname' => $this->get('lastname'),
					'email' => $this->get('email'),
					'mobile_no' => $this->get('mobile_no'),
					'password' => hash('md5', $this->get('password') . config_item('encryption_key')),
				]);

				if (empty($data)) {
					$this->response(['status' => FALSE, 'message' => 'Unable to register user.']);
				} else {
					$this->response([
						'data' => $data,
						'status' => TRUE,
						'message' => 'You have successfully registered.',
					], REST_Controller::HTTP_OK); // OK (200) Reponse code
				}

			}
		} else {
			//die("fdfdff");
			$check_avail = $this->Users_model->check_fb_token([
				'fb_token' => $fbtoken,
				'status' => 0,
			]);

			if (empty($check_avail)) {

				$data = $this->Users_model->insert([
					'firstname' => $this->get('firstname'),
					'lastname' => $this->get('lastname'),
					'email' => $this->get('email'),
					'mobile_no' => $this->get('mobile_no'),
					'fb_token' => $this->get('fb_token'),
				]);

				$this->response(['status' => TRUE, 'userId' => $data, 'message' => 'You have successfully registered with facebook.']);

			} else {
				$this->response(['status' => TRUE, 'userId' => $check_avail[0]->id, 'message' => 'User already exists.']);
			}
		}
	}

	// User registration request
	function userregister_post() {

		$fbtoken = $this->post('fb_token');

		if (empty($fbtoken)) {
			$checkemail = $this->Users_model->get_by_params([
				'email' => $this->post('email'),
				'status' => 0,
			]); // Check email exists

			if (count($checkemail) > 0) {
				$this->response(['status' => FALSE, 'message' => $this->lang->line('email_exists')]);
			} else {
				$data = $this->Users_model->insert([
					'firstname' => trim($this->post('firstname')),
					//'lastname'	=> $this->post('lastname'),
					'email' => trim($this->post('email')),
					'mobile_no' => trim($this->post('mobile_no')),
					'password' => hash('md5', $this->post('password') . config_item('encryption_key')),
				]);

				if (empty($data)) {
					$this->response(['status' => FALSE, 'message' => $this->lang->line('register_error')], REST_Controller::HTTP_OK);
					// OK (200) Reponse code
				} else {
					$this->response([
						'userId' => $data,
						'status' => TRUE,
						'message' => $this->lang->line('register_success'),
					], REST_Controller::HTTP_OK); // OK (200) Reponse code
				}
			}
		} else {
			//die("fb");
			// Social register
			$email = trim($this->post('email'));
			$check_avail = $this->Users_model->check_fb_token([
				//'fb_token'	=> $fbtoken,
				'email' => $email,
				'is_social' => '0',
			]); // Check facebook token exists or not

			if (empty($check_avail)) {
				$data = $this->Users_model->insert([
					'firstname' => trim($this->post('firstname')),
					//'lastname'	=> $this->post('lastname'),
					'email' => $email,
					'mobile_no' => trim($this->post('mobile_no')),
					'fb_token' => trim($this->post('fb_token')),
					'is_social' => 0,
				]);

				$this->response(['status' => TRUE, 'userId' => $data, 'message' => $this->lang->line('facebook_success')], REST_Controller::HTTP_OK); // OK (200) Reponse code

			} else {
				$this->response(['status' => FALSE, 'userId' => $check_avail[0]->id, 'message' => $this->lang->line('facebook_emailexists')], REST_Controller::HTTP_OK); // OK (200) Reponse code
			}
		}
	}

	function userlogin_get() {

		$email = trim($this->get('email'));
		$password = trim($this->get('password'));

		if (empty($password)) {
			$user = $this->Users_model->get_by_params([
				'email' => $email,
				'fb_token' => $this->get('fb_token'),
				'is_social' => 0,
			]);
		} else {
			$password = hash('md5', $password . config_item('encryption_key'));

			$user = $this->Users_model->get_by_params([
				'email' => $email,
				'password' => $password,
				'status' => 0,
			]);
		}
		//var_dump($user);exit;

		if (!empty($user)) {
			//user is avail
			$this->response([
				'status' => TRUE,
				'data' => $user[0],
				'message' => $this->lang->line('login_success'),
			], REST_Controller::HTTP_OK);
		} else {
			$this->response([
				'status' => FALSE,
				'message' => $this->lang->line('login_error'),
			], REST_Controller::HTTP_OK);
		}
	}

	// User social login request
	function userSocialLogin_post() {

		$email = $this->post('email');
		//$is_social = $this->post('is_social');
		//$password = hash('md5', $password . config_item('encryption_key'));

		$user = $this->Users_model->get_by_params([
			'email' => $email,
			'is_social' => 0,
		]); // Check email and is_social with status 0 (active)

		if (!empty($user)) {
			//user is avail
			$this->response([
				'status' => TRUE,
				'data' => $user[0],
				'message' => 'You have been successfully logged in.',
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				'message' => 'User not found.',
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	// User login request
	function userlogin_post() {

		$email = trim($this->post('email'));
		$password = trim($this->post('password'));

		if (empty($password)) {
			$user = $this->Users_model->check_fb_token([
				'email' => $email,
				'fb_token' => trim($this->post('fb_token')),
				'is_social' => 0,
			]);
		} else {
			$password = hash('md5', $password . config_item('encryption_key'));

			$user = $this->Users_model->get_by_params([
				'email' => $email,
				'password' => $password,
				'status' => 0,
			]); // Check email and password with status 0 (active)
		}

		if (!empty($user)) {
			//user is avail
			$this->response([
				'status' => TRUE,
				'data' => $user[0],
				'message' => $this->lang->line('login_success'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				'message' => $this->lang->line('login_error'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	// Get profile of user
	function getUserProfile_get() {

		//$action = $this->post('flag'); // Add/Update
		$userID = $this->get('userId');
		$profileData = array();

		if (!empty($userID)) {
			$where = array('id' => $userID);

			$userInfo = $this->Users_model->get_by_params($where);

			foreach ($userInfo as $value) {
				$userInfo[0]->mobile_no = urlencode($value->mobile_no);
			}
			//exit;
			/*if(!empty($userInfo)){
				echo json_encode(array('status'=>TRUE,'data'=>$userInfo,'message'=>'User profile details found.'));
			}
			else{
				echo json_encode(array('status'=>FALSE,'data'=>[],'message'=> 'User profile details not found.'));
			}*/

			if (!empty($userInfo)) {
				$this->response([
					'status' => TRUE,
					'data' => $userInfo,
					'message' => $this->lang->line('userdetails_found'),
				], REST_Controller::HTTP_OK);
			} else {
				$this->response([
					'status' => FALSE,
					'data' => [],
					'message' => $this->lang->line('userdetails_notfound'),
				], REST_Controller::HTTP_OK);
			}
		}
	}

	// Update User Profile Information
	function updateUserProfile_post() {

		$userID = $this->post('userId');
		$profileData = array(
			'firstname' => $this->post('name'),
			//'lastname'	=> $this->post('lastname'),
			'mobile_no' => $this->post('mobile_no'),
			'email' => $this->post('email'),
			//'address'	=> $this->post('address')
		);

		if (!empty($userID)) {

			$updated = $this->Users_model->update_profile($userID, $profileData);

			if ($updated == 1) {
				$this->response([
					'status' => TRUE,
					'message' => $this->lang->line('profileupdate'),
				], REST_Controller::HTTP_OK);
			} else {
				$this->response([
					'status' => FALSE,
					'data' => [],
					'message' => $this->lang->line('profileerror'),
				], REST_Controller::HTTP_OK);
			}
		}
	}

	// Update user password
	function changeUserPassword_post() {

		$userID = $this->post('userId');
		$is_social = $this->post('isSocial');

		$oldpwd = hash('md5', $this->post('oldpassword') . config_item('encryption_key'));

		$passwordData = array(
			'password' => hash('md5', $this->post('newpassword') . config_item('encryption_key')),
		);

		if ($is_social == 1) {

			$updated = $this->Users_model->saveNewPass($userID, $passwordData);
			if ($updated == TRUE) {
				$this->response([
					'status' => TRUE,
					'message' => $this->lang->line('pwdchange'),
				], REST_Controller::HTTP_OK);
			} else {
				$this->response([
					'status' => FALSE,
					'data' => [],
					'message' => $this->lang->line('userpwd_notfound'),
				], REST_Controller::HTTP_OK);
			}
		} else {
			$checked = $this->Users_model->checkOldPass($userID, $oldpwd);

			if ($checked == 1) {

				$updated = $this->Users_model->saveNewPass($userID, $passwordData);

				if ($updated == TRUE) {
					$this->response([
						'status' => TRUE,
						'message' => $this->lang->line('pwdchange'),
					], REST_Controller::HTTP_OK);
				} else {
					$this->response([
						'status' => FALSE,
						'data' => [],
						'message' => $this->lang->line('userpwd_notfound'),
					], REST_Controller::HTTP_OK);
				}
			} else {
				$this->response([
					'status' => FALSE,
					'data' => [],
					'message' => $this->lang->line('pwdnot_match'),
				], REST_Controller::HTTP_OK);
			}
		}

	}

	// Forgot Password User
	function forgotPassword_post() {
		$emailId = $this->post('email');

		$checked = $this->Users_model->getWhere($emailId);
		//print_r($checked);exit;
		if ($checked > 0) {
			// True
			$code = $this->randomKey(20); // Random key generate
			/* $passwordData = array(
	          'password' => hash('md5', $code . config_item('encryption_key'))
	        );*/

			$passdata = array(
				'password' => $code,
			);

			$this->load->library('email');
			$this->email->set_newline("\r\n");
			$this->email->from('info@searchnative.com', 'Searchnative');
			$this->email->to($emailId);
			$this->email->subject("Reset your Password");

			$body = $this->load->view('forgotpass_template', $passdata, TRUE);
			$this->email->message($body);

			//echo $body;exit;

			if ($this->email->send()) {
				$updated = $this->Users_model->temp_reset_password($emailId, $code);

				if ($updated == 1) {
					$this->response([
						'status' => TRUE,
						'message' => $this->lang->line('pwdlink_send'),
					], REST_Controller::HTTP_OK);
				}
			} else {
				//echo $this->email->print_debugger();
				$this->response([
					'status' => False,
					'message' => $this->lang->line('pwdlink_error'),
				], REST_Controller::HTTP_OK);
			}
		} else {
			// False
			$this->response([
				'status' => False,
				'message' => $this->lang->line('dbemail_error'),
			], REST_Controller::HTTP_OK);
		}
	}

	// Reset temporary code
	function resetPassword_get($temp_pass) {
		$tempcode = $this->get('code');

		$validcode = $this->Users_model->is_temp_pass_valid($tempcode); // Check code is valid or not

		if ($validcode == 1) {
			// True
			$this->response([
				'status' => TRUE,
				'message' => $this->lang->line('code_valid'),
			], REST_Controller::HTTP_OK);
		} else {
			// False
			$this->response([
				'status' => False,
				'message' => $this->lang->line('codeinvalid'),
			], REST_Controller::HTTP_OK);
		}
	}

	// Random key generate for password
	function randomKey($length) {
		$pool = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));

		for ($i = 0; $i < $length; $i++) {
			$key .= $pool[mt_rand(0, count($pool) - 1)];
		}
		return $key;
	}

	// Upload file
	function uploadFile_post($name, $id, $lang = NULL) {
		$config['upload_path'] = FCPATH . 'uploads/recipes';
		$fullpath = base_url() . 'uploads/recipes/';

		if ($name['name'] != '') {
			$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|zip|rar|jpeg|mp4|mkv|flv|wmv|3gp|webm';
			$this->load->library('upload');

			$file_ext = explode(".", $name['name']);
			$ext_file = strtolower(end($file_ext));
			$config['file_name'] = time() . date('dmYhis') . '.' . $ext_file;
			//now we initialize the upload library
			$this->upload->initialize($config);
			// we retrieve the number of files that were uploaded
			$data = array('cbook_image' => $fullpath . $config['file_name']);

			if ($lang == 'ar') {
				$this->db->where('id', $id);
				$this->db->update('cookbooks_ar', $data);
			} else if ($lang == 'fr') {
				$this->db->where('id', $id);
				$this->db->update('cookbooks_fr', $data);
			} else {
				$this->db->where('id', $id);
				$this->db->update('cookbooks', $data);
			}
			$this->upload->do_upload('cookbook_media');
		}
	}

	// Add recipes to favourites/bookmark/wishlist
	function addtoBook_post() {

		$this->saveBookmarkRecipe_post(); // English

	}

	// Save recipes to cookbook (EN)
	function saveBookmarkRecipe_post() {

		$cooks = explode(',', $this->post('cookbook_id'));
		$count = 0;
		$bookmarkArray = array();
		$bookIdArray = array();

		$isBookmarkID = $this->Bookmarks_model->bookmarkCheck($this->post('user_id'), $cooks, $this->post('recipe_id'));

		foreach ($isBookmarkID as $key => $value) {
			$bookIdArray[] = $value['book_id'];
		}

		if (!empty($bookIdArray) && count($bookIdArray) > 0) {
			for ($i = 0; $i < count($cooks); $i++) {
				// Check book_id and create array book_id !=
				if (!in_array($cooks[$i], $bookIdArray)) {
					$bookmarkArray[] = array(
						'recipe_id' => $this->post('recipe_id'),
						'book_id' => $cooks[$i],
						'user_id' => $this->post('user_id'),
					); // Create bookmarks array
					$count++;
				}
			}
			//print_r($bookmarkArray);exit;
			if (!empty($bookmarkArray)) {
				$this->Bookmarks_model->create($bookmarkArray); // Added in bookmark if not in table
				$this->response([
					'status' => TRUE,
					'data' => [],
					'message' => $this->lang->line('cookbook_add'),
				], REST_Controller::HTTP_OK);
			}

			$this->response([
				'status' => FALSE,
				'data' => [],
				'message' => $this->lang->line('cookbook_exists'),
			], REST_Controller::HTTP_OK);

		} else {
			//die("add");
			for ($i = 0; $i < count($cooks); $i++) {
				$bookmarkArray[] = array(
					'recipe_id' => $this->post('recipe_id'),
					'book_id' => $cooks[$i],
					'user_id' => $this->post('user_id'),
				);
			}

			$this->Bookmarks_model->create($bookmarkArray); // Insert in bookmarks

			// Is bookmarked status update
			//$this->db->where('id',$this->post('recipe_id'));
			//$this->db->update('recipes', array('isbookmark' => 1));

			$this->response([
				'status' => TRUE,
				'data' => [],
				'message' => $this->lang->line('cookbook_add'),
			], REST_Controller::HTTP_OK);
		}

	}

	// Get Listing of CookBook Users
	function getUserCookbook_get() {

		return $this->listCookBook_get();
	}

	// Listing of CookBook
	function listCookBook_get() {

		$userID = $this->get('userid');

		$cookList = $this->Cookbooks_model->userCookbookList($userID);

		foreach ($cookList as $value) {
			$media = array();
			$value->recipe_media = array();

			$recipesList = $this->Bookmarks_model->getRecipiesbyBook($userID, $value->id);

			for ($k = 0; $k < count($recipesList); $k++) {
				$media[$k] = $recipesList[$k]->recipe_media;
			}
			$value->recipe_media = $media;

		}

		if (!empty($cookList)) {
			$this->response([
				'status' => TRUE,
				'cooklist' => $cookList,
				'message' => count($cookList) . ' ' . $this->lang->line('msg_found'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				'message' => $this->lang->line('no_cookbook'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}

	}

	// Add to cookbook
	function addCookbook_post() {

		$this->saveCookbook_post();
	}

	// Save to cookmenu book
	function saveCookbook_post() {

		$cookDetails = array(
			'user_id' => $this->post('user_id'),
			'cbook_name' => $this->post('cook_name'),
			//'cbook_description'	=>$this->post('cook_description')
		);
		$cook_Id = $this->Cookbooks_model->insert($cookDetails);

		if (!empty($cook_Id)) {
			$uploadImage = $this->uploadFile_post($_FILES['cookbook_media'], $cook_Id, $this->post('lang'));
			$this->response([
				'status' => TRUE,
				'cookID' => $cook_Id,
				'message' => $this->lang->line('cbook_add'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				'message' => $this->lang->line('cbook_error'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	function getBookmarksList_get() {

		$this->bookmarkRecipes_get();
	}

	// User bookmark recipes
	function bookmarkRecipes_get() {

		$bookmark_id = $this->get('bookId');
		$user_id = $this->get('userId');

		$bookmarkData = $this->Bookmarks_model->getBookmarkRecipes($bookmark_id, $user_id);

		if (!empty($bookmarkData)) {
			$this->response([
				'status' => TRUE,
				'bookmarkList' => $bookmarkData,
				'message' => $bookmarkData . ' ' . $this->lang->line('msg_found'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				'bookmarkList' => [],
				'message' => $this->lang->line('bookmark_notfound'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}

		//return $bookmarkData;
	}

	// Get user coookBook
	function updateCookBook_post() {

		$updateCookarray = array(
			'user_id' => $this->post('user_id'),
			'cbook_name' => $this->post('cook_name'),
			'cbook_description' => $this->post('cook_description'),
		);

		$this->editCookBook_post($updateCookarray);
	}

	// Edit cookBook item list
	function editCookBook_post($data) {

		$bookID = $this->post('cookbook_id');
		$userID = $this->post('user_id');

		if (!empty($_FILES['cookbook_media']['name'])) {

			$found = $this->Cookbooks_model->getCookBookUser($this->post('cookbook_id'), $this->post('user_id'));

			if (count($found) > 0) {
				$paths = explode('/', $found->cbook_image);
				unlink(FCPATH . 'uploads/recipes/' . $paths[6]); // Remove image from folder
			}
			$this->uploadFile_post($_FILES['cookbook_media'], $this->post('cookbook_id'), $this->post('lang'));
		}

		$userCooks = $this->Cookbooks_model->updateCook($data, $bookID, $userID);

		if ($userCooks == true) {

			$this->response([
				'status' => TRUE,
				//'updatedCookID'	=> $this->post('cookbook_id'),
				'message' => $this->lang->line('cbook_update'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				'message' => $this->lang->line('cbook_updateerror'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	// Get listof Cookbook recipies of user (en/ar/fr)
	function listCookBookRecipes_get() {

		$bookID = $this->get('book_id');
		$userID = $this->get('user_id');
		$lang = $this->get('lang');

		$likedorbookmarkrecipes = $this->Recipes_model->getUserLikeORBookmarkRecipes($userID);

		$cookRecipes = $this->Bookmarks_model->getRecipiesbyBook($userID, $bookID);
		$result = $this->Bookmarks_model->getIngredientsbyBook($userID, $bookID);
		$totalLike = $this->User_favourites_model->countRecipeCount();
		$ratings = $this->Ratings_model->getRating();
		$stepsRecipes = $this->Recipes_model->get_recipesStep();

		foreach ($cookRecipes as $cookRecp) {
			// $i=0;
			$ingredients = array();
			$likesrecipes = array();
			$steps = array();
			$rating = array();
			$cookRecp->ingredients = array();
			$cookRecp->steps = array();
			$cookRecp->is_like = 0;
			$cookRecp->userlikecount = 0;
			$cookRecp->avg_rating = 0;
			$cookRecp->rating_count = 0;

			for ($k = 0; $k < count($result); $k++) {
				if ($result[$k]->recipe_id == $cookRecp->recipe_id) {
					$ingredients[] = $result[$k];
				}

			}
			$cookRecp->ingredients = $ingredients;

			for ($k = 0; $k < count($ratings); $k++) {
				if ($ratings[$k]->recipe_id == $cookRecp->recipe_id) {
					$cookRecp->avg_rating = $ratings[$k]->avgRating;
					$cookRecp->rating_count = $ratings[$k]->totalRating;
				}

			}

			for ($k = 0; $k < count($stepsRecipes); $k++) {
				if (isset($stepsRecipes[$k]->image)) {
					$stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
				}
				//$stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
				if ($stepsRecipes[$k]->recipe_id == $cookRecp->recipe_id) {
					$steps[] = $stepsRecipes[$k];
					unset($stepsRecipes[$k]->image);
				}

			}
			$cookRecp->steps = $steps;

			foreach ($totalLike as $likerecipe) {
				if ($likerecipe->recipe_id == $cookRecp->recipe_id)
				//array_push($recipecount, $likerecipe->totalcount);
				{
					$cookRecp->userlikecount = $likerecipe->totalcount;
				}

			}

			for ($i = 0; $i < count($likedorbookmarkrecipes['likes']); $i++) {
				//echo $usedIng[$k]->recipe_id;
				if ($likedorbookmarkrecipes['likes'][$i]->recipe_id == $cookRecp->recipe_id) {
					$cookRecp->is_like = $likedorbookmarkrecipes['likes'][$i]->is_like;
				}
			}
		}

		if (!empty($result)) {
			$this->response([
				'status' => TRUE,
				'data' => $cookRecipes,
				//'ingredients'	=> $result,
				'message' => $this->lang->line('msg_rec_found'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				'message' => $this->lang->line('msg_rec_notfound'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	// Remove cookBook of users
	function deleteCookBook_get() {
		$userID = $this->get('user_id');
		$cookbookID = $this->get('cookbook_id');

		$success = $this->Cookbooks_model->removeCookBook($cookbookID, $userID);

		if ($success == TRUE) {
			$this->response([
				'status' => TRUE,
				'message' => $this->lang->line('cbook_delete'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code

		} else {
			$this->response([
				'status' => FALSE,
				'message' => $this->lang->line('cbbok_deleteerror'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	// Remove bookmark recipes of users
	function deleteBookRecipe_get() {
		//$lang = $this->get('lang');
		$userID = $this->get('user_id');
		$cookbookID = $this->get('cookbook_id');
		$recipeID = $this->get('recipe_id');

		$success = $this->Bookmarks_model->removeBookRecipe($cookbookID, $userID, $recipeID);
		if ($success == TRUE) {
			$this->response([
				'status' => TRUE,
				'message' => $this->lang->line('bookmarkrecipe_remove'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code

		} else {
			$this->response([
				'status' => FALSE,
				'message' => $this->lang->line('bookmarkrecipe_error'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	// User recipes like function
	function likeUserRecipe_post() {

		$flag = $this->post('isLike'); // Like or Unlike
		$likeDetails = array(
			'user_id' => $this->post('user_id'),
			'recipe_id' => $this->post('recipe_id'),
			'is_like' => 1,
		);

		$ratingcount = array();

		if ($flag == 1) {
			// User like recipes
			$likesRecipes = $this->User_favourites_model->insert($likeDetails);

			$totalLike = $this->User_favourites_model->countRecipeCount();

			foreach ($totalLike as $likerecipe) {
				if ($likerecipe->recipe_id == $this->post('recipe_id')) {
					$ratingcount['rating_count'] = $likerecipe->totalcount;
				}
			}

			if (!empty($likesRecipes)) {

				$this->response([
					'status' => TRUE,
					'data' => $ratingcount,
					'message' => $this->lang->line('recipe_like'),
				], REST_Controller::HTTP_OK); // OK (200) Reponse code
			} else {
				$this->response([
					'status' => FALSE,
					'message' => $this->lang->line('recipe_likeerror'),
				], REST_Controller::HTTP_OK); // OK (200) Reponse code
			}
		} else {
			// Check like or not
			$check = $this->User_favourites_model->isLike($this->post('user_id'), $this->post('recipe_id'));

			if ($check == TRUE) {
				// Remove like form table
				$unlike = $this->User_favourites_model->removeLike($this->post('user_id'), $this->post('recipe_id'));

				$totalLike = $this->User_favourites_model->countRecipeCount();

				foreach ($totalLike as $likerecipe) {
					if ($likerecipe->recipe_id == $this->post('recipe_id')) {
						$ratingcount['rating_count'] = $likerecipe->totalcount;
					}
				}

				if ($unlike == TRUE) {

					$this->response([
						'status' => TRUE,
						'data' => $ratingcount,
						//'message'	=> 'Recipe unliked.'
					], REST_Controller::HTTP_OK); // OK (200) Reponse code
				}
			} else {
				$this->response([
					'status' => FALSE,
				], REST_Controller::HTTP_OK); // OK (200) Reponse code
			}
		}
	}

	// User likes recipes list API
	function likeUserRecipesList_get() {
		$userID = $this->get('user_id');
		// Get user like recipeslist
		$likeList = $this->User_favourites_model->getUserLikeRecipes($userID);
		$totalLike = $this->User_favourites_model->countRecipeCount();
		$likedorbookmarkrecipes = $this->Recipes_model->getUserLikeORBookmarkRecipes($userID);

		$usedIng = $this->User_favourites_model->getUsedIngridents($userID);
		$stepsRecipes = $this->Recipes_model->get_recipesStep();
		$ratings = $this->Ratings_model->getRating();

		foreach ($likeList as $like) {
			// $i=0;
			$ingredients = array();
			$steps = array();
			$rating = array();
			$like->ingredients = array();
			$like->steps = array();
			$like->userlikecount = 0;
			$like->is_like = 0;
			$like->avg_rating = 0;
			$like->rating_count = 0;

			for ($k = 0; $k < count($usedIng); $k++) {
				//echo $usedIng[$k]->recipe_id;
				if ($usedIng[$k]->recipe_id == $like->recipe_id) {
					$ingredients[] = $usedIng[$k];
				}

			}
			$like->ingredients = $ingredients;
			//array_push($like->ingredients, $ingredients[$i]);

			for ($k = 0; $k < count($ratings); $k++) {
				if ($ratings[$k]->recipe_id == $like->recipe_id) {
					$like->avg_rating = $ratings[$k]->avgRating;
					$like->rating_count = $ratings[$k]->totalRating;
				}

			}

			for ($k = 0; $k < count($stepsRecipes); $k++) {
				if (isset($stepsRecipes[$k]->image)) {
					$stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
				}
				//$stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
				if ($stepsRecipes[$k]->recipe_id == $like->recipe_id) {
					$steps[] = $stepsRecipes[$k];
					unset($stepsRecipes[$k]->image);
				}

			}
			$like->steps = $steps;

			foreach ($totalLike as $likerecipe) {
				if ($likerecipe->recipe_id == $like->recipe_id)
				//array_push($recipecount, $likerecipe->totalcount);
				{
					$like->userlikecount = $likerecipe->totalcount;
				}

			}

			for ($i = 0; $i < count($likedorbookmarkrecipes['likes']); $i++) {
				//echo $usedIng[$k]->recipe_id;
				if ($likedorbookmarkrecipes['likes'][$i]->recipe_id == $like->recipe_id) {
					$like->is_like = $likedorbookmarkrecipes['likes'][$i]->is_like;
				}
			}
		}

		if (!empty($likeList)) {
			$this->response([
				'status' => TRUE,
				'data' => $likeList,
				//'ingredients'=> $usedIng,
				//'message'	=> 'Recipe like successfully.'
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				'message' => $this->lang->line('pls_like'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

}
