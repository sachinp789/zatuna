<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Recipe extends REST_Controller {

	/**
	 * Constructor
	 *
	 *  @return void
	 */
	function __construct() {
		parent::__construct();
		lang_switcher($_REQUEST['lang']);
		//echo $this->lang->line('msg');
		//exit;
		$this->load->model('Recipe_categories_model');
		$this->load->model('Recipes_model');
		$this->load->model('User_favourites_model');
		$this->load->model('Recipe_subcategories_model');
		$this->load->model('Ingredients_model');
		$this->load->model('Ratings_model');
		$this->load->model('Master_recipeingredients_model');
		$this->load->model('Master_recipeingredients_ar_model');
		$this->load->model('Master_recipeingredients_fr_model');
		$this->load->model('Measurements_model');
		$this->methods['user_get']['limit'] = 500; // 500 requests per hour per user/key
		$this->methods['user_post']['limit'] = 100; // 100 requests per hour per user/key
		$this->methods['user_delete']['limit'] = 50; // 50 requests per hour per user/key
	}

	// Upload file
	function uploadFile_post($name, $id, $lang = NULL) {
		$config['upload_path'] = FCPATH . 'uploads/recipes';
		$fullpath = base_url() . 'uploads/recipes/';

		if ($name['name'] != '') {
			$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|zip|rar|jpeg|mp4|mkv|flv|wmv|3gp|webm';
			$this->load->library('upload');

			$file_ext = explode(".", $name['name']);
			$ext_file = strtolower(end($file_ext));
			$config['file_name'] = time() . date('dmYhis') . '.' . $ext_file;
			//now we initialize the upload library
			$this->upload->initialize($config);
			// we retrieve the number of files that were uploaded
			$data = array('recipe_media' => $fullpath . $config['file_name']);

			if ($lang == 'ar') {
				$this->db->where('id', $id);
				$this->db->update('recipes_ar', $data);
				//$this->Recipes_model->update_recipes_id1($id,$data);
			} else if ($lang == 'fr') {
				$this->db->where('id', $id);
				$this->db->update('recipes_fr', $data);
				//$this->Recipes_model->update_recipes_id1($id,$data);
			} else {
				$this->Recipes_model->update_recipes_id1($id, $data);
			}

			$this->upload->do_upload('recipe_media');
			/*$this->upload->do_upload('recipe_media');

				    $this->response([
						'status'	=> TRUE,
						'data'      => [],
						'message'	=> 'Recipe is successfully added!',
						], REST_Controller::HTTP_OK); // OK (200) Reponse code
			*/
		}
	}

	function do_upload_multiple_files_post($files, $id, $lang = NULL) {

		$config['upload_path'] = FCPATH . 'uploads/SS';
		$fullpath = base_url() . 'uploads/SS/';

		$response = array();

		$config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf|zip|rar|jpeg|mp4|mkv|flv|wmv|3gp|webm';
		$this->load->library('upload');

		foreach ($files as $key => $value) {

			if (!empty($value['name'])) {

				$file_ext = explode(".", $value['name']);
				$name = $key;
				$ext_file = strtolower(end($file_ext));
				$config['file_name'] = rand(0, 10000000000000) . '.' . $ext_file;
				$response[$key] = $fullpath . $config['file_name'];

				$this->upload->initialize($config);

				if ($lang == 'ar') {
					$this->db->where('id', $id);
					$this->db->update('recipes_ar', $response);
					//$this->Recipes_model->update_recipes_id1($id,$data);
				} else if ($lang == 'fr') {
					$this->db->where('id', $id);
					$this->db->update('recipes_fr', $response);
					//$this->Recipes_model->update_recipes_id1($id,$data);
				} else {
					$this->Recipes_model->update_recipes_id1($id, $response);
				}

				$this->upload->do_upload($name);
			}
		}
		return $response;
	}

	// Get all recipies categories
	function recipecategory_get() {

		$catlists = $this->Recipe_categories_model->get_categories();

		if (!empty($catlists)) {
			$this->response([
				'status' => TRUE,
				'data' => $catlists,
				'message' => count($catlists) . ' ' . $this->lang->line('msg_found'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				'data' => $catlists,
				'message' => $this->lang->line('msg_notfound'),
			], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
		}

	}

	// Get all ingredients categories

	function ingredientscat_get() {

		$tag = $this->get('tag');
		$catlists = $this->Ingredients_model->get_ingredients_cats($tag);

		if (!empty($catlists)) {
			$this->response([
				'status' => TRUE,
				'data' => $catlists,
				'message' => count($catlists) . ' ' . $this->lang->line('msg_found'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				'data' => $catlists,
				'message' => $this->lang->line('msg_notfound'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	function recipeslist_get() {

		$combine_array = array();

		$userID = $this->get('userid');
		$recipies = $this->Recipes_model->get_recipes_data($userID);
		$likedorbookmarkrecipes = $this->Recipes_model->getUserLikeORBookmarkRecipes($userID);
		$totalLike = $this->User_favourites_model->countRecipeCount(); // Like Count
		//$recipemedia 	= $this->Recipes_model->get_recipes_media($userID);
		$ratings = $this->Ratings_model->getRating();
		$usedingred = $this->Ingredients_model->get_usedIngridents($userID);
		$stepsRecipes = $this->Recipes_model->get_recipesStep();

		foreach ($recipies as $recp) {
			// $i=0;
			$ingredients = array();
			$steps = array();
			$likesrecipes = array();
			$bookmarks = array();
			$rating = array();
			$recp->ingredients = array();
			$recp->steps = array();
			$recp->is_bookmark = 0;
			$recp->is_like = 0;
			$recp->userlikecount = 0;
			$recp->avg_rating = 0;
			$recp->rating_count = 0;

			for ($k = 0; $k < count($usedingred); $k++) {
				if ($usedingred[$k]->recipe_id == $recp->recipe_id) {
					$ingredients[] = $usedingred[$k];
				}

			}
			$recp->ingredients = $ingredients;

			for ($k = 0; $k < count($ratings); $k++) {
				if ($ratings[$k]->recipe_id == $recp->recipe_id) {
					$recp->avg_rating = $ratings[$k]->avgRating;
					$recp->rating_count = $ratings[$k]->totalRating;
				}

			}

			for ($k = 0; $k < count($stepsRecipes); $k++) {
				if (isset($stepsRecipes[$k]->image)) {
					$stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
				}
				//$stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
				if ($stepsRecipes[$k]->recipe_id == $recp->recipe_id) {
					$steps[] = $stepsRecipes[$k];
					unset($stepsRecipes[$k]->image);
				}

			}
			$recp->steps = $steps;

			for ($j = 0; $j < count($likedorbookmarkrecipes['bookmarks']); $j++) {
				//echo $usedIng[$k]->recipe_id;
				if ($likedorbookmarkrecipes['bookmarks'][$j]->recipe_id == $recp->recipe_id) {
					$recp->is_bookmark = $likedorbookmarkrecipes['bookmarks'][$j]->is_bookmark;

				}

			}

			for ($i = 0; $i < count($likedorbookmarkrecipes['likes']); $i++) {
				//echo $usedIng[$k]->recipe_id;
				if ($likedorbookmarkrecipes['likes'][$i]->recipe_id == $recp->recipe_id) {
					$recp->is_like = $likedorbookmarkrecipes['likes'][$i]->is_like;
				}
			}
			foreach ($totalLike as $likerecipe) {
				if ($likerecipe->recipe_id == $recp->recipe_id)
				//array_push($recipecount, $likerecipe->totalcount);
				{
					$recp->userlikecount = $likerecipe->totalcount;
				}

			}
		}

		if (!empty($recipies)) {
			$this->response([
				'status' => TRUE,
				'data' => $recipies,
				'message' => $this->lang->line('msg_found'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				//'data'			=> $recipies,
				'message' => $this->lang->line('msg_notfound'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	// Previous last seen recipes
	function getRecipeDetails_get() {

		$recipeID = $this->get('recipe_id');
		$userID = $this->get('user_id');
		$recipies = $this->Recipes_model->get_previouesrecipes_data($recipeID);
		$likedorbookmarkrecipes = $this->Recipes_model->getUserLikeORBookmarkRecipesByID($recipeID, $userID);
		//$recipemedia 	= $this->Recipes_model->get_recipes_media($recipeID);
		$totalLike = $this->User_favourites_model->countRecipeCount();
		$usedingred = $this->Ingredients_model->get_previous_usedIngridents($recipeID);
		$ratings = $this->Ratings_model->getRating();
		$stepsRecipes = $this->Recipes_model->get_recipesStep();

		foreach ($recipies as $recp) {
			// $i=0;
			$ingredients = array();
			$steps = array();
			$likesrecipes = array();
			$bookmarks = array();
			$rating = array();
			$recp->ingredients = array();
			$recp->steps = array();
			$recp->is_like = 0;
			$recp->is_bookmark = 0;
			$recp->userlikecount = 0;
			$recp->avg_rating = 0;
			$recp->rating_count = 0;

			for ($k = 0; $k < count($usedingred); $k++) {
				if ($usedingred[$k]->recipe_id == $recp->recipe_id) {
					$ingredients[] = $usedingred[$k];
				}

			}
			$recp->ingredients = $ingredients;

			for ($k = 0; $k < count($ratings); $k++) {
				if ($ratings[$k]->recipe_id == $recp->recipe_id) {
					$recp->avg_rating = $ratings[$k]->avgRating;
					$recp->rating_count = $ratings[$k]->totalRating;
				}

			}

			for ($k = 0; $k < count($stepsRecipes); $k++) {
				if (isset($stepsRecipes[$k]->image)) {
					$stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
				}
				//$stepsRecipes[$k]->path = base_url() . $stepsRecipes[$k]->image;
				if ($stepsRecipes[$k]->recipe_id == $recp->recipe_id) {
					$steps[] = $stepsRecipes[$k];
					unset($stepsRecipes[$k]->image);
				}

			}
			$recp->steps = $steps;

			for ($j = 0; $j < count($likedorbookmarkrecipes['bookmarks']); $j++) {
				//echo $usedIng[$k]->recipe_id;
				if ($likedorbookmarkrecipes['bookmarks'][$j]->recipe_id == $recp->recipe_id) {
					$recp->is_bookmark = $likedorbookmarkrecipes['bookmarks'][$j]->is_bookmark;

				}

			}

			for ($i = 0; $i < count($likedorbookmarkrecipes['likes']); $i++) {
				//echo $usedIng[$k]->recipe_id;
				if ($likedorbookmarkrecipes['likes'][$i]->recipe_id == $recp->recipe_id) {
					$recp->is_like = $likedorbookmarkrecipes['likes'][$i]->is_like;
				}
			}

			foreach ($totalLike as $likerecipe) {
				if ($likerecipe->recipe_id == $recp->recipe_id)
				//array_push($recipecount, $likerecipe->totalcount);
				{
					$recp->userlikecount = $likerecipe->totalcount;
				}

			}
		}

		if (!empty($recipies)) {
			$this->response([
				'status' => TRUE,
				'data' => $recipies,
				//'ingredients'	=> $usedingred,
				//'recipesmedia'	=> $recipemedia,
				'message' => $this->lang->line('msg_found'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				//'data'			=> $recipies,
				'message' => $this->lang->line('msg_notfound'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}

	}

	// Stored recipies data
	function saveRecipe_post($lang = 'en') {

		//return $this->do_upload_multiple_files_post($_FILES,4,$this->post('lang'));
		//exit;
		$recipe_id = NULL;
		$msg = '';
		if ($this->post('lang') == 'ar') {
			echo "arabic";exit;
			$recipe_id = $this->addReciepiAR_post();
			$msg = 'يضاف صفة بنجاح!';
		} else if ($this->post('lang') == 'fr') {
			echo "french";exit;
			$recipe_id = $this->addReciepiFR_post();
			$msg = 'La recette est ajoutée avec succès!';
		} else {
			$recipe_id = $this->addReciepiEN_post();
			$msg = 'Recipe is successfully added!';
		}

		if (!empty($recipe_id)) {

			$this->do_upload_multiple_files_post($_FILES, $recipe_id, $this->post('lang'));
			//$uploadImage = $this->uploadFile_post($_FILES['recipe_media'],$recipe_id,$this->post('lang'));

			$this->response([
				'status' => TRUE,
				'data' => [],
				'message' => $msg,
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				'data' => [],
				'message' => 'Error!',
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
		//$uploadImage = $this->uploadFile_post($_FILES['recipe_media'],$recipe_id,$this->post('lang'));
	}

	/**************************** NEW ***********************/
	function addReciepiEN_post() {
		$insertId = NULL;

		$ingarray = array();

		$recpdata = [
			'recipe_name' => $this->post('recipe_name'),
			'recipe_description' => $this->post('recipe_description'),
			'recipe_method' => $this->post('recipe_method'),
			'preparation_time' => $this->post('preparation_time'),
			'cooking_time' => $this->post('cooking_time'),
			'serving' => $this->post('serving'),
			'recipe_category_id' => $this->post('recipe_category'),
			'recipe_subcategory_id' => $this->post('recipe_subcategory'),
			'user_id' => $this->post('user_id'),
		];

		$insertId = $this->Recipes_model->insert($recpdata); // Recipes data stored

		$ingrdients = json_decode($this->post('ingredients')); // Convert in array

		for ($i = 0; $i < count($ingrdients); $i++) {

			if (is_numeric($ingrdients[$i]->ingredient_name)) {
				$ingarray['ingredient_id'] = $ingrdients[$i]->ingredient_name;
				$ingarray['qty'] = $ingrdients[$i]->qty;
				$ingarray['measure_type'] = $ingrdients[$i]->measure_type;
				$ingarray['measure_amount'] = $this->calculateMetric($ingrdients[$i]->measurement_name, $ingrdients[$i]->qty);
				$ingarray['measurement_id'] = $ingrdients[$i]->measurement_id;
				$ingarray['recipe_id'] = $insertId;

				$this->Master_recipeingredients_model->insert($ingarray); // Recipes and ingredient master data stored.
			} else {

				$ing_id[] = $this->Ingredients_model->insert(array(
					'name' => $ingrdients[$i]->ingredient_name)
				); // Ingredient name stored in table

				$ingarray['ingredient_id'] = $ing_id[$i];
				$ingarray['qty'] = $ingrdients[$i]->qty;
				$ingarray['measure_type'] = $ingrdients[$i]->measure_type;
				$ingarray['measure_amount'] = $this->calculateMetric($ingrdients[$i]->measurement_name, $ingrdients[$i]->qty);
				$ingarray['measurement_id'] = $ingrdients[$i]->measurement_id;
				$ingarray['recipe_id'] = $insertId;

				$this->Master_recipeingredients_model->insert($ingarray); // Recipes and ingredient master data stored.
			}

		}
/*
if(is_numeric($this->post('ingredient_name'))) {

$recpdata = [
'recipe_name' 			=> $this->post('recipe_name'),
'recipe_description' 	=> $this->post('recipe_description'),
'recipe_method' 		=> $this->post('recipe_method'),
'preparation_time' 		=> $this->post('preparation_time'),
'cooking_time' 			=> $this->post('cooking_time'),
'serving'			 	=> $this->post('serving'),
'recipe_category_id' 	=> $this->post('recipe_category'),
'recipe_subcategory_id' => $this->post('recipe_subcategory'),
'user_id' 				=> $this->post('user_id')
];

$insertId	= $this->Recipes_model->insert($recpdata); //

$masterdata = [
'recipe_id'				=> $insertId,
];

for($i=0;$i<count($_POST['ingredient_name']);$i++){
//$total = $this->calculateMetric($this->post('measurement_name'), $this->post('qty')[$i]);
$masterdata['ingredient_id'] = $this->post('ingredient_name')[$i];
// echo $this->post('qty');exit;
$masterdata['qty'] = $this->post('qty')[$i];
$masterdata['measurement_id'] = $this->post('measurement_id')[$i];
$masterdata['measure_amount'] = $this->calculateMetric($this->post('measurement_name'), $this->post('qty')[$i]);
$masterdata['measure_type'] = $this->post('measure_type');
$this->Master_recipeingredients_model->insert($masterdata);
}
//$this->Master_recipeingredients_model->insert($masterdata);	// MasterID
}
else{

// Ingredients details stored
for($i=0;$i<count($this->post('ingredient_name'));$i++)
{
$ing_id[] = $this->Ingredients_model->insert(array(
'name' => $this->post('ingredient_name')[$i])
);
}
// Recipies details stored
$recpdata = [
'recipe_name' 			=> $this->post('recipe_name'),
'recipe_description' 	=> $this->post('recipe_description'),
'recipe_method' 		=> $this->post('recipe_method'),
'preparation_time' 		=> $this->post('preparation_time'),
'cooking_time' 			=> $this->post('cooking_time'),
'serving'			 	=> $this->post('serving'),
'recipe_category_id' 	=> $this->post('recipe_category'),
'recipe_subcategory_id' => $this->post('recipe_subcategory'),
'user_id' 				=> $this->post('user_id')
];

$insertId	= $this->Recipes_model->insert($recpdata); //

// Master tables details
$recpingdata = [
'recipe_id'				=> $insertId
];

for($i=0;$i<count($this->post('ingredient_name'));$i++){
$recpingdata['ingredient_id'] = $ing_id[$i];
// echo $this->post('qty');exit;
$recpingdata['qty'] = $this->post('qty')[$i];
$recpingdata['measurement_id'] = $this->post('measurement_id')[$i];
$recpingdata['measure_amount'] = $this->calculateMetric($this->post('measurement_name'), $this->post('qty')[$i]);
$recpingdata['measure_type'] = $this->post('measure_type');
$this->Master_recipeingredients_model->insert($recpingdata);
}
//$this->Master_recipeingredients_model->insert($recpingdata);	// MasterID
}
 */
		//print_r($recpingdata);exit;

		//echo "id :- ".$insertId;exit;
		return $insertId;
	}
	/***************************** END ************************/

	/*********************** Arabic Recipes ***********************/
	function addReciepiAR_post() {

		$insertId = NULL;

		$ingarray = array();

		$recpdata = [
			'recipe_name' => $this->post('recipe_name'),
			'recipe_description' => $this->post('recipe_description'),
			'recipe_method' => $this->post('recipe_method'),
			'preparation_time' => $this->post('preparation_time'),
			'cooking_time' => $this->post('cooking_time'),
			'serving' => $this->post('serving'),
			'recipe_category_ar_id' => $this->post('recipe_category'),
			'recipe_subcategory_ar_id' => $this->post('recipe_subcategory'),
			'user_id' => $this->post('user_id'),
		];

		$insertId = $this->db->insert('recipes_ar', $recpdata);
		// Recipes data stored

		$ingrdients = json_decode($this->post('ingredients')); // Convert in array

		for ($i = 0; $i < count($ingrdients); $i++) {

			if (is_numeric($ingrdients[$i]->ingredient_name)) {
				$ingarray['ingredient_ar_id'] = $ingrdients[$i]->ingredient_name;
				$ingarray['qty'] = $ingrdients[$i]->qty;
				$ingarray['measure_type'] = $ingrdients[$i]->measure_type;
				$ingarray['measure_amount'] = $this->calculateMetric($ingrdients[$i]->measurement_name, $ingrdients[$i]->qty);
				$ingarray['measurement_id'] = $ingrdients[$i]->measurement_id;
				$ingarray['recipe_ar_id'] = $insertId;

				$this->Master_recipeingredients_ar_model->insert($ingarray); // Recipes and ingredient master data stored.
			} else {
				$ing_id[] = $this->insert('ingredients_ar', array(
					'name' => $ingrdients[$i]->ingredient_name)
				);

				$ingarray['ingredient_ar_id'] = $ing_id[$i];
				$ingarray['qty'] = $ingrdients[$i]->qty;
				$ingarray['measure_type'] = $ingrdients[$i]->measure_type;
				$ingarray['measure_amount'] = $this->calculateMetric($ingrdients[$i]->measurement_name, $ingrdients[$i]->qty);
				$ingarray['measurement_id'] = $ingrdients[$i]->measurement_id;
				$ingarray['recipe_ar_id'] = $insertId;

				$this->Master_recipeingredients_ar_model->insert($ingarray); // Recipes and ingredient master data stored.
			}

		}

		/*if(is_numeric($this->post('ingredient_name'))) {

						$recpdata = [
							'recipe_name' 			=> $this->post('recipe_name'),
							'recipe_description' 	=> $this->post('recipe_description'),
							'recipe_method' 		=> $this->post('recipe_method'),
							'preparation_time' 		=> $this->post('preparation_time'),
							'cooking_time' 			=> $this->post('cooking_time'),
							'serving'			 	=> $this->post('serving'),
							'recipe_category_ar_id' 	=> $this->post('recipe_category'),
							'recipe_subcategory_ar_id' => $this->post('recipe_subcategory'),
							'user_id' 				=> $this->post('user_id')
							];

						$recipie_id	= $this->db->insert('recipes_ar',$recpdata); //

						$masterdata = [
							'recipe_ar_id'				=> $recipie_id,
							];

						for($i=0;$i<count($_POST['ingredient_name']);$i++){
			                  $masterdata['ingredient_ar_id'] = $this->post('ingredient_name')[$i];
			                 // echo $this->post('qty');exit;
			                  $masterdata['qty'] = $this->post('qty')[$i];
			                  $masterdata['measurement_id'] = $this->post('measurement_id')[$i];
			                  $masterdata['measure_amount'] = $this->calculateMetric($this->post('measurement_name'), $this->post('qty')[$i]);
			                  $masterdata['measure_type'] = $this->post('measure_type');
			                  $this->Master_recipeingredients_ar_model->insert($masterdata);
			             }
						//$this->Master_recipeingredients_ar_model->insert($masterdata);	// MasterID

						return $recipe_id;
					}
					else{

						// Ingredients details stored
						for($i=0;$i<count($this->post('ingredient_name'));$i++)
						{
							$ing_id[] = $this->insert('ingredients_ar',array(
									'name' => $this->post('ingredient_name')[$i])
									);
						}

						$recpdata = [
							'recipe_name' 			=> $this->post('recipe_name'),
							'recipe_description' 	=> $this->post('recipe_description'),
							'recipe_method' 		=> $this->post('recipe_method'),
							'preparation_time' 		=> $this->post('preparation_time'),
							'cooking_time' 			=> $this->post('cooking_time'),
							'serving'			 	=> $this->post('serving'),
							'recipe_category_ar_id' 	=> $this->post('recipe_category'),
							'recipe_subcategory_ar_id' => $this->post('recipe_subcategory'),
							'user_id' 				=> $this->post('user_id')
							];

					$recipie_id	= $this->insert('recipes_ar',$recpdata); //

					$recpingdata = [
							'recipe_ar_id'				=> $recipie_id,
							];

					for($i=0;$i<count($_POST['ingredient_name']);$i++){
			                  $recpingdata['ingredient_ar_id'] = $ing_id[$i];
			                 // echo $this->post('qty');exit;
			                  $recpingdata['qty'] = $this->post('qty')[$i];
			                  $recpingdata['measurement_id'] = $this->post('measurement_id')[$i];
			                  $recpingdata['measure_amount'] = $this->calculateMetric($this->post('measurement_name'), $this->post('qty')[$i]);
			                  $recpingdata['measure_type'] = $this->post('measure_type');
			                  $this->Master_recipeingredients_ar_model->insert($recpingdata);
			             }

					return $recipe_id;
		*/
		return $insertId;
	}

	/********************************* END ***********************/

	/**************************** French details ***********************/
	function addReciepiFR_post() {

		$insertId = NULL;

		$ingarray = array();

		$recpdata = [
			'recipe_name' => $this->post('recipe_name'),
			'recipe_description' => $this->post('recipe_description'),
			'recipe_method' => $this->post('recipe_method'),
			'preparation_time' => $this->post('preparation_time'),
			'cooking_time' => $this->post('cooking_time'),
			'serving' => $this->post('serving'),
			'recipe_category_fr_id' => $this->post('recipe_category'),
			'recipe_subcategory_fr_id' => $this->post('recipe_subcategory'),
			'user_id' => $this->post('user_id'),
		];

		$insertId = $this->db->insert('recipes_fr', $recpdata);
		// Recipes data stored

		$ingrdients = json_decode($this->post('ingredients')); // Convert in array

		for ($i = 0; $i < count($ingrdients); $i++) {

			if (is_numeric($ingrdients[$i]->ingredient_name)) {
				$ingarray['ingredient_fr_id'] = $ingrdients[$i]->ingredient_name;
				$ingarray['qty'] = $ingrdients[$i]->qty;
				$ingarray['measure_type'] = $ingrdients[$i]->measure_type;
				$ingarray['measure_amount'] = $this->calculateMetric($ingrdients[$i]->measurement_name, $ingrdients[$i]->qty);
				$ingarray['measurement_id'] = $ingrdients[$i]->measurement_id;
				$ingarray['recipe_ar_id'] = $insertId;

				$this->Master_recipeingredients_fr_model->insert($ingarray); // Recipes and ingredient master data stored.
			} else {
				$ing_id[] = $this->insert('ingredients_fr', array(
					'name' => $ingrdients[$i]->ingredient_name)
				);

				$ingarray['ingredient_fr_id'] = $ing_id[$i];
				$ingarray['qty'] = $ingrdients[$i]->qty;
				$ingarray['measure_type'] = $ingrdients[$i]->measure_type;
				$ingarray['measure_amount'] = $this->calculateMetric($ingrdients[$i]->measurement_name, $ingrdients[$i]->qty);
				$ingarray['measurement_id'] = $ingrdients[$i]->measurement_id;
				$ingarray['recipe_ar_id'] = $insertId;

				$this->Master_recipeingredients_fr_model->insert($ingarray); // Recipes and ingredient master data stored.
			}

		}

		/*if(is_numeric($this->post('ingredient_name'))) {

						$recpdata = [
							'recipe_name' 			=> $this->post('recipe_name'),
							'recipe_description' 	=> $this->post('recipe_description'),
							'recipe_method' 		=> $this->post('recipe_method'),
							'preparation_time' 		=> $this->post('preparation_time'),
							'cooking_time' 			=> $this->post('cooking_time'),
							'serving'			 	=> $this->post('serving'),
							'recipe_category_fr_id' 	=> $this->post('recipe_category'),
							'recipe_subcategory_fr_id' => $this->post('recipe_subcategory'),
							'user_id' 				=> $this->post('user_id')
							];

						$recipie_id	= $this->insert('recipes_fr',$recpdata); //

						$masterdata = [
							'recipe_fr_id'			=> $recipie_id,
							];

						for($i=0;$i<count($_POST['ingredient_name']);$i++){
			                  $masterdata['ingredient_fr_id'] = $this->post('ingredient_name')[$i];
			                 // echo $this->post('qty');exit;
			                  $masterdata['qty'] = $this->post('qty')[$i];
			                  $masterdata['measurement_id'] = $this->post('measurement_id')[$i];
			                  $masterdata['measure_amount'] = $this->calculateMetric($this->post('measurement_name'), $this->post('qty')[$i]);
			                  $masterdata['measure_type'] = $this->post('measure_type');
			                  $this->Master_recipeingredients_fr_model->insert($masterdata);
			             }

						//$this->Master_recipeingredients_fr_model->insert($masterdata);	// MasterID

						return $recipe_id;
					}
					else{

						for($i=0;$i<count($this->post('ingredient_name'));$i++)
						{
							$ing_id[] = $this->insert('ingredients_fr',array(
									'name' => $this->post('ingredient_name')[$i])
							);
						}

						$recpdata = [
							'recipe_name' 			=> $this->post('recipe_name'),
							'recipe_description' 	=> $this->post('recipe_description'),
							'recipe_method' 		=> $this->post('recipe_method'),
							'preparation_time' 		=> $this->post('preparation_time'),
							'cooking_time' 			=> $this->post('cooking_time'),
							'serving'			 	=> $this->post('serving'),
							'recipe_category_fr_id' => $this->post('recipe_category'),
							'recipe_subcategory_fr_id' => $this->post('recipe_subcategory'),
							'user_id' 				=> $this->post('user_id')
							];

					$recipie_id	= $this->insert('recipes_fr',$recpdata); //

					$recpingdata = [
							'recipe_fr_id'			=> $recipie_id,
							];

					for($i=0;$i<count($_POST['ingredient_name']);$i++){
			                  $recpingdata['ingredient_fr_id'] = $ing_id[$i];
			                 // echo $this->post('qty');exit;
			                  $recpingdata['qty'] = $this->post('qty')[$i];
			                  $recpingdata['measurement_id'] = $this->post('measurement_id')[$i];
			                  $recpingdata['measure_amount'] = $this->calculateMetric($this->post('measurement_name'), $this->post('qty')[$i]);
			                  $recpingdata['measure_type'] = $this->post('measure_type');
			                  $this->Master_recipeingredients_fr_model->insert($recpingdata);
			             }

					return $recipe_id;
		*/
		return $insertId;
	}
	/********************************** END **********************/

	function getMeasurements_get() {
		$tag = $this->get('tag');
		$units = $this->Measurements_model->getUnits($tag);

		if (!empty($units)) {
			$this->response([
				'status' => TRUE,
				'data' => $units,
				'message' => count($units) . ' ' . $this->lang->line('msg_found'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				'data' => [],
				'message' => $this->lang->line('msg_notfound'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

	/*************** Calculate total value of food *********/
	function calculateMetric($param = '', $param2 = '') {
		$total_value = '';
		if ($param == 'gram') {
			$total_value = $param2 * 0.0353;
		} else if ($param == 'kilogram') {
			$total_value = $param2 * 2.2046;
		} else if ($param == 'litre') {
			$total_value = $param2 * 1.0566;
		} else if ($param == 'ml') {
			$total_value = $param2 * 0.0042;
		} else if ($param == 'ounce') {
			$total_value = $param2 * 28.35;
		} else if ($param == 'pounds') {
			$total_value = $param2 * 0.4536;
		} else if ($param == 'quart') {
			$total_value = $param2 * 0.9463;
		} else if ($param == 'cup') {
			$total_value = $param2 * 236.59;
		}

		return $total_value;
	}

	// Get Recipes sub category
	function recipesSubcategory_get() {
		$recipe_cat_id = $this->get('recipe_cat_id');

		$sublists = $this->Recipe_subcategories_model->get_subcategories_by_id($recipe_cat_id);

		if (!empty($sublists)) {
			$this->response([
				'status' => TRUE,
				'data' => $sublists,
				'message' => count($sublists) . ' ' . $this->lang->line('msg_found'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		} else {
			$this->response([
				'status' => FALSE,
				'data' => [],
				'message' => $this->lang->line('msg_notfound'),
			], REST_Controller::HTTP_OK); // OK (200) Reponse code
		}
	}

}