<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Cart extends REST_Controller {

	/**
	 * Constructor
	 *
	 *  @return void
	 */
	function __construct() {
		parent::__construct();
		lang_switcher($_REQUEST['lang']);
		$this->load->model('User_shoppingcart_model');
		$this->load->model('User_shoppingcart_ar_model');
		$this->load->model('User_shoppingcart_fr_model');
	}

	// Add shopping cart food glossary
	function addtoCart_post() {

		$this->addtoCartEN_post();
	}

	// Add shopping cart food glossary details
	function addtoCartEN_post() {
		// Make array of ingredients
		$ingredients = explode(',', $this->post('ingredient_id'));
		$cartArray = array();
		$items = array();

		// Check ingredients already added in cart
		$check = $this->User_shoppingcart_model->checkIngredients($ingredients, $this->post('recipe_id'), $this->post('user_id'));

		foreach ($check as $key => $value) {
			$cartArray[] = $value['ingredient_id'];
		}

		//print_r($cartArray);exit;

		if (!empty($cartArray) && count($cartArray) > 0) {
			for ($i = 0; $i < count($ingredients); $i++) {
				// Check ing_id and create array ing_id !=
				if (!in_array($ingredients[$i], $cartArray)) {
					$items[] = array(
						'recipe_id' => $this->post('recipe_id'),
						'ingredient_id' => $ingredients[$i],
						'user_id' => $this->post('user_id'),
					); // Create items array
				}
			}

			if (!empty($items)) {
				$this->User_shoppingcart_model->create($items); // Added in cart if not in table
				$this->response([
					'status' => TRUE,
					'data' => [],
					'message' => $this->lang->line('recipecart_add'),
				], REST_Controller::HTTP_OK);
			}
			$this->response([
				'status' => FALSE,
				'data' => [],
				'message' => $this->lang->line('cart_exists'),
			], REST_Controller::HTTP_OK);
		} else {
			for ($i = 0; $i < count($ingredients); $i++) {

				$items[] = array(
					'recipe_id' => $this->post('recipe_id'),
					'ingredient_id' => $ingredients[$i],
					'user_id' => $this->post('user_id'),
				);
			}
			$this->User_shoppingcart_model->create($items); // Insert in shoppint cart

			$this->response([
				'status' => TRUE,
				'data' => [],
				'message' => $this->lang->line('recipecart_add'),
			], REST_Controller::HTTP_OK);
		}
	}

	// Get Userwise cartlist
	function getCartdetails_get() {

		$user_ID = $this->get('user_id');
		$lang = $this->get('lang');

		$userCartlist = $this->User_shoppingcart_model->getCartlist($user_ID); // Get user shoppingcart list

		if (!empty($userCartlist)) {
			$this->response([
				'status' => TRUE,
				'data' => $userCartlist,
				'message' => count($userCartlist) . ' ' . $this->lang->line('msg_found'),
			], REST_Controller::HTTP_OK);
		} else {
			$this->response([
				'status' => FALSE,
				'data' => [],
				'message' => $this->lang->line('msg_rec_notfound'),
			], REST_Controller::HTTP_OK);
		}
	}

	// Remove User food glossary list
	function deleteCartIngredients_post() {

		$whereFood = array();
		$lang = $this->post('lang');
		$glossaryFood = json_decode($this->post('glossaryList')); // Convert in array

		for ($i = 0; $i < count($glossaryFood); $i++) {

			$whereFood['ingredient_id'] = $glossaryFood[$i]->ing_id;
			$whereFood['recipe_id'] = $glossaryFood[$i]->recipe_id;
			$whereFood['user_id'] = $glossaryFood[$i]->user_id;
			$success = $this->User_shoppingcart_model->removeFood($whereFood);
		}

		if ($success == 1) {
			$this->response([
				'status' => TRUE,
				'data' => [],
				'message' => $this->lang->line('recipecart_remove'),
			], REST_Controller::HTTP_OK);
		} else {
			$this->response([
				'status' => FALSE,
				'message' => $this->lang->line('cartrecipe_removerrror'),
			], REST_Controller::HTTP_OK);
		}
	}
}