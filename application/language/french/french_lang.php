<?php
// English Message

$lang['msg_found'] = "FR_Row found.";
$lang['msg_rec_found'] = "FR_Record found.";
$lang['msg_rec_notfound'] = "FR_No Record Found.";
$lang['msg_notfound'] = "FR_No data found.";

$lang['email_exists'] = "FR_Email already registered.";
$lang['register_error'] = "FR_Unable to register user.";
$lang['register_success'] = "FR_You have successfully registered.";
$lang['facebook_success'] = "FR_You have successfully registered with facebook.";
$lang['facebook_emailexists'] = "FR_You are already registered with email.";
$lang['login_success'] = "FR_You have been successfully logged in.";
$lang['login_error'] = "FR_Invalid email or password.";
$lang['login_status'] = "FR_Sorry ! your account is inactive. Please contact Administrator.";
$lang['userdetails_found'] = "FR_User profile details found.";
$lang['userdetails_notfound'] = "FR_User profile details not found.";
$lang['profileupdate'] = "FR_User profile updated successfully.";
$lang['profileerror'] = "FR_Error while updating user profile.";
$lang['pwdchange'] = "FR_Password has been successfully changed.";
$lang['userpwd_notfound'] = "FR_User details not found.";
$lang['pwdnot_match'] = "FR_Old password does not match.";
$lang['pwdlink_send'] = "FR_Please check your email, We have sent you a reset password link.";
$lang['pwdlink_error'] = "FR_Email was not sent, please contact your administrator.";
$lang['dbemail_error'] = "FR_Your email is not in our database.";
$lang['code_valid'] = "FR_Code is successfully verfied, Now enter your new password.";
$lang['codeinvalid'] = "FR_Code is not valid";

// Recipes CookBook / Bookmark

$lang['cookbook_add'] = "FR_Recipe successfully added in cookbook.";
$lang['cookbook_exists'] = "FR_You have already added this recipe.";
$lang['no_cookbook'] = "FR_No Cookbook available.";
$lang['cbook_add'] = "FR_Cookbook added successfully.";
$lang['cbook_error'] = "FR_Error while adding cookbook data.";
$lang['cbook_update'] = "FR_Cookbook record updated successfully.";
$lang['cbook_updateerror'] = "FR_Error while updating cookbook record.";
$lang['cbook_delete'] = "FR_CookBook details removed successfully.";
$lang['cbbok_deleteerror'] = "FR_Error while deleting cookbook details.";
$lang['bookmark_notfound'] = "FR_Bookmark details not found.";
$lang['bookmarkrecipe_remove'] = "FR_Recipe removed successfully.";
$lang['bookmarkrecipe_error'] = "FR_Error while removing recipe.";

// Recipes User Like / Bookmark

$lang['recipe_like'] = "FR_Recipe successfully liked.";
$lang['recipe_likeerror'] = "FR_Error to like recipes.";
$lang['pls_like'] = "FR_Please like recipes to add.";

// ShoppingCart Recipes

$lang['cart_exists'] = "FR_Items already added.";
$lang['recipecart_add'] = "FR_Recipe item added in shopping cart successfully.";
$lang['recipecart_remove'] = "FR_Ingredient item removed.";
$lang['cartrecipe_removerrror'] = "FR_Error to remove ingredient item from glossary list.";

// Rating

$lang['rating_add'] = "FR_Rating added successfully.";
$lang['rating_already_add'] = "FR_You have already rated to this recipe.";
$lang['rating_err'] = "FR_Error to give rating.";

// Common

$lang['status'] = "FR_Status";
$lang['status_name_act'] = "FR_Active";
$lang['status_name_dact'] = "FR_Inactive";
$lang['dash_change_pwd'] = "FR_Change Password";
$lang['dash_edit_profile'] = "FR_Edit profile";
$lang['logout_link'] = "FR_Logout";
$lang['btncreate'] = "FR_Create";
$lang['btnremove'] = "FR_Remove";
$lang['btnadd'] = "FR_Add";
$lang['btnupdate'] = "FR_Update";
$lang['btncancel'] = "FR_Cancel";
$lang['btnsave'] = "FR_Save";
$lang['btnsend'] = "FR_Send Notification";
$lang['btnedit'] = "FR_Edit";
$lang['btnfilter'] = "FR_Filter";
$lang['btnreset'] = "FR_Reset";
$lang['btnnotify'] = "FR_Notify";
$lang['welcome'] = "FR_Welcome to Dashboard";
$lang['dashboard_title'] = "FR_Dashboard";

// Sidebar Dashboard Option

$lang['home'] = "FR_Home";
$lang['basic_mngmt'] = "FR_Basic Management";
$lang['basic_user'] = "FR_User";
$lang['basic_unit'] = "FR_Unit";
$lang['basic_ingredients'] = "FR_Ingredients";
$lang['basic_ingredients_category'] = "FR_Ingredients Category";
$lang['basic_ingredients_child'] = "FR_Ingredients";
$lang['basic_recpdirectory'] = "FR_Recipes Directory";
$lang['basic_recpcat'] = "FR_Recipe Category";
$lang['basic_recpsubcat'] = "FR_Recipe SubCategory";
$lang['basic_recipes'] = "FR_Recipes";
$lang['basic_advsearch'] = "FR_Advance Search";
$lang['basic_setting'] = "FR_System Setting";
$lang['basic_configuration'] = "FR_Site Configuration";

// Unit Label

$lang['unit_index'] = "FR_Unit";
$lang['unit_edit'] = "FR_Edit Unit";
$lang['unit_add'] = "FR_Unit";
$lang['unit_name'] = "FR_Unit Name";
$lang['unit_name_ar'] = "FR_Unit Name_ar";
$lang['unit_name_fr'] = "FR_Unit Name_fr";
$lang['amount'] = "FR_Amount (in Grams)";
$lang['amount_us'] = "FR_Amount (in Ounce)";

// Ingredient Category Label

$lang['ing_catindex'] = "FR_Ingredient Category";
$lang['ing_catadd'] = "FR_Add Ingredient Category";
$lang['ing_catedit'] = "FR_Edit Ingredient Category";
$lang['ing_category'] = "FR_Ingredient Category";
$lang['ing_category_ar'] = "FR_Ingredient Category_ar";
$lang['ing_category_fr'] = "FR_Ingredient Category_fr";

// Ingredient Label

$lang['ing_index'] = "FR_Ingredients";
$lang['ing_add'] = "FR_Add Ingredients";
$lang['ing_edit'] = "FR_Edit Ingredients";
$lang['name'] = "FR_Name";
$lang['name_ar'] = "FR_Name_ar";
$lang['name_fr'] = "FR_Name_fr";
$lang['description'] = "FR_Description";
$lang['description_ar'] = "FR_Description_ar";
$lang['description_fr'] = "FR_Description_fr";
$lang['ingredient_category'] = "FR_Ingredient Category";
$lang['nutrition_unit'] = "FR_Nutrition Unit";
$lang['protein'] = "FR_Protein (100 Grams)";
$lang['fat'] = "FR_Fat (100 Grams)";
$lang['carbs'] = "FR_Carbs (100 Grams)";
$lang['kcal'] = "FR_Kcal (100 Grams)";

// Recipe Category Label

$lang['recp_catindex'] = "FR_Recipe Category";
$lang['recp_catadd'] = "FR_Add Recipe Category";
$lang['recp_catedit'] = "FR_Edit Recipe Category";
$lang['recp_name'] = "FR_Recipe Category";
$lang['recp_name_ar'] = "FR_Recipe Category_ar";
$lang['recp_name_fr'] = "FR_Recipe Category_fr";

// Recipe SubCategory Label

$lang['recp_subcatindex'] = "FR_Recipe Sub Category";
$lang['recp_subcatadd'] = "FR_Add Recipe Sub Category";
$lang['recp_subcatedit'] = "FR_Edit Recipe Category";
$lang['recp_cat'] = "FR_Recipe Category";
$lang['recp_subcat'] = "FR_Recipe Sub Category";
$lang['recp_subcat_ar'] = "FR_Recipe Sub Category_ar";
$lang['recp_subcat_fr'] = "FR_Recipe Sub Category_fr";

// Recipe Label

$lang['recp_index'] = "FR_Recipes";
$lang['recp_add'] = "FR_Add Recipe";
$lang['recp_edit'] = "FR_Edit Recipe";
$lang['name'] = "FR_Name";
$lang['name_ar'] = "FR_Name_ar";
$lang['name_fr'] = "FR_Name_fr";
$lang['media'] = "FR_Media Upload";
$lang['description'] = "FR_Description";
$lang['description_ar'] = "FR_Description_ar";
$lang['description_fr'] = "FR_Description_fr";
$lang['method'] = "FR_Preparation Method";
$lang['img_vid'] = "FR_Image / Video";
$lang['serving'] = "FR_Serving";
$lang['ingredients'] = "FR_Ingredients";
$lang['qty'] = "FR_Qty";
$lang['unit'] = "FR_Unit";
$lang['prep_time'] = "FR_Preparation Time";
$lang['cook_time'] = "FR_Cooking Time";
$lang['notes'] = "FR_Notes";
$lang['featured'] = "FR_Is Featured";
$lang['recipe_category'] = "FR_Recipe Category";
$lang['sub_category'] = "FR_Sub Category";
$lang['level'] = "FR_Level";
$lang['step_no'] = "FR_Step No";
$lang['step_desc'] = "FR_Description";
$lang['step_imgvid'] = "FR_Image / Video";
$lang['source_title'] = "FR_Source Title";
$lang['source_link'] = "FR_Source Link";

// Advance Search Label

$lang['search_index'] = "FR_Advance Search";
$lang['id'] = "FR_ID";
$lang['recp_name_search'] = "FR_Recipe Name";
$lang['prep_time_search'] = "FR_Preparation Time";
$lang['cook_time_search'] = "FR_Cooking Time";
$lang['ingname_search'] = "FR_Ingredient Name";
$lang['recp_cat_search'] = "FR_Recipe Category";

// Site configuration Label

$lang['site_index'] = "FR_System Configuration";
$lang['site_gen'] = "FR_General";
$lang['site_emailset'] = "FR_Email Setting";
$lang['site_title'] = "FR_Title";
$lang['site_logo'] = "FR_Site Logo";
$lang['site_description'] = "FR_Description";
$lang['site_lang'] = "FR_Language";
$lang['smtp_protocol'] = "FR_Smtp Protocol";
$lang['smtp_host'] = "FR_Smtp Host";
$lang['smtp_port'] = "FR_Smtp Port";
$lang['smtp_user'] = "FR_Smtp Username";
$lang['smtp_pwd'] = "FR_Smtp Password";

// Change password Label

$lang['ch_pwd'] = "FR_Password";
$lang['ch_newpwd'] = "FR_New Password";
$lang['ch_confpwd'] = "FR_Confirm Password";

// Admin Profile Label

$lang['admin_fname'] = "FR_First Name";
$lang['admin_lname'] = "FR_Last Name";
$lang['admin_email'] = "FR_Email";
$lang['admin_gender'] = "FR_Gender";
$lang['admin_mobile'] = "FR_Mobile";
$lang['admin_city'] = "FR_City";
$lang['admin_address'] = "FR_Address";
$lang['admin_photo'] = "FR_Photo";

// User Profile Label

$lang['user_index'] = "FR_User";
$lang['user_edit'] = "FR_Edit User";
$lang['user_fname'] = "FR_First Name";
$lang['user_email'] = "FR_Email";
$lang['user_pwd'] = "FR_Password";
$lang['user_gender'] = "FR_Gender";
$lang['user_mobile'] = "FR_Mobile";
$lang['user_address'] = "FR_Address";

// Notification

$lang['title'] = "FR_Title";
$lang['page_header_title'] = 'FR_Notifications';
$lang['page_title_index'] = "FR_Notification";
$lang['page_title_add'] = "FR_Add Notification";
$lang['page_title_name'] = "FR_Name";
$lang['page_title_edit'] = "FR_Edit Notification";
$lang['page_send_form'] = "FR_Send Notification";
$lang['page_send_title'] = "FR_Activity";
$lang['page_send_time'] = "FR_Time interval";
$lang['page_send_msg'] = "FR_Message";
?>