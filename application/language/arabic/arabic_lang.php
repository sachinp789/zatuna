<?php
// Arabic Message

$lang['msg_found'] = "AR_Row found.";
$lang['msg_rec_found'] = "AR_Record found.";
$lang['msg_rec_notfound'] = "AR_No Record Found.";
$lang['msg_notfound'] = "AR_No data found.";

$lang['email_exists'] = "AR_Email already registered.";
$lang['register_error'] = "AR_Unable to register user.";
$lang['register_success'] = "AR_You have successfully registered.";
$lang['facebook_success'] = "AR_You have successfully registered with facebook.";
$lang['facebook_emailexists'] = "AR_You are already registered with email.";
$lang['login_success'] = "AR_You have been successfully logged in.";
$lang['login_error'] = "AR_Invalid email or password.";
$lang['login_status'] = "AR_Sorry ! your account is inactive. Please contact Administrator.";
$lang['userdetails_found'] = "AR_User profile details found.";
$lang['userdetails_notfound'] = "AR_User profile details not found.";
$lang['profileupdate'] = "AR_User profile updated successfully.";
$lang['profileerror'] = "AR_Error while updating user profile.";
$lang['pwdchange'] = "AR_Password has been successfully changed.";
$lang['userpwd_notfound'] = "AR_User details not found.";
$lang['pwdnot_match'] = "AR_Old password does not match.";
$lang['pwdlink_send'] = "AR_Please check your email, We have sent you a reset password link.";
$lang['pwdlink_error'] = "AR_Email was not sent, please contact your administrator.";
$lang['dbemail_error'] = "AR_Your email is not in our database.";
$lang['code_valid'] = "AR_Code is successfully verfied, Now enter your new password.";
$lang['codeinvalid'] = "AR_Code is not valid";

// Recipes CookBook / Bookmark

$lang['cookbook_add'] = "AR_Recipe successfully added in cookbook.";
$lang['cookbook_exists'] = "AR_You have already added this recipe.";
$lang['no_cookbook'] = "AR_No Cookbook available.";
$lang['cbook_add'] = "AR_Cookbook added successfully.";
$lang['cbook_error'] = "AR_Error while adding cookbook data.";
$lang['cbook_update'] = "AR_Cookbook record updated successfully.";
$lang['cbook_updateerror'] = "AR_Error while updating cookbook record.";
$lang['cbook_delete'] = "AR_CookBook details removed successfully.";
$lang['cbbok_deleteerror'] = "AR_Error while deleting cookbook details.";
$lang['bookmark_notfound'] = "AR_Bookmark details not found.";
$lang['bookmarkrecipe_remove'] = "AR_Recipe removed successfully.";
$lang['bookmarkrecipe_error'] = "AR_Error while removing recipe.";

// Recipes User Like / Bookmark

$lang['recipe_like'] = "AR_Recipe successfully liked.";
$lang['recipe_likeerror'] = "AR_Error to like recipes.";
$lang['pls_like'] = "AR_Please like recipes to add.";

// ShoppingCart Recipes

$lang['cart_exists'] = "AR_Items already added.";
$lang['recipecart_add'] = "AR_Recipe item added in shopping cart successfully.";
$lang['recipecart_remove'] = "AR_Ingredient item removed.";
$lang['cartrecipe_removerrror'] = "AR_Error to remove ingredient item from glossary list.";

// Rating

$lang['rating_add'] = "AR_Rating added successfully.";
$lang['rating_already_add'] = "AR_You have already rated to this recipe.";
$lang['rating_err'] = "AR_Error to give rating.";

// Common

$lang['status'] = "AR_Status";
$lang['status_name_act'] = "AR_Active";
$lang['status_name_dact'] = "AR_Inactive";
$lang['dash_change_pwd'] = "AR_Change Password";
$lang['dash_edit_profile'] = "AR_Edit profile";
$lang['logout_link'] = "AR_Logout";
$lang['btncreate'] = "AR_Create";
$lang['btnremove'] = "AR_Remove";
$lang['btnadd'] = "AR_Add";
$lang['btnupdate'] = "AR_Update";
$lang['btncancel'] = "AR_Cancel";
$lang['btnsave'] = "AR_Save";
$lang['btnsend'] = "AR_Send Notification";
$lang['btnedit'] = "AR_Edit";
$lang['btnfilter'] = "AR_Filter";
$lang['btnreset'] = "AR_Reset";
$lang['btnnotify'] = "AR_Notify";
$lang['welcome'] = "AR_Welcome to Dashboard";
$lang['dashboard_title'] = "AR_Dashboard";

// Sidebar Dashboard Option

$lang['home'] = "AR_Home";
$lang['basic_mngmt'] = "AR_Basic Management";
$lang['basic_user'] = "AR_User";
$lang['basic_unit'] = "AR_Unit";
$lang['basic_ingredients'] = "AR_Ingredients";
$lang['basic_ingredients_category'] = "AR_Ingredients Category";
$lang['basic_ingredients_child'] = "AR_Ingredients";
$lang['basic_recpdirectory'] = "AR_Recipes Directory";
$lang['basic_recpcat'] = "AR_Recipe Category";
$lang['basic_recpsubcat'] = "AR_Recipe SubCategory";
$lang['basic_recipes'] = "AR_Recipes";
$lang['basic_advsearch'] = "AR_Advance Search";
$lang['basic_setting'] = "AR_System Setting";
$lang['basic_configuration'] = "AR_Site Configuration";

// Unit Label

$lang['unit_index'] = "AR_Unit";
$lang['unit_edit'] = "AR_Edit Unit";
$lang['unit_add'] = "AR_Unit";
$lang['unit_name'] = "AR_Unit Name";
$lang['unit_name_ar'] = "AR_Unit Name_ar";
$lang['unit_name_fr'] = "AR_Unit Name_fr";
$lang['amount'] = "AR_Amount (in Grams)";
$lang['amount_us'] = "AR_Amount (in Ounce)";

// Ingredient Category Label

$lang['ing_catindex'] = "AR_Ingredient Category";
$lang['ing_catadd'] = "AR_Add Ingredient Category";
$lang['ing_catedit'] = "AR_Edit Ingredient Category";
$lang['ing_category'] = "AR_Ingredient Category";
$lang['ing_category_ar'] = "AR_Ingredient Category_ar";
$lang['ing_category_fr'] = "AR_Ingredient Category_fr";

// Ingredient Label

$lang['ing_index'] = "AR_Ingredients";
$lang['ing_add'] = "AR_Add Ingredients";
$lang['ing_edit'] = "AR_Edit Ingredients";
$lang['name'] = "AR_Name";
$lang['name_ar'] = "AR_Name_ar";
$lang['name_fr'] = "AR_Name_fr";
$lang['description'] = "AR_Description";
$lang['description_ar'] = "AR_Description_ar";
$lang['description_fr'] = "AR_Description_fr";
$lang['ingredient_category'] = "AR_Ingredient Category";
$lang['nutrition_unit'] = "AR_Nutrition Unit";
$lang['protein'] = "AR_Protein (100 Grams)";
$lang['fat'] = "AR_Fat (100 Grams)";
$lang['carbs'] = "AR_Carbs (100 Grams)";
$lang['kcal'] = "AR_Kcal (100 Grams)";

// Recipe Category Label

$lang['recp_catindex'] = "AR_Recipe Category";
$lang['recp_catadd'] = "AR_Add Recipe Category";
$lang['recp_catedit'] = "AR_Edit Recipe Category";
$lang['recp_name'] = "AR_Recipe Category";
$lang['recp_name_ar'] = "AR_Recipe Category_ar";
$lang['recp_name_fr'] = "AR_Recipe Category_fr";

// Recipe SubCategory Label

$lang['recp_subcatindex'] = "AR_Recipe Sub Category";
$lang['recp_subcatadd'] = "AR_Add Recipe Sub Category";
$lang['recp_subcatedit'] = "AR_Edit Recipe Category";
$lang['recp_cat'] = "AR_Recipe Category";
$lang['recp_subcat'] = "AR_Recipe Sub Category";
$lang['recp_subcat_ar'] = "AR_Recipe Sub Category_ar";
$lang['recp_subcat_fr'] = "AR_Recipe Sub Category_fr";

// Recipe Label

$lang['recp_index'] = "AR_Recipes";
$lang['recp_add'] = "AR_Add Recipe";
$lang['recp_edit'] = "AR_Edit Recipe";
$lang['name'] = "AR_Name";
$lang['name_ar'] = "AR_Name_ar";
$lang['name_fr'] = "AR_Name_fr";
$lang['media'] = "AR_Media Upload";
$lang['description'] = "AR_Description";
$lang['description_ar'] = "AR_Description_ar";
$lang['description_fr'] = "AR_Description_fr";
$lang['method'] = "AR_Preparation Method";
$lang['img_vid'] = "AR_Image / Video";
$lang['serving'] = "AR_Serving";
$lang['ingredients'] = "AR_Ingredients";
$lang['qty'] = "AR_Qty";
$lang['unit'] = "AR_Unit";
$lang['prep_time'] = "AR_Preparation Time";
$lang['cook_time'] = "AR_Cooking Time";
$lang['notes'] = "AR_Notes";
$lang['featured'] = "AR_Is Featured";
$lang['recipe_category'] = "AR_Recipe Category";
$lang['sub_category'] = "AR_Sub Category";
$lang['level'] = "AR_Level";
$lang['step_no'] = "AR_Step No";
$lang['step_desc'] = "AR_Description";
$lang['step_imgvid'] = "AR_Image / Video";
$lang['source_title'] = "AR_Source Title";
$lang['source_link'] = "AR_Source Link";

// Advance Search Label

$lang['search_index'] = "AR_Advance Search";
$lang['id'] = "AR_ID";
$lang['recp_name_search'] = "AR_Recipe Name";
$lang['prep_time_search'] = "AR_Preparation Time";
$lang['cook_time_search'] = "AR_Cooking Time";
$lang['ingname_search'] = "AR_Ingredient Name";
$lang['recp_cat_search'] = "AR_Recipe Category";

// Site configuration Label

$lang['site_index'] = "AR_System Configuration";
$lang['site_gen'] = "AR_General";
$lang['site_emailset'] = "AR_Email Setting";
$lang['site_title'] = "AR_Title";
$lang['site_logo'] = "AR_Site Logo";
$lang['site_description'] = "AR_Description";
$lang['site_lang'] = "AR_Language";
$lang['smtp_protocol'] = "AR_Smtp Protocol";
$lang['smtp_host'] = "AR_Smtp Host";
$lang['smtp_port'] = "AR_Smtp Port";
$lang['smtp_user'] = "AR_Smtp Username";
$lang['smtp_pwd'] = "AR_Smtp Password";

// Change password Label

$lang['ch_pwdindex'] = "AR_Change Password";
$lang['ch_pwd'] = "AR_Password";
$lang['ch_newpwd'] = "AR_New Password";
$lang['ch_confpwd'] = "AR_Confirm Password";

// Admin Profile Label

$lang['admin_profile'] = "AR_Edit Profile";
$lang['admin_fname'] = "AR_First Name";
$lang['admin_lname'] = "AR_Last Name";
$lang['admin_email'] = "AR_Email";
$lang['admin_gender'] = "AR_Gender";
$lang['admin_mobile'] = "AR_Mobile";
$lang['admin_city'] = "AR_City";
$lang['admin_address'] = "AR_Address";
$lang['admin_photo'] = "AR_Photo";

// User Profile Label

$lang['user_index'] = "AR_User";
$lang['user_edit'] = "AR_Edit User";
$lang['user_fname'] = "AR_First Name";
$lang['user_email'] = "AR_Email";
$lang['user_pwd'] = "AR_Password";
$lang['user_gender'] = "AR_Gender";
$lang['user_mobile'] = "AR_Mobile";
$lang['user_address'] = "AR_Address";

// Notification

$lang['title'] = "AR_Title";
$lang['page_header_title'] = 'AR_Notifications';
$lang['page_title_index'] = "AR_Notification";
$lang['page_title_add'] = "AR_Add Notification";
$lang['page_title_name'] = "AR_Name";
$lang['page_title_edit'] = "AR_Edit Notification";
$lang['page_send_form'] = "AR_Send Notification";
$lang['page_send_title'] = "AR_Activity";
$lang['page_send_time'] = "AR_Time interval";
$lang['page_send_msg'] = "AR_Message";
?>