<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*  Start Header*/
$lang['title'] = 'كبار الأطباء';
$lang['errorLogin'] = 'البريد الإلكتروني أو كلمة السر خاطئة';
$lang['registerSuccess'] = 'التسجيل بنجاح';
$lang['forgotpasswordsuccess'] = 'يرجى التحقق من البريد الإلكتروني إلى كلمة مرور جديدة';
$lang['forgotEmailNotFound'] = 'لم يتم تسجيل البريد الإلكتروني في النظام';
$lang['menuHome'] = 'الصفحة الرئيسية';
$lang['menuDoctors'] = 'الأطباء';
$lang['menuContactUs'] = 'اتصل بنا';
$lang['menuSignIn'] = 'تسجيل الدخول';
$lang['menuOR'] = 'أو';
$lang['menuRegister'] = 'تسجيل';
$lang['menuMenu'] = 'قائمة طعام';

/*  End Header*/

/*  Start Footer*/

$lang['footerContent'] = 'أبجد هوز دولور الجلوس امات، إيليت دولور. ماسا';
$lang['footerAddress'] = '123 اكليس الطريق القديم، طريق سالفورد الجديد، الشرقي';
$lang['footerEmail'] = ' info@domain.com';
$lang['footerPhone'] = '+44 123 456 788-9';
$lang['footerContentB'] = 'أبجد هوز دولور الجلوس امات،إيليت. دولور. ماسا ';
$lang['footerAddressB'] = '123 اكليس الطريق القديم، طريق سالفورد الجديد، الشرقي';
$lang['footerEmailB'] = ' info@domain.com';
$lang['footerPhoneB'] = '+44 123 456 788-9';
$lang['footerUseFullLink'] = 'روابط مفيدة';
$lang['footerMenuAboutUs'] = 'معلومات عنا';
$lang['footerMenuLatestBlog'] = 'أحدث بلوق';
$lang['footerMenuTermCondition'] = 'الأحكام والشروط';
$lang['footerMenuPrivacyPolicy'] = ' سياسة الخصوصية';
$lang['footerMenuContactUS'] = 'اتصل بنا';
$lang['footerLine'] = '2016 جميع الحقوق محفوظة';
$lang['footerCopyRight'] = 'وثيقة المباشر';
$lang['Login'] = 'تسجيل الدخول';
$lang['Register'] = 'تسجيل';
$lang['loginEmail'] = 'البريد الإلكتروني';
$lang['loginPassword'] = 'كلمه السر';
$lang['loginForgotPass'] = 'هل نسيت كلمة المرور؟ ';
$lang['loginSubmitBTN'] = 'تسجيل الدخول';
$lang['loginFacebook'] = 'تسجيل الدخول باستخدام الفيسبوك ';
$lang['loginGuest'] = 'أدخل كضيف';
$lang['regFirstName'] = 'الاسم الاول';
$lang['regLastName'] = 'الكنية';
$lang['regEmail'] = 'البريد الإلكتروني';
$lang['regMobileNumber'] = 'رقم الهاتف المحمول';
$lang['regPassword'] = 'كلمه السر';
$lang['regCPassword'] = 'تأكيد كلمة المرور';
$lang['regAddress'] = 'عنوان';
$lang['regCountry'] = 'بلد';
$lang['regNewLatter'] = 'النشرة الإخبارية';
$lang['regTermCondition'] = 'الأحكام والشروط';
$lang['regBTN'] = 'تسجيل';
$lang['regFacebook'] = 'تسجيل الدخول باستخدام الفيسبوك';
$lang['regForgotPass'] = 'هل نسيت كلمة المرور';
$lang['regSignIn'] = 'تسجيل الدخول';
$lang['regSignUp'] = 'هل نسيت كلمة المرور';
$lang['ForgotPassword'] = 'هل نسيت كلمة المرور';
$lang['forgotPassEmail'] = 'البريد الإلكتروني';
$lang['forgotPassSubmitBTN'] = 'عرض';
$lang['validRegFirstname'] = 'أدخل الإسم الأول';
$lang['validRegfirstValid'] = 'إدخال اسم صالح';
$lang['validRegLastName'] = 'إدخال اسم آخر';
$lang['validRegLastvalid'] = 'إدخال اسم صالح';
$lang['validRegEmail'] = 'إدخال اسم صالح';
$lang['validRegEmailvalid'] = 'أدخل بريد إلكتروني صالح';
$lang['validRegEmailexist'] = 'البريد الالكتروني موجود بالفعل';
$lang['validRegMobile'] = 'إدخال أي المحمول';
$lang['validRegMobilemax'] = 'أدخل أقصى 13 أرقام';
$lang['validRegMobilevalid'] = 'ادخل رقم هاتف خلوي ساري المفعول';
$lang['validRegMobileMin'] = 'إدخال الحد الأدنى لعدد 10 أرقام';
$lang['validRegPassword'] = 'أدخل كلمة المرور';
$lang['validRegPasswordMin'] = 'إدخال الحد الأدنى لعدد 6 أرقام';
$lang['validRegCPassword'] = 'يدخل تأكيد كلمة المرور';
$lang['validRegPasswordEqual'] = 'كلمة المرور و تأكيد كلمة المرور لا تتطابق';
$lang['validRegAddress'] = 'أدخل العنوان';
$lang['validRegCountry'] = 'أدخل العنوان';
$lang['validRegTermCondition'] = 'أوافق على الشروط و الأحكام';
$lang['success'] = 'نجاح';

/*  End Footer*/

/*  Start Home */

$lang['homeSearchDoctors'] = 'الأطباء';
$lang['homeSearchHostpitals'] = 'المستشفيات';
$lang['homeSearchClinic'] = 'عيادات';
$lang['homeSearchLab'] = 'مختبرات';
$lang['homeSearchlocation'] = 'موقع';
$lang['homeSearchSpeciality'] = 'تخصص';
$lang['homeSearchGender'] = 'جنس';
$lang['homeSearchGenderMale'] = 'ذكر';
$lang['homeSearchGenderFemale'] = 'أنثى';
$lang['homeSearchName'] = 'اسم';
$lang['homeSearchBTN'] = 'بحث';
$lang['homeAddList'] = 'إضافة إلى قائمتنا';
$lang['homeAddDoctor'] = 'إضافة الطبيب';
$lang['homeAddHospital'] = 'إضافة المستشفى';
$lang['homeAddClinic'] = 'ضافة العيادة';
$lang['homeAddLab'] = 'إضافة المختبر';
$lang['homeRUDoctor'] = 'هل انت دكتور؟';
$lang['homeRUDoctor2'] = 'سجل الآن والوصول إلى آلاف المرضى';
$lang['homeRUDoctor3'] = 'ونحن نعلم مدى ما يلزم لتصبح واي quali الطبيب فاي إد ذلك اتخذنا بعيدا عن
                  المتاعب لننظر بها للمرضى الخاص بك مرة واحدة لديك واي إد فاي. سوف وثيقة المباشر إعطاء
                    لك سهولة الوصول إلى جميع المرضى، كل ما عليك هو الاشتراك!';

$lang['homeSignUP'] = 'أفتح حساب الأن';
$lang['homeContent1'] = 'أطباء أعلى على اذهب';
$lang['homeContent2'] = 'أحصل على التطبيق';
$lang['homeTopReviews'] = 'استعراضات أعلى';


/*  End Home*/

/*  Start Search */

$lang['searchPlaceHolder'] = 'تخصص';
$lang['searchbtn'] = 'بحث';
$lang['filterTitle'] = 'مرشحات';
$lang['filterLocation'] = 'موقع';
$lang['filterSpeciality'] = 'تخصص';
$lang['filterGender'] = 'جنس';
$lang['filterMale'] = 'ذكر';
$lang['filterFemale'] = 'أنثى';
$lang['filterNamePlace'] = 'اسم';
$lang['filterSearchBtn'] = 'بحث';
$lang['searchSortBy'] = 'ترتيب حسب';
$lang['searchNearByLocation'] = 'مكان قريب';
$lang['searchRatingReview'] = 'تقييم ونقد';
$lang['searchBookMark'] = 'المرجعية';
$lang['searchFeaturedDoctor'] = 'طبيب متميز';
$lang['searchNoRecords'] = 'لا تسجيلات';
$lang['searchGetDirection'] = 'احصل على اتجاه';

/*  End Search */