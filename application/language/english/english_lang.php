<?php
// English Message

$lang['msg_found'] = "Row found.";
$lang['msg_rec_found'] = "Record found.";
$lang['msg_rec_notfound'] = "No Record Found.";
$lang['msg_notfound'] = "No data found.";

$lang['email_exists'] = "Email already registered.";
$lang['register_error'] = "Unable to register user.";
$lang['register_success'] = "You have successfully registered.";
$lang['facebook_success'] = "You have successfully registered with facebook.";
$lang['facebook_emailexists'] = "You are already registered with email.";
$lang['login_success'] = "You have been successfully logged in.";
$lang['login_error'] = "Invalid email or password.";
$lang['login_status'] = "Sorry ! your account is inactive. Please contact Administrator.";
$lang['userdetails_found'] = "User profile details found.";
$lang['userdetails_notfound'] = "User profile details not found.";
$lang['profileupdate'] = "User profile updated successfully.";
$lang['profileerror'] = "Error while updating user profile.";
$lang['pwdchange'] = "Password has been successfully changed.";
$lang['userpwd_notfound'] = "User details not found.";
$lang['pwdnot_match'] = "Old password does not match.";
$lang['pwdlink_send'] = "Please check your email, We have sent you a reset password link.";
$lang['pwdlink_error'] = "Email was not sent, please contact your administrator.";
$lang['dbemail_error'] = "Your email is not in our database.";
$lang['code_valid'] = "Code is successfully verfied, Now enter your new password.";
$lang['codeinvalid'] = "Code is not valid";

// Recipes CookBook / Bookmark

$lang['cookbook_add'] = "Recipe successfully added in cookbook.";
$lang['cookbook_exists'] = "You have already added this recipe.";
$lang['no_cookbook'] = "No Cookbook available.";
$lang['cbook_add'] = "Cookbook added successfully.";
$lang['cbook_error'] = "Error while adding cookbook data.";
$lang['cbook_update'] = "Cookbook record updated successfully.";
$lang['cbook_updateerror'] = "Error while updating cookbook record.";
$lang['cbook_delete'] = "CookBook details removed successfully.";
$lang['cbbok_deleteerror'] = "Error while deleting cookbook details.";
$lang['bookmarkrecipe_remove'] = "Recipe removed successfully.";
$lang['bookmark_notfound'] = "Bookmark details not found.";
$lang['bookmarkrecipe_error'] = "Error while removing recipe.";

// Recipes User Like / Bookmark

$lang['recipe_like'] = "Recipe successfully liked.";
$lang['recipe_likeerror'] = "Error to like recipes.";
$lang['pls_like'] = "Please like recipes to add.";

// ShoppingCart Recipes

$lang['cart_exists'] = "Items already added.";
$lang['recipecart_add'] = "Recipe item added in shopping cart successfully.";
$lang['recipecart_remove'] = "Ingredient item removed.";
$lang['cartrecipe_removerrror'] = "Error to remove ingredient item from glossary list.";

// Rating

$lang['rating_add'] = "Rating added successfully.";
$lang['rating_already_add'] = "You have already rated to this recipe.";
$lang['rating_err'] = "Error to give rating.";

// Common

$lang['status'] = "Status";
$lang['status_name_act'] = "Active";
$lang['status_name_dact'] = "Inactive";
$lang['dash_change_pwd'] = "Change Password";
$lang['dash_edit_profile'] = "Edit profile";
$lang['logout_link'] = "Sign out";
$lang['btncreate'] = "Create";
$lang['btnremove'] = "Remove";
$lang['btnadd'] = "Add";
$lang['btnupdate'] = "Update";
$lang['btncancel'] = "Cancel";
$lang['btnsave'] = "Save";
$lang['btnedit'] = "Edit";
$lang['btnfilter'] = "Filter";
$lang['btnreset'] = "Reset";
$lang['btnsend'] = "Send Notification";
$lang['btnnotify'] = "Notify";
$lang['welcome'] = "Welcome to Dashboard";
$lang['dashboard_title'] = "Dashboard";

// Sidebar Dashboard Option

$lang['home'] = "Home";
$lang['basic_mngmt'] = "Basic Management";
$lang['basic_user'] = "User";
$lang['basic_unit'] = "Unit";
$lang['basic_ingredients'] = "Ingredients";
$lang['basic_ingredients_category'] = "Ingredients Category";
$lang['basic_ingredients_child'] = "Ingredients";
$lang['basic_recpdirectory'] = "Recipes Directory";
$lang['basic_recpcat'] = "Recipe Category";
$lang['basic_recpsubcat'] = "Recipe SubCategory";
$lang['basic_recipes'] = "Recipes";
$lang['basic_advsearch'] = "Advance Search";
$lang['basic_setting'] = "System Setting";
$lang['basic_configuration'] = "Site Configuration";

// Unit Label

$lang['unit_index'] = "Unit";
$lang['unit_edit'] = "Edit Unit";
$lang['unit_add'] = "Add Unit";
$lang['unit_name'] = "Unit Name";
$lang['unit_name_ar'] = "Unit Name_ar";
$lang['unit_name_fr'] = "Unit Name_fr";
$lang['amount'] = "Amount (in Grams)";
$lang['amount_us'] = "Amount (in Ounce)";

// Ingredient Category Label

$lang['ing_catindex'] = "Ingredient Category";
$lang['ing_catadd'] = "Add Ingredient Category";
$lang['ing_catedit'] = "Edit Ingredient Category";
$lang['ing_category'] = "Ingredient Category";
$lang['ing_category_ar'] = "Ingredient Category_ar";
$lang['ing_category_fr'] = "Ingredient Category_fr";

// Ingredient Label

$lang['ing_index'] = "Ingredients";
$lang['ing_add'] = "Add Ingredients";
$lang['ing_edit'] = "Edit Ingredients";
$lang['name'] = "Name";
$lang['name_ar'] = "Name_ar";
$lang['name_fr'] = "Name_fr";
$lang['description'] = "Description";
$lang['description_ar'] = "Description_ar";
$lang['description_fr'] = "Description_fr";
$lang['ingredient_category'] = "Ingredient Category";
$lang['nutrition_unit'] = "Nutrition Unit";
$lang['protein'] = "Protein (100 Grams)";
$lang['fat'] = "Fat (100 Grams)";
$lang['carbs'] = "Carbs (100 Grams)";
$lang['kcal'] = "Kcal (100 Grams)";

// Recipe Category Label

$lang['recp_catindex'] = "Recipe Category";
$lang['recp_catadd'] = "Add Recipe Category";
$lang['recp_catedit'] = "Edit Recipe Category";
$lang['recp_name'] = "Recipe Category";
$lang['recp_name_ar'] = "Recipe Category_ar";
$lang['recp_name_fr'] = "Recipe Category_fr";

// Recipe SubCategory Label

$lang['recp_subcatindex'] = "Recipe Sub Category";
$lang['recp_subcatadd'] = "Add Recipe Sub Category";
$lang['recp_subcatedit'] = "Edit Recipe Category";
$lang['recp_cat'] = "Recipe Category";
$lang['recp_subcat'] = "Recipe Sub Category";
$lang['recp_subcat_ar'] = "Recipe Sub Category_ar";
$lang['recp_subcat_fr'] = "Recipe Sub Category_fr";

// Recipe Label

$lang['recp_index'] = "Recipes";
$lang['recp_add'] = "Add Recipe";
$lang['recp_edit'] = "Edit Recipe";
$lang['name'] = "Name";
$lang['name_ar'] = "Name_ar";
$lang['name_fr'] = "Name_fr";
$lang['media'] = "Media Upload";
$lang['description'] = "Description";
$lang['description_ar'] = "Description_ar";
$lang['description_fr'] = "Description_fr";
$lang['method'] = "Preparation Method";
$lang['img_vid'] = "Image / Video";
$lang['serving'] = "Serving";
$lang['ingredients'] = "Ingredients";
$lang['qty'] = "Qty";
$lang['unit'] = "Unit";
$lang['prep_time'] = "Preparation Time";
$lang['cook_time'] = "Cooking Time";
$lang['notes'] = "Notes";
$lang['featured'] = "Is Featured";
$lang['recipe_category'] = "Recipe Category";
$lang['sub_category'] = "Sub Category";
$lang['level'] = "Level";
$lang['step_no'] = "Step No";
$lang['step_desc'] = "Description";
$lang['step_imgvid'] = "Image / Video";
$lang['source_title'] = "Source Title";
$lang['source_link'] = "Source Link";

// Advance Search Label

$lang['search_index'] = "Advance Search";
$lang['id'] = "ID";
$lang['recp_name_search'] = "Recipe Name";
$lang['prep_time_search'] = "Preparation Time";
$lang['cook_time_search'] = "Cooking Time";
$lang['ingname_search'] = "Ingredient Name";
$lang['recp_cat_search'] = "Recipe Category";

// Site configuration Label

$lang['site_index'] = "System Configuration";
$lang['site_gen'] = "General";
$lang['site_emailset'] = "Email Setting";
$lang['site_index'] = "System Configuration";
$lang['site_title'] = "Title";
$lang['site_logo'] = "Site Logo";
$lang['site_description'] = "Description";
$lang['site_lang'] = "Language";
$lang['smtp_protocol'] = "Smtp Protocol";
$lang['smtp_host'] = "Smtp Host";
$lang['smtp_port'] = "Smtp Port";
$lang['smtp_user'] = "Smtp Username";
$lang['smtp_pwd'] = "Smtp Password";

// Change password Label

$lang['ch_pwd'] = "Password";
$lang['ch_newpwd'] = "New Password";
$lang['ch_confpwd'] = "Confirm Password";

// Admin Profile Label

$lang['admin_fname'] = "First Name";
$lang['admin_lname'] = "Last Name";
$lang['admin_email'] = "Email";
$lang['admin_gender'] = "Gender";
$lang['admin_mobile'] = "Mobile";
$lang['admin_city'] = "City";
$lang['admin_address'] = "Address";
$lang['admin_photo'] = "Photo";

// User Profile Label

$lang['user_index'] = "User";
$lang['user_edit'] = "Edit User";
$lang['user_fname'] = "First Name";
$lang['user_email'] = "Email";
$lang['user_pwd'] = "Password";
$lang['user_gender'] = "Gender";
$lang['user_mobile'] = "Mobile";
$lang['user_address'] = "Address";

// Notification

$lang['title'] = "Title";
$lang['page_header_title'] = 'Notifications';
$lang['page_title_index'] = "Notification";
$lang['page_title_add'] = "Add Notification";
$lang['page_title_name'] = "Name";
$lang['page_title_edit'] = "Edit Notification";
$lang['page_send_form'] = "Send Notification";
$lang['page_send_title'] = "Activity";
$lang['page_send_time'] = "Time interval";
$lang['page_send_msg'] = "Message";
?>