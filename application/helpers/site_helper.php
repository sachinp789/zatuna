<?php
if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

if (!function_exists('site_configuration')) {

	/**
	 * Navigation show hide ul
	 * @param string $page
	 * @param mixed $pages
	 * @return string
	 */
	function site_configuration() {
		//get main CodeIgniter object
		$ci = &get_instance();

		//load databse library
		$ci->load->database();

		$q = $ci->db->get("site_configuration");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return array();
	}

}