<?php

function lang_switcher($lang='en'){

	$ci =& get_instance(); // CI instance

	if($lang == 'fr'){
		$ci->lang->load('french_lang', 'french');
	}
	else if($lang == 'ar'){
		$ci->lang->load('arabic_lang', 'arabic');
	}
	else{
		$ci->lang->load('english_lang', 'english');
	}
}   