<?php
if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

if (!function_exists('push_notification')) {

	function getMessages() {
		//get main CodeIgniter object
		$ci = &get_instance();

		//load databse library
		$ci->load->database();

		$q = $ci->db->get("notifications");
		if ($q->num_rows() > 0) {
			return $q->result();
		}
		return array();

	}
	// Send push notification
	function push_notification($token) {
		$ci = &get_instance(); // CI instance
		$passphrase = '';
		$message = 'Hello';
		$ctx = stream_context_create();

		stream_context_set_option($ctx, 'ssl', 'local_cert', FCPATH . 'zatuna_dev.pem');
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

		$fp = stream_socket_client(
			'ssl://gateway.sandbox.push.apple.com:2195', $error, $errstr, 30, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp) {
			exit("Failed to connect: $err $errstr" . PHP_EOL);
		}
		//$body['body'] =
		$body['aps'] = array(
			'alert' => $message,
			'sound' => 'default',
			'body' => 'Hdkjsdskjdshjdsnb',
		);
		//$body['image'] = base_url() . 'uploads/notifications' . $messages[0]->media;
		$payload = json_encode($body);
		$msg = chr(0) . pack('n', 32) . pack('H*', $token) . pack('n', strlen($payload)) . $payload;
		$result = fwrite($fp, $msg, strlen($msg));

		if (!$result) {
			return "false";
		} else {
			return "true";
		}
		fclose($fp);
	}

}