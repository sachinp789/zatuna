<?php

class User_shoppingcart_model extends MY_Model {
	
	protected $primary_key = 'id'; // PRIMARY KEY

	function create($data) {
    	$this->db->insert_batch('user_shoppingcart', $data);
	}

	function checkIngredients($data,$recipe_id,$user){
		$text = '';
		$this->db->select('ingredient_id');
		$this->db->where_in('ingredient_id', $data);
		$this->db->where('recipe_id', $recipe_id);
		$this->db->where('user_id', $user);
 		$query = $this->db->get('user_shoppingcart');
		if($query->num_rows() > 0){
			$text = $query->result_array();
		}
		else{
			$text = '';
		}
		return $text;	
	}

	// User shopping list of food glossary
	function getCartlist($user){ 
		$this->db->select('us.*,ing.name as ingredient_name,ing.name_ar as ingredient_name_ar,ing.name_fr as ingredient_name_fr,ingcat.id as ingredient_category_id,ingcat.name as ingredient_category_name,ingcat.name_ar as ingredient_category_name_ar,ingcat.name_fr as ingredient_category_name_fr,recp.recipe_name,recp.recipe_name_ar,recp.recipe_name_fr');
		//$recipeslist = $this->db->select('*');
		$this->db->from('user_shoppingcart AS us');
		$this->db->join('recipes AS recp', 'us.recipe_id = recp.id', 'LEFT');
		$this->db->join('ingredients AS ing', 'us.ingredient_id = ing.id', 'LEFT');
		$this->db->join('ingredient_categories AS ingcat', 'ing.ingredient_category_id = ingcat.id', 'LEFT');

		$this->db->where('us.status',0);
		$this->db->where('us.user_id',$user);
		//$this->db->group_by('us.recipe_id');
		$result = $this->db->get()->result();
		//echo $this->db->last_query();
		return $result;
	}

	// User food glossary remove
	function removeFood($whereArray){
		$this->db->where($whereArray);
		$query = $this->db->delete('user_shoppingcart');
		return $query;
	}
}  