<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Units_model extends MY_Model {

	protected $primary_key = 'id';

	/**
	 * Gets the units data.
	 *
	 * @param      integer  $ID     Ingreident ID
	 */
	function getUnitsData($ID) {
		$this->db->select('unit_gram,unit_ounce');
		$this->db->from('units');
		$this->db->where('id', $ID);
		$query = $this->db->get();
		return $query->row();
	}
}
