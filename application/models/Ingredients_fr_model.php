<?php

class Ingredients_fr_model extends MY_Model {
	
	protected $primary_key = 'id'; // PRIMARY KEY

	public function get_ingredients_cats($search){
		$this->db->select('id,name');
        $this->db->from('ingredient_categories_fr');
        $this->db->like('ingredient_categories_fr.name',$search);
        $this->db->where('status',0);
        $query = $this->db->get();
        return $query->result();
	}

    public function get_by_params($where = NULL){
    	$query = $this->db->get_where('ingredients_fr',$where);
    	return $query->result();
    }

    public function get_usedIngridents($userID){
        $usedlist = $this->db->select('mr.qty as nutrition,mr.recipe_id,mr.ingredient_fr_id,ing.id,ing.name as ingredient_name,ms.metric_name,ms.metric_value,ms.us_metric,ms.us_value');
        $this->db->from('master_recipeingredients_fr AS mr');
        $this->db->join('recipes_fr AS recp', 'mr.recipe_fr_id = recp.id');
        $this->db->join('ingredients_fr AS ing', 'mr.ingredient_fr_id = ing.id');
        $this->db->join('measurements as ms','mr.measurement_id = ms.id','LEFT');
        $this->db->where('mr.status',0);
        $this->db->where('recp.user_id',$userID);
        $this->db->group_by('mr.id');

        $result = $this->db->get()->result();
        //echo $this->db->last_query();exit;
        return $result;
    }

    public function get_usedIng($recipe_id){
        $this->db->select('ingredients.name,measurements.id as measurement_id,measurements.metric_name,measurements.metric_value,measurements.us_metric,measurements.us_value,master_recipeingredients_fr.measure_type,master_recipeingredients_fr.measure_amount,master_recipeingredients.recipe_fr_id');
        $this->db->from('ingredients_fr');
        $this->db->join('master_recipeingredients_fr','master_recipeingredients_fr.ingredient_fr_id = ingredients.id','LEFT');
        $this->db->join('measurements','measurements.id = master_recipeingredients.measurement_id','LEFT');
        $this->db->where('master_recipeingredients_fr.recipe_fr_id', $recipe_id);
        $query = $this->db->get()->result();
        return $query;
    }

    function getIngredients($search){
        $this->db->select('id,name');
        $this->db->from('ingredients_fr');
        $this->db->like('ingredients_fr.name',$search);
        $this->db->where('status',0);
        $query = $this->db->get();
        return $query->result();
    }
}  