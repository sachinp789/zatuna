<?php

class Bookmarks_fr_model extends MY_Model {
	
	protected $primary_key = 'id'; // PRIMARY KEY

	// Insert bookmarkdetails with bookid
	function create($data){
		$this->db->insert_batch('bookmarks_fr', $data);
	}

	// check bookmark recipes if user
	function bookmarkCheck($userID,$bookID,$recipeID){
		$text = '';
		$this->db->select('book_fr_id');
		$this->db->where_in('book_fr_id', $bookID);
		$this->db->where('recipe_fr_id', $recipeID);
		$this->db->where('user_id', $userID);
 		$query = $this->db->get('bookmarks_fr');
		if($query->num_rows() > 0){
			$text = $query->result_array();
		}
		else{
			$text = '';
		}
		return $text;	
		/*$this->db->select('id');
		$this->db->where('id', $bookID);
		$this->db->where('recipe_fr_id', $recipeID);
		$this->db->where('user_id', $userID);
		$query = $this->db->get('bookmarks_fr');

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->id;
		}
		else{
			return 0;
		}*/
	}

	// user bookmark recipes
	function getBookmarkRecipes($bookID,$userID){
		$this->db->select('bk.*,recp.recipe_name');
		$this->db->from('bookmarks_fr as bk');
		$this->db->join('recipes_fr as recp','bk.recipe_fr_id = recp.id');
		$this->db->where('bk.id', $bookID);
		$this->db->where('bk.user_id', $userID);
		$this->db->where('bk.status',1);

		$result = $this->db->get()->result();
		return $result;
	}


	function getRecipiesbyBook($userID,$bookID){
		 $recipeslist = $this->db->select('mr.*,rec.*,rc.cat_name as recipe_category,bk.user_id,bk.id as cookbook_id');
		//$recipeslist = $this->db->select('*');
		$this->db->from('master_recipeingredients_fr AS mr');
		$this->db->join('recipes_fr AS rec', 'mr.recipe_fr_id = rec.id', 'LEFT');
		$this->db->join('ingredients_fr AS ing', 'mr.ingredient_fr_id = ing.id', 'LEFT');
		$this->db->join('recipe_categories_fr AS rc', 'rec.recipe_category_fr_id = rc.id', 'LEFT');
		$this->db->join('bookmarks_fr as bk','bk.recipe_fr_id = rec.id');
		
		$this->db->where('bk.user_id',$userID);
        $this->db->where('bk.book_fr_id',$bookID);
        $this->db->group_by('bk.book_fr_id');

		$result = $this->db->get()->result();
		return $result;
	}

	function getIngredientsbyBook($userID,$bookID){
		$usedlist = $this->db->select('mr.qty as nutrition,mr.ingredient_fr_id,ing.id,ing.name as ingredient_name,ms.metric_name,ms.metric_value,ms.us_metric,ms.us_value,bk.id as cookbook_id,bk.recipe_fr_id,bk.user_id');
        $this->db->from('master_recipeingredients_fr AS mr');
        $this->db->join('recipes_fr AS recp', 'mr.recipe_fr_id = recp.id');
        $this->db->join('ingredients_fr AS ing', 'mr.ingredient_fr_id = ing.id');
        $this->db->join('measurements as ms','mr.measurement_id = ms.id','LEFT');
        $this->db->join('bookmarks_fr as bk','bk.recipe_fr_id = recp.id');
        //$this->db->where('mr.status',0);
        $this->db->where('bk.user_id',$userID);
        $this->db->where('bk.book_fr_id',$bookID);
        $this->db->group_by('bk.book_fr_id');

        $result = $this->db->get()->result();
        //echo $this->db->last_query();exit;
        return $result;
    }

    // user bookmark recipes with bookid and userid
	function getRecipes($bookID,$userID){
		$this->db->select('recipe_fr_id');
		$this->db->from('bookmarks_fr');
		$this->db->where('book_fr_id', $bookID);
		$this->db->where('user_id', $userID);
		//$this->db->where('bk.status',1);
		$this->db->group_by('recipe_fr_id');

		$result = $this->db->get()->result();
		return $result;
	}
}