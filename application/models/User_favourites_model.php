<?php

class User_favourites_model extends MY_Model {

	protected $primary_key = 'id'; // PRIMARY KEY

	// Check user recipes like query
	function isLike($userID, $recipeID) {
		$this->db->where('user_id', $userID);
		$this->db->where('recipe_id', $recipeID);
		$query = $this->db->get('user_favourites');
		if ($query->num_rows() > 0) {
			return true;
		} else {
			return false;
		}
	}

	// Check user recipes like query
	function removeLike($userID, $recipeID) {
		$this->db->where('user_id', $userID);
		$this->db->where('recipe_id', $recipeID);
		if ($this->db->delete('user_favourites')) {
			return true;
		} else {
			return false;
		}
	}

	function getUserLikeRecipes($userID) {
		$recipeslist = $this->db->select('mr.id,mr.recipe_id,rec.recipe_name,rec.recipe_name_ar,rec.recipe_name_fr,rec.recipe_media,rec.recipe_media_2,rec.recipe_media_3,rec.recipe_media_4,rec.recipe_media_5,rec.preparation_time');
		//$recipeslist = $this->db->select('*');
		$this->db->from('master_recipeingredients AS mr');
		$this->db->join('recipes AS rec', 'mr.recipe_id = rec.id', 'LEFT');
		$this->db->join('ingredients AS ing', 'mr.ingredient_id = ing.id', 'LEFT');
		$this->db->join('recipe_categories AS rc', 'rec.recipe_category_id = rc.id', 'LEFT');
		$this->db->join('admin AS ad', 'rec.admin_by = ad.admin_id', 'LEFT');
		//$this->db->join('cookbooks AS cb', 'cb.user_id = rec.user_id', 'LEFT');
		$this->db->join('user_favourites AS uf', 'uf.recipe_id = rec.id');
		//$this->db->join('ingredient_categories AS ic', 'ing.ingredient_category_id = ic.id', 'LEFT');
		//$this->db->join('measurements as ms','mr.measurement_id = ms.id','LEFT');
		//$this->db->where('rec.status', 0);
		if (!empty($userID)) {
			$this->db->where('uf.user_id', $userID);
		}
		$this->db->group_by('uf.id');

		$result = $this->db->get()->result();
		//$result[] = $this->countRecipeCount();
		//echo $this->db->last_query();exit;
		return $result;
	}

	public function getUsedIngridents($userID) {
		$usedlist = $this->db->select('mr.qty,mr.recipe_id,mr.ingredient_id,ing.id,ing.name as ingredient_name,ing.name_ar as ingredient_name_ar,ing.name_fr as ingredient_name_fr,ing.description,ing.description_ar,ing.description_fr,ing.kcal_grams,mr.measure_amount,un.unit_name,un.unit_name_ar,un.unit_name_fr,un.unit_gram as per_gram');
		$this->db->from('master_recipeingredients AS mr');
		$this->db->join('recipes AS recp', 'mr.recipe_id = recp.id');
		$this->db->join('ingredients AS ing', 'mr.ingredient_id = ing.id');
		$this->db->join('user_favourites AS uf', 'uf.recipe_id = recp.id');
		//$this->db->join('measurements as ms', 'mr.measurement_id = ms.id', 'LEFT');
		$this->db->join('units as un', 'mr.finalunit_id = un.id', 'LEFT');
		$this->db->where('mr.status', 0);
		if (!empty($userID)) {
			$this->db->where('uf.user_id', $userID);
		}
		//$this->db->group_by('uf.id');

		$result = $this->db->get()->result();
		//echo $this->db->last_query();exit;
		return $result;
	}

	// Recipe Like Count
	function countRecipeCount() {
		$this->db->select('recipe_id,count(recipe_id) as totalcount');
		$this->db->from('user_favourites');
		//$this->db->where('user_id',$userID);
		$this->db->where('is_like', 1);
		$this->db->group_by('recipe_id');
		return $this->db->get()->result();
		//echo $this->db->query();exit;
	}

}