<?php

class Devicetokens_model extends MY_Model {

	protected $primary_key = 'id'; // PRIMARY KEY

	/** Get Token **/
	function getTokens($token = NULL) {
		return $this->db->where('device_token', $token)
			->get('devicetokens')
			->num_rows();
	}
	/**
	 * Update token with time
	 *
	 * @param      string  $token  The token
	 * @param      array  $data   The data
	 */
	function updateToken($token, $data) {
		$this->db->where('device_token', $token);
		if ($this->db->update('devicetokens', $data)) {
			return 1;
		} else {
			return 0;
		}
	}
}