<?php

class Recipes_model extends MY_Model {

	protected $primary_key = 'id'; // PRIMARY KEY

	// GET RECIPES DATA
	public function get_recipes_data($filterOptions) {

		$pagenumber = ($filterOptions['pageNo']);
		$page = ($pagenumber * $filterOptions['perPage']);

		$recipeslist = $this->db->select('mr.id,mr.recipe_id,rec.recipe_name,rec.recipe_name_ar,rec.recipe_name_fr,rec.recipe_media,rec.recipe_media_2,rec.recipe_media_3,rec.recipe_media_4,rec.recipe_media_5,rec.is_featured');
		//$recipeslist = $this->db->select('*');
		$this->db->from('master_recipeingredients AS mr');
		$this->db->join('recipes AS rec', 'mr.recipe_id = rec.id');
		$this->db->join('ingredients AS ing', 'mr.ingredient_id = ing.id');
		$this->db->join('recipe_categories AS rc', 'rec.recipe_category_id = rc.id');
		$this->db->join('units AS un', 'mr.finalunit_id = un.id', 'LEFT');
		$this->db->join('admin AS ad', 'rec.admin_by = ad.admin_id', 'LEFT');

		//$this->db->join('user_favourites AS uf', 'uf.recipe_id = rec.id', 'LEFT');
		/*if(!empty($userID)){ // Get user like recipes
			//$this->db->where('uf.user_id !=','NULL');
			$this->db->where('uf.user_id',$userID);
			$this->db->where('uf.is_like',1);
			//$this->db->group_by('uf.user_id');
		}*/

		//$this->db->join('cookbooks AS cb', 'cb.user_id = rec.user_id', 'LEFT');
		//$this->db->join('ingredient_categories AS ic', 'ing.ingredient_category_id = ic.id', 'LEFT');
		//$this->db->join('measurements as ms','mr.measurement_id = ms.id','LEFT');
		$this->db->where('rec.status', 0);
		//$this->db->group_start();
		if (!empty($filterOptions['category'])) {
			$this->db->where('rc.id', $filterOptions['category']);
		}
		if (isset($filterOptions['mincalory']) && isset($filterOptions['maxcolory'])) {
			if (!empty($filterOptions['mincalory']) || $filterOptions['mincalory'] == 0) {
				if ($filterOptions['maxcolory'] >= 1) {
					$this->db->where("rec.total_calory BETWEEN {$filterOptions['mincalory']} AND {$filterOptions['maxcolory']}", NULL, FALSE);
				} /* else {
					$this->db->where("mr.measure_amount BETWEEN {$filterOptions['mincalory']} AND {$filterOptions['maxcolory']}", NULL, FALSE);
				}*/
				/*$this->db->where('mr.measure_amount >=' . $filterOptions['mincalory']);
			$this->db->where('mr.measure_amount <=' . $filterOptions['maxcolory']);*/
			}}
		//$this->db->group_end();
		/*if(!empty($userID)){
			$this->db->where('rec.user_id',$userID);
		*/
		$this->db->group_by('rec.id');
		if (isset($filterOptions['pageNo'])) {
			if ($page == '' || $page <= 0) {
				$filterOptions['perPage'] = 10;
				$page = 0;
			}}
		$this->db->limit($filterOptions['perPage'], $page);
		//$this->db->get()->num_rows();

		$result = $this->db->get()->result();
		//echo $this->db->last_query();exit;
		return $result;
	}

	public function getUserLikeORBookmarkRecipesByID($recipeID, $userID) {
		$LikeBookmark = array();

		$this->db->select('user_id,is_like,recipe_id');
		$this->db->from('user_favourites');
		$this->db->where('recipe_id', $recipeID);
		$this->db->where('user_id', $userID);
		$this->db->where('is_like', 1);
		//$this->db->group_by('recipe_id');
		//echo $this->db->last_query();exit;

		$LikeBookmark['likes'] = $this->db->get()->result();

		$this->db->select('user_id,status as is_bookmark,recipe_id');
		$this->db->from('bookmarks');
		$this->db->where('recipe_id', $recipeID);
		$this->db->where('user_id', $userID);
		$this->db->where('status', 1);
		//$this->db->group_by('recipe_id');

		$LikeBookmark['bookmarks'] = $this->db->get()->result();

		return $LikeBookmark;
	}

	public function getUserLikeORBookmarkRecipes($userID) {

		$LikeBookmark = array();

		$this->db->select('user_id,is_like,recipe_id');
		$this->db->from('user_favourites');
		$this->db->where('user_id', $userID);
		$this->db->where('is_like', 1);
		$this->db->group_by('recipe_id');
		//echo $this->db->last_query();exit;

		$LikeBookmark['likes'] = $this->db->get()->result();

		$this->db->select('user_id,status as is_bookmark,recipe_id');
		$this->db->from('bookmarks');
		$this->db->where('user_id', $userID);
		$this->db->where('status', 1);
		$this->db->group_by('recipe_id');

		$LikeBookmark['bookmarks'] = $this->db->get()->result();

		return $LikeBookmark;
	}

	// GET RECIPES DATA
	public function get_previouesrecipes_data($recipe_id) {
		//echo $page. ','.$limit;exit;
		$recipeslist = $this->db->select('mr.id,rec.id as recipe_id,rec.total_calory,rec.total_calory_ounce as total_calory_us,sum(mr.protein_amount) as total_protine,sum(mr.fat_amount) as total_fat,sum(mr.carbs_amount) as total_carbs,sum(mr.protein_amount_ounce) as total_protine_us,sum(mr.fat_amount_ounce) as total_fat_us,sum(mr.carbs_amount_ounce) as total_carbs_us,rec.recipe_name,rec.recipe_name_ar,rec.recipe_name_fr,rec.recipe_media,rec.recipe_media_2,rec.recipe_media_3,rec.recipe_media_4,rec.recipe_media_5,rec.preparation_time,rec.preparation_time,rec.cooking_time,rec.recipe_description,rec.recipe_description_ar,rec.recipe_description_fr,rec.serving,rec.source_title,rec.source_link,rec.is_featured,rc.cat_name as recipe_category,rc.cat_name_ar as recipe_category_ar,rc.cat_name_fr as recipe_category_fr');
		//$recipeslist = $this->db->select('*');
		$this->db->from('master_recipeingredients AS mr');
		$this->db->join('recipes AS rec', 'mr.recipe_id = rec.id', 'LEFT');
		$this->db->join('ingredients AS ing', 'mr.ingredient_id = ing.id', 'LEFT');
		$this->db->join('recipe_categories AS rc', 'rec.recipe_category_id = rc.id', 'LEFT');
		//$this->db->join('cookbooks AS cb', 'cb.user_id = rec.user_id', 'LEFT');
		//$this->db->join('ingredient_categories AS ic', 'ing.ingredient_category_id = ic.id', 'LEFT');
		//$this->db->join('measurements as ms','mr.measurement_id = ms.id','LEFT');
		//$this->db->where('rec.status', 0);
		if (!empty($recipe_id)) {
			$this->db->where('rec.id', $recipe_id);
		}
		$this->db->group_by('rec.id');

		$result = $this->db->get()->result();
		//echo $this->db->last_query();exit;
		return $result;
	}

	public function get_by_params($where = NULL) {
		$query = $this->db->get_where('recipes', $where);
		return $query->result();
	}
	/**
	 * Total number of Rows
	 */
	public function filterRecipeNumRows($filter) {

		$pagenumber = ($filter['pageNo']);
		$page = ($pagenumber * $filter['perPage']);

		$recipeslist = $this->db->select('mr.id,mr.recipe_id,rec.recipe_name,rec.recipe_name_ar,rec.recipe_name_fr,rec.recipe_media,rec.recipe_media_2,rec.recipe_media_3,rec.recipe_media_4,rec.recipe_media_5,rec.is_featured');
		//$recipeslist = $this->db->select('*');
		$this->db->from('master_recipeingredients AS mr');
		$this->db->join('recipes AS rec', 'mr.recipe_id = rec.id');
		$this->db->join('ingredients AS ing', 'mr.ingredient_id = ing.id');
		$this->db->join('recipe_categories AS rc', 'rec.recipe_category_id = rc.id');
		$this->db->join('units AS un', 'mr.finalunit_id = un.id', 'LEFT');
		$this->db->join('admin AS ad', 'rec.admin_by = ad.admin_id', 'LEFT');

		$this->db->where('rec.status', 0);
		//$this->db->group_start();
		if (!empty($filter['category'])) {
			$this->db->where('rc.id', $filter['category']);
		}
		if (isset($filter['mincalory']) && isset($filter['maxcolory'])) {
			if (!empty($filter['mincalory']) || $filter['mincalory'] == 0) {
				if ($filter['maxcolory'] >= 1) {
					$this->db->where("rec.total_calory BETWEEN {$filter['mincalory']} AND {$filter['maxcolory']}", NULL, FALSE);
					/*$this->db->where('mr.measure_amount >=' . $filter['maxcolory']);*/
				} /*else {
					$this->db->where("mr.measure_amount BETWEEN {$filter['mincalory']} AND {$filter['maxcolory']}", NULL, FALSE);
				}*/
				/*$this->db->where('mr.measure_amount >=' . $filter['mincalory']);
			$this->db->where('mr.measure_amount <=' . $filter['maxcolory']);*/
			}}
		//$this->db->group_end();

		$this->db->group_by('rec.id');
		if (isset($filter['pageNo'])) {
			if ($page == '' || $page <= 0) {
				$filter['perPage'] = '';
				$page = 0;
			} else {
				$page = $filter['perPage'];
			}
			//$this->db->limit($filter['perPage'], $page);
		}

		//$this->db->get()->num_rows();

		$result = $this->db->get()->num_rows();
		//echo $this->db->last_query();exit;
		return $result;
	}

	public function getSumRecipes() {
		$totalsumrecipes = $this->db->query('SELECT recipe_id, SUM(measure_amount) AS totalsum FROM master_recipeingredients GROUP BY recipe_id')->row()->totalsum;
		return $totalsumrecipes;
	}

	/**
	 * Get Maximum Calory
	 */
	public function maxiMumCalory() {

		$maxid = $this->db->query('SELECT recipe_id, SUM(measure_amount) AS maxid FROM master_recipeingredients GROUP BY recipe_id ORDER BY maxid DESC LIMIT 1')->row()->maxid;
		return $maxid;
	}

	// Update Query For Selected Recipes
	public function update_recipes_id1($id, $data) {
		$this->db->where('id', $id);
		$this->db->update('recipes', $data);
	}

	// Get all recipes images of userID
	public function get_recipes_media($userID) {
		$this->db->select('recipe_media,recipe_media_2,recipe_media_3,recipe_media_4,recipe_media_5');
		$this->db->from('recipes');
		if (!empty($userID)) {
			$this->db->where('user_id', $userID);
		}
		$images = $this->db->get()->result();
		return $images;
	}

	// Get Recipes Steps
	public function get_recipesStep() {
		$this->db->select('st.id,st.step_no,st.step_description,st.step_description_ar,st.step_description_fr,st.image,rec.id as recipe_id');
		$this->db->from('recipes as rec');
		$this->db->join('recipe_steps as st', 'st.recipe_id = rec.id');
		//$this->db->group_by('rec.id');
		return $this->db->get()->result();
	}
}