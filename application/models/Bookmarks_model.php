<?php

class Bookmarks_model extends MY_Model {

	protected $primary_key = 'id'; // PRIMARY KEY

	// Insert bookmarkdetails with bookid
	function create($data) {
		$this->db->insert_batch('bookmarks', $data);
	}

	// check bookmark recipes if user
	function bookmarkCheck($userID, $bookID, $recipeID) {

		//print_r($bookID);exit;

		$text = '';
		$this->db->select('book_id');
		$this->db->where_in('book_id', $bookID);
		$this->db->where('recipe_id', $recipeID);
		$this->db->where('user_id', $userID);
		$this->db->group_by('book_id');
		$query = $this->db->get('bookmarks');
		//echo $this->db->last_query();
		if ($query->num_rows() > 0) {
			$text = $query->result_array();
		} else {
			$text = '';
		}
		return $text;
		/*$this->db->select('id');
			$this->db->where_in('book_id', $bookID);
			$this->db->where('recipe_id', $recipeID);
			$this->db->where('user_id', $userID);
			$query = $this->db->get('bookmarks');
			//echo $this->db->last_query();exit;
			if($query->num_rows() > 0)
			{
				return 1;
			}
			else{
				return 0;
		*/
	}

	// user bookmark recipes
	function getBookmarkRecipes($bookID, $userID) {
		$this->db->select('bk.*,recp.recipe_name,recp.recipe_name_ar,recp.recipe_name_fr');
		$this->db->from('bookmarks as bk');
		$this->db->join('recipes as recp', 'bk.recipe_id = recp.id');
		$this->db->where('bk.id', $bookID);
		$this->db->where('bk.user_id', $userID);
		$this->db->where('bk.status', 1);

		$result = $this->db->get()->result();
		return $result;
	}

	function getRecipiesbyBook($userID, $bookID) {
		$recipeslist = $this->db->select('mr.id,rec.id as recipe_id,rec.recipe_name,rec.recipe_name_ar,rec.recipe_name_fr,rec.preparation_time,rec.recipe_media,rec.recipe_media_2,rec.recipe_media_3,rec.recipe_media_4,rec.recipe_media_5');
		//$recipeslist = $this->db->select('*');
		$this->db->from('master_recipeingredients AS mr');
		$this->db->join('recipes AS rec', 'mr.recipe_id = rec.id', 'LEFT');
		$this->db->join('ingredients AS ing', 'mr.ingredient_id = ing.id', 'LEFT');
		$this->db->join('recipe_categories AS rc', 'rec.recipe_category_id = rc.id', 'LEFT');
		$this->db->join('bookmarks as bk', 'bk.recipe_id = rec.id');

		$this->db->where('bk.user_id', $userID);
		$this->db->where('bk.book_id', $bookID);
		$this->db->group_by('bk.recipe_id');

		$result = $this->db->get()->result();
		//echo $this->db->last_query();exit;
		return $result;
	}

	function getIngredientsbyBook($userID, $bookID) {
		$usedlist = $this->db->select('mr.qty,mr.ingredient_id,ing.id,ing.name as ingredient_name,ing.name_ar as ingredient_name_ar,ing.name_fr as ingredient_name_fr,ing.description,ing.description_ar,ing.description_fr,ing.kcal_grams,mr.measure_amount,un.unit_name,un.unit_name_ar,un.unit_name_fr,un.unit_gram as per_gram,bk.id as cookbook_id,bk.recipe_id,bk.user_id');
		$this->db->from('master_recipeingredients AS mr');
		$this->db->join('recipes AS recp', 'mr.recipe_id = recp.id');
		$this->db->join('ingredients AS ing', 'mr.ingredient_id = ing.id');
		//$this->db->join('measurements as ms', 'mr.measurement_id = ms.id', 'LEFT');
		$this->db->join('units as un', 'mr.finalunit_id = un.id', 'LEFT');
		$this->db->join('bookmarks as bk', 'bk.recipe_id = recp.id');
		//$this->db->where('mr.status',0);
		$this->db->where('bk.user_id', $userID);
		$this->db->where('bk.book_id', $bookID);
		$this->db->group_by('ing.id');

		$result = $this->db->get()->result();
		//echo $this->db->last_query();exit;
		return $result;
	}

	// user bookmark recipes with bookid and userid
	function getRecipes($bookID, $userID) {
		$this->db->select('recipe_id');
		$this->db->from('bookmarks');
		$this->db->where('book_id', $bookID);
		$this->db->where('user_id', $userID);
		//$this->db->where('bk.status',1);
		$this->db->group_by('recipe_id');

		$result = $this->db->get()->result();
		return $result;
	}

	// Remove cookBook from bookmark details
	function removeBookRecipe($cookID, $userID, $recipeID) {
		$this->db->where('book_id', $cookID);
		$this->db->where('user_id', $userID);
		$this->db->where('recipe_id', $recipeID);
		if ($this->db->delete('bookmarks')) {
			return true;
		} else {
			return false;
		}
	}
}