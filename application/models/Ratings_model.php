<?php

class Ratings_model extends MY_Model {

	protected $primary_key = 'id'; // PRIMARY KEY

	/**
	 * Counts the number of rating.
	 */
	public function getRating($recipe_id) {
		$this->db->select('recipe_id,count(rating_score) as totalRating, AVG(rating_score) as avgRating');
		$this->db->from('ratings');
		if (!empty($recipe_id)) {
			$this->db->where('recipe_id', $recipe_id);
		}
		$this->db->group_by('recipe_id');
		return $this->db->get()->result();
	}

	/**
	 * { check rating exists of user recipe wise }
	 */
	public function get_by_params($where = NULL) {
		$query = $this->db->get_where('ratings', $where);
		return $query->result();
	}
}