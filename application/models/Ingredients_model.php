<?php

class Ingredients_model extends MY_Model {

	protected $primary_key = 'id'; // PRIMARY KEY

	public function get_ingredients_cats($search) {
		$this->db->select('id,name,name_ar,name_fr');
		$this->db->from('ingredient_categories');
		$this->db->like('ingredient_categories.name', $search);
		$this->db->where('status', 0);
		$query = $this->db->get();
		return $query->result();
	}

	public function get_by_params($where = NULL) {
		$query = $this->db->get_where('ingredients', $where);
		return $query->result();
	}

	public function get_usedIngridents($search) {
		$usedlist = $this->db->select('mr.qty,mr.measure_amount,mr.recipe_id,mr.ingredient_id,ing.id,ing.name as ingredient_name,ing.name_ar as ingredient_name_ar,ing.name_fr as ingredient_name_fr,ing.description,ing.description_ar,ing.description_fr,ing.kcal_grams,un.unit_name,un.unit_name_ar,un.unit_name_fr,un.unit_gram as per_gram,un.unit_name_us,un.unit_name_us_ar,un.unit_name_us_fr,un.unit_ounce as per_ounce');
		$this->db->from('master_recipeingredients AS mr');
		$this->db->join('recipes AS recp', 'mr.recipe_id = recp.id');
		$this->db->join('ingredients AS ing', 'mr.ingredient_id = ing.id');
		//$this->db->join('measurements as ms', 'mr.measurement_id = ms.id', 'LEFT');
		$this->db->join('units as un', 'mr.finalunit_id = un.id', 'LEFT');
		$this->db->where('mr.status', 0);
		/*if(!empty($search)){
			  $this->db->or_like('ingredients.name', $search);
		*/
		$this->db->group_by('mr.id');

		$result = $this->db->get()->result();
		//echo $this->db->last_query();exit;
		return $result;
	}

	public function get_previous_usedIngridents($recipeID) {
		$usedlist = $this->db->select('ing.id as ingredient_id,mr.recipe_id,mr.qty,ing.name as ingredient_name,ing.name_ar as ingredient_name_ar,ing.name_fr as ingredient_name_fr,ing.media,ing.description,ing.description_ar,ing.description_fr,ing.protein,ing.fat,ing.carbs,ing.kcal_grams,un.unit_name,un.unit_name_ar,un.unit_name_fr,un.unit_gram as per_gram,un.unit_name_us,un.unit_name_us_ar,un.unit_name_us_fr,un.unit_ounce as per_ounce');
		$this->db->from('master_recipeingredients AS mr');
		$this->db->join('recipes AS recp', 'mr.recipe_id = recp.id');
		$this->db->join('ingredients AS ing', 'mr.ingredient_id = ing.id');
		$this->db->join('units as un', 'mr.finalunit_id = un.id', 'LEFT');
		//$this->db->join('measurements as ms', 'mr.measurement_id = ms.id', 'LEFT');
		$this->db->where('mr.status', 0);
		if (!empty($recipeID)) {
			$this->db->where('recp.id', $recipeID);
		}
		$this->db->group_by('mr.id');

		$result = $this->db->get()->result();
		//echo $this->db->last_query();exit;
		return $result;
	}

	public function get_usedIng($recipe_id) {
		$this->db->select('ingredients.name,ingredients.name_ar,ingredients.name_fr,measurements.id as measurement_id,measurements.metric_name,measurements.metric_value,measurements.us_metric,measurements.us_value,master_recipeingredients.measure_type,master_recipeingredients.measure_amount,master_recipeingredients.recipe_id');
		$this->db->from('ingredients');
		$this->db->join('master_recipeingredients', 'master_recipeingredients.ingredient_id = ingredients.id', 'LEFT');
		$this->db->join('measurements', 'measurements.id = master_recipeingredients.measurement_id', 'LEFT');
		$this->db->where('master_recipeingredients.recipe_id', $recipe_id);
		$query = $this->db->get()->result();
		return $query;
	}

	function getIngredients($search) {
		$this->db->select('id,name,name_ar,name_fr');
		$this->db->from('ingredients');
		$this->db->like('ingredients.name', $search);
		$this->db->like('ingredients.name_ar', $search);
		$this->db->like('ingredients.name_fr', $search);
		$this->db->where('status', 0);
		$query = $this->db->get();
		return $query->result();
	}

	// Get ingredient details with ID
	function checkIngredient($ID) {
		$this->db->select('id,protein,fat,carbs,kcal_grams');
		$this->db->from('ingredients');
		$this->db->where('id', $ID);
		$query = $this->db->get();
		return $query->row();
	}
}