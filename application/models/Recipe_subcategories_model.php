<?php

class Recipe_subcategories_model extends MY_Model {
	
	protected $primary_key = 'id'; // PRIMARY KEY

	/******************* GET SUB CATEGORIES ******************/
	public function get_subcategories_by_id($recp_cat_id)
    {	
        $this->db->select('*');
        $this->db->from('recipe_subcategories');
        $this->db->where('recipe_category_id',$recp_cat_id);
        $this->db->where('status',0);
        $subcategories = $this->db->get();
        return $subcategories->result();		    
    }
}  