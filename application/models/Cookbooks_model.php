<?php

class Cookbooks_model extends MY_Model {

	protected $primary_key = 'id'; // PRIMARY KEY

	// Insert cookdetails
	function create($data) {
		$this->db->insert_batch('cookbooks', $data);
	}

	// User Cookbook List
	function userCookbookList($userID) {
		$this->db->select('cookbooks.id,cookbooks.cbook_name,cookbooks.user_id,bookmarks.recipe_id');
		$this->db->from('cookbooks');
		$this->db->join('bookmarks', 'bookmarks.book_id=cookbooks.id', 'LEFT');
		//$this->db->join('recipes', 'recipes.id=bookmarks.recipe_id', 'LEFT');
		$this->db->where('cookbooks.user_id', $userID);
		$this->db->group_by('cookbooks.id');
		//$this->db->order_by('cookbooks.id','desc');
		//$this->db->group_by('cookbooks.id');
		$result = $this->db->get()->result();
		//echo $this->db->last_query();exit;
		return $result;
		/*$this->db->select('*');
			$this->db->where('user_id',$userID);
			$query = $this->db->get('cookbooks');
		*/
	}
/*
function getMedia($bookID){

}*/

	// Cook details for update
	function updateCook($data, $bookID, $userID) {
		$this->db->where('id', $bookID);
		$this->db->where('user_id', $userID);
		//echo $this->db->last_query();exit;
		if ($this->db->update('cookbooks', $data)) {
			return true;
		} else {
			return false;
		}
	}

	// Remove cookBook details
	function removeCookBook($cookID, $userID) {
		$this->db->where('id', $cookID);
		$this->db->where('user_id', $userID);
		if ($this->db->delete('cookbooks')) {
			return true;
		} else {
			return false;
		}
	}

	// Get cookbook of user
	function getCookBookUser($bookID, $userID) {
		$this->db->select('cbook_image');
		$this->db->where('id', $bookID);
		$this->db->where('user_id', $userID);
		$query = $this->db->get('cookbooks');
		return $query->row();
	}

	/*// check bookmark recipes if user
		function cookBookCheck($userID,$cookbookID,$recipeID){
			$this->db->select('cookbooks.id as cookbook_id');
			$this->db->from('cookbooks');
			$this->db->join('bookmarks','cookbooks.id = bookmarks.book_id');
			$this->db->where('cookbooks.id', $cookbookID);
			$this->db->where('bookmarks.recipe_id', $recipeID);
			$this->db->where('cookbooks.user_id', $userID);
			$query = $this->db->get();

			if($query->num_rows() > 0)
			{
				$result = $query->row();
				return $result->cookbook_id;
			}
			else{
				return 0;
			}
	*/

}