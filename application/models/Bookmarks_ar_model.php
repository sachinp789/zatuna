<?php

class Bookmarks_ar_model extends MY_Model {
	
	protected $primary_key = 'id'; // PRIMARY KEY

	// Insert bookmarkdetails with bookid
	function create($data){
		$this->db->insert_batch('bookmarks_ar', $data);
	}

	// check bookmark recipes if user
	function bookmarkCheck($userID,$bookID,$recipeID){
		$text = '';
		$this->db->select('book_ar_id');
		$this->db->where_in('book_ar_id', $bookID);
		$this->db->where('recipe_ar_id', $recipeID);
		$this->db->where('user_id', $userID);
 		$query = $this->db->get('bookmarks_ar');
		if($query->num_rows() > 0){
			$text = $query->result_array();
		}
		else{
			$text = '';
		}
		return $text;	
		/*$this->db->select('id');
		$this->db->where('id', $bookID);
		$this->db->where('recipe_ar_id', $recipeID);
		$this->db->where('user_id', $userID);
		$query = $this->db->get('bookmarks_ar');

		if($query->num_rows() > 0)
		{
			$result = $query->row();
			return $result->id;
		}
		else{
			return 0;
		}*/
	}

	// user bookmark recipes
	function getBookmarkRecipes($bookID,$userID){
		$this->db->select('bk.*,recp.recipe_name');
		$this->db->from('bookmarks_ar as bk');
		$this->db->join('recipes_ar as recp','bk.recipe_ar_id = recp.id');
		$this->db->where('bk.id', $bookID);
		$this->db->where('bk.user_id', $userID);
		$this->db->where('bk.status',1);

		$result = $this->db->get()->result();
		return $result;
	}


	function getRecipiesbyBook($userID,$bookID){
		 $recipeslist = $this->db->select('mr.*,rec.*,rc.cat_name as recipe_category,bk.user_id,bk.id as cookbook_id');
		//$recipeslist = $this->db->select('*');
		$this->db->from('master_recipeingredients_ar AS mr');
		$this->db->join('recipes_ar AS rec', 'mr.recipe_ar_id = rec.id', 'LEFT');
		$this->db->join('ingredients_ar AS ing', 'mr.ingredient_ar_id = ing.id', 'LEFT');
		$this->db->join('recipe_categories_ar AS rc', 'rec.recipe_category_ar_id = rc.id', 'LEFT');
		$this->db->join('bookmarks_ar as bk','bk.recipe_ar_id = rec.id');
		
		$this->db->where('bk.user_id',$userID);
        $this->db->where('bk.book_ar_id',$bookID);
        $this->db->group_by('bk.book_ar_id');

		$result = $this->db->get()->result();
		return $result;
	}

	function getIngredientsbyBook($userID,$bookID){
		$usedlist = $this->db->select('mr.qty as nutrition,mr.ingredient_ar_id,ing.id,ing.name as ingredient_name,ms.metric_name,ms.metric_value,ms.us_metric,ms.us_value,bk.id as cookbook_id,bk.recipe_ar_id,bk.user_id');
        $this->db->from('master_recipeingredients_ar AS mr');
        $this->db->join('recipes_ar AS recp', 'mr.recipe_ar_id = recp.id');
        $this->db->join('ingredients_ar AS ing', 'mr.ingredient_ar_id = ing.id');
        $this->db->join('measurements as ms','mr.measurement_id = ms.id','LEFT');
        $this->db->join('bookmarks as bk','bk.recipe_ar_id = recp.id');
        //$this->db->where('mr.status',0);
        $this->db->where('bk.user_id',$userID);
        $this->db->where('bk.book_ar_id',$bookID);
        $this->db->group_by('bk.book_ar_id');

        $result = $this->db->get()->result();
        //echo $this->db->last_query();exit;
        return $result;
    }

    // user bookmark recipes with bookid and userid
	function getRecipes($bookID,$userID){
		$this->db->select('recipe_ar_id');
		$this->db->from('bookmarks_ar');
		$this->db->where('book_ar_id', $bookID);
		$this->db->where('user_id', $userID);
		//$this->db->where('bk.status',1);
		$this->db->group_by('recipe_ar_id');

		$result = $this->db->get()->result();
		return $result;
	}
}